gPlottingTrans=gPlotting; gPlotting=false;

ra_trans_1=5.65e6;
ra_trans_2=3.115e7;
ra_trans_hyst=2.15e7;

series_together_hyst;

control_lims=[0.9*min(min(all_control_params),min(all_control_params_hyst)),1.1*max(max(all_control_params),max(all_control_params_hyst))];

%plot 2D K=1 forcing parameters
figure
l=plot(all_control_params,all_params(9,:),'-s','DisplayName','$\mathcal{T}_\textrm{3D}(K=1)$','MarkerSize',6);
l.MarkerFaceColor = l.Color; %to fill the symbols
hold on
l=plot(all_control_params,all_params(8,:),'-s','DisplayName','$\mathcal{T}_\textrm{2D}(K=1)$','MarkerSize',6);
l.MarkerFaceColor = l.Color; %to fill the symbols
legend('Location','northwest')
set(gca,'ColorOrderIndex',1)
plot(all_control_params_hyst,all_params_hyst(9,:),'-d','MarkerSize',8);
plot(all_control_params_hyst,all_params_hyst(8,:),'-d','MarkerSize',8);
set(gca,'ColorOrderIndex',1)
errorbar(all_control_params,all_params(9,:),all_param_errors(9,:), 'vertical', 'LineStyle', 'none');
errorbar(all_control_params,all_params(8,:),all_param_errors(8,:), 'vertical', 'LineStyle', 'none');
set(gca,'ColorOrderIndex',1)
errorbar(all_control_params_hyst,all_params_hyst(9,:),all_param_errors_hyst(9,:), 'vertical', 'LineStyle', 'none');
errorbar(all_control_params_hyst,all_params_hyst(8,:),all_param_errors_hyst(8,:), 'vertical', 'LineStyle', 'none');
xline(ra_trans_1,'-.','LineWidth',1.8,'Color','#1ba497');
xline(ra_trans_2,'-.r','LineWidth',1.8);
xline(ra_trans_hyst,'-.m','LineWidth',1.8);
ylabel('Energetic transport (in $U^3H^{-1}$)')
xlabel('$\textrm{Ra}$')
xlim(control_lims)
yline(0,'k');
ylim([-2e-5,22e-5])
set(gca, 'XScale', 'log')
grid on
box on
xticks([2e6, 1e7, 5e7])
xticklabels({'$2\times10^6$', '$10^7$' ,'$5\times10^7$'})
yticks([0,0.5,1,1.5,2]*10^-4)

saveas(gcf,'Fig_factories/PRL/figs/trans','epsc')


gPlotting=gPlottingTrans;
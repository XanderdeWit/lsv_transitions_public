gPlottingTransMaps=gPlotting; gPlotting=false;

%plot kinetic transport maps
fig_pos=[435,383,250,168];
axes_pos=[0.1300,0.2361,0.7750,0.6889];
sub_axes_pos=[axes_pos(1)+(29.5/50)*axes_pos(3),axes_pos(2),(20.5/50)*axes_pos(3),axes_pos(4)];

%% low-Ra transition
lims=[-1.2e-5,1.2e-5];
gMyCM_trans=transformCM(gMyCM,8/256,0.1,11);

%case056
gSimname='series1/case056'; gTstart=getTstart(gSimname); make_domain;
transfer_stafield;

figure
set(gcf,'Position',fig_pos)
axes('Position',axes_pos)

imagesc(transQK_BC(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
%colorbar
%%xlabel('$Q$')
%ylabel('$K$')

axis image
%colorbar('TickLabelInterpreter','latex')
ylim([0.5,30.5])
xlim([0.5,50.5])
set(gca,'ytick',[1,10,20,30])
set(gca,'xtick',[1,10,20,30])
set(gca,'XTickLabel',[]);

axes('Parent',gcf,'Position',sub_axes_pos);
%figure

l=plot(sum(transQK_BC(1:maxQ,1:maxK),1).*correction_K(2:maxK+1),1:maxK,'-o','MarkerSize',2);
l.MarkerFaceColor = l.Color; %to fill the symbols
set(gca, 'Color', 'None')
set(gca,'Ycolor','None')
set(gca,'Xcolor',[0 0.4470 0.7410])
box off
xline(0,':','LineWidth',1.5);
xlim([-3e-5,7e-5])
ylim([0.5,30.5])
set(gca,'xtick',[-3e-5,0,3e-5,6e-5])
set(gca,'XTickLabel',[]);

saveas(gcf,'Fig_factories/PRL/figs/raw_trans_maps_bc_056','epsc')

figure
set(gcf,'Position',fig_pos)
axes('Position',axes_pos)

imagesc(transQK_BT(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
%colorbar
%%xlabel('$Q$')
%%ylabel('$K$')
axis image
%colorbar('TickLabelInterpreter','latex')
ylim([0.5,30.5])
xlim([0.5,50.5])
set(gca,'ytick',[1,10,20,30])
set(gca,'xtick',[1,10,20,30])
set(gca,'YTickLabel',[]);
set(gca,'XTickLabel',[]);

axes('Parent',gcf,'Position',sub_axes_pos);
%figure

l=plot(sum(transQK_BT(1:maxQ,1:maxK),1).*correction_K(2:maxK+1),1:maxK,'-o','MarkerSize',2);
l.MarkerFaceColor = l.Color; %to fill the symbols
set(gca, 'Color', 'None')
set(gca,'Ycolor','None')
set(gca,'Xcolor',[0 0.4470 0.7410])
box off
xline(0,':','LineWidth',1.5);
xlim([-3e-5,7e-5])
ylim([0.5,30.5])
set(gca,'xtick',[-3e-5,0,3e-5,6e-5])
set(gca,'XTickLabel',[]);

saveas(gcf,'Fig_factories/PRL/figs/raw_trans_maps_bt_056','epsc')

%case057
gSimname='series1/case057'; gTstart=getTstart(gSimname); make_domain;
transfer_stafield;

figure
set(gcf,'Position',fig_pos)
axes('Position',axes_pos)

imagesc(transQK_BC(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
%colorbar
%%xlabel('$Q$')
%ylabel('$K$')
axis image
%colorbar('TickLabelInterpreter','latex')
ylim([0.5,30.5])
xlim([0.5,50.5])
set(gca,'ytick',[1,10,20,30])
set(gca,'xtick',[1,10,20,30])
set(gca,'XTickLabel',[]);
set(gca,'YTickLabel',{'1','10','20',''});

axes('Parent',gcf,'Position',sub_axes_pos);
%figure

l=plot(sum(transQK_BC(1:maxQ,1:maxK),1).*correction_K(2:maxK+1),1:maxK,'-o','MarkerSize',2);
l.MarkerFaceColor = l.Color; %to fill the symbols
set(gca, 'Color', 'None')
set(gca,'Ycolor','None')
set(gca,'Xcolor',[0 0.4470 0.7410])
box off
xline(0,':','LineWidth',1.5);
xlim([-3e-5,7e-5])
ylim([0.5,30.5])
set(gca,'xtick',[-3e-5,0,3e-5,6e-5])
set(gca,'XTickLabel',[]);

saveas(gcf,'Fig_factories/PRL/figs/raw_trans_maps_bc_057','epsc')

figure
set(gcf,'Position',fig_pos)
axes('Position',axes_pos)

imagesc(transQK_BT(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
%colorbar
%%xlabel('$Q$')
%%ylabel('$K$')
axis image
%colorbar('TickLabelInterpreter','latex')
ylim([0.5,30.5])
xlim([0.5,50.5])
set(gca,'ytick',[1,10,20,30])
set(gca,'xtick',[1,10,20,30])
set(gca,'YTickLabel',[]);
set(gca,'XTickLabel',[]);

axes('Parent',gcf,'Position',sub_axes_pos);
%figure

l=plot(sum(transQK_BT(1:maxQ,1:maxK),1).*correction_K(2:maxK+1),1:maxK,'-o','MarkerSize',2);
l.MarkerFaceColor = l.Color; %to fill the symbols
set(gca, 'Color', 'None')
set(gca,'Ycolor','None')
set(gca,'Xcolor',[0 0.4470 0.7410])
box off
xline(0,':','LineWidth',1.5);
xlim([-3e-5,7e-5])
ylim([0.5,30.5])
set(gca,'xtick',[-3e-5,0,3e-5,6e-5])
set(gca,'XTickLabel',[]);

saveas(gcf,'Fig_factories/PRL/figs/raw_trans_maps_bt_057','epsc')

%% high-Ra transition
lims=[-1.2e-5,1.2e-5];
gMyCM_trans=transformCM(gMyCM,8/256,0.1,11);
%lims=[-3.5e-5,3.5e-5];
%gMyCM_trans=transformCM(gMyCM,10/256,0.07,20);

%case310
gSimname='series2/upper/case31'; gTstart=getTstart(gSimname); make_domain;
transfer_stafield;

figure
set(gcf,'Position',fig_pos)
axes('Position',axes_pos)

imagesc(transQK_BC(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
%colorbar
%%xlabel('$Q$')
%ylabel('$K$')
axis image
%colorbar('TickLabelInterpreter','latex')
ylim([0.5,30.5])
xlim([0.5,50.5])
set(gca,'ytick',[1,10,20,30])
set(gca,'xtick',[1,10,20,30])
set(gca,'XTickLabel',[]);

axes('Parent',gcf,'Position',sub_axes_pos);
%figure

l=plot(sum(transQK_BC(1:maxQ,1:maxK),1).*correction_K(2:maxK+1),1:maxK,'-o','MarkerSize',2);
l.MarkerFaceColor = l.Color; %to fill the symbols
set(gca, 'Color', 'None')
set(gca,'Ycolor','None')
set(gca,'Xcolor',[0 0.4470 0.7410])
box off
xline(0,':','LineWidth',1.5);
xlim([-3e-5,7e-5])
ylim([0.5,30.5])
set(gca,'xtick',[-3e-5,0,3e-5,6e-5])
set(gca,'XTickLabel',[]);

saveas(gcf,'Fig_factories/PRL/figs/raw_trans_maps_bc_310','epsc')

figure
set(gcf,'Position',fig_pos)
axes('Position',axes_pos)

imagesc(transQK_BT(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
%colorbar
%%xlabel('$Q$')
%%ylabel('$K$')
axis image
%colorbar('TickLabelInterpreter','latex')
ylim([0.5,30.5])
xlim([0.5,50.5])
set(gca,'ytick',[1,10,20,30])
set(gca,'xtick',[1,10,20,30])
set(gca,'YTickLabel',[]);
set(gca,'XTickLabel',[]);

axes('Parent',gcf,'Position',sub_axes_pos);
%figure

l=plot(sum(transQK_BT(1:maxQ,1:maxK),1).*correction_K(2:maxK+1),1:maxK,'-o','MarkerSize',2);
l.MarkerFaceColor = l.Color; %to fill the symbols
set(gca, 'Color', 'None')
set(gca,'Ycolor','None')
set(gca,'Xcolor',[0 0.4470 0.7410])
box off
xline(0,':','LineWidth',1.5);
xlim([-3e-5,7e-5])
ylim([0.5,30.5])
set(gca,'xtick',[-3e-5,0,3e-5,6e-5])
set(gca,'XTickLabel',[]);

saveas(gcf,'Fig_factories/PRL/figs/raw_trans_maps_bt_310','epsc')

%case057
gSimname='series2/upper/case313'; gTstart=getTstart(gSimname); make_domain;
transfer_stafield;

figure
set(gcf,'Position',fig_pos)
axes('Position',axes_pos)

imagesc(transQK_BC(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
%colorbar
%xlabel('$Q$')
%ylabel('$K$')
axis image
%colorbar('TickLabelInterpreter','latex')
ylim([0.5,30.5])
xlim([0.5,50.5])
set(gca,'ytick',[1,10,20,30])
set(gca,'xtick',[1,10,20,30])
set(gca,'XTickLabel',{'1','10','20',''});
set(gca,'YTickLabel',{'1','10','20',''});

axes('Parent',gcf,'Position',sub_axes_pos);
%figure

l=plot(sum(transQK_BC(1:maxQ,1:maxK),1).*correction_K(2:maxK+1),1:maxK,'-o','MarkerSize',2);
l.MarkerFaceColor = l.Color; %to fill the symbols
set(gca, 'Color', 'None')
set(gca,'Ycolor','None')
set(gca,'Xcolor',[0 0.4470 0.7410])
box off
xline(0,':','LineWidth',1.5);
xlim([-3e-5,7e-5])
ylim([0.5,30.5])
set(gca,'xtick',[-3e-5,0,3e-5,6e-5])
set(gca,'FontSize',13)

saveas(gcf,'Fig_factories/PRL/figs/raw_trans_maps_bc_313','epsc')

figure
set(gcf,'Position',fig_pos)
axes('Position',axes_pos)

imagesc(transQK_BT(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
%colorbar
%xlabel('$Q$')
%%ylabel('$K$')
axis image
%colorbar('TickLabelInterpreter','latex')
ylim([0.5,30.5])
xlim([0.5,50.5])
set(gca,'ytick',[1,10,20,30])
set(gca,'xtick',[1,10,20,30])
set(gca,'YTickLabel',[]);
set(gca,'XTickLabel',{'1','10','20',''});

axes('Parent',gcf,'Position',sub_axes_pos);
%figure

l=plot(sum(transQK_BT(1:maxQ,1:maxK),1).*correction_K(2:maxK+1),1:maxK,'-o','MarkerSize',2);
l.MarkerFaceColor = l.Color; %to fill the symbols
set(gca, 'Color', 'None')
set(gca,'Ycolor','None')
set(gca,'Xcolor',[0 0.4470 0.7410])
box off
xline(0,':','LineWidth',1.5);
xlim([-3e-5,7e-5])
ylim([0.5,30.5])
set(gca,'xtick',[-3e-5,0,3e-5,6e-5])
set(gca,'FontSize',13)

saveas(gcf,'Fig_factories/PRL/figs/raw_trans_maps_bt_313','epsc')

figure
set(gcf,'Position',[fig_pos(1),fig_pos(2),fig_pos(3),2*fig_pos(4)])
colormap(gMyCM_trans)
colorbar
caxis(lims)
colorbar('TickLabelInterpreter','latex')

saveas(gcf,'Fig_factories/PRL/figs/raw_trans_maps_colorbar','epsc')

gPlotting=gPlottingTransMaps;
gPlottingLSVexample=gPlotting; gPlotting=false;

%slices of horziontal kinetic energy
gSimname='series_interim/case17'; gTstart=getTstart(gSimname); make_domain;
hor_kin_3d;

figure
cutoff=round(0.75*d.nz);
shift=[1.8,0.25];
hor_kin=circshift(hor_kin,findIn(d.xms,shift(1))-1,1);
hor_kin=circshift(hor_kin,findIn(d.yms,shift(2))-1,2);
drawSlices(d.xms,d.yms,d.zs(1:cutoff),d.xms(1),d.yms(1),d.zs(cutoff),hor_kin(:,:,1:cutoff),[0,0.2],gMyCM,true);
hold on
box on
line(xlim,[0,0],[d.zs(cutoff),d.zs(cutoff)],'Color','k')
line([0,0],ylim,[d.zs(cutoff),d.zs(cutoff)],'Color','k')
line([0,0],[0,0],[0,d.zs(cutoff)],'Color','k')
set(gca,'xtick',[])
set(gca,'ytick',[])
set(gca,'ztick',[])
p=get(gcf,'Position');
set(gcf,'Position',[p(1),p(2),round(1.53*p(4)),p(4)])

cb=colorbar('TickLabelInterpreter','latex');
cb.Ticks=0:0.05:0.2;

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/PRL/figs/raw_lsv_example','-dpng','-painters','-r600')

gPlotting=gPlottingLSVexample;
gPlottingKins=gPlotting; gPlotting=false;

ra_trans_1=5.65e6;
ra_trans_2=3.115e7;
ra_trans_hyst=2.15e7;

series_together_hyst;

control_lims=[0.9*min(min(all_control_params),min(all_control_params_hyst)),1.1*max(max(all_control_params),max(all_control_params_hyst))];

%plot kinetic energy params
figure
l=plot(all_control_params,all_params(2,:),'-s','DisplayName','$E^\textrm{2D}$','MarkerSize',6);
l.MarkerFaceColor = l.Color; %to fill the symbols
hold on
l=plot(all_control_params,all_params(4,:),'-s','DisplayName','$\hat{E}^\textrm{2D}(K=1)$','MarkerSize',6);
l.MarkerFaceColor = l.Color; %to fill the symbols
l=plot(all_control_params,all_params(3,:),'-s','DisplayName','$E_H^\textrm{3D}$','MarkerSize',6);
l.MarkerFaceColor = l.Color; %to fill the symbols
l=plot(all_control_params,all_params(1,:),'-s','DisplayName','$E_V^\textrm{3D}$','MarkerSize',6);
l.MarkerFaceColor = l.Color; %to fill the symbols
h_leg=legend('Location','northwest');
HeightScaleFactor = 1.1;
NewHeight = h_leg.Position(4) * HeightScaleFactor;
h_leg.Position(2) = h_leg.Position(2) - (NewHeight - h_leg.Position(4));
h_leg.Position(4) = NewHeight;
set(gca,'ColorOrderIndex',1)
plot(all_control_params_hyst,all_params_hyst(2,:),'-d','MarkerSize',8);
plot(all_control_params_hyst,all_params_hyst(4,:),'-d','MarkerSize',8);
plot(all_control_params_hyst,all_params_hyst(3,:),'-d','MarkerSize',8);
plot(all_control_params_hyst,all_params_hyst(1,:),'-d','MarkerSize',8);
set(gca,'ColorOrderIndex',1)
errorbar(all_control_params,all_params(2,:),all_param_errors(2,:), 'vertical', 'LineStyle', 'none');
errorbar(all_control_params,all_params(4,:),all_param_errors(4,:), 'vertical', 'LineStyle', 'none');
errorbar(all_control_params,all_params(3,:),all_param_errors(3,:), 'vertical', 'LineStyle', 'none');
errorbar(all_control_params,all_params(1,:),all_param_errors(1,:), 'vertical', 'LineStyle', 'none');
set(gca,'ColorOrderIndex',1)
errorbar(all_control_params_hyst,all_params_hyst(2,:),all_param_errors_hyst(2,:), 'vertical', 'LineStyle', 'none');
errorbar(all_control_params_hyst,all_params_hyst(4,:),all_param_errors_hyst(4,:), 'vertical', 'LineStyle', 'none');
errorbar(all_control_params_hyst,all_params_hyst(3,:),all_param_errors_hyst(3,:), 'vertical', 'LineStyle', 'none');
errorbar(all_control_params_hyst,all_params_hyst(1,:),all_param_errors_hyst(1,:), 'vertical', 'LineStyle', 'none');
xline(ra_trans_1,'-.','LineWidth',1.8,'Color','#1ba497');
xline(ra_trans_2,'-.r','LineWidth',1.8);
xline(ra_trans_hyst,'-.m','LineWidth',1.8);
ylabel('Kinetic energy (in $U^2$)')
xlabel('$\textrm{Ra}$')
xlim(control_lims)
yline(0,'k');
%ylim([-2e-5,22e-5])
set(gca, 'XScale', 'log')
grid on
box on
xticks([2e6, 1e7, 5e7])
xticklabels({'$2\times10^6$', '$10^7$' ,'$5\times10^7$'})

saveas(gcf,'Fig_factories/PRL/figs/raw_kins','epsc')


gPlotting=gPlottingKins;
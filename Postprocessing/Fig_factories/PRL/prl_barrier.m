gPlottingPRLbarrier=gPlotting; gPlotting=false;

%kinetic energy
color_main=[0 0.4470 0.7410];
color_other=[0.8,0.8,0.8];

figure
gSimname='case6_ensemble_ave/case59'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_other)
hold on
gSimname='case6_ensemble_ave/case74'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_other)
gSimname='case6_ensemble_ave/case44'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_other)
gSimname='case6_ensemble_ave/case34'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_other)
gSimname='case6_ensemble_ave/case14'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_other)
gSimname='case6_ensemble_ave/case65'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_other)
gSimname='case6_ensemble_ave/case68'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_main)
yline(0.01325,'--k','LineWidth',1.5);
ylabel('Horizontal kinetic energy $E_H/U^2$')
xlabel('Time $t / (U^{-1}H)$')
ylim([0,0.0181])
grid on
box on

set(gcf,'renderer','Painters')
saveas(gcf,'Fig_factories/PRL/figs/raw_barrier_timeseries','epsc')

% f=gcf;
% f.PaperPositionMode='auto';
% print('Fig_factories/PRL/figs/raw_barrier_timeseries','-dpng','-painters','-r600')


%ensemble average
ensemble_ave;

%distribution of t_LSV
figure
c = [0 0.4470 0.7410];
stairs(m_ecdf_x_lsv,m_ecdf_lsv,'-','Color',c,'LineWidth',2.5)
hold on
stairs(m_ecdf_x_lsv,[m_ecdf_lo, m_ecdf_up],':','Color',0.7*c+0.3,'LineWidth',2)
plot(m_ecdf_x_lsv,1-exp(-(m_ecdf_x_lsv-m_fit_lsv.t0)/(m_fit_lsv.mu-m_fit_lsv.t0)),'--','Color',[0.8500 0.3250 0.0980],'LineWidth',2.8)
xlabel('LSV time $t_\textrm{LSV} / (U^{-1}H)$')
ylabel('Empirical cumulative distribution')
ylim([0,1])
xlim([0,m_ecdf_x_lsv(end)])
%area([0,m_fit_lsv.t0],[9,9],'FaceColor',[0,0,0],'FaceAlpha',0.2,'EdgeAlpha',0)
grid on
box on

saveas(gcf,'Fig_factories/PRL/figs/raw_barrier_distribution','epsc')

% f=gcf;
% f.PaperPositionMode='auto';
% print('Fig_factories/PRL/figs/raw_barrier_distribution','-dpng','-painters','-r600')

%histogram
figure
bins=10; edges=m_fit_lsv.t0+waiting_times(1)+(0:bins)*(waiting_times(end)-waiting_times(1))/bins;
histogram(lsv_times,edges,'FaceAlpha',1,'FaceColor',[0.4, 0.6682, 0.8446])
%xlabel('LSV time $t_\textrm{LSV} / (U^{-1}H)$')
ylabel('Histogram')
xlim([0,3.5e4])
grid on
box on
p=get(gcf,'Position');
set(gcf,'Position',[p(1),p(2),312,196])

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/PRL/figs/raw_barrier_histogram','-dpng','-painters','-r600')

gPlotting=gPlottingPRLbarrier;
gPlottingEnsemble=gPlotting; gPlotting=false;

%kinetic energy
color_main=[0 0.4470 0.7410];
color_other=[0.8 0.8 0.8];

set(0,'DefaultAxesFontSize',22)

figure
gSimname='case6_ensemble_ave/case59'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_other)
hold on
gSimname='case6_ensemble_ave/case74'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_other)
gSimname='case6_ensemble_ave/case44'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_other)
gSimname='case6_ensemble_ave/case34'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_other)
gSimname='case6_ensemble_ave/case14'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_other)
gSimname='case6_ensemble_ave/case65'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_other)
gSimname='case6_ensemble_ave/case68'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_main)
%yline(0.01325,'--k','LineWidth',1.5);
ylabel('Kinetic energy in LSV')
xlabel('Time')
ylim([0.003,0.0181])
xlim([0,14000])
grid on
box on

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Seminar/figs/ensemble','-dpng','-painters','-r600')


gPlotting=gPlottingEnsemble;
gPlottingKins=gPlotting; gPlotting=false;

ra_trans_1=5.65e6;
ra_trans_2=3.115e7;
ra_trans_hyst=2.15e7;

series_together_hyst;

set(0,'DefaultAxesFontSize',22)

control_lims=[0.9*min(min(all_control_params),min(all_control_params_hyst)),1.1*max(max(all_control_params),max(all_control_params_hyst))];

%plot kinetic energy params
figure
hold on
l=plot(all_control_params,all_params(4,:),'-s','Color',[0.8500 0.3250 0.0980],'DisplayName','$\hat{E}^\textrm{2D}(K=1)$','MarkerSize',7,'LineWidth',1.5);
l.MarkerFaceColor = l.Color; %to fill the symbols
plot(all_control_params_hyst,all_params_hyst(4,:),'-d','Color',[0.8500 0.3250 0.0980],'MarkerSize',10,'LineWidth',1.5);
errorbar(all_control_params,all_params(4,:),all_param_errors(4,:), 'vertical','Color',[0.8500 0.3250 0.0980], 'LineStyle', 'none');
errorbar(all_control_params_hyst,all_params_hyst(4,:),all_param_errors_hyst(4,:), 'vertical','Color',[0.8500 0.3250 0.0980], 'LineStyle', 'none');
xline(ra_trans_1,'-.','LineWidth',1.8,'Color',[0.3 0.3 0.3]);
xline(ra_trans_2,'-.r','LineWidth',1.8,'Color',[0.3 0.3 0.3]);
xline(ra_trans_hyst,'-.m','LineWidth',1.8,'Color',[0.3 0.3 0.3]);
ylabel('Kinetic energy in LSV')
xlabel('$\textrm{Ra}$')
xlim(control_lims)
yline(0,'k');
%ylim([-2e-5,22e-5])
set(gca, 'XScale', 'log')
grid on
box on
xticks([2e6, 1e7, 5e7])
xticklabels({'$2\times10^6$', '$10^7$' ,'$5\times10^7$'})

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Seminar/figs/kins','-dpng','-painters','-r600')


gPlotting=gPlottingKins;
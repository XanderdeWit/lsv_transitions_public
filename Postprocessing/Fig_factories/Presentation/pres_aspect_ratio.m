gPlottingAspectRatio=gPlotting; gPlotting=false;

set(0,'DefaultAxesFontSize',20)

%ar_together_bimodal;

control_lims=[0.9*(5e6),1.1*(8e6)];

figure
iplot=3;
l=gobjects(length(all_series),1);
for i=1:length(all_series)
    l(i)=plot(all_control_params{i},all_large_order_params{i}(iplot,:),'-s','MarkerSize',6,'LineWidth',1.5);
    l(i).MarkerFaceColor = l(i).Color; %to fill the symbols
    hold on
end
set(gca,'ColorOrderIndex',1)
for i=1:length(all_series)
    errorbar(all_control_params{i},all_large_order_params{i}(iplot,:),all_large_order_param_errors{i}(iplot,:), 'vertical', 'LineStyle', 'none');
end
ylabel('LSV kinetic energy ratio')
xlabel('Buoyancy $\textrm{Ra}$')
xlim(control_lims)
set(gca, 'XScale', 'log')
grid on
box on
for iseries=1:length(bimodals)
    for icase=1:length(bimodals{iseries})
        set(gca,'ColorOrderIndex',iseries)
        scatter(bimodal_control_params{iseries}{icase},low_large_order_params{iseries}{icase}(iplot),100,'d','filled')
        set(gca,'ColorOrderIndex',iseries)
        errorbar(bimodal_control_params{iseries}{icase},low_large_order_params{iseries}{icase}(iplot),low_large_order_param_errors{iseries}{icase}(iplot), 'vertical', 'LineStyle', 'none');
        
        set(gca,'ColorOrderIndex',iseries)
        scatter(bimodal_control_params{iseries}{icase},high_large_order_params{iseries}{icase}(iplot),100,'d','filled')
        set(gca,'ColorOrderIndex',iseries)
        errorbar(bimodal_control_params{iseries}{icase},high_large_order_params{iseries}{icase}(iplot),high_large_order_param_errors{iseries}{icase}(iplot), 'vertical', 'LineStyle', 'none');
    end
    
    if length(bimodals{iseries})>0
        adj_low=findIn(all_control_params{iseries},bimodal_control_params{iseries}{1})-1;
        adj_high=findIn(all_control_params{iseries},bimodal_control_params{iseries}{end})+1;
        low_large_order_params_arr=cell2mat(low_large_order_params{iseries});
        high_large_order_params_arr=cell2mat(high_large_order_params{iseries});
        dash_low_x=[all_control_params{iseries}(adj_low),cell2mat(bimodal_control_params{iseries})];
        dash_low_y=[all_large_order_params{iseries}(iplot,adj_low),low_large_order_params_arr(iplot,:)];
        dash_high_x=[cell2mat(bimodal_control_params{iseries}),all_control_params{iseries}(adj_high)];
        dash_high_y=[high_large_order_params_arr(iplot,:),all_large_order_params{iseries}(iplot,adj_high)];

        set(gca,'ColorOrderIndex',iseries)
        plot(dash_low_x,dash_low_y,':','LineWidth',1.5)
        set(gca,'ColorOrderIndex',iseries)
        plot(dash_high_x,dash_high_y,':','LineWidth',1.5)
    end
end

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/aspect_ratio_kins','-dpng','-painters','-r300')

figure
iplot=9;
for iseries=1:length(all_series)
    l=plot(all_control_params{iseries},all_params{iseries}(iplot,:),'-s','MarkerSize',6,'LineWidth',1.5);
    l.MarkerFaceColor = l.Color; %to fill the symbols
    hold on
end
set(gca,'ColorOrderIndex',1)
for iseries=1:length(all_series)
    errorbar(all_control_params{iseries},all_params{iseries}(iplot,:),all_param_errors{iseries}(iplot,:), 'vertical', 'LineStyle', 'none');
end
ylabel('Upscale transport into $K=1$')
xlabel('Buoyancy $\textrm{Ra}$')
xlim(control_lims)
set(gca, 'XScale', 'log')
grid on
box on
for iseries=1:length(bimodals)
    for icase=1:length(bimodals{iseries})
        set(gca,'ColorOrderIndex',iseries)
        scatter(bimodal_control_params{iseries}{icase},low_params{iseries}{icase}(iplot),100,'d','filled')
        set(gca,'ColorOrderIndex',iseries)
        errorbar(bimodal_control_params{iseries}{icase},low_params{iseries}{icase}(iplot),low_param_errors{iseries}{icase}(iplot), 'vertical', 'LineStyle', 'none');

        set(gca,'ColorOrderIndex',iseries)
        scatter(bimodal_control_params{iseries}{icase},high_params{iseries}{icase}(iplot),100,'d','filled')
        set(gca,'ColorOrderIndex',iseries)
        errorbar(bimodal_control_params{iseries}{icase},high_params{iseries}{icase}(iplot),high_param_errors{iseries}{icase}(iplot), 'vertical', 'LineStyle', 'none');
    end
    
    if length(bimodals{iseries})>0
        adj_low=findIn(all_control_params{iseries},bimodal_control_params{iseries}{1})-1;
        adj_high=findIn(all_control_params{iseries},bimodal_control_params{iseries}{end})+1;
        low_params_arr=cell2mat(low_params{iseries});
        high_params_arr=cell2mat(high_params{iseries});
        dash_low_x=[all_control_params{iseries}(adj_low),cell2mat(bimodal_control_params{iseries})];
        dash_low_y=[all_params{iseries}(iplot,adj_low),low_params_arr(iplot,:)];
        dash_high_x=[cell2mat(bimodal_control_params{iseries}),all_control_params{iseries}(adj_high)];
        dash_high_y=[high_params_arr(iplot,:),all_params{iseries}(iplot,adj_high)];

        set(gca,'ColorOrderIndex',iseries)
        plot(dash_low_x,dash_low_y,':','LineWidth',1.5)
        set(gca,'ColorOrderIndex',iseries)
        plot(dash_high_x,dash_high_y,':','LineWidth',1.5)
    end
end

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/aspect_ratio_trans','-dpng','-painters','-r300')

gPlotting=gPlottingAspectRatio;
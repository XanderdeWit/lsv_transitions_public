gPlottingIntro=gPlotting; gPlotting=false;

set(0,'DefaultAxesFontSize',20)

%flow snapshot
gSimname='series1/case10'; make_domain;

vorticity_3d;

figure
ome_z_bar=circshift(ome_z_bar,findIn(d.xs,0.95)-1,1);
ome_z_bar=circshift(ome_z_bar,findIn(d.ys,0.35)-1,2);
drawCross(d.xs,d.ys,ome_z_bar',[-4,4],'pcolor',gMyCM,true);

xticks([])
yticks([])
colorbar off

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/lsv_snap','-dpng','-painters','-r300')

%slices of horziontal kinetic energy
gSimname='series_interim/case17'; make_domain;
hor_kin_3d;

figure
cutoff=round(0.75*d.nz);
shift=[1.8,0.25];
hor_kin=circshift(hor_kin,findIn(d.xms,shift(1))-1,1);
hor_kin=circshift(hor_kin,findIn(d.yms,shift(2))-1,2);
drawSlices(d.xms,d.yms,d.zs(1:cutoff),d.xms(1),d.yms(1),d.zs(cutoff),hor_kin(:,:,1:cutoff),[0,0.2],gMyCM,true);
hold on
box on
line(xlim,[0,0],[d.zs(cutoff),d.zs(cutoff)],'Color','k')
line([0,0],ylim,[d.zs(cutoff),d.zs(cutoff)],'Color','k')
line([0,0],[0,0],[0,d.zs(cutoff)],'Color','k')
set(gca,'xtick',[])
set(gca,'ytick',[])
set(gca,'ztick',[])
p=get(gcf,'Position');
set(gcf,'Position',[p(1),p(2),round(1.53*p(4)),p(4)])
%xlabel('')
%ylabel('')
%zlabel('')
colorbar off

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/lsv_slices','-dpng','-painters','-r300')


gPlotting=gPlottingIntro;
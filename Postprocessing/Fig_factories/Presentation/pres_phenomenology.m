gPlottingPhenomenology=gPlotting; gPlotting=false;

set(0,'DefaultAxesFontSize',20)

% kin plots
gSimname='series1/case10'; gTstart=getTstart(gSimname); make_domain;

ts_kins;

t_stop=1500;

figure
it_stop=findIn(tsK.Time,t_stop);
area(tsK.Time(1:it_stop),[tsKv.Data(1:it_stop),tsKH3D.Data(1:it_stop),tsKH2D.Data(1:it_stop)])
ylabel('Kinetic energy')
xlabel('Time')
h_leg=legend({'3D (vert)','3D (hor)','2D'},'Location','northwest');

h_leg.PlotChildren = h_leg.PlotChildren([3,2,1]);
HeightScaleFactor = 1.2;
NewHeight = h_leg.Position(4) * HeightScaleFactor;
h_leg.Position(2) = h_leg.Position(2) - (NewHeight - h_leg.Position(4));
h_leg.Position(4) = NewHeight;

grid on
box on

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/kin_areas','-dpng','-painters','-r300')


ts_spectrum_KH2D;

it_stop=findIn(time,t_stop);

figure
for ik=1:7
    plot(time(1:it_stop),E2Dk(1:it_stop,ik),'LineWidth',2,'DisplayName',['$K=' num2str(k(ik)) '$'])
    hold on
end
legend('Location','southeast')
set(gca,'Children',flip(get(gca,'Children')))
ylabel('Spectral modes of 2D energy')
xlabel('Time')
set(gca,'YScale','log')
ylim([10^-5,10^-1])
grid on
box on

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/kin_modes','-dpng','-painters','-r300')

%show snapshots
stst3dir=[gDataDir gSimname '/stst3'];
slabname='slabz';

slab_is=[100, 500, 1100];

for i=1:length(slab_is)
    vx=h5read([stst3dir '/' slabname 'q1_' sprintf('%08d', slab_is(i)) '.h5'],'/var');
    vy=h5read([stst3dir '/' slabname 'q2_' sprintf('%08d', slab_is(i)) '.h5'],'/var');

    figure
    drawCross(d.xs,d.ys,getAxialVorticity(vx,vy,d)',[-3,3],'pcolor',gMyCM,true);
    set(gca,'xtick',[])
    set(gca,'ytick',[])
    xlabel('$x$')
    ylabel('$y$')
    
    f=gcf;
    f.PaperPositionMode='auto';
    set(gcf, 'Color', [0.9,0.9,0.9]);
    set(gcf, 'InvertHardcopy', 'off')
    print(['Fig_factories/Presentation/figs/vort_snap_' num2str(i)],'-dpng','-painters','-r300')
end

transfer_stafield;

lims=[-2.5e-5,2.5e-5];
gMyCM_trans=transformCM(gMyCM,10/256,0.07,20);

figure
imagesc(transQK_BT(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
colorbar
xlabel('$Q$')
%ylabel('$K$')
set(gca,'YTickLabel',[]);
axis image
colorbar('TickLabelInterpreter','latex')

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/energy_transfer_bt','-dpng','-painters','-r300')

figure
imagesc(transQK_BC(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
%colorbar
xlabel('$Q$')
ylabel('$K$')
axis image
%colorbar('TickLabelInterpreter','latex')

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/energy_transfer_bc','-dpng','-painters','-r300')

gPlotting=gPlottingPhenomenology;
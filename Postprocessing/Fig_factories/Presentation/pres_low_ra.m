gPlottingLowRa=gPlotting; gPlotting=false;

set(0,'DefaultAxesFontSize',20)

gSeries='series1/';

ra_trans=5.65e6;

series_analysis;

%plot kinetic energy LOP;
figure
yyaxis left
l=plot(control_params,large_order_params(3,:),'-s','Color',[0 0.4470 0.7410],'MarkerSize',6,'LineWidth',1.5);
l.MarkerFaceColor = l.Color; %to fill the symbols
hold on
errorbar(control_params,large_order_params(3,:),large_order_param_errors(3,:), 'vertical', 'Color',[0 0.4470 0.7410], 'LineStyle', 'none');
xline(ra_trans,'-.','LineWidth',1.8,'Color',[0.8500 0.3250 0.0980]);
ylabel('LSV kinetic energy ratio')
xlabel('Buoyancy $\textrm{Ra}$')
yline(0,'k');
xlim([0.9*min(control_params),max(control_params)+0.1*min(control_params)])
ylim([0,12])
grid on
box on
yyaxis right
ylabel('Anisotropy $\alpha$')
ylim([0,0.6])
ax = gca;
ax.YAxis(1).Color = 'k';
ax.YAxis(2).Color = 'k';

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/low_ra_kins','-dpng','-painters','-r300')

%plot energetic transport
figure
l=plot(control_params,params(9,:),'-s','Color',[0.4940 0.1840 0.5560],'MarkerSize',6,'LineWidth',1.5,'DisplayName','3D $<>$ 2D');
l.MarkerFaceColor = l.Color; %to fill the symbols
hold on
l=plot(control_params,params(8,:),'-s','Color',[0.9290 0.6940 0.1250],'MarkerSize',6,'LineWidth',1.5,'DisplayName','2D $<>$ 2D');
l.MarkerFaceColor = l.Color; %to fill the symbols
legend('Location','northwest')
errorbar(control_params,params(9,:),param_errors(9,:), 'vertical','Color',[0.4940 0.1840 0.5560], 'LineStyle', 'none');
errorbar(control_params,params(8,:),param_errors(8,:), 'vertical','Color',[0.9290 0.6940 0.1250], 'LineStyle', 'none');
xline(ra_trans,'-.','LineWidth',1.8,'Color',[0.8500 0.3250 0.0980]);
ylabel('Upscale transport into $K=1$')
xlabel('Buoyancy $\textrm{Ra}$')
xlim([0.9*min(control_params),max(control_params)+0.1*min(control_params)])
yline(0,'k');
%ylim([-0.02,0.3])
grid on
box on

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/low_ra_trans','-dpng','-painters','-r300')

%plot kinetic energy LOP with ani;
figure
yyaxis right
l=plot(control_params,anis(3,:),'-s','DisplayName','$\alpha$','Color',[0.4660 0.6740 0.1880],'MarkerSize',6,'LineWidth',1.5);
l.MarkerFaceColor = l.Color; %to fill the symbols
legend('Location','northwest')
hold on
errorbar(control_params,anis(3,:),ani_errors(3,:), 'vertical', 'Color',[0.4660 0.6740 0.1880], 'LineStyle', 'none');
ylabel('Anisotropy $\alpha$')
ylim([0,0.6])
yyaxis left
l=plot(control_params,large_order_params(3,:),'-s','Color',[0 0.4470 0.7410],'MarkerSize',6,'LineWidth',1.5);
l.MarkerFaceColor = l.Color; %to fill the symbols
hold on
errorbar(control_params,large_order_params(3,:),large_order_param_errors(3,:), 'vertical', 'Color',[0 0.4470 0.7410], 'LineStyle', 'none');
xline(ra_trans,'-.','LineWidth',1.8,'Color',[0.8500 0.3250 0.0980]);
ylabel('LSV kinetic energy ratio')
xlabel('Buoyancy $\textrm{Ra}$')
yline(0,'k');
xlim([0.9*min(control_params),max(control_params)+0.1*min(control_params)])
ylim([0,12])
grid on
box on
ax = gca;
ax.YAxis(1).Color = 'k';
ax.YAxis(2).Color = 'k';

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/low_ra_kins_ani','-dpng','-painters','-r300')

% horizontal kinetic energies
color_main=[0 0.4470 0.7410];
color_other=[0.8,0.8,0.8];

figure
gSimname='case6_ensemble_ave/case59'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_other)
hold on
gSimname='case6_ensemble_ave/case74'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_other)
gSimname='case6_ensemble_ave/case44'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_other)
gSimname='case6_ensemble_ave/case34'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_other)
gSimname='case6_ensemble_ave/case14'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_other)
gSimname='case6_ensemble_ave/case65'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_other)
gSimname='case6_ensemble_ave/case68'; gTstart=getTstart(gSimname); make_domain;
ts_kins;
plot(tsKH.Time,tsKH.Data,'LineWidth',2,'Color',color_main)
%yline(0.01325,'--k','LineWidth',1.5);
ylabel('Horizontal kinetic energy')
xlabel('Time')
ylim([0,0.0181])
xlim([0,14001])
grid on
box on

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/ensemble_timeseries','-dpng','-painters','-r300')

%ensemble average
ensemble_ave;

%distribution of t_LSV
figure
c = [0 0.4470 0.7410];
stairs(m_ecdf_x_lsv,m_ecdf_lsv,'-','Color',c,'LineWidth',2.5)
hold on
stairs(m_ecdf_x_lsv,[m_ecdf_lo, m_ecdf_up],':','Color',0.7*c+0.3,'LineWidth',2)
plot(m_ecdf_x_lsv,1-exp(-(m_ecdf_x_lsv-m_fit_lsv.t0)/(m_fit_lsv.mu-m_fit_lsv.t0)),'--','Color',[0.8500 0.3250 0.0980],'LineWidth',2.8)
xlabel('LSV time $t_\textrm{LSV}$')
ylabel('Empirical cumulative distribution')
ylim([0,1])
xlim([0,m_ecdf_x_lsv(end)])
%area([0,m_fit_lsv.t0],[9,9],'FaceColor',[0,0,0],'FaceAlpha',0.2,'EdgeAlpha',0)
grid on
box on

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/ensemble_dist','-dpng','-painters','-r300')

%histogram
figure
bins=10; edges=m_fit_lsv.t0+waiting_times(1)+(0:bins)*(waiting_times(end)-waiting_times(1))/bins;
histogram(lsv_times,edges,'FaceAlpha',1,'FaceColor',[0.4, 0.6682, 0.8446])
%xlabel('LSV time $t_\textrm{LSV} / (U^{-1}H)$')
ylabel('Histogram')
xlim([0,3.5e4])
grid on
box on
p=get(gcf,'Position');
set(gcf,'Position',[p(1),p(2),312,196])

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/ensemble_histogram','-dpng','-painters','-r300')

%calculate free energy and diffusion
free_energy_kin;

%Plot potential
figure
plot(energies,U,'LineWidth',2)
box on
grid on
xlim([0,0.015])
ylabel('``Free energy"')
xlabel('LSV order parameter')

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/langevin_energy','-dpng','-painters','-r300')

%calculate Monte-Carlo solutions of Langevin equation
langevin_monte_carlo;

%plot trajectories
figure
cm=linspace(0.5,1.3,256)'*[0, 0.4470, 0.7410];
trajs=[21,54,100,67,60,13];
hold on
for itraj=1:length(trajs)
    plot((0:length(qts{trajs(itraj)})-1)*dt,qts{trajs(itraj)},'LineWidth',2,'Color',cm(1+round(255*itraj/length(trajs)),:))
end
yline(threshold,'--k','LineWidth',1.5);
ylabel('LSV order parameter')
xlabel('Time')
grid on
box on

p=get(gcf,'Position');
set(gcf,'Position',[p(1),p(2),0.7*p(3),0.7*p(4)])

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/langevin_timeseries','-dpng','-painters','-r300')

%plot cumulative distribution
figure
c = [0 0.4470 0.7410];
stairs(m_ecdf_x_lsv,m_ecdf_lsv,'-','Color',c,'LineWidth',2.5)
hold on
stairs(m_ecdf_x_lsv,[m_ecdf_lo, m_ecdf_up],':','Color',0.7*c+0.3,'LineWidth',2)
plot(m_ecdf_x_lsv,1-exp(-(m_ecdf_x_lsv-m_fit_lsv.t0)/(m_fit_lsv.mu-m_fit_lsv.t0)),'--','Color',[0.8500 0.3250 0.0980],'LineWidth',2.8)
xlabel('LSV time $t_\mathrm{LSV}$')
ylabel('Cumulative distribution')
ylim([0,1])
xlim([0,m_ecdf_x_lsv(end)])
%area([0,m_fit_lsv.t0],[9,9],'FaceColor',[0,0,0],'FaceAlpha',0.2,'EdgeAlpha',0)
grid on
box on

p=get(gcf,'Position');
set(gcf,'Position',[p(1),p(2),0.7*p(3),0.7*p(4)])

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/langevin_dist','-dpng','-painters','-r300')


gPlotting=gPlottingLowRa;
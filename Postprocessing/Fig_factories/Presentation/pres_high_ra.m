gPlottingHighRa=gPlotting; gPlotting=false;

ra_trans_2=3.115e7;
ra_trans_hyst=2.15e7;

hyst_series;

set(0,'DefaultAxesFontSize',20)

control_lims=[0.9*min(min(control_params_lower(1:end-1)),min(control_params_upper(1:end-1))),max(max(control_params_lower(1:end-1)),max(control_params_upper(1:end-1)))+0.1*min(min(control_params_lower(1:end-1)),min(control_params_upper(1:end-1)))];

%plot kinetic energy params
figure
hold on
l=plot(control_params_upper(1:end-1),large_order_params_upper(3,1:end-1),'-s','Color',[0 0.4470 0.7410],'MarkerSize',6,'LineWidth',1.5);
l.MarkerFaceColor = l.Color; %to fill the symbols
plot(control_params_lower(1:end-1),large_order_params_lower(3,1:end-1),'-d','Color',[0 0.4470 0.7410],'MarkerSize',10,'LineWidth',1.5);
errorbar(control_params_upper(1:end-1),large_order_params_upper(3,1:end-1),large_order_param_errors_upper(3,1:end-1), 'vertical','Color',[0 0.4470 0.7410], 'LineStyle', 'none');
errorbar(control_params_lower(1:end-1),large_order_params_lower(3,1:end-1),large_order_param_errors_lower(3,1:end-1), 'vertical','Color',[0 0.4470 0.7410], 'LineStyle', 'none');
xline(ra_trans_2,'-.','LineWidth',1.8,'Color',[0.8500 0.3250 0.0980]);
xline(ra_trans_hyst,'-.','LineWidth',1.8,'Color',[0.8500 0.3250 0.0980]);
ylabel('LSV kinetic energy ratio')
xlabel('Buoyancy $\textrm{Ra}$')
xlim(control_lims)
yline(0,'k');
grid on
box on

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/high_ra_kins','-dpng','-painters','-r300')

%plot energy transport params
figure
hold on
l=plot(control_params_upper(1:end-1),params_upper(9,1:end-1),'-s','Color',[0.4940 0.1840 0.5560],'MarkerSize',6,'LineWidth',1.5,'DisplayName','3D $<>$ 2D');
l.MarkerFaceColor = l.Color; %to fill the symbols
l=plot(control_params_upper(1:end-1),params_upper(8,1:end-1),'-s','Color',[0.9290 0.6940 0.1250],'MarkerSize',6,'LineWidth',1.5,'DisplayName','2D $<>$ 2D');
l.MarkerFaceColor = l.Color; %to fill the symbols
legend('Location','northeast')
plot(control_params_lower(1:end-1),params_lower(9,1:end-1),'-d','Color',[0.4940 0.1840 0.5560],'MarkerSize',10,'LineWidth',1.5);
plot(control_params_lower(1:end-1),params_lower(8,1:end-1),'-d','Color',[0.9290 0.6940 0.1250],'MarkerSize',10,'LineWidth',1.5);
errorbar(control_params_upper(1:end-1),params_upper(9,1:end-1),param_errors_upper(9,1:end-1), 'vertical','Color',[0.4940 0.1840 0.5560], 'LineStyle', 'none');
errorbar(control_params_lower(1:end-1),params_lower(9,1:end-1),param_errors_lower(9,1:end-1), 'vertical','Color',[0.4940 0.1840 0.5560], 'LineStyle', 'none');
errorbar(control_params_upper(1:end-1),params_upper(8,1:end-1),param_errors_upper(8,1:end-1), 'vertical','Color',[0.9290 0.6940 0.1250], 'LineStyle', 'none');
errorbar(control_params_lower(1:end-1),params_lower(8,1:end-1),param_errors_lower(8,1:end-1), 'vertical','Color',[0.9290 0.6940 0.1250], 'LineStyle', 'none');
xline(ra_trans_2,'-.','LineWidth',1.8,'Color',[0.8500 0.3250 0.0980]);
xline(ra_trans_hyst,'-.','LineWidth',1.8,'Color',[0.8500 0.3250 0.0980]);
ylabel('Upscale transport into $K=1$')
xlabel('Buoyancy $\textrm{Ra}$')
xlim(control_lims)
ylim([-2e-5,20e-5])
yline(0,'k');
grid on
box on

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/high_ra_trans','-dpng','-painters','-r300')

%flow snapshots
gSimname='series2/upper/case25'; make_domain;

hor_kin_3d;

figure
hor_kin_bar=circshift(hor_kin_bar,findIn(d.xs,1.6)-1,1);
hor_kin_bar=circshift(hor_kin_bar,findIn(d.ys,1.2)-1,2);
drawCross(d.xms,d.yms,hor_kin_bar',[0,0.2],'pcolor',gMyCM,true);

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/high_ra_lsv','-dpng','-painters','-r300')

gSimname='series2/lower/case25'; make_domain;

hor_kin_3d;

figure
drawCross(d.xms,d.yms,hor_kin_bar',[0,0.08],'pcolor',gMyCM,true);

f=gcf;
f.PaperPositionMode='auto';
set(gcf, 'Color', [0.9,0.9,0.9]);
set(gcf, 'InvertHardcopy', 'off')
print('Fig_factories/Presentation/figs/high_ra_no_lsv','-dpng','-painters','-r300')


gPlotting=gPlottingHighRa;
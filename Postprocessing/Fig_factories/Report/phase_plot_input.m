gPlottingPhasePlotInput=gPlotting; gPlotting=false;

%get input data
phase_plot_pr1;

%Plot big phase plot without data
figure
hold on
limEk=[10^-6,10^-3];
limRa=[10^5,10^10];
Ek=logspace(log10(limEk(1)),log10(limEk(2)));
%plot critical Ra
Rac=(27*pi^4/4)^(1/3)*Ek.^(-4/3);
plot(Ek,Rac,'k')
area(Ek,Rac,'FaceColor',[0.7 0.7 0.7])
%plot lower boundary Favier et al. 2014
Ra_low=3.0*Rac;
split=findIn(Ek,10^-3.9);
plot(Ek(1:split),Ra_low(1:split),'k-')
%plot upper boundary Favier et al. 2014
Ra_upp=0.325*Ek.^-2;
plot(Ek(1:split),Ra_upp(1:split),'k-')

ylabel('Ra')
xlabel('Ek')
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')
xlim(limEk)
ylim(limRa)
grid on
box on
ar=[1 (log(limRa(2))-log(limRa(1)))/(2*(log(limEk(2))-log(limEk(1)))) 1];
set(gca,'PlotBoxAspectRatio',ar)

saveas(gcf,'Fig_factories/Report/figs/raw_2_phase_plot_input_big','epsc')

%Plot zoomed phase plot with data
figure
hold on
limEk=[10^-6,10^-3];
limRa=[10^5,10^10];
Ek=logspace(log10(limEk(1)),log10(limEk(2)));
%plot critical Ra
Rac=(27*pi^4/4)^(1/3)*Ek.^(-4/3);
plot(Ek,Rac,'k')
area(Ek,Rac,'FaceColor',[0.7 0.7 0.7])
%plot lower boundary Favier et al. 2014
Ra_low=3.0*Rac;
split=findIn(Ek,10^-3.5);
plot(Ek(1:split),Ra_low(1:split),'k-')
%plot upper boundary Favier et al. 2014
Ra_upp=0.3115*Ek.^-2;
plot(Ek(1:split),Ra_upp(1:split),'k-')

% x2 = [Ek, fliplr(Ek)];
% inBetween = [Ra_low, fliplr(Ra_upp)];
% f=fill(x2, inBetween, 'k');
% set(f,'facealpha',.06)

for i=1:length(mseries)
    for j=1:length(mseries(i).lsv)
        %scatter(mseries(i).ek(j),mseries(i).ra(j),40,'r','+','LineWidth',2)
        scatter(10^-4,mseries(i).ra(j),40,'r','+','LineWidth',2)
    end
end

%ylabel('Ra')
xlabel('Ek')
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')
xlim([0.6*10^-4,1.6*10^-4])
ylim([10^6,10^8])
grid on
box on
set(gca,'PlotBoxAspectRatio',ar)
%set(gca,'PlotBoxAspectRatio',[1 (log(limRa(2))-log(limRa(1)))/(2*(log(limEk(2))-log(limEk(1)))) 1])

saveas(gcf,'Fig_factories/Report/figs/raw_2_phase_plot_input_small','epsc')


gPlotting=gPlottingPhasePlotInput;
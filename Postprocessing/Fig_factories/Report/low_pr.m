gPlottingLowPr=gPlotting; gPlotting=false;
gSimname='series1B/case02'; gTstart=getTstart(gSimname); make_domain;

hor_kin_3d;

figure
drawSlices(d.xms,d.yms,d.zms,d.xms(1),d.yms(1),d.zms(end),hor_kin,[0,4e-3],gMyCM,true);
hold on
box on
line(xlim,[0,0],[d.zms(end),d.zms(end)],'Color','k')
line([0,0],ylim,[d.zms(end),d.zms(end)],'Color','k')
line([0,0],[0,0],[0,d.zms(end)],'Color','k')
set(gca,'xtick',[])
set(gca,'ytick',[])
set(gca,'ztick',[])
p=get(gcf,'Position');
set(gcf,'Position',[p(1),p(2),round(1.35*p(4)),p(4)])
colorbar('off')

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Report/figs/raw_6_low_pr_case02_slices','-dpng','-painters','-r600')

stst3dir=[gDataDir gSimname '/stst3'];
slabname='slabz'; %average
slab_i=645;
vx=h5read([stst3dir '/' slabname 'q1_' sprintf('%08d', slab_i) '.h5'],'/var');
vy=h5read([stst3dir '/' slabname 'q2_' sprintf('%08d', slab_i) '.h5'],'/var');
data=getHorKin(vx,vy,d);

figure
drawCross(d.xms,d.yms,data',[0,4e-3],'pcolor',gMyCM,true)

set(gca,'xtick',[])
set(gca,'ytick',[])

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Report/figs/6_low_pr_case02_zave','-dpng','-opengl','-r600')

figure
gSimname='series1B/case10'; gTstart=getTstart(gSimname); make_domain; ts_order_params;
plot(tsLRatio2Dk1.Time,tsLRatio2Dk1.Data,'LineWidth',2,'DisplayName','$\textrm{Ra}=10\times10^5$')
hold on
gSimname='series1B/case06'; gTstart=getTstart(gSimname); make_domain; ts_order_params;
plot(tsLRatio2Dk1.Time,tsLRatio2Dk1.Data,'LineWidth',2,'DisplayName','$\textrm{Ra}=6\times10^5$')
gSimname='series1B/case03'; gTstart=getTstart(gSimname); make_domain; ts_order_params;
plot(tsLRatio2Dk1.Time,tsLRatio2Dk1.Data,'LineWidth',2,'DisplayName','$\textrm{Ra}=3\times10^5$')
gSimname='series1B/case02'; gTstart=getTstart(gSimname); make_domain; ts_order_params;
plot(tsLRatio2Dk1.Time,tsLRatio2Dk1.Data,'LineWidth',2,'DisplayName','$\textrm{Ra}=2\times10^5$')
legend('Location','northwest')
set(gca,'Children',flip(get(gca,'Children')))
gSimname='series1B/case10'; gTstart=getTstart(gSimname); make_domain; ts_order_params;
tstar=650;
scatter(tsLRatio2Dk1.Time(findIn(tsLRatio2Dk1.Time,tstar)),tsLRatio2Dk1.Data(findIn(tsLRatio2Dk1.Time,tstar)),500,'k','p','filled')
grid on
box on
ylabel('$\mathcal{Q}_\textrm{2D}(K=1)$')
xlabel('Time $t/(U^{-1}H)$')
xlim([0,1350])
ylim([0,35])
p=get(gcf,'Position');
set(gcf,'Position',[p(1),p(2),round(1.2*p(4)),p(4)])

saveas(gcf,'Fig_factories/Report/figs/6_low_pr_timeseries','epsc')

gSimname='series1B/case10'; gTstart=getTstart(gSimname); make_domain;
stst3dir=[gDataDir gSimname '/stst3'];
slabname='slabz'; %average
slab_i=round((tstar-1)/4);
vx=h5read([stst3dir '/' slabname 'q1_' sprintf('%08d', slab_i) '.h5'],'/var');
vy=h5read([stst3dir '/' slabname 'q2_' sprintf('%08d', slab_i) '.h5'],'/var');
data=getHorKin(vx,vy,d);
data=circshift(data,-(findIn(d.xms,1.2)-1),1);

figure
drawCross(d.xms,d.yms,data',[0,0.6],'pcolor',gMyCM,true)

set(gca,'xtick',[])
set(gca,'ytick',[])

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Report/figs/6_low_pr_case10_zave','-dpng','-opengl','-r600')

gPlotting=gPlottingLowPr;

gPlottingJets=gPlotting; gPlotting=false;

%case5
gSimname='series1/case05'; gTstart=getTstart(gSimname); make_domain;
stst3dir=[gDataDir gSimname '/stst3'];
slab_i=2228;

clim=[-0.2, 0.2];

vx=h5read([stst3dir '/slabzq1_' sprintf('%08d', slab_i) '.h5'],'/var');
vy=h5read([stst3dir '/slabzq2_' sprintf('%08d', slab_i) '.h5'],'/var');

figure
drawCross(d.xs(1:end-1),d.yms,vx',clim,'pcolor',gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
xlabel('$x$')
ylabel('$y$')
colorbar('off')

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Report/figs/4_jets_case5_vx','-dpng','-opengl','-r600')

figure
drawCross(d.xms,d.ys(1:end-1),vy',clim,'pcolor',gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
xlabel('$x$')
%ylabel('$y$')

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Report/figs/4_jets_case5_vy','-dpng','-opengl','-r600')

%case7
gSimname='series1/case07'; gTstart=getTstart(gSimname); make_domain;
stst3dir=[gDataDir gSimname '/stst3'];
slab_i=3735;

clim=[-0.35, 0.35];

vx=h5read([stst3dir '/slabzq1_' sprintf('%08d', slab_i) '.h5'],'/var');
vy=h5read([stst3dir '/slabzq2_' sprintf('%08d', slab_i) '.h5'],'/var');

figure
drawCross(d.xs(1:end-1),d.yms,vx',clim,'pcolor',gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
xlabel('$x$')
ylabel('$y$')
colorbar('off')

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Report/figs/4_jets_case7_vx','-dpng','-opengl','-r600')

figure
drawCross(d.xms,d.ys(1:end-1),vy',clim,'pcolor',gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
xlabel('$x$')
%ylabel('$y$')

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Report/figs/4_jets_case7_vy','-dpng','-opengl','-r600')

%anisotropy
gSeries='series1/';

series_analysis;

ra_trans=5.65e6;

figure
l=plot(control_params,anis(1,:),'-s','DisplayName','$\alpha_\textrm{RMS}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
hold on
l=plot(control_params,anis(3,:),'-s','DisplayName','$\alpha_{\textrm{2D},\textrm{RMS}}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
l=plot(control_params,anis(2,:),'-s','DisplayName','$\alpha_{\textrm{2D},\textrm{$||$}}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
legend('Location','northwest')
set(gca,'ColorOrderIndex',1)
errorbar(control_params,anis(1,:),ani_errors(1,:), 'vertical', 'LineStyle', 'none');
errorbar(control_params,anis(3,:),ani_errors(3,:), 'vertical', 'LineStyle', 'none');
errorbar(control_params,anis(2,:),ani_errors(2,:), 'vertical', 'LineStyle', 'none');
xline(ra_trans,'-.r','LineWidth',1);
%area([5e6,ra_trans],[1,1],'FaceColor',[0.8500 0.3250 0.0980],'FaceAlpha',0.2,'EdgeAlpha',0)
ylabel('Anisotropy')
xlabel('$\textrm{Ra}$')
ylim([0,1])
xlim([0.9*min(control_params),max(control_params)+0.1*min(control_params)])
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/raw_4_jets_ani_series','epsc')

%timeseries anisotropy case7
gSimname='series1/case08'; gTstart=getTstart(gSimname); make_domain;

ts_order_params;

mm_time=500;
figure
plot(tsAniMS.Time,movmean(tsAniMS.Data,mm_time),'LineWidth',2,'DisplayName','$\alpha_\textrm{RMS}$')
hold on
plot(tsAniZaveMS.Time,movmean(tsAniZaveMS.Data,mm_time),'LineWidth',2,'DisplayName','$\alpha_{\textrm{2D},\textrm{RMS}}$')
plot(tsAniZaveXY.Time,movmean(tsAniZaveXY.Data,mm_time),'LineWidth',2,'DisplayName','$\alpha_{\textrm{2D},\textrm{$||$}}$')
legend('Location','northeast')
%area([100,950],[1,1],'FaceColor',[0.8500 0.3250 0.0980],'FaceAlpha',0.2,'EdgeAlpha',0)
ylabel('Anisotropy')
xlabel('Time $t / (U^{-1}H)$')
ylim([0,1])
xlim([0,tsAniMS.Time(end)])
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/raw_4_jets_ani_case8','epsc')

%timeseries LOPs kinetic energy case7

figure
plot(tsLRatioHor.Time,tsLRatioHor.Data,'LineWidth',2,'DisplayName','$\mathcal{Q}_\textrm{hor}$')
hold on
plot(tsLRatio2D.Time,tsLRatio2D.Data,'LineWidth',2,'DisplayName','$\mathcal{Q}_\textrm{2D}$')
plot(tsLRatio2Dk1.Time,tsLRatio2Dk1.Data,'LineWidth',2,'DisplayName','$\mathcal{Q}_\textrm{2D}(K=1)$')
legend('Location','northwest')
%area([100,950],[14,14],'FaceColor',[0.8500 0.3250 0.0980],'FaceAlpha',0.2,'EdgeAlpha',0)
ylabel('Characteristic quantity')
xlabel('Time $t / (U^{-1}H)$')
ylim([0,14])
xlim([0,tsLRatioHor.Time(end)])
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/raw_4_jets_ani_case8_lop','epsc')


gPlotting=gPlottingJets;
gPlottingTransportDetails=gPlotting; gPlotting=false;

gSimname='case1_sandbox_transport_details'; gTstart=getTstart(gSimname); make_domain;

bt_2dsnap_fourier_vs_real;

lims=[-2.5e-5,2.5e-5];
gMyCM_trans=transformCM(gMyCM,10/256,0.07,20);

%plot transfer maps
figure
imagesc(transferQK(1:50,1:50)',lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
colorbar
xlabel('$Q$')
%ylabel('$K$')
%colorbar('off')
set(gca,'YTickLabel',[]);
axis image
colorbar('TickLabelInterpreter','latex')

saveas(gcf,'Fig_factories/Report/figs/B_trans_bt_fourier','epsc')

figure
imagesc(-trans(2:51,2:51),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
colorbar
xlabel('$Q$')
%ylabel('$K$')
%colorbar('off')
set(gca,'YTickLabel',[]);
axis image
colorbar('TickLabelInterpreter','latex')

saveas(gcf,'Fig_factories/Report/figs/B_trans_bt_real','epsc')

bc_3dsnap_fourier_vs_real;

figure
imagesc(-trans(2:51,2:51),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
%colorbar
xlabel('$Q$')
ylabel('$K$')
colorbar('off')
%set(gca,'YTickLabel',[]);
axis image
%colorbar('TickLabelInterpreter','latex')

saveas(gcf,'Fig_factories/Report/figs/B_trans_bc_real','epsc')

figure
imagesc(transferQK_bcbcbt_total(1:50,1:50)',lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
%colorbar
xlabel('$Q$')
ylabel('$K$')
colorbar('off')
%set(gca,'YTickLabel',[]);
axis image
%colorbar('TickLabelInterpreter','latex')

saveas(gcf,'Fig_factories/Report/figs/B_trans_bc_fourier','epsc')


%show forcing in real space
bt_real_from_2d;
figure
drawCross(d.xms,d.yms,trans_K1_xy',[-0.02,0.02],'pcolor',gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
xlabel('$x$')
ylabel('$y$')

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Report/figs/B_trans_bt_k1_real','-dpng','-opengl','-r600')

figure
drawCross(d.xms,d.yms,getHorKin(q1,q2,d)',[0,0.08],'pcolor',gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
xlabel('$x$')
%ylabel('$y$')

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Report/figs/B_trans_k1_real_hor_kin','-dpng','-opengl','-r600')

bc_real_from_3d;

figure
lims=[-1,1]*max(abs(forcing_K_xy(:,:,2)),[],'all');
drawCross(d.xms,d.yms,forcing_K_xy(:,:,2)',[-0.04,0.04],'pcolor',gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
xlabel('$x$')
ylabel('$y$')

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Report/figs/B_trans_bc_k1_real','-dpng','-opengl','-r600')

kmask_correction;

figure
tiledlayout(2,1, 'TileSpacing', 'compact'); 
nexttile

plot(0:maxK_mask,nmodes,'LineWidth',2,'DisplayName','$\#\textrm{modes}(K)$')
hold on
plot(0:maxK_mask,nmodes_exact,'--','LineWidth',2,'DisplayName','$2\pi (K+1/2)$')
grid on
xlim([1,50])
legend('Location','northwest')
ylabel('Number of modes')
%xlabel('$K$')
set(gca,'XTickLabel',[]);

nexttile

plot(0:maxK_mask,correction_K,'-o')
hold on
plot(0:maxK_mask,(0:maxK_mask)*0+1,'--','LineWidth',2)
grid on
xlim([1,50])
ylim([0.8,1.2])
xlabel('$K$')
ylabel('Correction factor')

saveas(gcf,'Fig_factories/Report/figs/B_trans_correction','epsc')

gPlotting=gPlottingTransportDetails;
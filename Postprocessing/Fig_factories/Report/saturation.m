gPlottingSaturation=gPlotting; gPlotting=false;
gSimname='case20_full_trans'; gTstart=getTstart(gSimname); make_domain;

% kinetic transport
ts_kin_trans_four;

%plot ratios
mm_time=100;
figure
plot(tsDiss2DNoW.Time,movmean(-tsDiss2DNoW.Data,mm_time),'LineWidth',2,'DisplayName','$-\mathcal{D}_\textrm{2D}$')
hold on
plot(tsBC.Time,movmean(tsBC.Data,mm_time),'LineWidth',2,'DisplayName','$\mathcal{F}_\textrm{2D}$')
plot(tsBCK1.Time,movmean(tsBCK1.Data,mm_time),'LineWidth',2,'DisplayName','$\mathcal{T}_\textrm{bc}$')
plot(tsBTK1.Time,movmean(tsBTK1.Data,mm_time),'LineWidth',2,'DisplayName','$\mathcal{T}_\textrm{bt}$')
legend('Location','northwest')
set(gca,'ColorOrderIndex',1)
p=plot(tsDiss2DNoW.Time,-tsDiss2DNoW.Data,'LineWidth',2,'DisplayName',tsDiss2DNoW.Name);
p.Color = [p.Color 0.15];
p=plot(tsBC.Time,tsBC.Data,'LineWidth',2,'DisplayName',tsBC.Name);
p.Color = [p.Color 0.15];
p=plot(tsBCK1.Time,tsBCK1.Data,'LineWidth',2,'DisplayName',tsBCK1.Name);
p.Color = [p.Color 0.15];
p=plot(tsBTK1.Time,tsBTK1.Data,'LineWidth',2,'DisplayName',tsBTK1.Name);
p.Color = [p.Color 0.15];
kids=get(gca,'Children');
set(gca,'Children',kids([7,8,5,6,4,1,2,3])) %make sure transparent values are on background
xlabel('Time $t/(U^{-1}H)$')
xlim([0,2500])
ylim([-1e-4,6e-4])
ylabel('Energetic transport')
yline(0,'k');
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/A_saturation_trans','epsc')

%compare series2/upper with scaling 1/Ro^2
series_rossby;

%plot individual parameters
figure
l=plot(control_params,params(2,:),'-s','DisplayName','$E_\mathrm{2D}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
hold on
l=plot(control_params,params(4,:),'-s','DisplayName','$\hat{E}_\mathrm{2D}(K=1)$');
l.MarkerFaceColor = l.Color; %to fill the symbols
l=plot(control_params,params(3,:),'-s','DisplayName','$E^H_\mathrm{3D}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
l=plot(control_params,params(1,:),'-s','DisplayName','$E^V_\mathrm{3D}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
h_leg=legend('Location','northeast');
HeightScaleFactor = 1.1;
NewHeight = h_leg.Position(4) * HeightScaleFactor;
h_leg.Position(2) = h_leg.Position(2) - (NewHeight - h_leg.Position(4));
h_leg.Position(4) = NewHeight;
set(gca,'ColorOrderIndex',1)
errorbar(control_params,params(2,:),param_errors(2,:), 'vertical', 'LineStyle', 'none');
errorbar(control_params,params(4,:),param_errors(4,:), 'vertical', 'LineStyle', 'none');
errorbar(control_params,params(3,:),param_errors(3,:), 'vertical', 'LineStyle', 'none');
errorbar(control_params,params(1,:),param_errors(1,:), 'vertical', 'LineStyle', 'none');
ylabel('Kinetic energy (in $U^2$)')
xlabel('$\textrm{Ro}$')
xlim([0.9*min(control_params),max(control_params)+0.1*min(control_params)])
grid on
box on
%plot rotating condendsate scaling of Seshasayanan & Alexakis (2018)
% U^2 ~ Ome^2 L^2
% in convective units: U^2 ~ 1/Ro^2*Gamma^2
ro_plot=linspace(min(control_params),max(control_params),100);
constant=0.008;
plot(ro_plot,constant./ro_plot.^2,'k--','LineWidth',1.8)

saveas(gcf,'Fig_factories/Report/figs/A_saturation_rossby_scaling','epsc')

gPlotting=gPlottingSaturation;
gPlottingPerf=gPlotting; gPlotting=false;

%get performance data
performance;

%Plot performance
figure
l=plot(cores,walltime_per_iter_pr1,'-d');%,'DisplayName','$256\times256\times144$, $Pr=1$');
l.MarkerFaceColor = l.Color; %to fill the symbols
ylabel('Walltime per iteration (s)')
xlabel('Number of cores')
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')
xlim([0.7,100])
ylim([0.2,20])
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/2_performance','epsc')


gPlotting=gPlottingPerf;
gPlottingSeries2=gPlotting; gPlotting=false;
gHystSeries='series2/';

ra_trans=3.115e7;
ra_trans_hyst=2.15e7;

hyst_series;

%cut off Ra=5e7
control_params_lower=control_params_lower(1:end-1);
order_params_lower=order_params_lower(:,1:end-1);
order_param_errors_lower=order_param_errors_lower(:,1:end-1);
large_order_params_lower=large_order_params_lower(:,1:end-1);
large_order_param_errors_lower=large_order_param_errors_lower(:,1:end-1);
params_lower=params_lower(:,1:end-1);
param_errors_lower=param_errors_lower(:,1:end-1);
anis_lower=anis_lower(:,1:end-1);
ani_errors_lower=ani_errors_lower(:,1:end-1);
control_params_upper=control_params_upper(1:end-1);
order_params_upper=order_params_upper(:,1:end-1);
order_param_errors_upper=order_param_errors_upper(:,1:end-1);
large_order_params_upper=large_order_params_upper(:,1:end-1);
large_order_param_errors_upper=large_order_param_errors_upper(:,1:end-1);
params_upper=params_upper(:,1:end-1);
param_errors_upper=param_errors_upper(:,1:end-1);
anis_upper=anis_upper(:,1:end-1);
ani_errors_upper=ani_errors_upper(:,1:end-1);

control_lims=[0.9*min(min(control_params_lower),min(control_params_upper)),max(max(control_params_lower),max(control_params_upper))+0.1*min(min(control_params_lower),min(control_params_upper))];

% plot kinetic energy LOPs;
figure
l=plot(control_params_upper,large_order_params_upper(1,:),'-s','DisplayName','$\mathcal{Q}_\textrm{hor}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
hold on
l=plot(control_params_upper,large_order_params_upper(2,:),'-s','DisplayName','$\mathcal{Q}_\textrm{2D}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
l=plot(control_params_upper,large_order_params_upper(3,:),'-s','DisplayName','$\mathcal{Q}_\textrm{2D}(K=1)$');
l.MarkerFaceColor = l.Color; %to fill the symbols
legend('Location','northeast')
set(gca,'ColorOrderIndex',1)
plot(control_params_lower,large_order_params_lower(1,:),'-d');
plot(control_params_lower,large_order_params_lower(2,:),'-d');
plot(control_params_lower,large_order_params_lower(3,:),'-d');
set(gca,'ColorOrderIndex',1)
errorbar(control_params_upper,large_order_params_upper(1,:),large_order_param_errors_upper(1,:), 'vertical', 'LineStyle', 'none');
errorbar(control_params_upper,large_order_params_upper(2,:),large_order_param_errors_upper(2,:), 'vertical', 'LineStyle', 'none');
errorbar(control_params_upper,large_order_params_upper(3,:),large_order_param_errors_upper(3,:), 'vertical', 'LineStyle', 'none');
set(gca,'ColorOrderIndex',1)
errorbar(control_params_lower,large_order_params_lower(1,:),large_order_param_errors_lower(1,:), 'vertical', 'LineStyle', 'none');
errorbar(control_params_lower,large_order_params_lower(2,:),large_order_param_errors_lower(2,:), 'vertical', 'LineStyle', 'none');
errorbar(control_params_lower,large_order_params_lower(3,:),large_order_param_errors_lower(3,:), 'vertical', 'LineStyle', 'none');
xline(ra_trans,'-.r','LineWidth',1);
xline(ra_trans_hyst,'-.m','LineWidth',1);
ylabel('Kinetic energy ratio')
xlabel('$\textrm{Ra}$')
yline(0,'k');
xlim(control_lims)
ylim([-1,12])
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/5_series2_kin','epsc')

% plot vorticity LOPs;
figure
l=plot(control_params_upper,large_order_params_upper(4,:),'-s','DisplayName','$\mathcal{Q}_{\textrm{$\omega$},\textrm{2D}}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
hold on
l=plot(control_params_upper,large_order_params_upper(5,:),'-s','DisplayName','$\mathcal{Q}_{\textrm{$\omega$},\textrm{2D}}(K=1)$');
l.MarkerFaceColor = l.Color; %to fill the symbols
legend('Location','northeast')
set(gca,'ColorOrderIndex',1)
plot(control_params_lower,large_order_params_lower(4,:),'-d');
plot(control_params_lower,large_order_params_lower(5,:),'-d');
set(gca,'ColorOrderIndex',1)
errorbar(control_params_upper,large_order_params_upper(4,:),large_order_param_errors_upper(4,:), 'vertical', 'LineStyle', 'none');
errorbar(control_params_upper,large_order_params_upper(5,:),large_order_param_errors_upper(5,:), 'vertical', 'LineStyle', 'none');
set(gca,'ColorOrderIndex',1)
errorbar(control_params_lower,large_order_params_lower(4,:),large_order_param_errors_lower(4,:), 'vertical', 'LineStyle', 'none');
errorbar(control_params_lower,large_order_params_lower(5,:),large_order_param_errors_lower(5,:), 'vertical', 'LineStyle', 'none');
xline(ra_trans,'-.r','LineWidth',1);
xline(ra_trans_hyst,'-.m','LineWidth',1);
ylabel('Vorticity ratio')
xlabel('$\textrm{Ra}$')
yline(0,'k');
xlim(control_lims)
ylim([-0.02,0.42])
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/5_series2_vort','epsc')

%plot kinetic transport 2D
figure
l=plot(control_params_upper,params_upper(10,:),'-s','DisplayName','$\mathcal{F}_\textrm{2D}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
hold on
l=plot(control_params_upper,params_upper(11,:),'-s','DisplayName','$\mathcal{D}_\textrm{2D}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
legend('Location','northeast')
set(gca,'ColorOrderIndex',1)
plot(control_params_lower,params_lower(10,:),'-d');
plot(control_params_lower,params_lower(11,:),'-d');
set(gca,'ColorOrderIndex',1)
errorbar(control_params_upper,params_upper(10,:),param_errors_upper(10,:), 'vertical', 'LineStyle', 'none');
errorbar(control_params_upper,params_upper(11,:),param_errors_upper(11,:), 'vertical', 'LineStyle', 'none');
set(gca,'ColorOrderIndex',1)
errorbar(control_params_lower,params_lower(10,:),param_errors_lower(10,:), 'vertical', 'LineStyle', 'none');
errorbar(control_params_lower,params_lower(11,:),param_errors_lower(11,:), 'vertical', 'LineStyle', 'none');
xline(ra_trans,'-.r','LineWidth',1);
xline(ra_trans_hyst,'-.m','LineWidth',1);
ylabel('Energetic transport (in $U^3H^{-1}$)')
xlabel('$\textrm{Ra}$')
xlim(control_lims)
yline(0,'k');
%ylim([-4e-4,4e-4])
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/5_series2_trans_2D','epsc')

%plot kinetic transport 2D K=1
figure
l=plot(control_params_upper,params_upper(9,:),'-s','DisplayName','$\mathcal{T}_\textrm{bc}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
hold on
l=plot(control_params_upper,params_upper(8,:),'-s','DisplayName','$\mathcal{T}_\textrm{bt}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
legend('Location','northeast')
set(gca,'ColorOrderIndex',1)
plot(control_params_lower,params_lower(9,:),'-d');
plot(control_params_lower,params_lower(8,:),'-d');
set(gca,'ColorOrderIndex',1)
errorbar(control_params_upper,params_upper(9,:),param_errors_upper(9,:), 'vertical', 'LineStyle', 'none');
errorbar(control_params_upper,params_upper(8,:),param_errors_upper(8,:), 'vertical', 'LineStyle', 'none');
set(gca,'ColorOrderIndex',1)
errorbar(control_params_lower,params_lower(9,:),param_errors_lower(9,:), 'vertical', 'LineStyle', 'none');
errorbar(control_params_lower,params_lower(8,:),param_errors_lower(8,:), 'vertical', 'LineStyle', 'none');
xline(ra_trans,'-.r','LineWidth',1);
xline(ra_trans_hyst,'-.m','LineWidth',1);
ylabel('Energetic transport (in $U^3H^{-1}$)')
xlabel('$\textrm{Ra}$')
xlim(control_lims)
yline(0,'k');
ylim([-2e-5,20e-5])
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/5_series2_trans_k1','epsc')

%plot kinetic transport maps
gSimname='series2/upper/case20'; gTstart=getTstart(gSimname); make_domain;
transfer_stafield;

lims=[-3.5e-5,3.5e-5];
gMyCM_trans=transformCM(gMyCM,10/256,0.07,20);

figure
imagesc(transQK_BT(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
%colorbar
xlabel('$Q$')
ylabel('$K$')
%set(gca,'YTickLabel',[]);
axis image
%colorbar('TickLabelInterpreter','latex')

saveas(gcf,'Fig_factories/Report/figs/5_series2_trans_bt_20','epsc')

figure
imagesc(transQK_BC(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
%colorbar
xlabel('$Q$')
ylabel('$K$')
%set(gca,'YTickLabel',[]);
axis image
%colorbar('TickLabelInterpreter','latex')

saveas(gcf,'Fig_factories/Report/figs/5_series2_trans_bc_20','epsc')

gSimname='series2/upper/case31'; gTstart=getTstart(gSimname); make_domain;
transfer_stafield;

figure
imagesc(transQK_BT(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
%colorbar
xlabel('$Q$')
%ylabel('$K$')
set(gca,'YTickLabel',[]);
axis image
%colorbar('TickLabelInterpreter','latex')

saveas(gcf,'Fig_factories/Report/figs/5_series2_trans_bt_31','epsc')

figure
imagesc(transQK_BC(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
%colorbar
xlabel('$Q$')
%ylabel('$K$')
set(gca,'YTickLabel',[]);
axis image
%colorbar('TickLabelInterpreter','latex')

saveas(gcf,'Fig_factories/Report/figs/5_series2_trans_bc_31','epsc')

gSimname='series2/upper/case313'; gTstart=getTstart(gSimname); make_domain;
transfer_stafield;

figure
imagesc(transQK_BT(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
colorbar
xlabel('$Q$')
%ylabel('$K$')
set(gca,'YTickLabel',[]);
axis image
colorbar('TickLabelInterpreter','latex')

saveas(gcf,'Fig_factories/Report/figs/5_series2_trans_bt_313','epsc')

figure
imagesc(transQK_BC(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
colorbar
xlabel('$Q$')
%ylabel('$K$')
set(gca,'YTickLabel',[]);
axis image
colorbar('TickLabelInterpreter','latex')

saveas(gcf,'Fig_factories/Report/figs/5_series2_trans_bc_313','epsc')

gSeries='series2/upper/';
transfer_stafield_series;

cm=pink(350);
cm=cm(1:256,:);
cm=flip(cm,1);

ra_low=ras(1);
ra_high=ras(end);

figure
for i=1:ncases
    plot(bt_k1{i}'.*correction_K(2:maxQ+1),'LineWidth',2,'Color',cm(1+round(255*(ras(i)-ra_low)/(ra_high-ra_low)),:),...
     'DisplayName',['$\textrm{Ra}=' num2str(ras(i)/10^(floor(log10(ras(i))))) '\times 10^' num2str(floor(log10(ras(i)))) '$'])
    hold on
end
xlabel('$Q$')
%ylabel('$-\bar{\mathbf{u}}_{K=1} \cdot (\bar{\mathbf{u}} \cdot \nabla \bar{\mathbf{u}}_Q)/(U^3H^{-1})$')
ylabel('$T_\textrm{bt}(1,Q)/(U^3H^{-1})$')
xlim([1,maxQ])
grid on
box on
colormap(cm)
colorbar
caxis([ra_low, ra_high])
colorbar('TickLabelInterpreter','latex')
if i_transition_low > 0
     plot(bt_k1{i_transition_low}'.*correction_K(2:maxQ+1),'LineWidth',2,'Color','#6ab900')
end
if i_transition_high > 0
     plot(bt_k1{i_transition_high}'.*correction_K(2:maxQ+1),'LineWidth',2,'Color','r')
end
ylim([-3e-5,3e-5])

saveas(gcf,'Fig_factories/Report/figs/5_series2_trans_bt_k1','epsc')

figure
for i=1:ncases
    plot(bc_k1{i}'.*correction_K(2:maxQ+1),'LineWidth',2,'Color',cm(1+round(255*(ras(i)-ra_low)/(ra_high-ra_low)),:),...
     'DisplayName',['$\textrm{Ra}=' num2str(ras(i)/10^(floor(log10(ras(i))))) '\times 10^' num2str(floor(log10(ras(i)))) '$'])
    hold on
end
xlabel('$Q$')
%ylabel('$-\bar{\mathbf{u}}_{K=1} \cdot (\mathbf{u}^\prime \cdot \nabla \mathbf{u}^\prime_Q)/(U^3H^{-1})$')
ylabel('$T_\textrm{bc}(1,Q)/(U^3H^{-1})$')
xlim([1,maxQ])
grid on
box on
colormap(cm)
colorbar
caxis([ra_low, ra_high])
colorbar('TickLabelInterpreter','latex')
if i_transition_low > 0
     plot(bc_k1{i_transition_low}'.*correction_K(2:maxQ+1),'LineWidth',2,'Color','#6ab900')
end
if i_transition_high > 0
     plot(bc_k1{i_transition_high}'.*correction_K(2:maxQ+1),'LineWidth',2,'Color','r')
end
ylim([-3e-5,3e-5])

saveas(gcf,'Fig_factories/Report/figs/5_series2_trans_bc_k1','epsc')

gPlotting=gPlottingSeries2;
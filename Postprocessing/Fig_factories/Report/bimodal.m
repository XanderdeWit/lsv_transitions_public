gPlottingBimodal=gPlotting; gPlotting=false;
gSimname='series1_ar/small/case062'; gTstart=getTstart(gSimname); make_domain;

%timepoints
t_low=10616;
t_high=6480;

%bimodal analysis
ts_bimodal;

figure
mmtime=500; %X convective units
plot(lop_tss(1).Time,movmean(lop_tss(1).Data,mmtime),'LineWidth',2,'DisplayName','$\mathcal{Q}_\textrm{hor}$')
hold on
plot(lop_tss(2).Time,movmean(lop_tss(2).Data,mmtime),'LineWidth',2,'DisplayName','$\mathcal{Q}_\textrm{2D}$')
plot(lop_tss(3).Time,movmean(lop_tss(3).Data,mmtime),'LineWidth',2,'DisplayName','$\mathcal{Q}_\textrm{2D}(K=1)$')
legend('Location','northwest')
set(gca,'ColorOrderIndex',1)
p=plot(lop_tss(1).Time,lop_tss(1).Data,'LineWidth',2);
p.Color = [p.Color 0.15];
p=plot(lop_tss(2).Time,lop_tss(2).Data,'LineWidth',2);
p.Color = [p.Color 0.15];
p=plot(lop_tss(3).Time,lop_tss(3).Data,'LineWidth',2);
p.Color = [p.Color 0.15];
set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
%horizontal lines
plot(lop_tss(3).Time,0*lop_tss(3).Time+lop_stats_low(3,1),'k-','LineWidth',2);
plot(lop_tss(3).Time,0*lop_tss(3).Time+lop_stats_high(3,1),'k-','LineWidth',2);
plot(lop_tss(3).Time,0*lop_tss(3).Time+lop_stats_low(3,1)+lop_stats_low(3,2),'k--','LineWidth',2);
plot(lop_tss(3).Time,0*lop_tss(3).Time+lop_stats_high(3,1)+lop_stats_high(3,2),'k--','LineWidth',2);
plot(lop_tss(3).Time,0*lop_tss(3).Time+lop_stats_low(3,1)-lop_stats_low(3,2),'k--','LineWidth',2);
plot(lop_tss(3).Time,0*lop_tss(3).Time+lop_stats_high(3,1)-lop_stats_high(3,2),'k--','LineWidth',2);
ylabel('Characteristic quantity')
xlabel('Time $t/(U^{-1}H)$')
yline(0,'k');
grid on
box on
ylim([-0.5,5])
ymax=ylim;
%area([tstart_low,tend_low],[ymax(2),ymax(2)],'FaceColor','k','FaceAlpha',0.2,'EdgeAlpha',0)
%area([tstart_high,tend_high],[ymax(2),ymax(2)],'FaceColor','k','FaceAlpha',0.2,'EdgeAlpha',0)
xlim([0,lop_tss(3).Time(end)])
mmlop3=movmean(lop_tss(3).Data,mmtime);
scatter(t_low,mmlop3(findIn(lop_tss(3).Time,t_low)),500,'k','p','filled')
scatter(t_high,mmlop3(findIn(lop_tss(3).Time,t_high)),500,'k','p','filled')
p=get(gcf,'Position');
set(gcf,'Position',[p(1),p(2),2.6*p(4),p(4)])

saveas(gcf,'Fig_factories/Report/figs/raw_6_bimodal_timeseries','epsc')

%show snapshots
stst3dir=[gDataDir gSimname '/stst3'];
slabname='slabz';

slab_i_low=round(t_low/4);
slab_i_high=round(t_high/4);

%low snap
vx=h5read([stst3dir '/' slabname 'q1_' sprintf('%08d', slab_i_low) '.h5'],'/var');
vy=h5read([stst3dir '/' slabname 'q2_' sprintf('%08d', slab_i_low) '.h5'],'/var');
time=h5read([stst3dir '/' slabname 'q1_' sprintf('%08d', slab_i_low) '.h5'],'/time');

figure
drawCross(d.xms,d.yms,getHorKin(vx,vy,d)',[0,0.02],'pcolor',gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
xlabel('$x$')
%ylabel('$y$')
%title(['$t= ' num2str(time) '$'])
cbh=colorbar('TickLabelInterpreter','latex');
cbh.Ticks=[0,0.005,0.01,0.015,0.02];

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Report/figs/6_bimodal_snap_low','-dpng','-opengl','-r600')

% figure
% drawCross(d.xs,d.ys,getAxialVorticity(vx,vy,d)',[-2,2],'pcolor',gMyCM,true);
% set(gca,'xtick',[])
% set(gca,'ytick',[])
% xlabel('$x$')
% ylabel('$y$')
% title(['$t= ' num2str(time) '$'])

%high snap
vx=h5read([stst3dir '/' slabname 'q1_' sprintf('%08d', slab_i_high) '.h5'],'/var');
vy=h5read([stst3dir '/' slabname 'q2_' sprintf('%08d', slab_i_high) '.h5'],'/var');
time=h5read([stst3dir '/' slabname 'q1_' sprintf('%08d', slab_i_high) '.h5'],'/time');

vx=circshift(vx,findIn(d.xs,0.7)-1,1);
vy=circshift(vy,findIn(d.xs,0.7)-1,1);

figure
drawCross(d.xms,d.yms,getHorKin(vx,vy,d)',[0,0.02],'pcolor',gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
xlabel('$x$')
ylabel('$y$')
colorbar('off')
%title(['$t= ' num2str(time) '$'])

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Report/figs/6_bimodal_snap_high','-dpng','-opengl','-r600')

% figure
% drawCross(d.xs,d.ys,getAxialVorticity(vx,vy,d)',[-2,2],'pcolor',gMyCM,true);
% set(gca,'xtick',[])
% set(gca,'ytick',[])
% xlabel('$x$')
% ylabel('$y$')
% title(['$t= ' num2str(time) '$'])


gPlotting=gPlottingBimodal;
gPlottingQuantities=gPlotting; gPlotting=false;
%gSimname='series1/case10'; gTstart=getTstart(gSimname); make_domain;

%kinetic energy
gSimname='series1/case10'; gTstart=getTstart(gSimname); make_domain;
hor_kin_3d;

ratioHorHor=circshift(ratioHorHor,(findIn(d.xms,0.8)-1),1);
ratioHorHor=circshift(ratioHorHor,(findIn(d.yms,0.4)-1),2);

figure
drawCross(d.xms,d.yms,ratioHorHor',[0,25],'pcolor',gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
xlabel('$x$')
ylabel('$y$')
colorbar('off')

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Report/figs/3_quantities_kin_hor_space','-dpng','-opengl','-r600')

ratio2D=circshift(ratio2D,(findIn(d.xms,0.8)-1),1);
ratio2D=circshift(ratio2D,(findIn(d.yms,0.4)-1),2);

figure
drawCross(d.xms,d.yms,ratio2D',[0,25],'pcolor',gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
xlabel('$x$')
%ylabel('$y$')

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Report/figs/3_quantities_kin_2d_space','-dpng','-opengl','-r600')

gSimname='series1/case10'; gTstart=getTstart(gSimname); make_domain;
ts_kin_ratios;

figure
plot(tsLRatioHor.Time,tsLRatioHor.Data,'LineWidth',2,'DisplayName','$\mathcal{Q}_\textrm{hor}$')
hold on
plot(tsLRatio2D.Time,tsLRatio2D.Data,'LineWidth',2,'DisplayName','$\mathcal{Q}_\textrm{2D}$')
plot(tsLRatio2Dk1.Time,tsLRatio2Dk1.Data,'LineWidth',2,'DisplayName','$\mathcal{Q}_\textrm{2D}(K=1)$')
legend('Location','southeast')
ylabel('Characteristic quantity')
xlabel('Time $t/(U^{-1}H)$')
ylim([0,14])
xlim([0,tsLRatioHor.Time(end)])
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/3_quantities_kin','epsc')

%vorticity
gSimname='series1/case10'; gTstart=getTstart(gSimname); make_domain;
vorticity_3d;

vort2DLRatio=circshift(vort2DLRatio,(findIn(d.xms,0.8)-1),1);
vort2DLRatio=circshift(vort2DLRatio,(findIn(d.yms,0.4)-1),2);

figure
drawCross(d.xs,d.ys,vort2DLRatio',[0,2.5],'pcolor',gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
xlabel('$x$')
ylabel('$y$')

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Report/figs/3_quantities_vort_space','-dpng','-opengl','-r600')

gSimname='series1/case10'; gTstart=getTstart(gSimname); make_domain;
ts_vort;

figure
plot(tsLRatioVort2D.Time,tsLRatioVort2D.Data,'LineWidth',2,'DisplayName','$\mathcal{Q}_{\textrm{$\omega$},\textrm{2D}}$')
hold on
plot(tsLRatioVort2Dk1.Time,tsLRatioVort2Dk1.Data,'LineWidth',2,'DisplayName','$\mathcal{Q}_{\textrm{$\omega$},\textrm{2D}}(K=1)$')
legend('Location','southeast')
ylabel('Characteristic quantity')
xlabel('Time $t/(U^{-1}H)$')
xlim([0,tsLRatioVort2D.Time(end)])
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/3_quantities_vort','epsc')

% kinetic transport
ts_kin_trans_four;

%plot ratios
mm_time=100;
figure
plot(tsDiss2DNoW.Time,movmean(tsDiss2DNoW.Data,mm_time),'LineWidth',2,'DisplayName','$\mathcal{D}_\textrm{2D}$')
hold on
plot(tsBC.Time,movmean(tsBC.Data,mm_time),'LineWidth',2,'DisplayName','$\mathcal{F}_\textrm{2D}$')
plot(tsBCK1.Time,movmean(tsBCK1.Data,mm_time),'LineWidth',2,'DisplayName','$\mathcal{T}_\textrm{bc}$')
plot(tsBTK1.Time,movmean(tsBTK1.Data,mm_time),'LineWidth',2,'DisplayName','$\mathcal{T}_\textrm{bt}$')
legend('Location','northwest')
set(gca,'ColorOrderIndex',1)
p=plot(tsDiss2DNoW.Time,tsDiss2DNoW.Data,'LineWidth',2,'DisplayName',tsDiss2DNoW.Name);
p.Color = [p.Color 0.15];
p=plot(tsBC.Time,tsBC.Data,'LineWidth',2,'DisplayName',tsBC.Name);
p.Color = [p.Color 0.15];
p=plot(tsBCK1.Time,tsBCK1.Data,'LineWidth',2,'DisplayName',tsBCK1.Name);
p.Color = [p.Color 0.15];
p=plot(tsBTK1.Time,tsBTK1.Data,'LineWidth',2,'DisplayName',tsBTK1.Name);
p.Color = [p.Color 0.15];
set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
xlabel('Time $t/(U^{-1}H)$')
xlim([tsDiss2DNoW.Time(1),tsDiss2DNoW.Time(end)])
ylabel('Energetic transport')
yline(0,'k');
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/3_quantities_trans','epsc')

gPlotting=gPlottingQuantities;
gPlottingLangevin=gPlotting; gPlotting=false;

%calculate free energy and diffusion
free_energy_kin;

%Plot potential
figure
plot(energies,U,'LineWidth',2)
box on
grid on
xlim([0,0.015])
ylabel('$V(\mathcal{Q})/(U^5H^{-1})$')
xlabel('$\mathcal{Q}/U^2$')

saveas(gcf,'Fig_factories/Report/figs/4I_potential','epsc')

%Plot diffusion constant
c=[0 0.4470 0.7410];
figure
scatter(energies,B,10,c*0.4+0.6,'s','filled')
hold on
Bmm=movmean(B,nlevels/12);
plot(energies(20:end-30),Bmm(20:end-30),'LineWidth',2,'Color',c)
box on
grid on
xlim([0,0.015])
ylim([0,3e-9])
ylabel('$B(\mathcal{Q})/(U^5H^{-1})$')
xlabel('$\mathcal{Q}/U^2$')

saveas(gcf,'Fig_factories/Report/figs/4I_diffusion','epsc')

%calculate validation
free_energy_kin_verify;

%plot distribution
[epdf,edges] = histcounts(delta_es,100,'Normalization', 'pdf');
xi=0.5*(edges(2:end)+edges(1:end-1));
figure
plot(xi,epdf,'LineWidth',2);
set(gca, 'YScale', 'log')
grid on
hold on
mu=mean(delta_es);
sigma=std(delta_es);
pd = makedist('Normal','mu',mu,'sigma',sigma);
plot(xi(1:end-3),pdf(pd,xi(1:end-3)),'--','LineWidth',2)
ylim([10^floor(log10(min(epdf(epdf~=0)))-1),10^ceil(log10(max(epdf)))])
xlim([-1.1*max(abs(delta_es)),1.1*max(abs(delta_es))])
xlabel('$(\mathcal{Q}-\mathcal{Q}^\prime)/U^2$')
ylabel('$p(\mathcal{Q},t^\prime+\Delta t | \mathcal{Q}^\prime, t^\prime)/U^{-2}$')

saveas(gcf,'Fig_factories/Report/figs/4I_validation_distribution','epsc')

%Q-Q plot
figure
h=qqplot(delta_es);
h(1).MarkerEdgeColor=c;
h(3).LineWidth=1.5;
h(3).Color=[0.8500 0.3250 0.0980];
grid on
box on
ylabel('Quantiles of $(\mathcal{Q}-\mathcal{Q}^\prime)/U^2$')
xlabel('Gaussian quantiles')
xticks([-5,-2.5,0,2.5,5])
title('')
ylim([-4.5e-4,4.5e-4])

saveas(gcf,'Fig_factories/Report/figs/4I_validation_qq','epsc')

%calculate Monte-Carlo solutions of Langevin equation
langevin_monte_carlo;

%plot trajectories
figure
cm=linspace(0.5,1.3,256)'*[0, 0.4470, 0.7410];
trajs=[21,54,100,67,60,13];
hold on
for itraj=1:length(trajs)
    plot((0:length(qts{trajs(itraj)})-1)*dt,qts{trajs(itraj)},'LineWidth',2,'Color',cm(1+round(255*itraj/length(trajs)),:))
end
yline(threshold,'--k','LineWidth',1.5);
ylabel('$\mathcal{Q}^\mathrm{MC}$')
xlabel('Time $t / (U^{-1}H)$')
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/4I_montecarlo_trajs','epsc')

%plot cumulative distribution
figure
c = [0 0.4470 0.7410];
stairs(m_ecdf_x_lsv,m_ecdf_lsv,'-','Color',c,'LineWidth',2.5)
hold on
stairs(m_ecdf_x_lsv,[m_ecdf_lo, m_ecdf_up],':','Color',0.7*c+0.3,'LineWidth',2)
plot(m_ecdf_x_lsv,1-exp(-(m_ecdf_x_lsv-m_fit_lsv.t0)/(m_fit_lsv.mu-m_fit_lsv.t0)),'--','Color',[0.8500 0.3250 0.0980],'LineWidth',2.8)
xlabel('Monte-Carlo LSV time $t_\mathrm{LSV}^\mathrm{MC} / (U^{-1}H)$')
ylabel('Empirical cumulative distribution')
ylim([0,1])
xlim([0,m_ecdf_x_lsv(end)])
%area([0,m_fit_lsv.t0],[9,9],'FaceColor',[0,0,0],'FaceAlpha',0.2,'EdgeAlpha',0)
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/raw_4I_montecarlo_dist','epsc')

gPlotting=gPlottingLangevin;
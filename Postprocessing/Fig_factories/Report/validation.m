gPlottingValidation=gPlotting; gPlotting=false;
gSimname='series1/case10'; gTstart=getTstart(gSimname); make_domain;

%bulk
kolmogorov;

%Plots of number of kolmogs per cell
figure
hold on
plot(nkolmog_x,d.zms,'LineWidth',2)
plot(nkolmog_z,d.zms,'LineWidth',2)
hold off
xlabel('Kolmogorov scales per cell $\underline{\Delta}/\eta$')
ylabel('$z/H$')
legend({'$\Delta x/\eta$','$\Delta z/\eta$'},'Location','northeast')
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/2_validation_kolmogorov','epsc')

%boundary
boundary_layer;

%Plot T RMS and T mean with BLs
figure
hold on
plot(TeRMS,d.zms,'LineWidth',2,'DisplayName','Temperature RMS')
plot(TeMean,d.zms,'LineWidth',2,'DisplayName','Mean temperature')
hold off
legend
yline(TRMS_TopZ,'--','LineWidth',1.5);
yline(TRMS_BottomZ,'--','LineWidth',1.5);
xlabel('Temperature $T/\Delta T$')
%ylabel('$z/H$')
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/2_validation_boundary','epsc')


gPlotting=gPlottingValidation;
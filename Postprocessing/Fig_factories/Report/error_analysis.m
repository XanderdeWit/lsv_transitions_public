gPlottingErrorAnalysis=gPlotting; gPlotting=false;
gSimname='series1/case10'; gTstart=getTstart(gSimname); make_domain;

ts_kin_ratios;

%re-assign ts as truncated
istart = findIn(tsLRatio2D.Time,gTstart);
tsLRatio2D=timeseries(tsLRatio2D.Data(istart:end),tsLRatio2D.Time(istart:end),'Name',tsLRatio2D.Name);

%set t(0)=0
tsLRatio2D=timeseries(tsLRatio2D.Data,tsLRatio2D.Time-tsLRatio2D.Time(1),'Name',tsLRatio2D.Name);

%calculate mean
meanTsLRatio2D=mean(tsLRatio2D,'Weighting','none');

%calculate auto-correlation
acorLRatio2D=xcorr(tsLRatio2D.Data-meanTsLRatio2D);
acorLRatio2D=acorLRatio2D(length(tsLRatio2D.Time):end)/acorLRatio2D(length(tsLRatio2D.Time));

%find first zero crossing
zci = @(v) find(v(:).*circshift(v(:), [-1 0]) <= 0);
zcisLRatio2D=zci(acorLRatio2D);
if length(zcisLRatio2D) > 0
    decorTimeLRatio2D=tsLRatio2D.Time(zcisLRatio2D(1));
else
    decorTimeLRatio2D=tsLRatio2D.Time(end);
end

%plot
figure
plot(tsLRatio2D.Time,acorLRatio2D,'LineWidth',2)
xline(decorTimeLRatio2D,'--','LineWidth',1.5);
grid on
box on
xlim([0,30])
ylabel('Autocorrelation $\mathcal{A}_{\mathcal{Q}_\mathrm{2D}\mathcal{Q}_\mathrm{2D}}(\Delta t)/\mathcal{A}_{\mathcal{Q}_\mathrm{2D}\mathcal{Q}_\mathrm{2D}}(0)$')
xlabel('Time $\Delta t /(U^{-1}H)$')

saveas(gcf,'Fig_factories/Report/figs/D_auto-corr','epsc')

gPlotting=gPlottingErrorAnalysis;
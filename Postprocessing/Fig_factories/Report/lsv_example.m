gPlottingLSV_example=gPlotting; gPlotting=false;

% slices of horziontal kinetic energy
gSimname='series_interim/case17'; gTstart=getTstart(gSimname); make_domain;
hor_kin_3d;

figure
cutoff=round(0.75*d.nz);
shift=[1.8,0.25];
hor_kin=circshift(hor_kin,findIn(d.xms,shift(1))-1,1);
hor_kin=circshift(hor_kin,findIn(d.yms,shift(2))-1,2);
drawSlices(d.xms,d.yms,d.zs(1:cutoff),d.xms(1),d.yms(1),d.zs(cutoff),hor_kin(:,:,1:cutoff),[0,0.2],gMyCM,true);
hold on
box on
line(xlim,[0,0],[d.zs(cutoff),d.zs(cutoff)],'Color','k')
line([0,0],ylim,[d.zs(cutoff),d.zs(cutoff)],'Color','k')
line([0,0],[0,0],[0,d.zs(cutoff)],'Color','k')
set(gca,'xtick',[])
set(gca,'ytick',[])
set(gca,'ztick',[])
p=get(gcf,'Position');
set(gcf,'Position',[p(1),p(2),round(1.53*p(4)),p(4)])

cb=colorbar('TickLabelInterpreter','latex');
cb.Ticks=0:0.05:0.2;

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Report/figs/raw_3_example_kin_slices','-dpng','-painters','-r600')

% figure
% drawSlices(d.xms,d.yms,d.zs(1:end-1),[],1.5,0.2,hor_kin,[0,0.12],gMyCM,true);
% set(gca,'xtick',[])
% set(gca,'ytick',[])
% set(gca,'ztick',[])
% box on
% p=get(gcf,'Position');
% set(gcf,'Position',[p(1),p(2),round(1.53*p(4)),p(4)])
% 
% f=gcf;
% f.PaperPositionMode='auto';
% print('Fig_factories/Report/figs/3_example_kin_slices','-dpng','-opengl','-r600')

%snapshot vorticity and okubo-weiss
gSimname='series1/case10'; gTstart=getTstart(gSimname); make_domain;
slab_i=2300;

vx=h5read([gDataDir gSimname '/stst3/slabzq1_' sprintf('%08d', slab_i) '.h5'],'/var');
vy=h5read([gDataDir gSimname '/stst3/slabzq2_' sprintf('%08d', slab_i) '.h5'],'/var');

xshift=findIn(d.xs,1.6)-1;
yshift=findIn(d.ys,0.2)-1;
vx=circshift(vx,xshift,1);
vx=circshift(vx,yshift,2);
vy=circshift(vy,xshift,1);
vy=circshift(vy,yshift,2);

ome_z=getAxialVorticity(vx,vy,d);

figure
drawCross(d.xs,d.ys,ome_z',[-5 5],'pcolor',gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
xlabel('$x$')
ylabel('$y$')

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Report/figs/3_example_vort','-dpng','-opengl','-r600')

okubo_weiss=getOkuboWeis(vx,vy,d);

figure
drawCross(d.xms,d.yms,okubo_weiss',[-8 8],'pcolor',gMyCM,true);
set(gca,'xtick',[])
set(gca,'ytick',[])
xlabel('$x$')
%ylabel('$y$')

f=gcf;
f.PaperPositionMode='auto';
print('Fig_factories/Report/figs/3_example_okubo_weiss','-dpng','-opengl','-r600')

%kinetic energy
ts_kins;

t_stop=1500;

figure
it_stop=findIn(tsK.Time,t_stop);
area(tsK.Time(1:it_stop),[tsKv.Data(1:it_stop),tsKH3D.Data(1:it_stop),tsKH2D.Data(1:it_stop)])
ylabel('Kinetic energy $E_\textrm{tot}/U^2$')
xlabel('Time $t/(U^{-1}H)$')
h_leg=legend({'$E_\mathrm{3D}^V$','$E_\mathrm{3D}^H$','$E_\mathrm{2D}$'},'Location','northwest');

h_leg.PlotChildren = h_leg.PlotChildren([3,2,1]);
HeightScaleFactor = 1.2;
NewHeight = h_leg.Position(4) * HeightScaleFactor;
h_leg.Position(2) = h_leg.Position(2) - (NewHeight - h_leg.Position(4));
h_leg.Position(4) = NewHeight;

grid on
box on

saveas(gcf,'Fig_factories/Report/figs/3_example_kin_area','epsc')

%spectrum
spectrum_from_3d;

figure
loglog(k,ETk+Evk,'DisplayName','$\hat{E}_\textrm{tot}$','LineWidth',2)
hold on
loglog(k,E3Dk+Evk,'DisplayName','$\hat{E}_\textrm{3D}$','LineWidth',2.8)
loglog(k,E2Dk,'DisplayName','$\hat{E}_\textrm{2D}$','LineWidth',2)
hold off
legend
cs=get(gca,'Children');
set(gca,'Children',cs([3,1,2]))
xlabel('Horizontal wavenumber $K$')
ylabel('$\hat{E}(K)/U^2$')
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/3_example_spectrum','epsc')

ts_spectrum_KH2D;

it_stop=findIn(time,t_stop);

figure
for ik=1:7
    plot(time(1:it_stop),E2Dk(1:it_stop,ik),'LineWidth',2,'DisplayName',['$K=' num2str(k(ik)) '$'])
    hold on
end
legend('Location','southeast')
set(gca,'Children',flip(get(gca,'Children')))
ylabel('$\hat{E}_\textrm{2D}(K)/U^2$')
xlabel('Time $t/(U^{-1}H)$')
set(gca,'YScale','log')
ylim([10^-5,10^-1])
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/3_example_spectrum_evolution','epsc')

%buoy spectrum and transport

buoy_spectrum_3d;

figure
loglog(k(2:end),VzTk(2:end),'LineWidth',2)
xlabel('Horizontal wavenumber $K$')
ylabel('Buoyancy $ w T (K)/(U\Delta T)$')
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/3_example_buoy_spectrum','epsc')

transfer_stafield;

lims=[-2.5e-5,2.5e-5];
gMyCM_trans=transformCM(gMyCM,10/256,0.07,20);

figure
imagesc(transQK_BT(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
colorbar
xlabel('$Q$')
%ylabel('$K$')
set(gca,'YTickLabel',[]);
axis image
colorbar('TickLabelInterpreter','latex')

saveas(gcf,'Fig_factories/Report/figs/3_example_trans_bt','epsc')

figure
imagesc(transQK_BC(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
%colorbar
xlabel('$Q$')
ylabel('$K$')
axis image
%colorbar('TickLabelInterpreter','latex')

saveas(gcf,'Fig_factories/Report/figs/3_example_trans_bc','epsc')


gPlotting=gPlottingLSV_example;
gPlottingAspectRatio=gPlotting; gPlotting=false;

%Kinetic energy in LSV cases
ar_kins;

figure
l=plot(ars,kinH2D,'-s','DisplayName','$E_\mathrm{2D}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
hold on
l=plot(ars,kinH2Dk1,'-s','DisplayName','$\hat{E}_\mathrm{2D}(K=1)$');
l.MarkerFaceColor = l.Color; %to fill the symbols
l=plot(ars,kinH3D,'-s','DisplayName','$E^H_\mathrm{3D}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
l=plot(ars,kinV,'-s','DisplayName','$E^V_\mathrm{3D}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
h_leg=legend('Location','northwest');
HeightScaleFactor = 1.1;
NewHeight = h_leg.Position(4) * HeightScaleFactor;
h_leg.Position(2) = h_leg.Position(2) - (NewHeight - h_leg.Position(4));
h_leg.Position(4) = NewHeight;
set(gca,'ColorOrderIndex',1)
errorbar(ars,kinH2D,kinH2D_err, 'vertical', 'LineStyle', 'none');
errorbar(ars,kinH2Dk1,kinH2Dk1_err, 'vertical', 'LineStyle', 'none');
errorbar(ars,kinH3D,kinH3D_err, 'vertical', 'LineStyle', 'none');
errorbar(ars,kinV,kinV_err, 'vertical', 'LineStyle', 'none');
ylabel('Kinetic energy (in $U^2$)')
xlabel('Aspect ratio $\Gamma$')
xlim([1.5,3.25])
ylim([0.0015,0.05])
set(gca, 'XScale', 'log')
set(gca, 'YScale', 'log')
grid on
box on
yticks([2e-3, 1e-2, 4e-2])
plot([1.9,2.6],0.009*[1,(2.6/1.9)^2],'k--','LineWidth',2)
annotation('textbox',[.52 .31 .3 .3],'String','$\sim\Gamma^2$','FitBoxToText','on','Interpreter','latex','LineStyle','none','FontSize',20);

saveas(gcf,'Fig_factories/Report/figs/6_aspect_ratio_lsv_kins','epsc')

i_map=[3,1,2,5,4];
[~,i_map_inv]=sort(i_map);

figure
l=gobjects(n,1);
for i=1:n
    iplot=i_map(i);
    l(i)=loglog(spec_k{iplot}./ars(iplot),spec_ETk{iplot}+spec_Evk{iplot},'LineWidth',1.5,'DisplayName',['$\Gamma=' num2str(ars(iplot),2) '$']);
    hold on
end
xlabel('Horizontal wavenumber $K/\Gamma$')
ylabel('Energy spectrum $\hat{E}_\textrm{tot}(K)/U^2$')
legend(l(flip(i_map_inv)),'Location','northeast');
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/6_aspect_ratio_lsv_spectra','epsc')

%Transitions
ar_together_bimodal;

control_lims=[0.9*(5e6),1.1*(8e6)];

figure
iplot=3;
l=gobjects(length(all_series),1);
for i=1:length(all_series)
    l(i)=plot(all_control_params{i},all_large_order_params{i}(iplot,:),'-s','DisplayName',['$\Gamma=' num2str(ars(i_map(i)),2) '$']);
    l(i).MarkerFaceColor = l(i).Color; %to fill the symbols
    hold on
end
legend(l(flip(i_map_inv)),'Location','northwest')
set(gca,'ColorOrderIndex',1)
for i=1:length(all_series)
    errorbar(all_control_params{i},all_large_order_params{i}(iplot,:),all_large_order_param_errors{i}(iplot,:), 'vertical', 'LineStyle', 'none');
end
ylabel('Kinetic energy ratio $\mathcal{Q}_\textrm{2D}(K=1)$')
xlabel(control_param_name)
xlim(control_lims)
set(gca, 'XScale', 'log')
grid on
box on
for iseries=1:length(bimodals)
    for icase=1:length(bimodals{iseries})
        set(gca,'ColorOrderIndex',iseries)
        scatter(bimodal_control_params{iseries}{icase},low_large_order_params{iseries}{icase}(iplot),80,'d','filled')
        set(gca,'ColorOrderIndex',iseries)
        errorbar(bimodal_control_params{iseries}{icase},low_large_order_params{iseries}{icase}(iplot),low_large_order_param_errors{iseries}{icase}(iplot), 'vertical', 'LineStyle', 'none');
        
        set(gca,'ColorOrderIndex',iseries)
        scatter(bimodal_control_params{iseries}{icase},high_large_order_params{iseries}{icase}(iplot),80,'d','filled')
        set(gca,'ColorOrderIndex',iseries)
        errorbar(bimodal_control_params{iseries}{icase},high_large_order_params{iseries}{icase}(iplot),high_large_order_param_errors{iseries}{icase}(iplot), 'vertical', 'LineStyle', 'none');
    end
    
    if length(bimodals{iseries})>0
        adj_low=findIn(all_control_params{iseries},bimodal_control_params{iseries}{1})-1;
        adj_high=findIn(all_control_params{iseries},bimodal_control_params{iseries}{end})+1;
        low_large_order_params_arr=cell2mat(low_large_order_params{iseries});
        high_large_order_params_arr=cell2mat(high_large_order_params{iseries});
        dash_low_x=[all_control_params{iseries}(adj_low),cell2mat(bimodal_control_params{iseries})];
        dash_low_y=[all_large_order_params{iseries}(iplot,adj_low),low_large_order_params_arr(iplot,:)];
        dash_high_x=[cell2mat(bimodal_control_params{iseries}),all_control_params{iseries}(adj_high)];
        dash_high_y=[high_large_order_params_arr(iplot,:),all_large_order_params{iseries}(iplot,adj_high)];

        set(gca,'ColorOrderIndex',iseries)
        plot(dash_low_x,dash_low_y,':','LineWidth',1)
        set(gca,'ColorOrderIndex',iseries)
        plot(dash_high_x,dash_high_y,':','LineWidth',1)
    end
end

saveas(gcf,'Fig_factories/Report/figs/6_aspect_ratio_transition_kin','epsc')

figure
iplot=5;
for i=1:length(all_series)
    l=plot(all_control_params{i},all_large_order_params{i}(iplot,:),'-s');
    l.MarkerFaceColor = l.Color; %to fill the symbols
    hold on
end
set(gca,'ColorOrderIndex',1)
for i=1:length(all_series)
    errorbar(all_control_params{i},all_large_order_params{i}(iplot,:),all_large_order_param_errors{i}(iplot,:), 'vertical', 'LineStyle', 'none');
end
ylabel('Vorticity ratio $\mathcal{Q}_{\textrm{$\omega$},\textrm{2D}}(K=1)$')
xlabel(control_param_name)
xlim(control_lims)
set(gca, 'XScale', 'log')
grid on
box on
for iseries=1:length(bimodals)
    for icase=1:length(bimodals{iseries})
        set(gca,'ColorOrderIndex',iseries)
        scatter(bimodal_control_params{iseries}{icase},low_large_order_params{iseries}{icase}(iplot),80,'d','filled')
        set(gca,'ColorOrderIndex',iseries)
        errorbar(bimodal_control_params{iseries}{icase},low_large_order_params{iseries}{icase}(iplot),low_large_order_param_errors{iseries}{icase}(iplot), 'vertical', 'LineStyle', 'none');

        set(gca,'ColorOrderIndex',iseries)
        scatter(bimodal_control_params{iseries}{icase},high_large_order_params{iseries}{icase}(iplot),80,'d','filled')
        set(gca,'ColorOrderIndex',iseries)
        errorbar(bimodal_control_params{iseries}{icase},high_large_order_params{iseries}{icase}(iplot),high_large_order_param_errors{iseries}{icase}(iplot), 'vertical', 'LineStyle', 'none');
    end
    
    if length(bimodals{iseries})>0
        adj_low=findIn(all_control_params{iseries},bimodal_control_params{iseries}{1})-1;
        adj_high=findIn(all_control_params{iseries},bimodal_control_params{iseries}{end})+1;
        low_large_order_params_arr=cell2mat(low_large_order_params{iseries});
        high_large_order_params_arr=cell2mat(high_large_order_params{iseries});
        dash_low_x=[all_control_params{iseries}(adj_low),cell2mat(bimodal_control_params{iseries})];
        dash_low_y=[all_large_order_params{iseries}(iplot,adj_low),low_large_order_params_arr(iplot,:)];
        dash_high_x=[cell2mat(bimodal_control_params{iseries}),all_control_params{iseries}(adj_high)];
        dash_high_y=[high_large_order_params_arr(iplot,:),all_large_order_params{iseries}(iplot,adj_high)];

        set(gca,'ColorOrderIndex',iseries)
        plot(dash_low_x,dash_low_y,':','LineWidth',1)
        set(gca,'ColorOrderIndex',iseries)
        plot(dash_high_x,dash_high_y,':','LineWidth',1)
    end
end

saveas(gcf,'Fig_factories/Report/figs/6_aspect_ratio_transition_vort','epsc')

figure
iplot=9;
for iseries=1:length(all_series)
    l=plot(all_control_params{iseries},all_params{iseries}(iplot,:),'-s');
    l.MarkerFaceColor = l.Color; %to fill the symbols
    hold on
end
set(gca,'ColorOrderIndex',1)
for iseries=1:length(all_series)
    errorbar(all_control_params{iseries},all_params{iseries}(iplot,:),all_param_errors{iseries}(iplot,:), 'vertical', 'LineStyle', 'none');
end
ylabel('Baroclinic transport $\mathcal{T}_\textrm{bc}/(U^3H^{-1})$')
xlabel(control_param_name)
xlim(control_lims)
set(gca, 'XScale', 'log')
grid on
box on
for iseries=1:length(bimodals)
    for icase=1:length(bimodals{iseries})
        set(gca,'ColorOrderIndex',iseries)
        scatter(bimodal_control_params{iseries}{icase},low_params{iseries}{icase}(iplot),80,'d','filled')
        set(gca,'ColorOrderIndex',iseries)
        errorbar(bimodal_control_params{iseries}{icase},low_params{iseries}{icase}(iplot),low_param_errors{iseries}{icase}(iplot), 'vertical', 'LineStyle', 'none');

        set(gca,'ColorOrderIndex',iseries)
        scatter(bimodal_control_params{iseries}{icase},high_params{iseries}{icase}(iplot),80,'d','filled')
        set(gca,'ColorOrderIndex',iseries)
        errorbar(bimodal_control_params{iseries}{icase},high_params{iseries}{icase}(iplot),high_param_errors{iseries}{icase}(iplot), 'vertical', 'LineStyle', 'none');
    end
    
    if length(bimodals{iseries})>0
        adj_low=findIn(all_control_params{iseries},bimodal_control_params{iseries}{1})-1;
        adj_high=findIn(all_control_params{iseries},bimodal_control_params{iseries}{end})+1;
        low_params_arr=cell2mat(low_params{iseries});
        high_params_arr=cell2mat(high_params{iseries});
        dash_low_x=[all_control_params{iseries}(adj_low),cell2mat(bimodal_control_params{iseries})];
        dash_low_y=[all_params{iseries}(iplot,adj_low),low_params_arr(iplot,:)];
        dash_high_x=[cell2mat(bimodal_control_params{iseries}),all_control_params{iseries}(adj_high)];
        dash_high_y=[high_params_arr(iplot,:),all_params{iseries}(iplot,adj_high)];

        set(gca,'ColorOrderIndex',iseries)
        plot(dash_low_x,dash_low_y,':','LineWidth',1)
        set(gca,'ColorOrderIndex',iseries)
        plot(dash_high_x,dash_high_y,':','LineWidth',1)
    end
end

saveas(gcf,'Fig_factories/Report/figs/6_aspect_ratio_transition_bc','epsc')

figure
iplot=8;
for iseries=1:length(all_series)
    l=plot(all_control_params{iseries},all_params{iseries}(iplot,:),'-s');
    l.MarkerFaceColor = l.Color; %to fill the symbols
    hold on
end
set(gca,'ColorOrderIndex',1)
for iseries=1:length(all_series)
    errorbar(all_control_params{iseries},all_params{iseries}(iplot,:),all_param_errors{iseries}(iplot,:), 'vertical', 'LineStyle', 'none');
end
ylabel('Barotropic transport $\mathcal{T}_\textrm{bt}/(U^3H^{-1})$')
xlabel(control_param_name)
xlim(control_lims)
set(gca, 'XScale', 'log')
grid on
box on
for iseries=1:length(bimodals)
    for icase=1:length(bimodals{iseries})
        set(gca,'ColorOrderIndex',iseries)
        scatter(bimodal_control_params{iseries}{icase},low_params{iseries}{icase}(iplot),80,'d','filled')
        set(gca,'ColorOrderIndex',iseries)
        errorbar(bimodal_control_params{iseries}{icase},low_params{iseries}{icase}(iplot),low_param_errors{iseries}{icase}(iplot), 'vertical', 'LineStyle', 'none');

        set(gca,'ColorOrderIndex',iseries)
        scatter(bimodal_control_params{iseries}{icase},high_params{iseries}{icase}(iplot),80,'d','filled')
        set(gca,'ColorOrderIndex',iseries)
        errorbar(bimodal_control_params{iseries}{icase},high_params{iseries}{icase}(iplot),high_param_errors{iseries}{icase}(iplot), 'vertical', 'LineStyle', 'none');
    end
    
    if length(bimodals{iseries})>0
        adj_low=findIn(all_control_params{iseries},bimodal_control_params{iseries}{1})-1;
        adj_high=findIn(all_control_params{iseries},bimodal_control_params{iseries}{end})+1;
        low_params_arr=cell2mat(low_params{iseries});
        high_params_arr=cell2mat(high_params{iseries});
        dash_low_x=[all_control_params{iseries}(adj_low),cell2mat(bimodal_control_params{iseries})];
        dash_low_y=[all_params{iseries}(iplot,adj_low),low_params_arr(iplot,:)];
        dash_high_x=[cell2mat(bimodal_control_params{iseries}),all_control_params{iseries}(adj_high)];
        dash_high_y=[high_params_arr(iplot,:),all_params{iseries}(iplot,adj_high)];

        set(gca,'ColorOrderIndex',iseries)
        plot(dash_low_x,dash_low_y,':','LineWidth',1)
        set(gca,'ColorOrderIndex',iseries)
        plot(dash_high_x,dash_high_y,':','LineWidth',1)
    end
end

saveas(gcf,'Fig_factories/Report/figs/6_aspect_ratio_transition_bt','epsc')

gPlotting=gPlottingAspectRatio;
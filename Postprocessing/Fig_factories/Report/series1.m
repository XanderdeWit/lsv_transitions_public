gPlottingSeries1=gPlotting; gPlotting=false;
gSeries='series1/';

ra_trans=5.65e6;

series_analysis;

%plot kinetic energy LOPs;
figure
l=plot(control_params,large_order_params(1,:),'-s','DisplayName','$\mathcal{Q}_\textrm{hor}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
hold on
l=plot(control_params,large_order_params(2,:),'-s','DisplayName','$\mathcal{Q}_\textrm{2D}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
l=plot(control_params,large_order_params(3,:),'-s','DisplayName','$\mathcal{Q}_\textrm{2D}(K=1)$');
l.MarkerFaceColor = l.Color; %to fill the symbols
legend('Location','northwest')
set(gca,'ColorOrderIndex',1)
errorbar(control_params,large_order_params(1,:),large_order_param_errors(1,:), 'vertical', 'LineStyle', 'none');
errorbar(control_params,large_order_params(2,:),large_order_param_errors(2,:), 'vertical', 'LineStyle', 'none');
errorbar(control_params,large_order_params(3,:),large_order_param_errors(3,:), 'vertical', 'LineStyle', 'none');
xline(ra_trans,'-.r','LineWidth',1);
ylabel('Kinetic energy ratio')
xlabel('$\textrm{Ra}$')
yline(0,'k');
xlim([0.9*min(control_params),max(control_params)+0.1*min(control_params)])
ylim([-1,12])
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/4_series1_kin','epsc')

%plot vorticity LOPs;
figure
l=plot(control_params,large_order_params(4,:),'-s','DisplayName','$\mathcal{Q}_{\textrm{$\omega$},\textrm{2D}}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
hold on
l=plot(control_params,large_order_params(5,:),'-s','DisplayName','$\mathcal{Q}_{\textrm{$\omega$},\textrm{2D}}(K=1)$');
l.MarkerFaceColor = l.Color; %to fill the symbols
legend('Location','northwest')
set(gca,'ColorOrderIndex',1)
errorbar(control_params,large_order_params(4,:),large_order_param_errors(4,:), 'vertical', 'LineStyle', 'none');
errorbar(control_params,large_order_params(5,:),large_order_param_errors(5,:), 'vertical', 'LineStyle', 'none');
xline(ra_trans,'-.r','LineWidth',1);
ylabel('Vorticity ratio')
xlabel('$\textrm{Ra}$')
xlim([0.9*min(control_params),max(control_params)+0.1*min(control_params)])
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/4_series1_vort','epsc')

%plot kinetic transport 2D
figure
l=plot(control_params,params(10,:),'-s','DisplayName','$\mathcal{F}_\textrm{2D}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
hold on
l=plot(control_params,params(11,:),'-s','DisplayName','$\mathcal{D}_\textrm{2D}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
legend('Location','northwest')
set(gca,'ColorOrderIndex',1)
errorbar(control_params,params(10,:),param_errors(10,:), 'vertical', 'LineStyle', 'none');
errorbar(control_params,params(11,:),param_errors(11,:), 'vertical', 'LineStyle', 'none');
xline(ra_trans,'-.r','LineWidth',1);
ylabel('Energetic transport (in $U^3H^{-1}$)')
xlabel('$\textrm{Ra}$')
xlim([0.9*min(control_params),max(control_params)+0.1*min(control_params)])
yline(0,'k');
%ylim([-0.08,0.6])
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/4_series1_trans_2D','epsc')

%plot kinetic transport 2D K=1
figure
l=plot(control_params,params(9,:),'-s','DisplayName','$\mathcal{T}_\textrm{bc}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
hold on
l=plot(control_params,params(8,:),'-s','DisplayName','$\mathcal{T}_\textrm{bt}$');
l.MarkerFaceColor = l.Color; %to fill the symbols
legend('Location','northwest')
set(gca,'ColorOrderIndex',1)
errorbar(control_params,params(9,:),param_errors(9,:), 'vertical', 'LineStyle', 'none');
errorbar(control_params,params(8,:),param_errors(8,:), 'vertical', 'LineStyle', 'none');
xline(ra_trans,'-.r','LineWidth',1);
ylabel('Energetic transport (in $U^3H^{-1}$)')
xlabel('$\textrm{Ra}$')
xlim([0.9*min(control_params),max(control_params)+0.1*min(control_params)])
yline(0,'k');
%ylim([-0.02,0.3])
grid on
box on

saveas(gcf,'Fig_factories/Report/figs/4_series1_trans_k1','epsc')

%plot kinetic transport maps
gSimname='series1/case04'; gTstart=getTstart(gSimname); make_domain;
transfer_stafield;

lims=[-1.2e-5,1.2e-5];
gMyCM_trans=transformCM(gMyCM,8/256,0.1,11);

figure
imagesc(transQK_BT(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
%colorbar
xlabel('$Q$')
ylabel('$K$')
%set(gca,'YTickLabel',[]);
axis image
%colorbar('TickLabelInterpreter','latex')

saveas(gcf,'Fig_factories/Report/figs/4_series1_trans_bt_04','epsc')

figure
imagesc(transQK_BC(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
%colorbar
xlabel('$Q$')
ylabel('$K$')
%set(gca,'YTickLabel',[]);
axis image
%colorbar('TickLabelInterpreter','latex')

saveas(gcf,'Fig_factories/Report/figs/4_series1_trans_bc_04','epsc')

gSimname='series1/case056'; gTstart=getTstart(gSimname); make_domain;
transfer_stafield;

figure
imagesc(transQK_BT(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
%colorbar
xlabel('$Q$')
%ylabel('$K$')
set(gca,'YTickLabel',[]);
axis image
%colorbar('TickLabelInterpreter','latex')

saveas(gcf,'Fig_factories/Report/figs/4_series1_trans_bt_056','epsc')

figure
imagesc(transQK_BC(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
%colorbar
xlabel('$Q$')
%ylabel('$K$')
set(gca,'YTickLabel',[]);
axis image
%colorbar('TickLabelInterpreter','latex')

saveas(gcf,'Fig_factories/Report/figs/4_series1_trans_bc_056','epsc')

gSimname='series1/case057'; gTstart=getTstart(gSimname); make_domain;
transfer_stafield;

figure
imagesc(transQK_BT(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
colorbar
xlabel('$Q$')
%ylabel('$K$')
set(gca,'YTickLabel',[]);
axis image
colorbar('TickLabelInterpreter','latex')

saveas(gcf,'Fig_factories/Report/figs/4_series1_trans_bt_057','epsc')

figure
imagesc(transQK_BC(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims)
set(gca,'YDir','normal')
colormap(gMyCM_trans)
colorbar
xlabel('$Q$')
%ylabel('$K$')
set(gca,'YTickLabel',[]);
axis image
colorbar('TickLabelInterpreter','latex')

saveas(gcf,'Fig_factories/Report/figs/4_series1_trans_bc_057','epsc')


transfer_stafield_series;

cm=pink(350);
cm=cm(1:256,:);

ra_low=ras(1);
ra_high=ras(end);

figure
for i=1:ncases
    plot(bt_k1{i}'.*correction_K(2:maxQ+1),'LineWidth',2,'Color',cm(1+round(255*(ras(i)-ra_low)/(ra_high-ra_low)),:),...
     'DisplayName',['$\textrm{Ra}=' num2str(ras(i)/10^(floor(log10(ras(i))))) '\times 10^' num2str(floor(log10(ras(i)))) '$'])
    hold on
end
xlabel('$Q$')
%ylabel('$-\bar{\mathbf{u}}_{K=1} \cdot (\bar{\mathbf{u}} \cdot \nabla \bar{\mathbf{u}}_Q)/(U^3H^{-1})$')
ylabel('$T_\textrm{bt}(1,Q)/(U^3H^{-1})$')
xlim([1,maxQ])
grid on
box on
colormap(cm)
colorbar
caxis([ra_low, ra_high])
colorbar('TickLabelInterpreter','latex')
if i_transition_low > 0
     plot(bt_k1{i_transition_low}'.*correction_K(2:maxQ+1),'LineWidth',2,'Color','#6ab900')
end
if i_transition_high > 0
     plot(bt_k1{i_transition_high}'.*correction_K(2:maxQ+1),'LineWidth',2,'Color','r')
end
ylim([-1e-5,1e-5])

saveas(gcf,'Fig_factories/Report/figs/4_series1_trans_bt_k1','epsc')

figure
for i=1:ncases
    plot(bc_k1{i}'.*correction_K(2:maxQ+1),'LineWidth',2,'Color',cm(1+round(255*(ras(i)-ra_low)/(ra_high-ra_low)),:),...
     'DisplayName',['$\textrm{Ra}=' num2str(ras(i)/10^(floor(log10(ras(i))))) '\times 10^' num2str(floor(log10(ras(i)))) '$'])
    hold on
end
xlabel('$Q$')
%ylabel('$-\bar{\mathbf{u}}_{K=1} \cdot (\mathbf{u}^\prime \cdot \nabla \mathbf{u}^\prime_Q)/(U^3H^{-1})$')
ylabel('$T_\textrm{bc}(1,Q)/(U^3H^{-1})$')
xlim([1,maxQ])
grid on
box on
colormap(cm)
colorbar
caxis([ra_low, ra_high])
colorbar('TickLabelInterpreter','latex')
if i_transition_low > 0
     plot(bc_k1{i_transition_low}'.*correction_K(2:maxQ+1),'LineWidth',2,'Color','#6ab900')
end
if i_transition_high > 0
     plot(bc_k1{i_transition_high}'.*correction_K(2:maxQ+1),'LineWidth',2,'Color','r')
end
ylim([-3e-5,3e-5])

saveas(gcf,'Fig_factories/Report/figs/4_series1_trans_bc_k1','epsc')

gPlotting=gPlottingSeries1;
classdef Domain
    
    properties
        nx,nxm,xs,xms,X,dx
        ny,nym,ys,yms,Y,dy
        nz,nzm,zs,zms,H,dz
        g3rc,g3rm
    end
    
    methods
        function snap3Dtrimmed=trim3D(d,snap3D)
            snap3Dtrimmed=snap3D(1:end-1,1:end-1,:);
        end
        
        function cellV=faceToCell(d,faceV,axes)
            if ndims(faceV) == 3
                cellV = d.faceToCell3D(faceV,axes);
            else
                cellV = d.faceToCell2D(faceV,axes);
            end
        end
        
        function cellV=faceToCell3D(d,faceV,axes)
            if ismember(1,axes)
                faceV=0.5*(faceV(1:end-1,:,:)+faceV(2:end,:,:));
            end
            if ismember(2,axes)
                faceV=0.5*(faceV(:,1:end-1,:)+faceV(:,2:end,:));
            end
            if ismember(3,axes)
                faceV=0.5*(faceV(:,:,1:end-1)+faceV(:,:,2:end));
            end
            cellV=faceV;
        end
        
        function cellV=faceToCell2D(d,faceV,axes)
            if ismember(1,axes)
                faceV=0.5*(faceV(1:end-1,:)+faceV(2:end,:));
            end
            if ismember(2,axes)
                faceV=0.5*(faceV(:,1:end-1)+faceV(:,2:end));
            end
            cellV=faceV;
        end
        
        function snapPeriodic=makePeriodic(d,snap,axes)
            if ndims(snap) == 3
                snapPeriodic = d.makePeriodic3D(snap,axes);
            else
                snapPeriodic = d.makePeriodic2D(snap,axes);
            end
        end

        function snapPeriodic=makePeriodic3D(d,snap,axes)
            if ismember(1,axes)
                snap(end+1,:,:)=snap(1,:,:);
            end
            if ismember(2,axes)
                snap(:,end+1,:)=snap(:,1,:);
            end
            snapPeriodic = snap;
        end

        function snapPeriodic=makePeriodic2D(d,snap,axes)
            if ismember(1,axes)
                snap(end+1,:)=snap(1,:);
            end
            if ismember(2,axes)
                snap(:,end+1)=snap(:,1);
            end
            snapPeriodic = snap;
        end
    end
end


function ts=interpTs(ts,tnew)

if nargin < 2
    nt=length(ts.Time);
    tnew=linspace(ts.Time(1),ts.Time(end),nt)';
else
    %make sure tnew is a column vector
    if size(tnew,1)==1
        tnew=tnew';
    end
end

[tUnique,iUnique]=unique(ts.Time);
ts=timeseries(interp1(tUnique,squeeze(ts.Data(iUnique)),tnew,'linear','extrap'),tnew,'Name',ts.Name);

end
%caclulate Okubo-Weiss parameter from slab

%read z-averaged velocity field
vx=h5read([gDataDir gSimname '/stst3/slabzq1_00002021.h5'],'/var'); 
vy=h5read([gDataDir gSimname '/stst3/slabzq2_00002021.h5'],'/var');

%Vorticity
ome = getAxialVorticity(vx,vy,d);

%Okubo-Weiss
oku_wei = getOkuboWeis(vx,vy,d);

%plot
figure
drawCross(d.xs,d.ys,ome',[-max(abs(ome),[],'all'),max(abs(ome),[],'all')],'pcolor',gMyCM,true);
title(['Vorticity ' gSimname])

figure
drawCross(d.xms,d.yms,oku_wei',[-max(abs(oku_wei),[],'all'),max(abs(oku_wei),[],'all')],'pcolor',gMyCM,true);
title(['Okubo-Weiss ' gSimname])

oku_wei_mean=mean(oku_wei,'all')
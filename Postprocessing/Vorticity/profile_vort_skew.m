%Plot axial vorticity skewness

averaging_time=h5read([gDataDir gSimname '/stafield_master.h5'],'/averaging_time');

ome2=h5read([gDataDir gSimname '/stafield_data.h5'],'/Vorti3_m2');
ome2=ome2./double(averaging_time);
ome3=h5read([gDataDir gSimname '/stafield_data.h5'],'/Vorti3_m3');
ome3=ome3./double(averaging_time);

ome_skew = ome3./(ome2.^(3/2));

if gPlotting
    figure
    scatter(ome_skew,d.zms,10,'s','filled')
    title(['Axial vorticity skewness ' gSimname])
    ylabel('$z$')
    xlabel('Skewness')
    grid on
    box on
end
%get the KH2D kinetic energy time series from the z-averaged slabs

%filename example: stst3/slabzq1_00000001.h5

%find files
files=dir([gDataDir gSimname '/stst3']);
slab_is=0;
for ifile=3:length(files)
    if files(ifile).name(5:7) == 'zq1'
        if slab_is == 0
            slab_is=str2num(files(ifile).name(9:16));
        else
            slab_is=[slab_is; str2num(files(ifile).name(9:16))];
        end
    end
end

%read slabs and calculate spectrum
k=0:min(d.nxm,d.nym)/2;
vort_zave_k=zeros(length(slab_is),length(k));
time=zeros(length(slab_is),1);
%tic
parfor islab_i=1:length(slab_is)
%for islab_i=1:length(slab_is)
    mVx_zave=h5read([gDataDir gSimname '/stst3/slabzq1_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/var');
    mtime=h5read([gDataDir gSimname '/stst3/slabzq1_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/time');
    
    mVy_zave=h5read([gDataDir gSimname '/stst3/slabzq2_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/var');
    mtimeVy=h5read([gDataDir gSimname '/stst3/slabzq2_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/time');
    
    if mtime ~= mtimeVy
        disp('Error: time_vx != time_vy');
    end
    
    mVort_zave = getAxialVorticity(mVx_zave,mVy_zave,d);
    mVort_zave = d.faceToCell(mVort_zave,[1,2]);
    
    mVort_zave_kk=fft2(mVort_zave)./(d.nxm*d.nym);
    
    mVort_zave_k=zeros(1,length(k));
    
    kx=[0:d.nxm/2,-d.nxm/2+1:-1];
    ky=[0:d.nym/2,-d.nym/2+1:-1];
    for ikx=1:d.nxm
        for iky=1:d.nym
            mkx=kx(ikx);
            mky=ky(iky);
            %bin all wavenumbers with kH <= sqrt(kx^2+ky^2) < kH+1 together
            mk=floor(sqrt(mkx^2+mky^2));
            if mk<length(k)
                %take absolute value of the fourier component
                mVort_zave_k(1+mk)=mVort_zave_k(1+mk)+abs(mVort_zave_kk(ikx,iky))^2;
            end
        end
    end
    
    vort_zave_k(islab_i,:)=mVort_zave_k;
    
    time(islab_i)=mtime;
end
%toc

%time-averaged spectrum
mean_vort_zave_k=mean(vort_zave_k,1);
if gTstart ~= -1
    itstart=findIn(time,gTstart);
    mean_vort_zave_k=mean(vort_zave_k(itstart:end,:),1);
end

%make timeseries of k=1 mode and calculate statistics
tsVortM2zaveK1_slabs=timeseries(vort_zave_k(:,2),time,'Name','VortM2zave k=1');

if gPlotting
    %Plot time-averaged spectrum
    figure
    loglog(k(2:end),mean_vort_zave_k(2:end))
    title(['z-averaged vorticity spectrum ' gSimname])
    xlabel('Horizontal wavenumber $k_H$')
    ylabel('$\langle\omega\rangle_z^2(k_H)$')
    grid on
    box on
    
    %Plot time series of modes
    figure
    hold on
    for ik=2:10
        scatter(time,vort_zave_k(:,ik),10,'s','filled','DisplayName',['k=' num2str(k(ik))])
    end
    legend('Location','northwest')
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    hold off
    title(['z-averaged vorticity spectral modes ' gSimname])
    ylabel('$\langle\omega\rangle_z^2(k_H)$')
    xlabel('Convective time')
    grid on
    box on
end
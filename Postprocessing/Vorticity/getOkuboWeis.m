function oku_wei=getOkuboWeis(Vx,Vy,d)
%move all velocities to cell center
u=d.faceToCell(d.makePeriodic(Vx,1),1);
v=d.faceToCell(d.makePeriodic(Vy,2),2);

%calculate derivatives
dudx=getDerivativeXY(u,d.dx,1,d);
dudy=getDerivativeXY(u,d.dy,2,d);
dvdx=getDerivativeXY(v,d.dx,1,d);
dvdy=getDerivativeXY(v,d.dy,2,d);

oku_wei = 4 .* dudy .* dvdx + (dudx - dvdy).^2;

end

function deriv=getDerivativeXY(y,dx,axis,d)
    deriv=circshift(diff(d.makePeriodic(y,axis),1,axis),axis); %face value
    deriv=d.faceToCell(d.makePeriodic(deriv,axis),axis); %cell value
    deriv=deriv./dx;
end
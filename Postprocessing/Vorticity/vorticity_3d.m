%Plot axial vorticity from 3D snapshot

Vx=h5read([gDataDir gSimname '/continua_q1.h5'],'/Vth');
Vx=d.trim3D(Vx);

Vy=h5read([gDataDir gSimname '/continua_q2.h5'],'/Vr');
Vy=d.trim3D(Vy);

ome_z = getAxialVorticity(Vx,Vy,d);

%Average in vertical direction
ome_z_bar = mean(ome_z .* reshape(d.g3rm,1,1,d.nzm),3);

if gPlotting
    %Plot cross sections
    lims=1.2*[-max(ome_z_bar,[],'all'),max(ome_z_bar,[],'all')];
    plot3DHorCross(d.xs,d.ys,d.zms,ome_z,'Axial vorticity',lims,'pcolor',gMyCM,true)
    plot3DVertCross(d.xs,d.ys,d.zms,ome_z,'Axial vorticity',lims,'pcolor',gMyCM,true)

    %Plot vertical average
    figure
    %lims=[-max(ome_z_bar,[],'all'),max(ome_z_bar,[],'all')];
    drawCross(d.xs,d.ys,ome_z_bar',lims,'pcolor',gMyCM,true);
    title('Vertically averaged axial vorticity')
end

%2d axial vorticity ratio
vort2DRatio = ome_z_bar.^2 ./ mean(ome_z.^2 .* reshape(d.g3rm,1,1,d.nzm),3);

if gPlotting
    figure
    lims=[0,1];
    drawCross(d.xs,d.ys,vort2DRatio',lims,'pcolor',gMyCM,true);
    title('2D axial vorticity ratio')
end

%2d axial vorticity ratio
vort2DLRatio = ome_z_bar.^2 ./ (mean(ome_z.^2 .* reshape(d.g3rm,1,1,d.nzm),3)-ome_z_bar.^2);

if gPlotting
    figure
    lims=[0,2.5];
    drawCross(d.xs,d.ys,vort2DLRatio',lims,'pcolor',gMyCM,true);
    title('2D axial vorticity large ratio')
end

%signed 2d axial vorticity ratio
signed_ome_z_bar=ome_z_bar;
signed_ome_z_bar(ome_z_bar<0)=0;
signedVort2DRatio = signed_ome_z_bar.^2 ./ mean(ome_z.^2 .* reshape(d.g3rm,1,1,d.nzm),3);

if gPlotting
    figure
    lims=[0,1];
    drawCross(d.xs,d.ys,signedVort2DRatio',lims,'pcolor',gMyCM,true);
    title('Signed 2D axial vorticity ratio')

    % %histogram
    % %ome_z_hist=ome_z(:,:,2:end);
    % iz_hist=floor(d.nz/2);
    % ome_z_hist=ome_z(:,:,iz_hist);
    % ome_z_hist=ome_z_hist(:);
    % figure
    % histogram(ome_z_hist,'Normalization','pdf')
    % title(['Histogram of axial vorticity at z=' num2str(d.zms(iz_hist),2)])
end
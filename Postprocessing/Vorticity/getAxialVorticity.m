function ome_z=getAxialVorticity(Vx,Vy,d)
%Make Vx periodic in y-direction and vice-versa
VxPer=d.makePeriodic(Vx,2);
VyPer=d.makePeriodic(Vy,1);

dVx = circshift(diff(VxPer,1,2),1,2);
dVy = circshift(diff(VyPer,1,1),1,1);

ome_z = dVy./d.dx - dVx./d.dy;

ome_z = d.makePeriodic(ome_z,[1,2]);

%NOTE: ome_z is defined at cell edge, mid-z

end

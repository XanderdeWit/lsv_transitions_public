%Plot vorticity time series

file = fopen([gDataDir gSimname '/vort.out'],'r');
data = fscanf(file,'%f %f\n',[6 Inf]);

tsVortM2=timeseries(data(2,:)',data(1,:)','Name','VortM2');
tsVortM3=timeseries(data(3,:)',data(1,:)','Name','VortM3');

tsVortM2zave=timeseries(data(4,:)',data(1,:)','Name','VortM2zave');
tsVortM3zave=timeseries(data(5,:)',data(1,:)','Name','VortM3zave');

tsVortM2zaveK1=timeseries(data(6,:)',data(1,:)','Name','VortM2zaveK1');

%Interpolate to uniform time grid and remove duplicate points
tsVortM2=interpTs(tsVortM2);
tsVortM3=interpTs(tsVortM3);
tsVortM2zave=interpTs(tsVortM2zave);
tsVortM3zave=interpTs(tsVortM3zave);
tsVortM2zaveK1=interpTs(tsVortM2zaveK1);

tsSkew=timeseries(tsVortM3.Data ./ tsVortM2.Data.^(3/2), tsVortM3.Time, 'Name', 'Axial vorticity skewness');
tsSkewzave=timeseries(tsVortM3zave.Data ./ tsVortM2zave.Data.^(3/2), tsVortM3zave.Time, 'Name', 'z-averaged axial vorticity skewness');

tsSkewzaveRatio=timeseries(tsSkewzave.Data ./ tsSkew.Data, tsSkewzave.Time, 'Name', 'z-averaged skewness ratio');

tsVort2DRatio=timeseries(tsVortM2zave.Data ./ tsVortM2.Data, tsVortM2zave.Time, 'Name', '2D vorticity ratio');

%m_ts_vort_gPlotting=gPlotting; gPlotting=false;
%ts_vort_zave_spectrum;
%gPlotting=m_ts_vort_gPlotting;
%tsVortM2zaveK1_slabs=interpTs(tsVortM2zaveK1_slabs,tsVortM2.Time);

%tsVort2Dk1Ratio_slabs=timeseries(tsVortM2zaveK1_slabs.Data ./ tsVortM2.Data, tsVortM2zave.Time, 'Name', '2D k=1 vorticity ratio slabs');
%tsVort2Dk1_2Ratio_slabs=timeseries(tsVortM2zaveK1_slabs.Data ./ tsVortM2zave.Data, tsVortM2zave.Time, 'Name', '2D k=1 vorticity ratio 2 slabs');

tsVort2Dk1Ratio=timeseries(tsVortM2zaveK1.Data ./ tsVortM2.Data, tsVortM2zave.Time, 'Name', '2D k=1 vorticity ratio');
tsVort2Dk1_2Ratio=timeseries(tsVortM2zaveK1.Data ./ tsVortM2zave.Data, tsVortM2zave.Time, 'Name', '2D k=1 vorticity ratio 2');

tsLRatioVort2D=timeseries(tsVortM2zave.Data ./ (tsVortM2.Data - tsVortM2zave.Data), tsVortM2zave.Time, 'Name', '2D vorticity ratio');
tsLRatioVort2Dk1=timeseries(tsVortM2zaveK1.Data ./ (tsVortM2.Data - tsVortM2zave.Data), tsVortM2zave.Time, 'Name', '2D k=1 vorticity ratio');
tsLRatioVort2Dk1_2=timeseries(tsVortM2zaveK1.Data ./ (tsVortM2zave.Data - tsVortM2zaveK1.Data), tsVortM2zave.Time, 'Name', '2D k=1 vorticity ratio 2');

%Do error analysis
vort2DRatioStats=getTsStats(tsVort2DRatio,gPlotting,gPlotting,gTstart);
vort2Dk1RatioStats=getTsStats(tsVort2Dk1Ratio,gPlotting,gPlotting,gTstart);
vort2Dk1_2RatioStats=getTsStats(tsVort2Dk1_2Ratio,gPlotting,gPlotting,gTstart);

lratioVort2DStats=getTsStats(tsLRatioVort2D,gPlotting,gPlotting,gTstart);
lratioVort2Dk1Stats=getTsStats(tsLRatioVort2Dk1,gPlotting,gPlotting,gTstart);
lratioVort2Dk1_2Stats=getTsStats(tsLRatioVort2Dk1_2,gPlotting,gPlotting,gTstart);

vortM2zaveStats=getTsStats(tsVortM2zave,gPlotting,gPlotting,gTstart);
vortM2Stats=getTsStats(tsVortM2,gPlotting,gPlotting,gTstart);
vortM2zaveK1Stats=getTsStats(tsVortM2zaveK1,gPlotting,gPlotting,gTstart);

if gPlotting
    %plot moments
    figure
    hold on
    scatter(tsVortM2.Time,tsVortM2.Data,10,'s','filled')
    scatter(tsVortM3.Time,tsVortM3.Data,10,'s','filled')
    title(['Axial vorticity moments ' gSimname])
    xlabel('Convective time')
    ylabel('Moments')
    legend({tsVortM2.Name,tsVortM3.Name},'Location','northeast')
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    grid on
    box on

    %plot skewness
    figure
    scatter(tsSkew.Time,tsSkew.Data,10,'s','filled')
    title(['Axial vorticity skewness ' gSimname])
    xlabel('Convective time')
    ylabel('Skewness')
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    grid on
    box on

    %plot moments zave
    figure
    hold on
    scatter(tsVortM2zave.Time,tsVortM2zave.Data,10,'s','filled')
    scatter(tsVortM3zave.Time,tsVortM3zave.Data,10,'s','filled')
    title(['z-averaged axial vorticity moments ' gSimname])
    xlabel('Convective time')
    ylabel('Moments')
    legend({tsVortM2zave.Name,tsVortM3zave.Name},'Location','northeast')
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    grid on
    box on

    %plot skewness zave
    figure
    scatter(tsSkewzave.Time,tsSkewzave.Data,10,'s','filled')
    title(['z-averaged axial vorticity skewness ' gSimname])
    xlabel('Convective time')
    ylabel('Skewness')
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    grid on
    box on
    
    %plot z-averaged skewness ratio
    figure
    scatter(tsSkewzaveRatio.Time,tsSkewzaveRatio.Data,10,'s','filled')
    title(['z-averaged skewness ratio ' gSimname])
    xlabel('Convective time')
    ylabel('Ratio')
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    grid on
    box on
    ylim([-1,2])
    
    %plot 2D axial vorticity ratio
    figure
    scatter(tsVort2DRatio.Time,tsVort2DRatio.Data,10,'s','filled','DisplayName',tsVort2DRatio.Name)
    hold on
    scatter(tsVort2Dk1Ratio.Time,tsVort2Dk1Ratio.Data,10,'s','filled','DisplayName',tsVort2Dk1Ratio.Name)
    scatter(tsVort2Dk1_2Ratio.Time,tsVort2Dk1_2Ratio.Data,10,'s','filled','DisplayName',tsVort2Dk1_2Ratio.Name)
    %scatter(tsVort2Dk1Ratio_slabs.Time,tsVort2Dk1Ratio_slabs.Data,10,'s','filled','DisplayName',tsVort2Dk1Ratio_slabs.Name)
    %scatter(tsVort2Dk1_2Ratio_slabs.Time,tsVort2Dk1_2Ratio_slabs.Data,10,'s','filled','DisplayName',tsVort2Dk1_2Ratio_slabs.Name)
    legend
    title(['2D axial vorticity ratio ' gSimname])
    xlabel('Convective time')
    ylabel('Ratio')
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    grid on
    box on
    
    %plot 2D axial vorticity large ratio
    figure
    scatter(tsLRatioVort2D.Time,tsLRatioVort2D.Data,10,'s','filled','DisplayName',tsLRatioVort2D.Name)
    hold on
    scatter(tsLRatioVort2Dk1.Time,tsLRatioVort2Dk1.Data,10,'s','filled','DisplayName',tsLRatioVort2Dk1.Name)
    scatter(tsLRatioVort2Dk1_2.Time,tsLRatioVort2Dk1_2.Data,10,'s','filled','DisplayName',tsLRatioVort2Dk1_2.Name)
    legend
    title(['2D axial vorticity large ratio ' gSimname])
    xlabel('Convective time')
    ylabel('Ratio')
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    grid on
    box on
end
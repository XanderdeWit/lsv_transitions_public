Vx=h5read([gDataDir gSimname '/continua_q1.h5'],'/Vth');
Vx=d.trim3D(Vx);

Vy=h5read([gDataDir gSimname '/continua_q2.h5'],'/Vr');
Vy=d.trim3D(Vy);

ome_z = getAxialVorticity(Vx,Vy,d);
ome_z=d.trim3D(ome_z);

ome_z_zave = mean(ome_z.*reshape(d.g3rm,1,1,d.nzm),3);

ome2_t=(d.nzm/(d.nzm-1))*mean(ome_z.^2 .* reshape(d.g3rm,1,1,d.nzm),'all')
ome3_t=(d.nzm/(d.nzm-1))*mean(ome_z.^3 .* reshape(d.g3rm,1,1,d.nzm),'all')

ome2_zave=(d.nzm/(d.nzm-1))*mean(ome_z_zave.^2,'all')
ome3_zave=(d.nzm/(d.nzm-1))*mean(ome_z_zave.^3,'all')
%Movie of horizontal cross section of vertical vorticity

stst3dir=[gDataDir gSimname '/stst3'];

%The color axis needs to be calibrated, minimum and maximum are printed after rendering
%clim='auto';
clim=[-3 3];

%slab_i_start=-1;
slab_i_start=1100;
%slab_i_end=-1;
slab_i_end=1150;

makeMovie(stst3dir,@getData,d.xs,d.ys,slab_i_start,slab_i_end,clim,'pcolor',gMyCM,[800,650],0.2,d)

function [data,time]=getData(stst3dir,slab_i,d)
    slabname='slabz'; %mid-height

    vx=h5read([stst3dir '/' slabname 'q1_' sprintf('%08d', slab_i) '.h5'],'/var');
    vy=h5read([stst3dir '/' slabname 'q2_' sprintf('%08d', slab_i) '.h5'],'/var');

    time=h5read([stst3dir '/' slabname 'q1_' sprintf('%08d', slab_i) '.h5'],'/time');
    timeVy=h5read([stst3dir '/' slabname 'q2_' sprintf('%08d', slab_i) '.h5'],'/time');

    if time ~= timeVy
        disp('Error: time_vx != time_vy');
    end

    data=getAxialVorticity(vx,vy,d);
end
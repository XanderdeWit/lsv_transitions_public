function h=drawCross(xs,ys,values,valueLim,plotStyle,cm,axisImage)

[xmesh,ymesh]=meshgrid(xs,ys);

if strcmp(plotStyle,'pcolor')
    h=pcolor(xmesh,ymesh,values);
    shading interp
    colormap(cm)
    colorbar('TickLabelInterpreter','latex')
    caxis(valueLim)
elseif strcmp(plotStyle,'contourf')
    [~,h]=contourf(xmesh,ymesh,values);
    colormap(cm)
    colorbar('TickLabelInterpreter','latex')
    caxis(valueLim)
elseif strcmp(plotStyle,'surf')
    h=surf(xmesh,ymesh,values);
    zlim(valueLim)
else
    disp('Wrong plot style!')
    h=-1;
end

if axisImage
    axis image
end

end


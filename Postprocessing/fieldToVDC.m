function fieldToVDC(field,xs,ys,zs,shift,filename)

% disp('Substract mean')
% for i=1:size(field,3)
%   fslab=field(:,:,i);
%   field(:,:,i)=field(:,:,i)-mean(fslab(:));
% end

% Shift field
if sum(abs(shift))~=0
    disp('Shift field')
    field=circshift(field,findIn(xs,shift(1))-1,1);
    field=circshift(field,findIn(ys,shift(2))-1,2);
end

disp('Write raw data')
fid=fopen('VAPOR/rawdata.raw','w+');
fwrite(fid,field,'float32');
fclose(fid);

disp('Write coordinates')
fileID = fopen('VAPOR/xcoords.txt','w');
fprintf(fileID,'%d\n',xs);
fclose(fileID);
fileID = fopen('VAPOR/ycoords.txt','w');
fprintf(fileID,'%d\n',ys);
fclose(fileID);
mzs=zeros(1,length(xs));
mzs(1:length(zs))=zs;
mzs(length(zs)+1:end)=zs(end);
fileID = fopen('VAPOR/zcoords.txt','w');
fprintf(fileID,'%d\n',mzs);
fclose(fileID);

disp('Converting to VDC')
if isfile(['VAPOR/' filename '.vdc'])
    system(['rm VAPOR/' filename '.vdc']);
    system(['rm -rf VAPOR/' filename '_data']);
end
system(['/Applications/vapor.app/Contents/MacOS/vdccreate -dimension ' num2str(size(field,1)) 'x' num2str(size(field,2)) 'x' num2str(size(field,3)) ' -xcoords VAPOR/xcoords.txt -ycoords VAPOR/ycoords.txt -zcoords VAPOR/zcoords.txt -numts 1 -vars3d myfield VAPOR/' filename '.vdc']);
system(['/Applications/vapor.app/Contents/MacOS/raw2vdc -ts 0 -varname myfield VAPOR/' filename '.vdc VAPOR/rawdata.raw']);

disp('Removing raw file and coordinate files')
system('rm VAPOR/rawdata.raw VAPOR/xcoords.txt VAPOR/ycoords.txt VAPOR/zcoords.txt');


end

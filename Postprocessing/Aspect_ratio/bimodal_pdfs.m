all_series={'series1/','series1_ar/small/','series1_ar/small2/','series1_ar/large/','series1_ar/large2/'};
bimodals={{},{'case06','case062','case064'},{'case058','case06'}};


for iseries=1:length(bimodals)
    for icase=1:length(bimodals{iseries})
        gSimname=[all_series{iseries} char(bimodals{iseries}{icase})];
        
        gPlottingBimodalPDFs=gPlotting; gPlotting=false;
        
        ts_bimodal;
        
        gPlotting=gPlottingBimodalPDFs;
        
        qs=tsLRatio2Dk1.Data;
        %qs=tsKH2Dk1.Data;
        
        q_low=lop_stats_low(3,1);
        q_high=lop_stats_high(3,1);
        
        if gPlotting
            [epdf,edges] = histcounts(qs,100,'Normalization', 'pdf');
            xi=0.5*(edges(2:end)+edges(1:end-1));
            figure
            plot(xi,epdf,'LineWidth',2);
            %set(gca, 'YScale', 'log')
            grid on
            hold on
            %mu=mean(qs)
            %sigma=std(qs)
            %pd = makedist('Normal','mu',mu,'sigma',sigma);
            %plot(xi,pdf(pd,xi),'--','LineWidth',2)
            %ylim([10^floor(log10(min(epdf))),10^ceil(log10(max(epdf)))])
            xlabel('$\mathcal{Q}$')
            ylabel('PDF $\mathcal{Q}$')
            title(gSimname)
            xline(q_low,'r--','LineWidth',1)
            xline(q_high,'r--','LineWidth',1)

            figure
            [f,xi]=ksdensity(qs);
            plot(xi,f,'LineWidth',2)
            xlabel('$\mathcal{Q}$')
            ylabel('PDF $\mathcal{Q}$')
            grid on
            title(gSimname)
            xline(q_low,'r--','LineWidth',1)
            xline(q_high,'r--','LineWidth',1)
        end
    end
end

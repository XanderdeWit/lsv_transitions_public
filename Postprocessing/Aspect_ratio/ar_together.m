all_series={'series1/','series1_ar/small/','series1_ar/small2/','series1_ar/large/','series1_ar/large2/'};

m_series_ar_together_gPlotting=gPlotting; gPlotting=false; m_series_ar_together_gSeries=gSeries;

all_control_params={};
all_order_params={};
all_order_param_errors={};
all_large_order_params={};
all_large_order_param_errors={};
all_params={};
all_param_errors={};
all_anis={};
all_ani_errors={};

for i=1:length(all_series)

    gSeries=char(all_series(i));
    series_analysis;

    all_control_params{i}=control_params;
    all_order_params{i}=order_params;
    all_order_param_errors{i}=order_param_errors;
    all_large_order_params{i}=large_order_params;
    all_large_order_param_errors{i}=large_order_param_errors;
    all_params{i}=params;
    all_param_errors{i}=param_errors;
    all_anis{i}=anis;
    all_ani_errors{i}=ani_errors;

end

gPlotting=m_series_ar_together_gPlotting; gSeries=m_series_ar_together_gSeries;

if gPlotting
    control_lims=[0.9*(5e6),1.1*(8e6)];
    
%     %plot order parameters separately
%     for iplot=1:size(all_order_params{1},1)
%         figure
%         for i=1:length(all_series)
%             l=plot(all_control_params{i},all_order_params{i}(iplot,:),'-s');
%             l.MarkerFaceColor = l.Color; %to fill the symbols
%             hold on
%         end
%         set(gca,'ColorOrderIndex',1)
%         for i=1:length(all_series)
%             errorbar(all_control_params{i},all_order_params{i}(iplot,:),all_order_param_errors{i}(iplot,:), 'vertical', 'LineStyle', 'none');
%         end
%         title(char(order_param_names(iplot)))
%         ylabel('Order parameter')
%         xlabel(control_param_name)
%         %ylim([0,1])
%         xlim(control_lims)
%         set(gca, 'XScale', 'log')
%         grid on
%         box on
%     end
    
%     %plot large order parameters separately
%     for iplot=1:size(all_large_order_params{1},1)
%         figure
%         for i=1:length(all_series)
%             l=plot(all_control_params{i},all_large_order_params{i}(iplot,:),'-s');
%             l.MarkerFaceColor = l.Color; %to fill the symbols
%             hold on
%         end
%         set(gca,'ColorOrderIndex',1)
%         for i=1:length(all_series)
%             errorbar(all_control_params{i},all_large_order_params{i}(iplot,:),all_large_order_param_errors{i}(iplot,:), 'vertical', 'LineStyle', 'none');
%         end
%         title(char(large_order_param_names(iplot)))
%         ylabel('Large order parameter')
%         xlabel(control_param_name)
%         %ylim([0,1])
%         xlim(control_lims)
%         set(gca, 'XScale', 'log')
%         grid on
%         box on
%     end

    %plot parameters separately
    for iplot=1:size(all_params{1},1)
        figure
        for i=1:length(all_series)
            l=plot(all_control_params{i},all_params{i}(iplot,:),'-s');
            l.MarkerFaceColor = l.Color; %to fill the symbols
            hold on
        end
        set(gca,'ColorOrderIndex',1)
        for i=1:length(all_series)
            errorbar(all_control_params{i},all_params{i}(iplot,:),all_param_errors{i}(iplot,:), 'vertical', 'LineStyle', 'none');
        end
        title(char(param_names(iplot)))
        ylabel('Parameter')
        xlabel(control_param_name)
        %ylim([0,1])
        xlim(control_lims)
        set(gca, 'XScale', 'log')
        grid on
        box on
    end

    
end


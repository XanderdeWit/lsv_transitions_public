function [tstart_low,tend_low,tstart_high,tend_high] = getTBimodal(simname)

switch simname
    case 'series1_ar/small/case06'
        tstart_low=21000; tend_low=24000; tstart_high=18200; tend_high=18500;
    case 'series1_ar/small/case062'
        %tstart_low=9000; tend_low=11000; tstart_high=12500; tend_high=14500;
        %tstart_low=16900; tend_low=17300; tstart_high=23000; tend_high=24400;
        tstart_low=27440; tend_low=28240; tstart_high=23000; tend_high=24400;
    case 'series1_ar/small/case064'
        tstart_low=17200; tend_low=17500; tstart_high=15550; tend_high=16800;
        
    case 'series1_ar/small2/case058'
        tstart_low=15100; tend_low=15950; tstart_high=16900; tend_high=17700;
    case 'series1_ar/small2/case06'
        %tstart_low=4600; tend_low=5270; tstart_high=8000; tend_high=12000;
        %tstart_low=13800; tend_low=14300; tstart_high=16000; tend_high=18500;
        %tstart_low=24200; tend_low=24600; tstart_high=16000; tend_high=18500;
        tstart_low=25800; tend_low=27300; tstart_high=16000; tend_high=18500;
    
        
    otherwise
        tstart_low=-1; tend_low=-1; tstart_high=-1; tend_high=-1;
end

end


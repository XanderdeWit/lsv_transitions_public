m_bimodal_gPlotting=gPlotting; gPlotting=false;
ts_order_params; %get order parameters
gPlotting=m_bimodal_gPlotting;

[tstart_low,tend_low,tstart_high,tend_high] = getTBimodal(gSimname);

[op_stats_low,op_stats_high]=arrayfun(@(ts) getBimodalStats(ts,tstart_low,tend_low,tstart_high,tend_high),op_tss,'un',0);
[lop_stats_low,lop_stats_high]=arrayfun(@(ts) getBimodalStats(ts,tstart_low,tend_low,tstart_high,tend_high),lop_tss,'un',0);
[p_stats_low,p_stats_high]=arrayfun(@(ts) getBimodalStats(ts,tstart_low,tend_low,tstart_high,tend_high),p_tss,'un',0);
[ani_stats_low,ani_stats_high]=arrayfun(@(ts) getBimodalStats(ts,tstart_low,tend_low,tstart_high,tend_high),ani_tss,'un',0);

op_stats_low=cell2mat(op_stats_low); op_stats_high=cell2mat(op_stats_high);
lop_stats_low=cell2mat(lop_stats_low); lop_stats_high=cell2mat(lop_stats_high);
p_stats_low=cell2mat(p_stats_low); p_stats_high=cell2mat(p_stats_high);
ani_stats_low=cell2mat(ani_stats_low); ani_stats_high=cell2mat(ani_stats_high);

if gPlotting
    together=[4,3,4];
    iOPstart=1;
    for iplot=1:length(together)
        iOPstop=sum(together(1:iplot));
        figure
        hold on
        for i=iOPstart:iOPstop
            if ismember(i,plot_movmean)
                scatter(op_tss(i).Time,movmean(op_tss(i).Data,mm_time),10,'s','filled','DisplayName',op_tss(i).Name)
            else
                scatter(op_tss(i).Time,op_tss(i).Data,10,'s','filled','DisplayName',op_tss(i).Name)
            end
        end
        legend('Location','northwest')
        set(gca,'ColorOrderIndex',1)
        for i=iOPstart:iOPstop
            plot(op_tss(i).Time,0*op_tss(i).Time+op_stats_low(i,1),'--','LineWidth',2);
        end
        set(gca,'ColorOrderIndex',1)
        for i=iOPstart:iOPstop
            plot(op_tss(i).Time,0*op_tss(i).Time+op_stats_high(i,1),'--','LineWidth',2);
        end
        if gTstart ~= -1
            xline(gTstart,'--r');
        end
        title(['Order parameters ' gSimname])
        ylabel('Order parameter')
        xlabel('Convective time')
        %ylim([0,1])
        grid on
        box on
        iOPstart=iOPstop+1;
        ymax=ylim;
        area([tstart_low,tend_low],[ymax(2),ymax(2)],'FaceColor','k','FaceAlpha',0.2,'EdgeAlpha',0)
        area([tstart_high,tend_high],[ymax(2),ymax(2)],'FaceColor','k','FaceAlpha',0.2,'EdgeAlpha',0)
    end
    
    together=[3,3];
    iLOPstart=1;
    for iplot=1:length(together)
        iLOPstop=sum(together(1:iplot));
        figure
        hold on
        for i=iLOPstart:iLOPstop
            %scatter(lop_tss(i).Time,movmean(lop_tss(i).Data,500),10,'s','filled','DisplayName',lop_tss(i).Name)
            scatter(lop_tss(i).Time,lop_tss(i).Data,10,'s','filled','DisplayName',lop_tss(i).Name)
        end
        legend('Location','northwest')
        set(gca,'ColorOrderIndex',1)
        for i=iLOPstart:iLOPstop
            plot(lop_tss(i).Time,0*lop_tss(i).Time+lop_stats_low(i,1),'--','LineWidth',2);
        end
        set(gca,'ColorOrderIndex',1)
        for i=iLOPstart:iLOPstop
            plot(lop_tss(i).Time,0*lop_tss(i).Time+lop_stats_high(i,1),'--','LineWidth',2);
        end
        if gTstart ~= -1
            xline(gTstart,'--r');
        end
        title(['Large order parameters ' gSimname])
        ylabel('Large order parameter')
        xlabel('Convective time')
        yline(0,'k');
        grid on
        box on
        iLOPstart=iLOPstop+1;
        ymax=ylim;
        area([tstart_low,tend_low],[ymax(2),ymax(2)],'FaceColor','k','FaceAlpha',0.2,'EdgeAlpha',0)
        area([tstart_high,tend_high],[ymax(2),ymax(2)],'FaceColor','k','FaceAlpha',0.2,'EdgeAlpha',0)
    end
end

function [stats_low,stats_high]=getBimodalStats(ts,tstart_low,tend_low,tstart_high,tend_high)

    ts_low=timeseries(ts.Data(findIn(ts.Time,tstart_low):findIn(ts.Time,tend_low)),ts.Time(findIn(ts.Time,tstart_low):findIn(ts.Time,tend_low)));
    ts_high=timeseries(ts.Data(findIn(ts.Time,tstart_high):findIn(ts.Time,tend_high)),ts.Time(findIn(ts.Time,tstart_high):findIn(ts.Time,tend_high)));
    
    stats_low=getTsStats(ts_low,false,false);
    stats_high=getTsStats(ts_high,false,false);
end
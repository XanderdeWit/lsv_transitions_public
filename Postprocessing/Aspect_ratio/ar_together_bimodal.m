bimodals={{},{'case06','case062','case064'},{'case058','case06'}};

bimodal_control_params={};
low_order_params={};
low_order_param_errors={};
low_large_order_params={};
low_large_order_param_errors={};
low_params={};
low_param_errors={};
low_anis={};
low_ani_errors={};
high_order_params={};
high_order_param_errors={};
high_large_order_params={};
high_large_order_param_errors={};
high_params={};
high_param_errors={};
high_anis={};
high_ani_errors={};

m_series_ar_together_bimodal_gPlotting=gPlotting; gPlotting=false; m_series_ar_together_gSeries=gSeries;
ar_together;

for iseries=1:length(bimodals)
    for icase=1:length(bimodals{iseries})
        gSimname=[all_series{iseries} char(bimodals{iseries}{icase})];
        
        ts_bimodal;
        
        pr=h5read([gDataDir gSimname '/stafield_master.h5'],'/Pr');
        ra=h5read([gDataDir gSimname '/stafield_master.h5'],'/Ra');
        ro=h5read([gDataDir gSimname '/stafield_master.h5'],'/Ro');

        bimodal_control_params{iseries}{icase}=ra; %NB: get control parameter
        
        low_order_params{iseries}{icase}=op_stats_low(:,1);
        low_order_param_errors{iseries}{icase}=op_stats_low(:,3);
        low_large_order_params{iseries}{icase}=lop_stats_low(:,1);
        low_large_order_param_errors{iseries}{icase}=lop_stats_low(:,3);
        low_params{iseries}{icase}=p_stats_low(:,1);
        low_param_errors{iseries}{icase}=p_stats_low(:,3);
        low_anis{iseries}{icase}=ani_stats_low(:,1);
        low_ani_errors{iseries}{icase}=ani_stats_low(:,3);
        high_order_params{iseries}{icase}=op_stats_high(:,1);
        high_order_param_errors{iseries}{icase}=op_stats_high(:,3);
        high_large_order_params{iseries}{icase}=lop_stats_high(:,1);
        high_large_order_param_errors{iseries}{icase}=lop_stats_high(:,3);
        high_params{iseries}{icase}=p_stats_high(:,1);
        high_param_errors{iseries}{icase}=p_stats_high(:,3);
        high_anis{iseries}{icase}=ani_stats_high(:,1);
        high_ani_errors{iseries}{icase}=ani_stats_high(:,3);
        
    end
end

gPlotting=m_series_ar_together_bimodal_gPlotting; gSeries=m_series_ar_together_gSeries;

if gPlotting
    control_lims=[0.9*(5e6),1.1*(8e6)];
    
%     %plot order parameters separately
%     for iplot=1:size(all_order_params{1},1)
%         figure
%         for i=1:length(all_series)
%             l=plot(all_control_params{i},all_order_params{i}(iplot,:),'-s');
%             l.MarkerFaceColor = l.Color; %to fill the symbols
%             hold on
%         end
%         set(gca,'ColorOrderIndex',1)
%         for i=1:length(all_series)
%             errorbar(all_control_params{i},all_order_params{i}(iplot,:),all_order_param_errors{i}(iplot,:), 'vertical', 'LineStyle', 'none');
%         end
%         title(char(order_param_names(iplot)))
%         ylabel('Order parameter')
%         xlabel(control_param_name)
%         %ylim([0,1])
%         xlim(control_lims)
%         set(gca, 'XScale', 'log')
%         grid on
%         box on
%         %TODO add bimodal points
%     end
    
%     %plot large order parameters separately
%     for iplot=1:size(all_large_order_params{1},1)
%         figure
%         for i=1:length(all_series)
%             l=plot(all_control_params{i},all_large_order_params{i}(iplot,:),'-s');
%             l.MarkerFaceColor = l.Color; %to fill the symbols
%             hold on
%         end
%         set(gca,'ColorOrderIndex',1)
%         for i=1:length(all_series)
%             errorbar(all_control_params{i},all_large_order_params{i}(iplot,:),all_large_order_param_errors{i}(iplot,:), 'vertical', 'LineStyle', 'none');
%         end
%         title(char(large_order_param_names(iplot)))
%         ylabel('Large order parameter')
%         xlabel(control_param_name)
%         %ylim([0,1])
%         xlim(control_lims)
%         set(gca, 'XScale', 'log')
%         grid on
%         box on
%         %TODO add bimodal points
%     end

    %plot parameters separately
    for iplot=1:size(all_params{1},1)
        figure
        for iseries=1:length(all_series)
            l=plot(all_control_params{iseries},all_params{iseries}(iplot,:),'-s');
            l.MarkerFaceColor = l.Color; %to fill the symbols
            hold on
        end
        set(gca,'ColorOrderIndex',1)
        for iseries=1:length(all_series)
            errorbar(all_control_params{iseries},all_params{iseries}(iplot,:),all_param_errors{iseries}(iplot,:), 'vertical', 'LineStyle', 'none');
        end
        title(char(param_names(iplot)))
        ylabel('Parameter')
        xlabel(control_param_name)
        %ylim([0,1])
        xlim(control_lims)
        set(gca, 'XScale', 'log')
        grid on
        box on
        for iseries=1:length(bimodals)
            for icase=1:length(bimodals{iseries})
                set(gca,'ColorOrderIndex',iseries)
                scatter(bimodal_control_params{iseries}{icase},low_params{iseries}{icase}(iplot),80,'d','filled')
                set(gca,'ColorOrderIndex',iseries)
                errorbar(bimodal_control_params{iseries}{icase},low_params{iseries}{icase}(iplot),low_param_errors{iseries}{icase}(iplot), 'vertical', 'LineStyle', 'none');
                
                set(gca,'ColorOrderIndex',iseries)
                scatter(bimodal_control_params{iseries}{icase},high_params{iseries}{icase}(iplot),80,'d','filled')
                set(gca,'ColorOrderIndex',iseries)
                errorbar(bimodal_control_params{iseries}{icase},high_params{iseries}{icase}(iplot),high_param_errors{iseries}{icase}(iplot), 'vertical', 'LineStyle', 'none');
            end
        end
    end

    
end


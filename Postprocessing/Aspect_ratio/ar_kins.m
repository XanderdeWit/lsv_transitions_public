all_series={'series1_ar/small/','series1_ar/small2/','series1/','series1_ar/large2/','series1_ar/large/'}; %in increasing order of aspect ratio
mcase='case07';

m_series_ar_kins_gPlotting=gPlotting; gPlotting=false; 

n=length(all_series);

ars=zeros(1,n);

kinH3D=zeros(1,n);
kinV=zeros(1,n);
kinH2D=zeros(1,n);
kinH2Dk1=zeros(1,n);

kinH3D_err=zeros(1,n);
kinV_err=zeros(1,n);
kinH2D_err=zeros(1,n);
kinH2Dk1_err=zeros(1,n);

spec_k={};
spec_ETk={};
spec_E3Dk={};
spec_E2Dk={};
spec_Evk={};

for i=1:n

    gSimname=[char(all_series(i)) mcase];
    
    gTstart=getTstart(gSimname); make_domain;
    
    ars(i)=d.X;
    
    ts_kin_ratios;
    
    kinH3D(i)=kH3DStats(1);
    kinV(i)=kvStats(1);
    kinH2D(i)=kH2DStats(1);
    kinH2Dk1(i)=kH2Dk1Stats(1);
    
    kinH3D_err(i)=kH3DStats(3);
    kinV_err(i)=kvStats(3);
    kinH2D_err(i)=kH2DStats(3);
    kinH2Dk1_err(i)=kH2Dk1Stats(3);
    
    spectrum_from_3d;
    
    spec_k{i}=k;
    spec_ETk{i}=ETk;
    spec_E3Dk{i}=E3Dk;
    spec_E2Dk{i}=E2Dk;
    spec_Evk{i}=Evk;

end

gPlotting=m_series_ar_kins_gPlotting;

if gPlotting
    
    %kin vs ar
    figure
    l=plot(ars,kinH2D,'-s','DisplayName','2D hor');
    l.MarkerFaceColor = l.Color; %to fill the symbols
    hold on
    l=plot(ars,kinH2Dk1,'-s','DisplayName','2D hor k=1');
    l.MarkerFaceColor = l.Color; %to fill the symbols
    l=plot(ars,kinH3D,'-s','DisplayName','3D hor');
    l.MarkerFaceColor = l.Color; %to fill the symbols
    l=plot(ars,kinV,'-s','DisplayName','3D vert');
    l.MarkerFaceColor = l.Color; %to fill the symbols
    legend('Location','northwest')
    set(gca,'ColorOrderIndex',1)
    errorbar(ars,kinH2D,kinH2D_err, 'vertical', 'LineStyle', 'none');
    errorbar(ars,kinH2Dk1,kinH2Dk1_err, 'vertical', 'LineStyle', 'none');
    errorbar(ars,kinH3D,kinH3D_err, 'vertical', 'LineStyle', 'none');
    errorbar(ars,kinV,kinV_err, 'vertical', 'LineStyle', 'none');
    title(mcase)
    ylabel('Kinetic energy')
    xlabel('Aspect ratio $\Gamma$')
    xlim([1.5,3.25])
    ylim([0,0.04])
    grid on
    box on
    
    %kin/ar^2 vs ar
    figure
    set(gca,'ColorOrderIndex',1)
    l=plot(ars,kinH2D./ars.^2,'-s','DisplayName','2D hor');
    l.MarkerFaceColor = l.Color; %to fill the symbols
    hold on
    l=plot(ars,kinH2Dk1./ars.^2,'-s','DisplayName','2D hor k=1');
    l.MarkerFaceColor = l.Color; %to fill the symbols
    legend('Location','northwest')
    set(gca,'ColorOrderIndex',1)
    errorbar(ars,kinH2D./ars.^2,kinH2D_err./ars.^2, 'vertical', 'LineStyle', 'none');
    errorbar(ars,kinH2Dk1./ars.^2,kinH2Dk1_err./ars.^2, 'vertical', 'LineStyle', 'none');
    title(mcase)
    ylabel('Kinetic energy $/\Gamma^2$')
    xlabel('Aspect ratio $\Gamma$')
    xlim([1.5,3.25])
    ylim([0,5e-3])
    ylim
    grid on
    box on
    
    %total spectrum
    figure
    for i=1:n
        loglog(spec_k{i}./ars(i),spec_ETk{i}+spec_Evk{i})
        hold on
    end
    title(['Total kinetic energy spectrum ' mcase])
    xlabel('Horizontal wavenumber $K/\Gamma$')
    ylabel('$E(k_H)$')
    grid on
    box on
    
    %3D spectrum
    figure
    for i=1:n
        loglog(spec_k{i}./ars(i),spec_E3Dk{i}+spec_Evk{i})
        hold on
    end
    title(['3D kinetic energy spectrum ' mcase])
    xlabel('Horizontal wavenumber $K/\Gamma$')
    ylabel('$E(k_H)$')
    grid on
    box on
    
    %2D spectrum
    figure
    for i=1:n
        loglog(spec_k{i}./ars(i),spec_E2Dk{i})
        hold on
    end
    title(['2D kinetic energy spectrum ' mcase])
    xlabel('Horizontal wavenumber $K/\Gamma$')
    ylabel('$E(k_H)$')
    grid on
    box on
end


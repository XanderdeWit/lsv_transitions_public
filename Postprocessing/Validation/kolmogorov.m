%Display kolmogorov (and batchelor) scale analysis

kinDiss=h5read([gDataDir gSimname '/stafield_data.h5'],'/kindiss_mean')';
averagingTime=h5read([gDataDir gSimname '/stafield_master.h5'],'/averaging_time');
kinDiss=kinDiss./double(averagingTime);

pr=h5read([gDataDir gSimname '/stafield_master.h5'],'/Pr');
ra=h5read([gDataDir gSimname '/stafield_master.h5'],'/Ra');

kolmog=calculateKolmogorov(kinDiss,pr,ra);

nkolmog_x = d.dx./kolmog;
nkolmog_y = d.dy./kolmog;
nkolmog_z = d.dz./kolmog;

if gPlotting
    %Plot kinetic dissipation
    figure
    scatter(kinDiss,d.zms,10,'s','filled')
    title(['Kinetic dissipation ' gSimname])
    xlabel('Kinetic dissipation $\langle\tilde{\epsilon}\rangle$')
    ylabel('$z$')
    grid on
    box on

    %Plot kolmogorov scale
    figure
    scatter(kolmog,d.zms,10,'s','filled')
    title(['Kolmogorov scale ' gSimname])
    xlabel('Kolmogorov scale $\tilde{\eta}$')
    ylabel('$z$')
    grid on
    box on

    %Plots of number of kolmogs per cell
    figure
    hold on
    scatter(nkolmog_x,d.zms,10,'s','filled')
    scatter(nkolmog_y,d.zms,10,'s','filled')
    scatter(nkolmog_z,d.zms,10,'s','filled')
    hold off
    title(['Kolmogorov scales per cell ' gSimname])
    xlabel('Kolmogorov scales per cell $d\vec{r}/\eta$')
    ylabel('$z$')
    legend({'$dx/\eta$','$dy/\eta$','$dz/\eta$'},'Location','northeast')
    grid on
    box on
end

function eta=calculateKolmogorov(kinDiss,prandtl,rayleigh)
    eta=(prandtl/rayleigh).^(3/8).*kinDiss.^(-1/4); %NOTE: for Pr>1 fluid, look at Batchelor scale by multiplying with Pr^-1/2
end
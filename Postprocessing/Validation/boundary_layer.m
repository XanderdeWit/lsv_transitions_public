%Analyse boundary layer thickness

averagingTime=h5read([gDataDir gSimname '/stafield_master.h5'],'/averaging_time');

TeTotal=h5read([gDataDir gSimname '/stafield_data.h5'],'/Te_m1')';
Te2Total=h5read([gDataDir gSimname '/stafield_data.h5'],'/Te_m2')';
TeRMS=sqrt(Te2Total/double(averagingTime)-(TeTotal/double(averagingTime)).^2);

TeMean=TeTotal/double(averagingTime);

iz_midheight=round(d.nzm/2);

%Find boundary layers
[TRMS_TopZ,~,~] = findBL(TeRMS,iz_midheight,true,true,'Temperature RMS',d);
[TRMS_BottomZ,~,~] = findBL(TeRMS,iz_midheight,false,true,'Temperature RMS',d);

%thermal dissipation
TheDiss=h5read([gDataDir gSimname '/stafield_data.h5'],'/thediss_mean');
TheDiss=TheDiss/double(averagingTime);

[TD_TopZ,~,~] = findBL(TheDiss,iz_midheight,true,true,'Thermal dissipation',d);
[TD_BottomZ,~,~] = findBL(TheDiss,iz_midheight,false,true,'Thermal dissipation',d);


if gPlotting
    %Plot T RMS and T mean with BLs
    figure
    hold on
    scatter(TeRMS,d.zms,10,'s','filled','DisplayName','Temperature RMS $\sqrt{\langle (T - \langle T \rangle)^2 \rangle}$')
    scatter(TeMean,d.zms,10,'s','filled','DisplayName','Mean temperature $\langle T \rangle$')
    hold off
    yline(TRMS_TopZ,'--','LineWidth',2,'DisplayName','Boundary layer');
    legend
    yline(TRMS_BottomZ,'--','LineWidth',2);
    title(['Boundary layers ' gSimname])
    xlabel('$\sqrt{\langle (T - \langle T \rangle)^2 \rangle}$, $\langle T \rangle$')
    ylabel('$z$')
    grid on
    box on
    
    %Plot Thermal dissipation with BLs
    figure
    scatter(TheDiss,d.zms,10,'s','filled')
    yline(TD_TopZ,'--','LineWidth',2,'DisplayName','Boundary layer');
    yline(TD_BottomZ,'--','LineWidth',2);
    title(['Boundary layers ' gSimname])
    xlabel('Thermal dissipation')
    ylabel('$z$')
    grid on
    box on
end


function [zmax,thickness,nthickness]=findBL(rms,izhalf,upper,print,qname,d)
    if upper
        [~,izmax] = max(rms(izhalf:end));
        izmax = izmax + izhalf - 1;
        zmax = d.zms(izmax);
        thickness = d.H - zmax;
        nthickness = d.nzm - izmax + 1;
    else
        [~,izmax]=max(rms(1:izhalf));
        zmax = d.zms(izmax);
        thickness = zmax;
        nthickness = izmax;
    end
    
    if print
        if upper
            disp(['Examinging upper BL of ' qname])
        else
            disp(['Examinging lower BL of ' qname])
        end
        
        disp(['Found maximum ' qname ' at z=' num2str(zmax)])
        disp(['Such that estimated BL thickness is ' num2str(thickness)])
        disp(['And there are ' num2str(nthickness) ' grid cells withing this BL'])
    end
end
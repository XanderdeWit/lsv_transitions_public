function plot3DVertCross(xs,ys,zs,values,valueTitle,valueLim,plotStyle,cm,axisImage)

%initial location of cross section
yicross=floor(length(ys)/2);

figure

crossplot=drawCross(xs,zs,valuemeshFromValues(values,yicross),valueLim,plotStyle,cm,axisImage);

updateTitle(valueTitle,ys,yicross)

slider = uicontrol('style','slider','min',1,'max',length(ys),'value',yicross,'position',[10 10 300 10]);
addlistener(slider,'ContinuousValueChange',@(hObject, event) updateplot(crossplot,floor(get(slider,'Value')),ys,values,valueTitle,plotStyle));
end

function updateplot(crossplot,yicross,ys,values,valueTitle,plotStyle)
    if strcmp(plotStyle,'pcolor')
        crossplot.CData=valuemeshFromValues(values,yicross);
    else
        crossplot.ZData=valuemeshFromValues(values,yicross);
    end
    updateTitle(valueTitle,ys,yicross)
end

function updateTitle(valueTitle,ys,yicross)
    title(['Cross section ' valueTitle ' for y=' num2str(ys(yicross),2)])
end

function valuemesh=valuemeshFromValues(values,yicross)
    valuemesh=squeeze(values(:,yicross,:))';
end
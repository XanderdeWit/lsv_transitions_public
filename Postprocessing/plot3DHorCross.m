function plot3DHorCross(xs,ys,zs,values,valueTitle,valueLim,plotStyle,cm,axisImage)

%initial location of cross section
zicross=floor(length(zs)/2);

figure

crossplot=drawCross(xs,ys,valuemeshFromValues(values,zicross),valueLim,plotStyle,cm,axisImage);

updateTitle(valueTitle,zs,zicross)

slider = uicontrol('style','slider','min',1,'max',length(zs),'value',zicross,'position',[10 10 300 10]);
addlistener(slider,'ContinuousValueChange',@(hObject, event) updateplot(crossplot,floor(get(slider,'Value')),zs,values,valueTitle,plotStyle));
end

function updateplot(crossplot,zicross,zs,values,valueTitle,plotStyle)
    if strcmp(plotStyle,'pcolor')
        crossplot.CData=valuemeshFromValues(values,zicross);
    else
        crossplot.ZData=valuemeshFromValues(values,zicross);
    end
    updateTitle(valueTitle,zs,zicross)
end

function updateTitle(valueTitle,zs,zicross)
    title(['Cross section ' valueTitle ' for z=' num2str(zs(zicross),2)])
end

function valuemesh=valuemeshFromValues(values,zicross)
    valuemesh=squeeze(values(:,:,zicross))';
end
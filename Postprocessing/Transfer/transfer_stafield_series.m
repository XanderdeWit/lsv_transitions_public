
series=gSeries; %NB: directory of this series, appended by '/'
%series='series2/upper/';
%series='series2/lower/';

%find cases
dirs=dir([gDataDir series]);
dirs=dirs(3:end);

ncases=length(dirs);

%all_simnames={'series1/case03','series1/case04','series1/case05','series1/case056','series1/case057','series1/case06','series1/case07','series1/case08'};
%ncases=length(all_simnames);

if strcmp(series,'series1/')
    i_transition_high=8;
    i_transition_low=7;
elseif strcmp(series,'series2/upper/')
    i_transition_high=8;
    i_transition_low=9;
elseif strcmp(series,'series2/lower/')
    i_transition_high=2;
    i_transition_low=3;
else
    i_transition_high=-1; %set to -1 if you don't want to plot a red line
    i_transition_low=-1; %set to -1 if you don't want to plot a green line
end

bt_k1=cell(1,ncases);
bc_k1=cell(1,ncases);

ras=zeros(1,ncases);

m_transfer_stafield_together_gPlotting=gPlotting; gPlotting=false;
for i=1:ncases
    
    %gSimname=char(all_simnames(i));
    gSimname=[series dirs(i).name];
    
    transfer_stafield;
    
    bt_k1(i)={transQK_BT(1:maxQ,1)};
    bc_k1(i)={transQK_BC(1:maxQ,1)};
    
    ras(i)=h5read([gDataDir gSimname '/stafield_master.h5'],'/Ra');
end
gPlotting=m_transfer_stafield_together_gPlotting;

kmask_correction;

if gPlotting
    
    cm=pink(350);
    %cm=bone(300);
    %cm=copper(256);
    cm=cm(1:256,:);
    if ~strcmp(series,'series1/')
        cm=flip(cm,1);
    end
    ra_low=ras(1);
    ra_high=ras(end);
    
    % Kmask un-corrected
    figure
    for i=1:ncases
        plot(bt_k1{i},'LineWidth',2,'Color',cm(1+round(255*(ras(i)-ra_low)/(ra_high-ra_low)),:),...
         'DisplayName',['$\textrm{Ra}=' num2str(ras(i)/10^(floor(log10(ras(i))))) '\times 10^' num2str(floor(log10(ras(i)))) '$'])
        hold on
    end
    xlabel('$Q$')
    ylabel('$-\bar{u}_{K=1} \cdot \bar{u} \cdot \nabla \bar{u}$')
    xlim([1,maxQ])
    %legend
    grid on
    box on
    colormap(cm)
    colorbar
    caxis([ra_low, ra_high])
    colorbar('TickLabelInterpreter','latex')
    if i_transition_low > 0
         plot(bt_k1{i_transition_low},'LineWidth',2,'Color','#80e000')
    end
    if i_transition_high > 0
         plot(bt_k1{i_transition_high},'LineWidth',2,'Color','r')
    end
    
    figure
    for i=1:ncases
        plot(bc_k1{i},'LineWidth',2,'Color',cm(1+round(255*(ras(i)-ra_low)/(ra_high-ra_low)),:),...
         'DisplayName',['$\textrm{Ra}=' num2str(ras(i)/10^(floor(log10(ras(i))))) '\times 10^' num2str(floor(log10(ras(i)))) '$'])
        hold on
    end
    xlabel('$Q$')
    ylabel('$-\bar{u}_{K=1} \cdot u^\prime \cdot \nabla u^\prime$')
    xlim([1,maxQ])
    %legend
    grid on
    box on
    colormap(cm)
    colorbar
    caxis([ra_low, ra_high])
    colorbar('TickLabelInterpreter','latex')
    if i_transition_low > 0
         plot(bc_k1{i_transition_low},'LineWidth',2,'Color','#80e000')
    end
    if i_transition_high > 0
         plot(bc_k1{i_transition_high},'LineWidth',2,'Color','r')
    end
    
    % Kmask corrected
    figure
    for i=1:ncases
        plot(bt_k1{i}'.*correction_K(2:maxQ+1),'LineWidth',2,'Color',cm(1+round(255*(ras(i)-ra_low)/(ra_high-ra_low)),:),...
         'DisplayName',['$\textrm{Ra}=' num2str(ras(i)/10^(floor(log10(ras(i))))) '\times 10^' num2str(floor(log10(ras(i)))) '$'])
        hold on
    end
    xlabel('$Q$')
    ylabel('$-\bar{u}_{K=1} \cdot \bar{u} \cdot \nabla \bar{u}$')
    xlim([1,maxQ])
    %legend
    grid on
    box on
    colormap(cm)
    colorbar
    caxis([ra_low, ra_high])
    colorbar('TickLabelInterpreter','latex')
    if i_transition_low > 0
         plot(bt_k1{i_transition_low}'.*correction_K(2:maxQ+1),'LineWidth',2,'Color','#80e000')
    end
    if i_transition_high > 0
         plot(bt_k1{i_transition_high}'.*correction_K(2:maxQ+1),'LineWidth',2,'Color','r')
    end
    
    figure
    for i=1:ncases
        plot(bc_k1{i}'.*correction_K(2:maxQ+1),'LineWidth',2,'Color',cm(1+round(255*(ras(i)-ra_low)/(ra_high-ra_low)),:),...
         'DisplayName',['$\textrm{Ra}=' num2str(ras(i)/10^(floor(log10(ras(i))))) '\times 10^' num2str(floor(log10(ras(i)))) '$'])
        hold on
    end
    xlabel('$Q$')
    ylabel('$-\bar{u}_{K=1} \cdot u^\prime \cdot \nabla u^\prime$')
    xlim([1,maxQ])
    %legend
    grid on
    box on
    colormap(cm)
    colorbar
    caxis([ra_low, ra_high])
    colorbar('TickLabelInterpreter','latex')
    if i_transition_low > 0
         plot(bc_k1{i_transition_low}'.*correction_K(2:maxQ+1),'LineWidth',2,'Color','#80e000')
    end
    if i_transition_high > 0
         plot(bc_k1{i_transition_high}'.*correction_K(2:maxQ+1),'LineWidth',2,'Color','r')
    end
end
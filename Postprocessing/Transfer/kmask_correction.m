%calculate kmask
maxK_mask=min(d.nxm,d.nym)/4-1;
kx=[0:d.nxm/2,-d.nxm/2+1:-1];
ky=[0:d.nym/2,-d.nym/2+1:-1];
Kmask=zeros(length(kx),length(ky),maxK_mask+1);
for ikx=1:length(kx)
    for iky=1:length(ky)
        mkx=kx(ikx);
        mky=ky(iky);
        %bin all wavenumbers with kH <= sqrt(kx^2+ky^2) < kH+1 together
        mK=floor(sqrt(mkx^2+mky^2));
        if mK<=maxK_mask
            Kmask(ikx,iky,mK+1)=1;
        end
    end
end

nmodes=squeeze(sum(Kmask,[1,2]))';

nmodes_exact=((0:maxK_mask)+0.5)*2*pi;

correction_K=nmodes_exact./nmodes;

correction_KQ=correction_K'.*correction_K;

if false
    figure
    %subplot(2,1,1);
    tiledlayout(2,1, 'TileSpacing', 'compact'); 
    nexttile
    
    plot(0:maxK_mask,nmodes,'LineWidth',2,'DisplayName','$\#\textrm{modes}(K)$')
    hold on
    plot(0:maxK_mask,nmodes_exact,'--','LineWidth',2,'DisplayName','$2\pi (K+1/2)$')
    grid on
    xlim([1,maxK_mask])
    legend('Location','northwest')
    ylabel('Number of modes')
    %xlabel('$K$')
    set(gca,'XTickLabel',[]);

    %subplot(2,1,2);
    nexttile
    
    plot(0:maxK_mask,correction_K,'-o')
    hold on
    plot(0:maxK_mask,(0:maxK_mask)*0+1,'--','LineWidth',2)
    grid on
    xlim([1,maxK_mask])
    ylim([0.8,1.2])
    xlabel('$K$')
    ylabel('Correction factor')
    

    figure
    imagesc(correction_KQ,[0.5,1.5])
    set(gca,'YDir','normal')
    colormap(gMyCM)
    colorbar
end
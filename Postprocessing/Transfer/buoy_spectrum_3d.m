dens=h5read([gDataDir gSimname '/continua_dens.h5'],'/dens');
dens=d.trim3D(dens);

Vz=h5read([gDataDir gSimname '/continua_q3.h5'],'/Vz');
Vz=d.trim3D(Vz);

%move Vz to cell centre
Vz(:,:,end+1)=zeros(d.nxm,d.nym); Vz=d.faceToCell(Vz,3);

%local convection
VzT=Vz.*dens;

%calculate spectrum
k=0:min(d.nxm,d.nym)/2;
VzTk=zeros(1,length(k));
for iz=1:d.nzm
    mVzT=VzT(:,:,iz);
    
    mVzTkk=fft2(mVzT)./(d.nxm*d.nym);
    
    mVzTk=zeros(1,length(k));
    
    kx=[0:d.nxm/2,-d.nxm/2+1:-1];
    ky=[0:d.nym/2,-d.nym/2+1:-1];
    for ikx=1:d.nxm
        for iky=1:d.nym
            mkx=kx(ikx);
            mky=ky(iky);
            %bin all wavenumbers with kH <= sqrt(kx^2+ky^2) < kH+1 together
            mk=floor(sqrt(mkx^2+mky^2));
            if mk<length(k)
                %take absolute value of the fourier component
                mVzTk(1+mk)=mVzTk(1+mk)+abs(mVzTkk(ikx,iky));
            end
        end
    end
    VzTk=VzTk+mVzTk*d.g3rm(iz)/d.nzm;
end

if gPlotting
    figure
    loglog(k(2:end),VzTk(2:end))
    title(['Buoyent forcing spectrum ' gSimname])
    xlabel('Horizontal wavenumber $k_H$')
    ylabel('$\langle v_z T \rangle(k_H)$')
    grid on
    box on
end
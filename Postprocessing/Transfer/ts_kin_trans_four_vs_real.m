m_ts_kin_trans_four_vs_real_gPlotting=gPlotting; gPlotting=false;
ts_kin_trans_real;
ts_kin_trans_four;
gPlotting=m_ts_kin_trans_four_vs_real_gPlotting;

tsTransNoW=interpTs(tsTransNoW,tsBTK1.Time);
tsTransBC=interpTs(tsTransBC,tsBCK1.Time);
tsDiss2D=interpTs(tsDiss2D,tsDiss2DNoW.Time);
tsFtot=interpTs(tsFtot,tsBC.Time);

if gPlotting
    %plot parameters
    mm_time=100;
    figure
    scatter(tsBTK1.Time,movmean(tsBTK1.Data,mm_time),10,'s','filled','DisplayName',[tsBTK1.Name ' Fourier'])
    hold on
    scatter(tsTransNoW.Time,movmean(tsTransNoW.Data,mm_time),10,'s','filled','DisplayName',[tsTransNoW.Name ' real'])
    legend
    set(gca,'ColorOrderIndex',1)
    scatter(tsBTK1.Time,tsBTK1.Data,10,'s','filled','MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsTransNoW.Time,tsTransNoW.Data,10,'s','filled','MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
    title(['Kinetic transport ' gSimname])
    xlabel('Convective time')
    ylabel('Forcing')
    %if gTstart ~= -1
    %    xline(gTstart,'--r');
    %end
    grid on
    box on
    
    mm_time=100;
    figure
    scatter(tsBCK1.Time,movmean(tsBCK1.Data,mm_time),10,'s','filled','DisplayName',[tsBCK1.Name ' Fourier'])
    hold on
    scatter(tsTransBC.Time,movmean(tsTransBC.Data,mm_time),10,'s','filled','DisplayName',[tsTransBC.Name ' real'])
    legend
    set(gca,'ColorOrderIndex',1)
    scatter(tsBCK1.Time,tsBCK1.Data,10,'s','filled','MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsTransBC.Time,tsTransBC.Data,10,'s','filled','MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
    title(['Kinetic transport ' gSimname])
    xlabel('Convective time')
    ylabel('Forcing')
    %if gTstart ~= -1
    %    xline(gTstart,'--r');
    %end
    grid on
    box on
    
    mm_time=100;
    figure
    scatter(tsDiss2DNoW.Time,movmean(tsDiss2DNoW.Data,mm_time),10,'s','filled','DisplayName',[tsDiss2DNoW.Name ' Fourier'])
    hold on
    scatter(tsDiss2D.Time,movmean(tsDiss2D.Data,mm_time),10,'s','filled','DisplayName',[tsDiss2D.Name ' real'])
    legend
    set(gca,'ColorOrderIndex',1)
    scatter(tsDiss2DNoW.Time,tsDiss2DNoW.Data,10,'s','filled','MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsDiss2D.Time,tsDiss2D.Data,10,'s','filled','MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
    title(['Kinetic transport ' gSimname])
    xlabel('Convective time')
    ylabel('Forcing')
    %if gTstart ~= -1
    %    xline(gTstart,'--r');
    %end
    grid on
    box on
    
    mm_time=100;
    figure
    scatter(tsBC.Time,movmean(tsBC.Data,mm_time),10,'s','filled','DisplayName',[tsBC.Name ' Fourier'])
    hold on
    scatter(tsFtot.Time,movmean(tsFtot.Data,mm_time),10,'s','filled','DisplayName',[tsFtot.Name ' real'])
    legend
    set(gca,'ColorOrderIndex',1)
    scatter(tsBC.Time,tsBC.Data,10,'s','filled','MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsFtot.Time,tsFtot.Data,10,'s','filled','MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
    title(['Kinetic transport ' gSimname])
    xlabel('Convective time')
    ylabel('Forcing')
    %if gTstart ~= -1
    %    xline(gTstart,'--r');
    %end
    grid on
    box on
    
end
%read velocity field
Vx=h5read([gDataDir gSimname '/continua_q1.h5'],'/Vth'); Vx=d.trim3D(Vx);
Vy=h5read([gDataDir gSimname '/continua_q2.h5'],'/Vr'); Vy=d.trim3D(Vy);
Vz=h5read([gDataDir gSimname '/continua_q3.h5'],'/Vz'); Vz=d.trim3D(Vz);

pr=h5read([gDataDir gSimname '/stafield_master.h5'],'/Pr');
ra=h5read([gDataDir gSimname '/stafield_master.h5'],'/Ra');

%move all velocities to cell center
Vx=d.faceToCell(d.makePeriodic(Vx,1),1);
Vy=d.faceToCell(d.makePeriodic(Vy,2),2);
Vz(:,:,end+1)=zeros(d.nxm,d.nym); Vz=d.faceToCell(Vz,3);

%calculate z-averaged velocity
Vx_zave=mean(Vx.*reshape(d.g3rm,1,1,d.nzm),3);
Vy_zave=mean(Vy.*reshape(d.g3rm,1,1,d.nzm),3);
Vz_zave=mean(Vz.*reshape(d.g3rm,1,1,d.nzm),3);

%calculate Laplacian
lap_Vx_zave=4*del2(Vx_zave,d.dy,d.dx);
lap_Vy_zave=4*del2(Vy_zave,d.dy,d.dx);
lap_Vz_zave=4*del2(Vz_zave,d.dy,d.dx);

%calculate dissipation
dissipation_2d_xy = sqrt(pr/ra)*(Vx_zave.*lap_Vx_zave + Vy_zave.*lap_Vy_zave + Vz_zave.*lap_Vz_zave);
dissipation_2d = mean(dissipation_2d_xy,'all')

dissipation_2d_now_xy = sqrt(pr/ra)*(Vx_zave.*lap_Vx_zave + Vy_zave.*lap_Vy_zave);
dissipation_2d_now = mean(dissipation_2d_now_xy,'all')

if gPlotting
    figure
    lims=[-1,1]*0.5*max(abs(dissipation_2d_xy),[],'all');
    drawCross(d.xms,d.yms,dissipation_2d_xy',lims,'pcolor',gMyCM,true);
    title(['Dissipation 2D flow ' gSimname])
    
    figure
    lims=[-1,1]*0.5*max(abs(dissipation_2d_now_xy),[],'all');
    drawCross(d.xms,d.yms,dissipation_2d_now_xy',lims,'pcolor',gMyCM,true);
    title(['Dissipation 2D flow (no w) ' gSimname])
end

% v5: read input from file implemented

disp('preparing...')
tic

param=load('param.in');
% maxK, maxQ define largest K and Q values to be included in the energy transfer analysis
maxK=param(1);
maxQ=param(2);
% size of domain
Lx=param(3);
Ly=param(4);
% 1 for 3DRBcart113 and !=1 for Multigrid
code=param(5);
% grid size
dinfo=h5info('continua_q1.h5','/Vth');
resol=dinfo.Dataspace.Size;
nx=resol(1)-1;
ny=resol(2)-1;
nz=resol(3);

% define x,y wavenumbers
kLx=2*pi/Lx;
kLy=2*pi/Ly;

% construct horizontal grid
dx=Lx/nx;
dy=Ly/ny;
x=dx*(ones(nx,1)*(1:1:nx)-0.5);
y=dy*((1:1:ny)'*ones(1,ny)-0.5);

% read vertical grid
if code==1
  a=load('axicor.out');
  zm=a(1:(nz+1),2); % edge/face
  zc=a(1:nz,3);     % center
  clear a
else
  zm=h5read('field_gridc.h5','/zf'); % edge/face
  zc=h5read('field_gridc.h5','/zc'); % center
  zm(end+1)=1.0;
end
cellheight=zm(2:(nz+1))-zm(1:nz);
clear zm

% the required fields
transferQK=zeros(maxQ,maxK,nz);
transferQK_total=zeros(maxQ,maxK);
transferKQ=zeros(maxQ,maxK,nz);
transferKQ_total=zeros(maxQ,maxK);
symm_break=zeros(maxQ,maxK,nz);
symm_break_total=zeros(maxQ,maxK);

% prepare wavenumbers - note that I use integer indices, later to be converted to actual wavenumbers 2*pi/L
kx=(1:1:nx)'*ones(1,nx)-nx/2-1;
ky=ones(ny,1)*(1:1:ny)-ny/2-1;
kh=ceil(sqrt(kx.^2+ky.^2));

toc

% ready to make a big loop!
disp('big loop')
poolobj = parpool(24);
tic
parfor m=1:nz

  tStart = tic;

  % for z derivative
  if m==1
    dzi=zc(m);
  else
    dzi=zc(m)-zc(m-1);
  end
  if m==nz
    dzip=1-zc(m);
  else
    dzip=zc(m+1)-zc(m);
  end
  
  % read slabs and interpolate to cell center - "up", "middle" and "down" slabs
  if m==1
    % "down" slab is zero for now as it is the bottom plate
    q1=h5read('continua_q1.h5','/Vth',[1 1 m],[nx ny 2]);
    u=zeros(nx,ny,2);
    u(1:nx-1,:,:)=0.5*(q1(1:nx-1,:,:)+q1(2:nx,:,:));
    u(nx,:,:)=0.5*(q1(1,:,:)+q1(nx,:,:));
    u_u=u(:,:,2);
    u_m=u(:,:,1);
    u_d=zeros(nx,ny);
    q2=h5read('continua_q2.h5','/Vr',[1 1 m],[nx ny 2]);
    v=zeros(nx,ny,2);
    v(:,1:ny-1,:)=0.5*(q2(:,1:ny-1,:)+q2(:,2:ny,:));
    v(:,ny,:)=0.5*(q2(:,1,:)+q2(:,ny,:));
    v_u=v(:,:,2);
    v_m=v(:,:,1);
    v_d=zeros(nx,ny);
    q3=h5read('continua_q3.h5','/Vz',[1 1 m],[nx ny 3]);
    w=0.5*(q3(:,:,1:end-1)+q3(:,:,2:end));
    w_u=w(:,:,2);
    w_m=w(:,:,1);
    w_d=zeros(nx,ny);
  elseif m==nz
    % "up" slab is zero for now as it is the top plate
    q1=h5read('continua_q1.h5','/Vth',[1 1 m-1],[nx ny 2]);
    u=zeros(nx,ny,2);
    u(1:nx-1,:,:)=0.5*(q1(1:nx-1,:,:)+q1(2:nx,:,:));
    u(nx,:,:)=0.5*(q1(1,:,:)+q1(nx,:,:));
    u_u=zeros(nx,ny);
    u_m=u(:,:,2);
    u_d=u(:,:,1);
    q2=h5read('continua_q2.h5','/Vr',[1 1 m-1],[nx ny 2]);
    v=zeros(nx,ny,2);
    v(:,1:ny-1,:)=0.5*(q2(:,1:ny-1,:)+q2(:,2:ny,:));
    v(:,ny,:)=0.5*(q2(:,1,:)+q2(:,ny,:));
    v_u=zeros(nx,ny);
    v_m=v(:,:,2);
    v_d=v(:,:,1);
    q3=h5read('continua_q3.h5','/Vz',[1 1 m-1],[nx ny 2]);
    w_u=zeros(nx,ny);
    w_m=.5*q3(:,:,2);
    w_d=0.5*(q3(:,:,1)+q3(:,:,2));
  else
    q1=h5read('continua_q1.h5','/Vth',[1 1 m-1],[nx ny 3]);
    u=zeros(nx,ny,3);
    u(1:nx-1,:,:)=0.5*(q1(1:nx-1,:,:)+q1(2:nx,:,:));
    u(nx,:,:)=0.5*(q1(1,:,:)+q1(nx,:,:));
    u_u=u(:,:,3);
    u_m=u(:,:,2);
    u_d=u(:,:,1);
    q2=h5read('continua_q2.h5','/Vr',[1 1 m-1],[nx ny 3]);
    v=zeros(nx,ny,3);
    v(:,1:ny-1,:)=0.5*(q2(:,1:ny-1,:)+q2(:,2:ny,:));
    v(:,ny,:)=0.5*(q2(:,1,:)+q2(:,ny,:));
    v_u=v(:,:,3);
    v_m=v(:,:,2);
    v_d=v(:,:,1);
    if m<(nz-1)
      q3=h5read('continua_q3.h5','/Vz',[1 1 m-1],[nx ny 4]);
      w=0.5*(q3(:,:,1:end-1)+q3(:,:,2:end));
      w_u=w(:,:,3);
    else % if m==(nz-1)
      q3=h5read('continua_q3.h5','/Vz',[1 1 m-1],[nx ny 3]);
      w=0.5*(q3(:,:,1:end-1)+q3(:,:,2:end));
      w_u=.5*q3(:,:,3);
    end
    w_m=w(:,:,2);
    w_d=w(:,:,1);
  end
    
  % calculate Fourier-transformed velocity fields
  Fu_u=fftshift(fft2(u_u));
  Fv_u=fftshift(fft2(v_u));
  Fw_u=fftshift(fft2(w_u));
  Fu_m=fftshift(fft2(u_m));
  Fv_m=fftshift(fft2(v_m));
  Fw_m=fftshift(fft2(w_m));
  Fu_d=fftshift(fft2(u_d));
  Fv_d=fftshift(fft2(v_d));
  Fw_d=fftshift(fft2(w_d));

  % loop over wavenumbers K and Q
  for Q=1:maxQ
    for K=1:maxK
      [ixK,iyK]=find(kh==K);
      [ixQ,iyQ]=find(kh==Q);
      for kk=1:size(ixK,1)
        for qq=1:size(ixQ,1)
          k=ixK(kk);
          l=iyK(kk);
          r=ixQ(qq);
          s=iyQ(qq);
          kplusr=nx/2+1+kx(k,l)+kx(r,s);
          lpluss=ny/2+1+ky(k,l)+ky(r,s);
          dursdz=(Fu_u(r,s)*dzi^2 + Fu_m(r,s)*(dzip^2-dzi^2) - Fu_d(r,s)*dzip^2)/(dzip*dzi*(dzip+dzi));
          dvrsdz=(Fv_u(r,s)*dzi^2 + Fv_m(r,s)*(dzip^2-dzi^2) - Fv_d(r,s)*dzip^2)/(dzip*dzi*(dzip+dzi));
          dwrsdz=(Fw_u(r,s)*dzi^2 + Fw_m(r,s)*(dzip^2-dzi^2) - Fw_d(r,s)*dzip^2)/(dzip*dzi*(dzip+dzi));
          dukldz=(Fu_u(k,l)*dzi^2 + Fu_m(k,l)*(dzip^2-dzi^2) - Fu_d(k,l)*dzip^2)/(dzip*dzi*(dzip+dzi));
          dvkldz=(Fv_u(k,l)*dzi^2 + Fv_m(k,l)*(dzip^2-dzi^2) - Fv_d(k,l)*dzip^2)/(dzip*dzi*(dzip+dzi));
          dwkldz=(Fw_u(k,l)*dzi^2 + Fw_m(k,l)*(dzip^2-dzi^2) - Fw_d(k,l)*dzip^2)/(dzip*dzi*(dzip+dzi));
          dwkrlsdz=(Fw_u(kplusr,lpluss)*dzi^2 + Fw_m(kplusr,lpluss)*(dzip^2-dzi^2) - Fw_d(kplusr,lpluss)*dzip^2)/(dzip*dzi*(dzip+dzi));
          
          transferQK(Q,K,m)=transferQK(Q,K,m)- ...
            real(1i*(Fu_m(k,l)*Fu_m(r,s) + Fv_m(k,l)*Fv_m(r,s) + Fw_m(k,l)*Fw_m(r,s)) * ... 
                 (kx(r,s)*kLx*conj(Fu_m(kplusr,lpluss)) + ky(r,s)*kLy*conj(Fv_m(kplusr,lpluss)))) - ...
            real((Fu_m(k,l)*dursdz + Fv_m(k,l)*dvrsdz + Fw_m(k,l)*dwrsdz)*conj(Fw_m(kplusr,lpluss)));
          
          transferKQ(Q,K,m)=transferKQ(Q,K,m)- ...
            real(1i*(Fu_m(k,l)*Fu_m(r,s) + Fv_m(k,l)*Fv_m(r,s) + Fw_m(k,l)*Fw_m(r,s)) * ... 
                 (kx(k,l)*kLx*conj(Fu_m(kplusr,lpluss)) + ky(k,l)*kLy*conj(Fv_m(kplusr,lpluss)))) - ...
            real((dukldz*Fu_m(r,s) + dvkldz*Fv_m(r,s) + dwkldz*Fw_m(r,s))*conj(Fw_m(kplusr,lpluss)));
          
          symm_break(Q,K,m)=symm_break(Q,K,m)- ...
            real((dukldz*Fu_m(r,s) + dvkldz*Fv_m(r,s) + dwkldz*Fw_m(r,s) + ...
                  Fu_m(k,l)*dursdz + Fv_m(k,l)*dvrsdz + Fw_m(k,l)*dwrsdz) * conj(Fw_m(kplusr,lpluss))) - ...
            real((Fu_m(k,l)*Fu_m(r,s) + Fv_m(k,l)*Fv_m(r,s) + Fw_m(k,l)*Fw_m(r,s)) * conj(dwkrlsdz));
        end
      end
    end
  end
  
  tElapsed = toc(tStart);

  fprintf('%i %f\n',m,tElapsed)
end
averageTime = toc/nz;
fprintf('Average iteration time  %f\n',averageTime)
toc
delete(poolobj)

for m=1:nz
  transferQK_total=transferQK_total+transferQK(:,:,m)*cellheight(m)*dx*dy;
  transferKQ_total=transferKQ_total+transferKQ(:,:,m)*cellheight(m)*dx*dy;
  symm_break_total=symm_break_total+symm_break(:,:,m)*cellheight(m)*dx*dy;
end
  
save('transfer.mat',...
                    'transferQK','transferQK_total',...
                    'transferKQ','transferKQ_total',...
                    'symm_break','symm_break_total','zc')
disp('Saved to transfer.mat')

exit

% maxK, maxQ define largest K value to be included in the energy transfer analysis
maxK=min(d.nxm,d.nym)/4-1;

%read velocity field
q1=h5read([gDataDir gSimname '/continua_q1.h5'],'/Vth'); q1=d.trim3D(q1);
q2=h5read([gDataDir gSimname '/continua_q2.h5'],'/Vr'); q2=d.trim3D(q2);
q3=h5read([gDataDir gSimname '/continua_q3.h5'],'/Vz'); q3=d.trim3D(q3);

%move all velocities to cell center
q1=d.faceToCell(d.makePeriodic(q1,1),1);
q2=d.faceToCell(d.makePeriodic(q2,2),2);
q3(:,:,end+1)=zeros(d.nxm,d.nym); q3=d.faceToCell(q3,3);

kx=[0:d.nxm/2,-d.nxm/2+1:-1];
ky=[0:d.nym/2,-d.nym/2+1:-1];

Kmask=zeros(length(kx),length(ky),maxK+1);
for ikx=1:length(kx)
    for iky=1:length(ky)
        mkx=kx(ikx);
        mky=ky(iky);
        %bin all wavenumbers with kH <= sqrt(kx^2+ky^2) < kH+1 together
        mK=floor(sqrt(mkx^2+mky^2));
        if mK<=maxK
            Kmask(ikx,iky,mK+1)=1;
        end
    end
end

%tic
trans_all=zeros(maxK+1,maxK+1,d.nzm);

%DEFINE m=1 VALUES
u_K=getFilteredVelocity(q1(:,:,1),Kmask);
v_K=getFilteredVelocity(q2(:,:,1),Kmask);
w_K=getFilteredVelocity(q3(:,:,1),Kmask);

%down values are zero
u_d_K=zeros(d.nxm,d.nym,maxK+1);
v_d_K=zeros(d.nxm,d.nym,maxK+1);
w_d_K=zeros(d.nxm,d.nym,maxK+1);

for m=2:d.nzm %can't do this in parallel
tic
    %calculate up values
    if m==d.nzm
        %up values are zero
        u_u_K=zeros(d.nxm,d.nym,maxK+1);
        v_u_K=zeros(d.nxm,d.nym,maxK+1);
        w_u_K=zeros(d.nxm,d.nym,maxK+1);
    else
        u_u_K=getFilteredVelocity(q1(:,:,m+1),Kmask);
        v_u_K=getFilteredVelocity(q2(:,:,m+1),Kmask);
        w_u_K=getFilteredVelocity(q3(:,:,m+1),Kmask);
    end
    
    %get regular velocities
    u=q1(:,:,m);
    v=q2(:,:,m);
    w=q3(:,:,m);
    
    %calculate derivatives
    du_Qdx=getDerivativeXY(u_K,d.dx,1,d);
    du_Qdy=getDerivativeXY(u_K,d.dy,2,d);
    du_Qdz=getDerivativeZ(u_u_K,u_d_K,d.dz(m));
    dv_Qdx=getDerivativeXY(v_K,d.dx,1,d);
    dv_Qdy=getDerivativeXY(v_K,d.dy,2,d);
    dv_Qdz=getDerivativeZ(v_u_K,v_d_K,d.dz(m));
    dw_Qdx=getDerivativeXY(w_K,d.dx,1,d);
    dw_Qdy=getDerivativeXY(w_K,d.dy,2,d);
    dw_Qdz=getDerivativeZ(w_u_K,w_d_K,d.dz(m));
    
    %calculate transport from Q to K, u_K * u * grad u_Q
    %NB: neglecting d/dz terms!
    trans=zeros(maxK+1,maxK+1);
    for iQ=1:size(trans,2) %loop here to keep it memory-friendly
        mtrans=u_K(:,:,:).*u(:,:).*du_Qdx(:,:,iQ) +...
            u_K(:,:,:).*v(:,:).*du_Qdy(:,:,iQ) +...
            u_K(:,:,:).*w(:,:).*du_Qdz(:,:,iQ) +...
            v_K(:,:,:).*u(:,:).*dv_Qdx(:,:,iQ) +...
            v_K(:,:,:).*v(:,:).*dv_Qdy(:,:,iQ) +...
            v_K(:,:,:).*w(:,:).*dv_Qdz(:,:,iQ) +...
            w_K(:,:,:).*u(:,:).*dw_Qdx(:,:,iQ) +...
            w_K(:,:,:).*v(:,:).*dw_Qdy(:,:,iQ) +...
            w_K(:,:,:).*w(:,:).*dw_Qdz(:,:,iQ);
        trans(:,iQ)=squeeze(mean(mtrans,[1,2]));
    end
    
    %up to middle, middle to down
    %only u_d_K,v_d_K,w_d_K and u_K,v_K,w_K are used, so only copy those
    u_d_K=u_K; v_d_K=v_K; w_d_K=w_K;
    u_K=u_u_K; v_K=v_u_K; w_K=w_u_K;
    
    %save
    trans_all(:,:,m)=trans;
toc
end
%toc

trans_mean=mean(trans_all .* reshape(d.g3rm,1,1,d.nzm),3);

figure
imagesc(-trans_mean',[-3e-3,3e-3])
set(gca,'YDir','normal')
colormap(gMyCM)
colorbar
xlabel('$K$')
ylabel('$Q$')

function val_K=getFilteredVelocity(val,Kmask)
    %caltulate fourier transform
    val_k=fft2(val);
    
    %filter in fourier space
    val_K_k=val_k.*Kmask;

    %back to real space
    val_K=zeros(size(val,1),size(val,2),size(Kmask,3));
    for iK=1:size(val_K,3)
        val_K(:,:,iK)=ifft2(val_K_k(:,:,iK));
    end
end

function deriv=getDerivativeXY(y,dx,axis,d)
    deriv=circshift(diff(d.makePeriodic(y,axis),1,axis),axis); %face value
    deriv=d.faceToCell(d.makePeriodic(deriv,axis),axis); %cell value
    deriv=deriv./dx;
end

function deriv=getDerivativeZ(y_u,y_d,dz)
    deriv=0.5*(y_u-y_d); %central difference
    deriv=deriv./dz;
end

%calculate transfer map from 3D snapshot

% maxK, maxQ define largest K and Q values to be included in the energy transfer analysis
maxK=min(d.nxm,d.nym)/4-1;
maxQ=maxK;

%read z-averaged velocity field
q1=h5read([gDataDir gSimname '/stst3/slabzq1_00000001.h5'],'/var'); 
q2=h5read([gDataDir gSimname '/stst3/slabzq2_00000001.h5'],'/var'); 
%Vx=h5read([gDataDir gSimname '/continua_q1.h5'],'/Vth'); Vx=d.trim3D(Vx);
%Vy=h5read([gDataDir gSimname '/continua_q2.h5'],'/Vr'); Vy=d.trim3D(Vy);
%q1=mean(Vx.*reshape(d.g3rm,1,1,d.nzm),3);
%q2=mean(Vy.*reshape(d.g3rm,1,1,d.nzm),3);

%move all velocities to cell center
q1=d.faceToCell(d.makePeriodic(q1,1),1);
q2=d.faceToCell(d.makePeriodic(q2,2),2);

% the required fields
transferQK=zeros(maxQ,maxK);

% prepare wavenumbers - note that I use integer indices, later to be converted to actual wavenumbers 2*pi/L
kx=[0:d.nxm/2,-d.nxm/2+1:-1];
ky=[0:d.nym/2,-d.nym/2+1:-1];
%kx=mod((1:d.nxm)+d.nxm/2-1,d.nxm)-d.nxm/2;
%ky=kx;
kh=floor(sqrt(kx.^2+ky'.^2));

%kLx=1;
%kLy=1;
kLx=2*pi/d.X;
kLy=2*pi/d.Y;

u=q1;
v=q2;
    
% calculate Fourier-transformed velocity fields
Fu=fft2(u)/(d.nxm*d.nym);
Fv=fft2(v)/(d.nxm*d.nym);

mcount=0;

tic
% loop over wavenumbers K and Q
for iKx=1:length(kx)
    for iKy=1:length(ky)
        K=kh(iKx,iKy);
        if K<=maxK && K>0
        for iQx=1:length(kx)
            for iQy=1:length(ky)
                Q=kh(iQx,iQy);
                if Q<=maxQ && Q>0

                kplusr=kx(iKx)+kx(iQx)+1;
                %kplusr=mod(kx(iKx)+kx(iQx),d.nxm)+1;
                if kplusr < 1
                    kplusr=kplusr+d.nxm;
                end
                
                lpluss=ky(iKy)+ky(iQy)+1;
                if lpluss<1
                    lpluss=lpluss+d.nxm;
                end
                
                mcount=mcount+1;

                transferQK(Q,K)=transferQK(Q,K)- ...
                real(1i*(Fu(iKx,iKy)*Fu(iQx,iQy) + Fv(iKx,iKy)*Fv(iQx,iQy)) * ... 
                     (kx(iQx)*kLx*conj(Fu(kplusr,lpluss)) + ky(iQy)*kLy*conj(Fv(kplusr,lpluss))));
                 
                end
            end
        end
        end
    end
end
toc

figure
imagesc(-transferQK,[-3e-6,3e-6])
set(gca,'YDir','normal')
colormap(gMyCM)
colorbar
xlabel('$Q$')
ylabel('$K$')
title('$\bar{u}_K \cdot \bar{u} \cdot \nabla \bar{u}_Q$')

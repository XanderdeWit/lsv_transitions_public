% maxK, maxQ define largest K value to be included in the energy transfer analysis
maxK=min(d.nxm,d.nym)/4-1;

%read velocity field
q1=h5read([gDataDir gSimname '/continua_q1.h5'],'/Vth'); q1=d.trim3D(q1);
q2=h5read([gDataDir gSimname '/continua_q2.h5'],'/Vr'); q2=d.trim3D(q2);
q3=h5read([gDataDir gSimname '/continua_q3.h5'],'/Vz'); q3=d.trim3D(q3);

%move all velocities to cell center
q1=d.faceToCell(d.makePeriodic(q1,1),1);
q2=d.faceToCell(d.makePeriodic(q2,2),2);
q3(:,:,end+1)=zeros(d.nxm,d.nym); q3=d.faceToCell(q3,3);

kx=[0:d.nxm/2,-d.nxm/2+1:-1];
ky=[0:d.nym/2,-d.nym/2+1:-1];

Kmask=zeros(length(kx),length(ky),maxK+1);
for ikx=1:length(kx)
    for iky=1:length(ky)
        mkx=kx(ikx);
        mky=ky(iky);
        %bin all wavenumbers with kH <= sqrt(kx^2+ky^2) < kH+1 together
        mK=floor(sqrt(mkx^2+mky^2));
        if mK<=maxK
            Kmask(ikx,iky,mK+1)=1;
        end
    end
end

%tic
trans_all=zeros(maxK+1,maxK+1,d.nzm);
%parfor m=1:d.nzm
for m=1:d.nzm
tic
    u=q1(:,:,m);
    v=q2(:,:,m);
    w=q3(:,:,m);
    
    u_k=fft2(u);
    v_k=fft2(v);
    w_k=fft2(w);
    
    %filter in fourier space
    u_K_k=u_k.*Kmask;
    v_K_k=v_k.*Kmask;
    w_K_k=w_k.*Kmask;

    %back to real space
    u_K=zeros(d.nxm,d.nym,maxK+1);
    v_K=zeros(d.nxm,d.nym,maxK+1);
    w_K=zeros(d.nxm,d.nym,maxK+1);
    for iK=1:size(u_K,3)
        u_K(:,:,iK)=ifft2(u_K_k(:,:,iK));
        v_K(:,:,iK)=ifft2(v_K_k(:,:,iK));
        w_K(:,:,iK)=ifft2(w_K_k(:,:,iK));
    end

    %calculate derivatives
    du_Qdx=getDerivative(u_K,d.dx,1,d);
    du_Qdy=getDerivative(u_K,d.dy,2,d);
    dv_Qdx=getDerivative(v_K,d.dx,1,d);
    dv_Qdy=getDerivative(v_K,d.dy,2,d);
    dw_Qdx=getDerivative(w_K,d.dx,1,d);
    dw_Qdy=getDerivative(w_K,d.dy,2,d);
 
%     %calculate transport from Q to K, u_K * u * grad u_Q
%     %NB: neglecting d/dz terms!
%     trans=zeros(maxK+1,maxK+1);
%     for iK=1:size(trans,1)
%         for iQ=1:size(trans,2)
%             mtrans=u_K(:,:,iK).*u(:,:).*du_Qdx(:,:,iQ) +...
%                 u_K(:,:,iK).*v(:,:).*du_Qdy(:,:,iQ) +...
%                 v_K(:,:,iK).*u(:,:).*dv_Qdx(:,:,iQ) +...
%                 v_K(:,:,iK).*v(:,:).*dv_Qdy(:,:,iQ) +...
%                 w_K(:,:,iK).*u(:,:).*dw_Qdx(:,:,iQ) +...
%                 w_K(:,:,iK).*v(:,:).*dw_Qdy(:,:,iQ);
%             trans(iK,iQ)=mean(mtrans,'all');
%         end
%     end
    
    %calculate transport from Q to K, u_K * u * grad u_Q
    %NB: neglecting d/dz terms!
    trans=zeros(maxK+1,maxK+1);
    for iQ=1:size(trans,2) %loop here to keep it memory-friendly
        mtrans=u_K(:,:,:).*u(:,:).*du_Qdx(:,:,iQ) +...
            u_K(:,:,:).*v(:,:).*du_Qdy(:,:,iQ) +...
            v_K(:,:,:).*u(:,:).*dv_Qdx(:,:,iQ) +...
            v_K(:,:,:).*v(:,:).*dv_Qdy(:,:,iQ) +...
            w_K(:,:,:).*u(:,:).*dw_Qdx(:,:,iQ) +...
            w_K(:,:,:).*v(:,:).*dw_Qdy(:,:,iQ);
        trans(:,iQ)=squeeze(mean(mtrans,[1,2]));
    end
    
    %save
    trans_all(:,:,m)=trans;
toc
end
%toc

trans_mean=mean(trans_all .* reshape(d.g3rm,1,1,d.nzm),3);

function deriv=getDerivative(y,dx,axis,d)
    deriv=circshift(diff(d.makePeriodic(y,axis),1,axis),axis); %face value
    deriv=d.faceToCell(d.makePeriodic(deriv,axis),axis); %cell value
    deriv=deriv./dx;
end

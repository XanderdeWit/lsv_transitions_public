% maxK, maxQ define largest K value to be included in the energy transfer analysis
maxK=min(d.nxm,d.nym)/4-1;

%read z-averaged velocity field
q1=h5read([gDataDir gSimname '/stst3/slabzq1_00002089.h5'],'/var'); 
q2=h5read([gDataDir gSimname '/stst3/slabzq2_00002089.h5'],'/var');

%move all velocities to cell center
q1=d.faceToCell(d.makePeriodic(q1,1),1);
q2=d.faceToCell(d.makePeriodic(q2,2),2);

kx=[0:d.nxm/2,-d.nxm/2+1:-1];
ky=[0:d.nym/2,-d.nym/2+1:-1];

Kmask=zeros(length(kx),length(ky),maxK+1);
for ikx=1:length(kx)
    for iky=1:length(ky)
        mkx=kx(ikx);
        mky=ky(iky);
        %bin all wavenumbers with kH <= sqrt(kx^2+ky^2) < kH+1 together
        mK=floor(sqrt(mkx^2+mky^2));
        if mK<=maxK
            Kmask(ikx,iky,mK+1)=1;
        end
    end
end

tic
    
u=q1;
v=q2;

u_K=getFilteredVelocity(u,Kmask);
v_K=getFilteredVelocity(v,Kmask);

%calculate derivatives
du_Qdx=getDerivativeXY(u_K,d.dx,1,d);
du_Qdy=getDerivativeXY(u_K,d.dy,2,d);
dv_Qdx=getDerivativeXY(v_K,d.dx,1,d);
dv_Qdy=getDerivativeXY(v_K,d.dy,2,d);

%calculate transport from Q to K, u_K * u * grad u_Q
%NB: neglecting d/dz and w terms!
trans=zeros(maxK+1,maxK+1);
for iQ=1:size(trans,2) %loop here to keep it memory-friendly
    mtrans=u_K(:,:,:).*u(:,:).*du_Qdx(:,:,iQ) +...
        u_K(:,:,:).*v(:,:).*du_Qdy(:,:,iQ) +...
        v_K(:,:,:).*u(:,:).*dv_Qdx(:,:,iQ) +...
        v_K(:,:,:).*v(:,:).*dv_Qdy(:,:,iQ) ;
    trans(:,iQ)=squeeze(mean(mtrans,[1,2]));
end

toc

function val_K=getFilteredVelocity(val,Kmask)
    %caltulate fourier transform
    val_k=fft2(val);
    
    %filter in fourier space
    val_K_k=val_k.*Kmask;

    %back to real space
    val_K=zeros(size(val,1),size(val,2),size(Kmask,3));
    for iK=1:size(val_K,3)
        val_K(:,:,iK)=ifft2(val_K_k(:,:,iK));
    end
end

function deriv=getDerivativeXY(y,dx,axis,d)
    deriv=circshift(diff(d.makePeriodic(y,axis),1,axis),axis); %face value
    deriv=d.faceToCell(d.makePeriodic(deriv,axis),axis); %cell value
    deriv=deriv./dx;
end

%Calculate 2D energy transferred towards the K=1 mode

%filename example: stst3/slabzq1_00000001.h5

%find files
files=dir([gDataDir gSimname '/stst3']);
slab_is=0;
for ifile=3:length(files)
    if files(ifile).name(5:7) == 'zq1'
        if slab_is == 0
            slab_is=str2num(files(ifile).name(9:16));
        else
            slab_is=[slab_is; str2num(files(ifile).name(9:16))];
        end
    end
end

% maxK, maxQ define largest K and Q values to be included in the energy transfer analysis
maxK=min(d.nxm,d.nym)/4-1;
maxQ=maxK;

%kLx=1;
%kLy=1;
kLx=2*pi/d.X;
kLy=2*pi/d.Y;

% prepare wavenumbers - note that I use integer indices, later to be converted to actual wavenumbers 2*pi/L
kx=(1:1:d.nxm)'*ones(1,d.nxm)-d.nxm/2-1;
ky=ones(d.nym,1)*(1:1:d.nym)-d.nym/2-1;
kh=floor(sqrt(kx.^2+ky.^2)); %NB: was originally ceil

trans_K1s=zeros(length(slab_is),1);
time=zeros(length(slab_is),1);

maxQ=1;
Q=1;

[ixQ,iyQ]=find(kh==Q);

%tic
%for islab_i=1:length(slab_is)
parfor islab_i=1:length(slab_is)
    %read z-averaged velocity field
    mVx_zave=h5read([gDataDir gSimname '/stst3/slabzq1_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/var');
    mtime=h5read([gDataDir gSimname '/stst3/slabzq1_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/time');
    
    mVy_zave=h5read([gDataDir gSimname '/stst3/slabzq2_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/var');
    mtimeVy=h5read([gDataDir gSimname '/stst3/slabzq2_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/time');
    
    if mtime ~= mtimeVy
        disp('Error: time_vx != time_vy');
    end

    %move all velocities to cell center
    u=d.faceToCell(d.makePeriodic(mVx_zave,1),1);
    v=d.faceToCell(d.makePeriodic(mVy_zave,2),2);

    % the required fields
    transferQK=zeros(maxQ,maxK);

    % calculate Fourier-transformed velocity fields
    Fu=fftshift(fft2(u))/(d.nxm*d.nym);
    Fv=fftshift(fft2(v))/(d.nxm*d.nym);

    % loop over wavenumbers K and Q
    %for Q=1:maxQ
        for K=1:maxK
          [ixK,iyK]=find(kh==K);
          %[ixQ,iyQ]=find(kh==Q);
          for kk=1:size(ixK,1)
            for qq=1:size(ixQ,1)
              k=ixK(kk);
              l=iyK(kk);
              r=ixQ(qq);
              s=iyQ(qq);
              kplusr=d.nxm/2+1+kx(k,l)+kx(r,s);
              lpluss=d.nym/2+1+ky(k,l)+ky(r,s);

              transferQK(Q,K)=transferQK(Q,K)- ...
                real(1i*(Fu(k,l)*Fu(r,s) + Fv(k,l)*Fv(r,s)) * ... 
                     (kx(r,s)*kLx*conj(Fu(kplusr,lpluss)) + ky(r,s)*kLy*conj(Fv(kplusr,lpluss))));
            end
          end
        end
    %end

    trans_K1s(islab_i)=-sum(transferQK);
    time(islab_i)=mtime;
end
%toc

mean(trans_K1s)

tsTransK1=timeseries(trans_K1s,time,'Name','transfer to K=1');

%interpolate to uniform grid
tsTransK1=interpTs(tsTransK1);

%moving mean
tsTransK1mm=timeseries(movmean(tsTransK1.Data,50),tsTransK1.Time,'Name','moving mean transfer to K=1');

if gPlotting    
    %Plot time series
    figure
    scatter(tsTransK1.Time,tsTransK1.Data,10,'s','filled')
    hold on
    plot(tsTransK1mm.Time,tsTransK1mm.Data,'LineWidth',2)
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    hold off
    title(['2D energy transfer to K=1 mode ' gSimname])
    ylabel('Energy transfer')
    xlabel('Convective time')
    grid on
    box on
end

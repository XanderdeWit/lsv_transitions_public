figure
imagesc(-transferQK',[-3e-7,3e-7])
set(gca,'YDir','normal')
colormap(gMyCM)
colorbar
xlabel('$K$')
ylabel('$Q$')

figure
imagesc(-trans(2:end,2:end)',[-3e-7,3e-7])
set(gca,'YDir','normal')
colormap(gMyCM)
colorbar
xlabel('$K$')
ylabel('$Q$')

trans_mirrored=zeros(size(trans));
for i=1:size(trans,1)
    for j=1:size(trans,2)
        if i > j
            trans_mirrored(i,j)=trans(i,j);
        elseif i < j
            trans_mirrored(i,j)=-trans(j,i);
        end
    end
end

figure
imagesc(-trans_mirrored(2:end,2:end)',[-3e-7,3e-7])
set(gca,'YDir','normal')
colormap(gMyCM)
colorbar
xlabel('$K$')
ylabel('$Q$')

% figure
% imagesc(trans_mirrored(2:end,2:end)-transferQK,[-3e-7,3e-7])
% set(gca,'YDir','normal')
% colormap(gMyCM)
% colorbar

figure
plot(trans(2,2:end))
hold on
plot(transferQK(1,:))
grid on
legend
xlabel('$Q$')

figure
plot(trans(2:end,2))
hold on
plot(transferQK(:,1))
grid on
legend
xlabel('$K$')

figure
plot(cumsum(trans_mirrored(2:end,2)))
hold on
plot(cumsum(transferQK(:,1)))
grid on
legend
xlabel('$K$')

% figure
% plot(trans_sumQ(2:end))
% hold on
% plot(sum(trans_mirrored(2:end,:),2))
% grid on
% legend
% 
% figure
% plot(cumsum(trans_sumQ(2:end)))
% hold on
% plot(cumsum(sum(trans_mirrored(2:end,:),2)))
% grid on
% legend

% figure
% imagesc(trans_mean,[-3e-5,3e-5])
% set(gca,'YDir','normal')
% colormap(gMyCM)
% colorbar

% figure
% imagesc(trans_all(:,:,60),[-3e-5,3e-5])
% set(gca,'YDir','normal')
% colormap(gMyCM)
% colorbar
% 
% figure
% imagesc(transferQK(:,:,60),[-3e9,3e9])
% set(gca,'YDir','normal')
% colormap(gMyCM)
% colorbar
% 
% figure
% imagesc(trans,[-3e-7,3e-7])
% set(gca,'YDir','normal')
% colormap(gMyCM)
% colorbar
% 
% figure
% plot(trans_sumQ)

% figure
% plot(sum(trans,1))
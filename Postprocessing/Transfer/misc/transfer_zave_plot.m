% maxK, maxQ define largest K value to be included in the energy transfer analysis
maxK=min(d.nxm,d.nym)/4-1;

%read velocity field
Vx=h5read([gDataDir gSimname '/continua_q1.h5'],'/Vth'); Vx=d.trim3D(Vx);
Vy=h5read([gDataDir gSimname '/continua_q2.h5'],'/Vr'); Vy=d.trim3D(Vy);
Vz=h5read([gDataDir gSimname '/continua_q3.h5'],'/Vz'); Vz=d.trim3D(Vz);

%move all velocities to cell center
Vx=d.faceToCell(d.makePeriodic(Vx,1),1);
Vy=d.faceToCell(d.makePeriodic(Vy,2),2);
Vz(:,:,end+1)=zeros(d.nxm,d.nym); Vz=d.faceToCell(Vz,3);

%calculate z-averaged velocity
Vx_zave=mean(Vx.*reshape(d.g3rm,1,1,d.nzm),3);
Vy_zave=mean(Vy.*reshape(d.g3rm,1,1,d.nzm),3);
Vz_zave=mean(Vz.*reshape(d.g3rm,1,1,d.nzm),3);

kx=[0:d.nxm/2,-d.nxm/2+1:-1];
ky=[0:d.nym/2,-d.nym/2+1:-1];

Kmask=zeros(length(kx),length(ky),maxK+1);
for ikx=1:length(kx)
    for iky=1:length(ky)
        mkx=kx(ikx);
        mky=ky(iky);
        %bin all wavenumbers with kH <= sqrt(kx^2+ky^2) < kH+1 together
        mK=floor(sqrt(mkx^2+mky^2));
        if mK<=maxK
            Kmask(ikx,iky,mK+1)=1;
        end
    end
end

tic
    
u=Vx_zave;
v=Vy_zave;
w=Vz_zave;

u_K=getFilteredVelocity(u,Kmask);
v_K=getFilteredVelocity(v,Kmask);
w_K=getFilteredVelocity(w,Kmask);

%calculate derivatives
du_Qdx=getDerivativeXY(u_K,d.dx,1,d);
du_Qdy=getDerivativeXY(u_K,d.dy,2,d);
dv_Qdx=getDerivativeXY(v_K,d.dx,1,d);
dv_Qdy=getDerivativeXY(v_K,d.dy,2,d);
dw_Qdx=getDerivativeXY(w_K,d.dx,1,d);
dw_Qdy=getDerivativeXY(w_K,d.dy,2,d);

%calculate transport from Q to K, u_K * u * grad u_Q
%NB: neglecting d/dz terms!
trans=zeros(maxK+1,maxK+1);
for iQ=1:size(trans,2) %loop here to keep it memory-friendly
    mtrans=u_K(:,:,:).*u(:,:).*du_Qdx(:,:,iQ) +...
        u_K(:,:,:).*v(:,:).*du_Qdy(:,:,iQ) +...
        v_K(:,:,:).*u(:,:).*dv_Qdx(:,:,iQ) +...
        v_K(:,:,:).*v(:,:).*dv_Qdy(:,:,iQ) +...
        w_K(:,:,:).*u(:,:).*dw_Qdx(:,:,iQ) +...
        w_K(:,:,:).*v(:,:).*dw_Qdy(:,:,iQ);
    trans(:,iQ)=squeeze(mean(mtrans,[1,2]));
end

%mirror the results in diagonal, as the real space calculation is accurate for K>Q
trans_mirrored=zeros(size(trans));
for i=1:size(trans,1)
    for j=1:size(trans,2)
        if i > j
            trans_mirrored(i,j)=trans(i,j);
        elseif i < j
            trans_mirrored(i,j)=-trans(j,i);
        end
    end
end

trans_sumQ=sum(trans,2);

toc

if gPlotting
    figure
    plot(-trans_sumQ(2:end),1:maxK,'LineWidth',1.5)
    ylabel('$K$')
    xlabel('$\bar{u}_K \cdot \bar{u} \cdot \nabla \bar{u}_Q$')
    box on
    grid on

    figure
    imagesc(-trans(2:end,2:end),[-3e-6,3e-6])
    set(gca,'YDir','normal')
    colormap(gMyCM)
    colorbar
    xlabel('$Q$')
    ylabel('$K$')
    title('$\bar{u}_K \cdot \bar{u} \cdot \nabla \bar{u}_Q$')

    figure
    imagesc(-trans_mirrored(2:end,2:end),[-3e-6,3e-6])
    set(gca,'YDir','normal')
    colormap(gMyCM)
    colorbar
    xlabel('$Q$')
    ylabel('$K$')
    title('$\bar{u}_K \cdot \bar{u} \cdot \nabla \bar{u}_Q$')
end


function val_K=getFilteredVelocity(val,Kmask)
    %caltulate fourier transform
    val_k=fft2(val);
    
    %filter in fourier space
    val_K_k=val_k.*Kmask;

    %back to real space
    val_K=zeros(size(val,1),size(val,2),size(Kmask,3));
    for iK=1:size(val_K,3)
        val_K(:,:,iK)=ifft2(val_K_k(:,:,iK));
    end
end

function deriv=getDerivativeXY(y,dx,axis,d)
    deriv=circshift(diff(d.makePeriodic(y,axis),1,axis),axis); %face value
    deriv=d.faceToCell(d.makePeriodic(deriv,axis),axis); %cell value
    deriv=deriv./dx;
end

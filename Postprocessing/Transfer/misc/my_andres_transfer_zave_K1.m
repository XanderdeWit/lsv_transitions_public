%calculate transfer map from 3D snapshot

% maxK, maxQ define largest K and Q values to be included in the energy transfer analysis
maxK=min(d.nxm,d.nym)/4-1;
maxQ=maxK;

maxK=1;

%read z-averaged velocity field
q1=h5read([gDataDir gSimname '/stst3/slabzq1_00002089.h5'],'/var'); 
q2=h5read([gDataDir gSimname '/stst3/slabzq2_00002089.h5'],'/var'); 

%move all velocities to cell center
q1=d.faceToCell(d.makePeriodic(q1,1),1);
q2=d.faceToCell(d.makePeriodic(q2,2),2);

% the required fields
transferQK=zeros(maxQ,maxK);

% prepare wavenumbers - note that I use integer indices, later to be converted to actual wavenumbers 2*pi/L
kx=(1:1:d.nxm)'*ones(1,d.nxm)-d.nxm/2-1;
ky=ones(d.nym,1)*(1:1:d.nym)-d.nym/2-1;
kh=floor(sqrt(kx.^2+ky.^2)); %NB: was originally ceil

tic

%kLx=1;
%kLy=1;
kLx=2*pi/d.X;
kLy=2*pi/d.Y;

u=q1;
v=q2;
    
% calculate Fourier-transformed velocity fields
Fu=fftshift(fft2(u))/(d.nxm*d.nym);
Fv=fftshift(fft2(v))/(d.nxm*d.nym);

% loop over wavenumbers K and Q
for Q=1:maxQ
    for K=1:maxK
      [ixK,iyK]=find(kh==K);
      [ixQ,iyQ]=find(kh==Q);
      for kk=1:size(ixK,1)
        for qq=1:size(ixQ,1)
          k=ixK(kk);
          l=iyK(kk);
          r=ixQ(qq);
          s=iyQ(qq);
          kplusr=d.nxm/2+1+kx(k,l)+kx(r,s);
          lpluss=d.nym/2+1+ky(k,l)+ky(r,s);

          transferQK(Q,K)=transferQK(Q,K)- ...
            real(1i*(Fu(k,l)*Fu(r,s) + Fv(k,l)*Fv(r,s)) * ... 
                 (kx(r,s)*kLx*conj(Fu(kplusr,lpluss)) + ky(r,s)*kLy*conj(Fv(kplusr,lpluss))));
        end
      end
    end
end
toc

sum(transferQK)
  

%Calculate 2D energy transferred towards the K=1 mode

%filename example: stst3/slabzq1_00000001.h5

%find files
files=dir([gDataDir gSimname '/stst3']);
slab_is=0;
for ifile=3:length(files)
    if files(ifile).name(5:7) == 'zq1'
        if slab_is == 0
            slab_is=str2num(files(ifile).name(9:16));
        else
            slab_is=[slab_is; str2num(files(ifile).name(9:16))];
        end
    end
end

kx=[0:d.nxm/2,-d.nxm/2+1:-1];
ky=[0:d.nym/2,-d.nym/2+1:-1];

K1mask=zeros(length(kx),length(ky));
K1mask(1,2)=1;
K1mask(1,end)=1;
K1mask(2,1)=1;
K1mask(2,2)=1;
K1mask(2,end)=1;
K1mask(end,1)=1;
K1mask(end,2)=1;
K1mask(end,end)=1;

trans_K1s=zeros(length(slab_is),1);
trans_Q1s=zeros(length(slab_is),1);
trans_K1_flts=zeros(length(slab_is),1);
trans_Q1_flts=zeros(length(slab_is),1);
time=zeros(length(slab_is),1);

tic
%for islab_i=1:length(slab_is)
parfor islab_i=1:length(slab_is)
    %read z-averaged velocity field
    mVx_zave=h5read([gDataDir gSimname '/stst3/slabzq1_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/var');
    mtime=h5read([gDataDir gSimname '/stst3/slabzq1_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/time');
    
    mVy_zave=h5read([gDataDir gSimname '/stst3/slabzq2_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/var');
    mtimeVy=h5read([gDataDir gSimname '/stst3/slabzq2_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/time');
    
    if mtime ~= mtimeVy
        disp('Error: time_vx != time_vy');
    end

    %move all velocities to cell center
    u=d.faceToCell(d.makePeriodic(mVx_zave,1),1);
    v=d.faceToCell(d.makePeriodic(mVy_zave,2),2);
    
    %filter velocity to only include K=1 mode
    u_K1=getFilteredVelocity(u,K1mask);
    v_K1=getFilteredVelocity(v,K1mask);

    % FILTERED VELOCITY ON THE RIGHT
    %calculate derivatives
    du_Q1dx=getDerivativeXY(u_K1,d.dx,1,d);
    du_Q1dy=getDerivativeXY(u_K1,d.dy,2,d);
    dv_Q1dx=getDerivativeXY(v_K1,d.dx,1,d);
    dv_Q1dy=getDerivativeXY(v_K1,d.dy,2,d);

    %calculate transport to K, u_K * u * grad u_Q
    %NB: neglecting d/dz and w terms!
    mtrans=u.*u.*du_Q1dx +...
            u.*v.*du_Q1dy +...
            v.*u.*dv_Q1dx +...
            v.*v.*dv_Q1dy ;
    trans_Q1=mean(mtrans,[1,2]);
    
    %filter K1 mode
    mtrans=(u-u_K1).*u.*du_Q1dx +...
            (u-u_K1).*v.*du_Q1dy +...
            (v-v_K1).*u.*dv_Q1dx +...
            (v-v_K1).*v.*dv_Q1dy ;
    trans_Q1_flt=mean(mtrans,[1,2]);

    %FILTERED VELOCITY ON THE LEFT
    %calculate derivatives
    dudx=getDerivativeXY(u,d.dx,1,d);
    dudy=getDerivativeXY(u,d.dy,2,d);
    dvdx=getDerivativeXY(v,d.dx,1,d);
    dvdy=getDerivativeXY(v,d.dy,2,d);

    %calculate transport to K, u_K * u * grad u_Q
    %NB: neglecting d/dz and w terms!
    mtrans=u_K1.*u.*dudx +...
            u_K1.*v.*dudy +...
            v_K1.*u.*dvdx +...
            v_K1.*v.*dvdy ;
    trans_K1=mean(mtrans,[1,2]);
    
    dudx=getDerivativeXY(u-u_K1,d.dx,1,d);
    dudy=getDerivativeXY(u-u_K1,d.dy,2,d);
    dvdx=getDerivativeXY(v-v_K1,d.dx,1,d);
    dvdy=getDerivativeXY(v-v_K1,d.dy,2,d);
    
    %calculate transport to K, u_K * u * grad u_Q
    %NB: neglecting d/dz and w terms!
    mtrans=u_K1.*u.*dudx +...
            u_K1.*v.*dudy +...
            v_K1.*u.*dvdx +...
            v_K1.*v.*dvdy ;
    trans_K1_flt=mean(mtrans,[1,2]);
    
    %save
    trans_K1s(islab_i)=trans_K1;
    trans_Q1s(islab_i)=trans_Q1;
    trans_K1_flts(islab_i)=trans_K1_flt;
    trans_Q1_flts(islab_i)=trans_Q1_flt;
    time(islab_i)=mtime;
end
toc

tsTransNoW_slabs_K1=timeseries(-trans_K1s,time,'Name','Horizontal transport 2D to K=1 slabs');
tsTransNoW_slabs_Q1=timeseries(trans_Q1s,time,'Name','Horizontal transport 2D from Q=1 slabs');
tsTransNoW_slabs_K1_flt=timeseries(-trans_K1_flts,time,'Name','Horizontal transport 2D from K=1 flt slabs');
tsTransNoW_slabs_Q1_flt=timeseries(trans_Q1_flts,time,'Name','Horizontal transport 2D from Q=1 flt slabs');

%interpolate to uniform grid
tsTransNoW_slabs_K1=interpTs(tsTransNoW_slabs_K1);
tsTransNoW_slabs_Q1=interpTs(tsTransNoW_slabs_Q1,tsTransNoW_slabs_K1.Time);
tsTransNoW_slabs_K1_flt=interpTs(tsTransNoW_slabs_K1_flt,tsTransNoW_slabs_K1.Time);
tsTransNoW_slabs_Q1_flt=interpTs(tsTransNoW_slabs_Q1_flt,tsTransNoW_slabs_K1.Time);

%get precalculated values
file = fopen([gDataDir gSimname '/kin_trans.out'],'r');
data = fscanf(file,'%f %f\n',[6 Inf]);
tsTransNoW=timeseries(data(6,:)',data(1,:)','Name','Horizontal transport 2D from Q=1 flt');
tsTransNoW=interpTs(tsTransNoW,tsTransNoW_slabs_K1.Time);

%moving mean
tsTransNoWmm_slabs_K1=timeseries(movmean(tsTransNoW_slabs_K1.Data,300),tsTransNoW_slabs_K1.Time,'Name','moving mean transfer to K=1 slabs');
tsTransNoWmm_slabs_Q1=timeseries(movmean(tsTransNoW_slabs_Q1.Data,300),tsTransNoW_slabs_K1.Time,'Name','moving mean transfer from Q=1 slabs');
tsTransNoWmm_slabs_K1_flt=timeseries(movmean(tsTransNoW_slabs_K1_flt.Data,300),tsTransNoW_slabs_K1.Time,'Name','moving mean transfer from K=1 flt slabs');
tsTransNoWmm_slabs_Q1_flt=timeseries(movmean(tsTransNoW_slabs_Q1_flt.Data,300),tsTransNoW_slabs_K1.Time,'Name','moving mean transfer from Q=1 flt slabs');
tsTransNoWmm=timeseries(movmean(tsTransNoW.Data,300),tsTransNoW_slabs_K1.Time,'Name','moving mean transfer to K=1');

if gPlotting
    %Plot time series
    figure
    plot(tsTransNoWmm_slabs_K1.Time,tsTransNoWmm_slabs_K1.Data,'LineWidth',2,'DisplayName',tsTransNoW_slabs_K1.Name)
    hold on
    plot(tsTransNoWmm_slabs_Q1.Time,tsTransNoWmm_slabs_Q1.Data,'LineWidth',2,'DisplayName',tsTransNoW_slabs_Q1.Name)
    plot(tsTransNoWmm_slabs_K1_flt.Time,tsTransNoWmm_slabs_K1_flt.Data,'LineWidth',2,'DisplayName',tsTransNoW_slabs_K1_flt.Name)
    plot(tsTransNoWmm_slabs_Q1_flt.Time,tsTransNoWmm_slabs_Q1_flt.Data,'LineWidth',2,'DisplayName',tsTransNoW_slabs_Q1_flt.Name)
    plot(tsTransNoWmm.Time,tsTransNoWmm.Data,'LineWidth',2,'DisplayName',tsTransNoW.Name)
    legend
    set(gca,'ColorOrderIndex',1)
    scatter(tsTransNoW_slabs_K1.Time,tsTransNoW_slabs_K1.Data,10,'s','filled','MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsTransNoW_slabs_Q1.Time,tsTransNoW_slabs_Q1.Data,10,'s','filled','MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsTransNoW_slabs_K1_flt.Time,tsTransNoW_slabs_K1_flt.Data,10,'s','filled','MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsTransNoW_slabs_Q1_flt.Time,tsTransNoW_slabs_Q1_flt.Data,10,'s','filled','MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsTransNoW.Time,tsTransNoW.Data,10,'s','filled','MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    set(gca,'Children',fftshift(get(gca,'Children')))
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    hold off
    title(['2D energy transfer to K=1 mode ' gSimname])
    ylabel('Energy transfer')
    xlabel('Convective time')
    grid on
    box on
end

function val_K=getFilteredVelocity(val,Kmask)
    %caltulate fourier transform
    val_k=fft2(val);
    
    %filter in fourier space
    val_K_k=val_k.*Kmask;

    %back to real space
    val_K=ifft2(val_K_k);
end

function deriv=getDerivativeXY(y,dx,axis,d)
    deriv=circshift(diff(d.makePeriodic(y,axis),1,axis),axis); %face value
    deriv=d.faceToCell(d.makePeriodic(deriv,axis),axis); %cell value
    deriv=deriv./dx;
end
%calculate transfer map from 3D snapshot

% maxK, maxQ define largest K and Q values to be included in the energy transfer analysis
maxK=min(d.nxm,d.nym)/4-1;
maxQ=maxK;

maxK=20;
maxQ=20;

kLx=1;
kLy=1;

% the required fields
transferQK=zeros(maxQ,maxK,d.nzm);
transferQK_total=zeros(maxQ,maxK);
%transferKQ=zeros(maxQ,maxK,d.nzm);
%transferKQ_total=zeros(maxQ,maxK);
%symm_break=zeros(maxQ,maxK,d.nzm);
%symm_break_total=zeros(maxQ,maxK);

% prepare wavenumbers - note that I use integer indices, later to be converted to actual wavenumbers 2*pi/L
kx=(1:1:d.nxm)'*ones(1,d.nxm)-d.nxm/2-1;
ky=ones(d.nym,1)*(1:1:d.nym)-d.nym/2-1;
kh=ceil(sqrt(kx.^2+ky.^2));

%read velocity field
q1=h5read([gDataDir gSimname '/continua_q1.h5'],'/Vth'); q1=d.trim3D(q1);
q2=h5read([gDataDir gSimname '/continua_q2.h5'],'/Vr'); q2=d.trim3D(q2);
q3=h5read([gDataDir gSimname '/continua_q3.h5'],'/Vz'); q3=d.trim3D(q3);

%move all velocities to cell center
q1=d.faceToCell(d.makePeriodic(q1,1),1);
q2=d.faceToCell(d.makePeriodic(q2,2),2);
q3(:,:,end+1)=zeros(d.nxm,d.nym); q3=d.faceToCell(q3,3);

tic
%parfor m=1:d.nzm
for m=1:d.nzm
%for m=60:60
  % for z derivative
  if m==1
    dzi=d.zms(m);
  else
    dzi=d.zms(m)-d.zms(m-1);
  end
  if m==d.nzm
    dzip=1-d.zms(m);
  else
    dzip=d.zms(m+1)-d.zms(m);
  end
  
  % get slabs and define "up", "middle" and "down" slabs
  if m==1
      % "down" slab is zero for now as it is the bottom plate
      u_d=zeros(d.nxm,d.nym);
      v_d=zeros(d.nxm,d.nym);
      w_d=zeros(d.nxm,d.nym);
  else
      u_d=q1(:,:,m-1);
      v_d=q2(:,:,m-1);
      w_d=q3(:,:,m-1);
  end
  u_m=q1(:,:,m);
  v_m=q2(:,:,m);
  w_m=q3(:,:,m);
  if m==d.nzm
      % "up" slab is zero for now as it is the top plate
      u_u=zeros(d.nxm,d.nym);
      v_u=zeros(d.nxm,d.nym);
      w_u=zeros(d.nxm,d.nym);
  else
      u_u=q1(:,:,m+1);
      v_u=q1(:,:,m+1);
      w_u=q1(:,:,m+1);
  end
    
  % calculate Fourier-transformed velocity fields
  Fu_u=fftshift(fft2(u_u))/(d.nxm*d.nym);
  Fv_u=fftshift(fft2(v_u))/(d.nxm*d.nym);
  Fw_u=fftshift(fft2(w_u))/(d.nxm*d.nym);
  Fu_m=fftshift(fft2(u_m))/(d.nxm*d.nym);
  Fv_m=fftshift(fft2(v_m))/(d.nxm*d.nym);
  Fw_m=fftshift(fft2(w_m))/(d.nxm*d.nym);
  Fu_d=fftshift(fft2(u_d))/(d.nxm*d.nym);
  Fv_d=fftshift(fft2(v_d))/(d.nxm*d.nym);
  Fw_d=fftshift(fft2(w_d))/(d.nxm*d.nym);

  % loop over wavenumbers K and Q
  for Q=1:maxQ
    for K=1:maxK
      [ixK,iyK]=find(kh==K);
      [ixQ,iyQ]=find(kh==Q);
      for kk=1:size(ixK,1)
        for qq=1:size(ixQ,1)
          k=ixK(kk);
          l=iyK(kk);
          r=ixQ(qq);
          s=iyQ(qq);
          kplusr=d.nxm/2+1+kx(k,l)+kx(r,s);
          lpluss=d.nym/2+1+ky(k,l)+ky(r,s);
          dursdz=(Fu_u(r,s)*dzi^2 + Fu_m(r,s)*(dzip^2-dzi^2) - Fu_d(r,s)*dzip^2)/(dzip*dzi*(dzip+dzi));
          dvrsdz=(Fv_u(r,s)*dzi^2 + Fv_m(r,s)*(dzip^2-dzi^2) - Fv_d(r,s)*dzip^2)/(dzip*dzi*(dzip+dzi));
          dwrsdz=(Fw_u(r,s)*dzi^2 + Fw_m(r,s)*(dzip^2-dzi^2) - Fw_d(r,s)*dzip^2)/(dzip*dzi*(dzip+dzi));
          dukldz=(Fu_u(k,l)*dzi^2 + Fu_m(k,l)*(dzip^2-dzi^2) - Fu_d(k,l)*dzip^2)/(dzip*dzi*(dzip+dzi));
          dvkldz=(Fv_u(k,l)*dzi^2 + Fv_m(k,l)*(dzip^2-dzi^2) - Fv_d(k,l)*dzip^2)/(dzip*dzi*(dzip+dzi));
          dwkldz=(Fw_u(k,l)*dzi^2 + Fw_m(k,l)*(dzip^2-dzi^2) - Fw_d(k,l)*dzip^2)/(dzip*dzi*(dzip+dzi));
          dwkrlsdz=(Fw_u(kplusr,lpluss)*dzi^2 + Fw_m(kplusr,lpluss)*(dzip^2-dzi^2) - Fw_d(kplusr,lpluss)*dzip^2)/(dzip*dzi*(dzip+dzi));
          
          transferQK(Q,K,m)=transferQK(Q,K,m)- ...
            real(1i*(Fu_m(k,l)*Fu_m(r,s) + Fv_m(k,l)*Fv_m(r,s) + Fw_m(k,l)*Fw_m(r,s)) * ... 
                 (kx(r,s)*kLx*conj(Fu_m(kplusr,lpluss)) + ky(r,s)*kLy*conj(Fv_m(kplusr,lpluss)))) - ...
            real((Fu_m(k,l)*dursdz + Fv_m(k,l)*dvrsdz + Fw_m(k,l)*dwrsdz)*conj(Fw_m(kplusr,lpluss)));
          
 %         transferKQ(Q,K,m)=transferKQ(Q,K,m)- ...
 %           real(1i*(Fu_m(k,l)*Fu_m(r,s) + Fv_m(k,l)*Fv_m(r,s) + Fw_m(k,l)*Fw_m(r,s)) * ... 
 %                (kx(k,l)*kLx*conj(Fu_m(kplusr,lpluss)) + ky(k,l)*kLy*conj(Fv_m(kplusr,lpluss)))) - ...
 %           real((dukldz*Fu_m(r,s) + dvkldz*Fv_m(r,s) + dwkldz*Fw_m(r,s))*conj(Fw_m(kplusr,lpluss)));
 %         
 %         symm_break(Q,K,m)=symm_break(Q,K,m)- ...
 %           real((dukldz*Fu_m(r,s) + dvkldz*Fv_m(r,s) + dwkldz*Fw_m(r,s) + ...
 %                 Fu_m(k,l)*dursdz + Fv_m(k,l)*dvrsdz + Fw_m(k,l)*dwrsdz) * conj(Fw_m(kplusr,lpluss))) - ...
 %           real((Fu_m(k,l)*Fu_m(r,s) + Fv_m(k,l)*Fv_m(r,s) + Fw_m(k,l)*Fw_m(r,s)) * conj(dwkrlsdz));
        end
      end
    end
  end
end
toc

transferQK_total=mean(transferQK.*reshape(d.g3rm,1,1,d.nzm),3);
%transferKQ_total=mean(transferKQ.*reshape(d.g3rm,1,1,d.nzm),3);
%symm_break_total=mean(symm_break.*reshape(d.g3rm,1,1,d.nzm),3);

%transferQK_total=mean(transferQK,3);
%transferKQ_total=mean(transferKQ,3);
%symm_break_total=mean(symm_break,3);

%figure
%plot(transferQK_total)

figure
imagesc(transferQK_total,[-3e-3,3e-3])
set(gca,'YDir','normal')
colormap(gMyCM)
colorbar
xlabel('$K$')
ylabel('$Q$')
  

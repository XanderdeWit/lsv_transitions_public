%read velocity field
Vx=h5read([gDataDir gSimname '/continua_q1.h5'],'/Vth'); Vx=d.trim3D(Vx);
Vy=h5read([gDataDir gSimname '/continua_q2.h5'],'/Vr'); Vy=d.trim3D(Vy);
Vz=h5read([gDataDir gSimname '/continua_q3.h5'],'/Vz'); Vz=d.trim3D(Vz);

%move all velocities to cell center
Vx=d.faceToCell(d.makePeriodic(Vx,1),1);
Vy=d.faceToCell(d.makePeriodic(Vy,2),2);
Vz(:,:,end+1)=zeros(d.nxm,d.nym); Vz=d.faceToCell(Vz,3);

%calculate z-averaged velocity
Vx_zave=mean(Vx.*reshape(d.g3rm,1,1,d.nzm),3);
Vy_zave=mean(Vy.*reshape(d.g3rm,1,1,d.nzm),3);
Vz_zave=mean(Vz.*reshape(d.g3rm,1,1,d.nzm),3);

%calculate gradient
[dVx_dy,dVx_dx,dVx_dz]=gradient(Vx,d.dx,d.dy,d.zms);
[dVy_dy,dVy_dx,dVy_dz]=gradient(Vy,d.dx,d.dy,d.zms);
[dVz_dy,dVz_dx,dVz_dz]=gradient(Vz,d.dx,d.dy,d.zms);

%calculate z-averaged gradients
ws=reshape(d.g3rm,1,1,d.nzm); %weights
dVx_dy_zave=mean(dVx_dy.*ws,3); dVx_dx_zave=mean(dVx_dx.*ws,3); dVx_dz_zave=mean(dVx_dz.*ws,3);
dVy_dy_zave=mean(dVy_dy.*ws,3); dVy_dx_zave=mean(dVy_dx.*ws,3); dVy_dz_zave=mean(dVy_dz.*ws,3);
dVz_dy_zave=mean(dVz_dy.*ws,3); dVz_dx_zave=mean(dVz_dx.*ws,3); dVz_dz_zave=mean(dVz_dz.*ws,3);

%calculate fluctuating forces
forcex_first = Vx .* dVx_dx + Vy .* dVx_dy + Vz .* dVx_dz;
forcey_first = Vx .* dVy_dx + Vy .* dVy_dy + Vz .* dVy_dz;
forcez_first = Vx .* dVz_dx + Vy .* dVz_dy + Vz .* dVz_dz;

forcex_first_zave=mean(forcex_first.*ws,3);
forcey_first_zave=mean(forcey_first.*ws,3);
forcez_first_zave=mean(forcez_first.*ws,3);

forcex_second = Vx_zave .* dVx_dx_zave + Vy_zave .* dVx_dy_zave + Vz_zave .* dVx_dz_zave;
forcey_second = Vx_zave .* dVy_dx_zave + Vy_zave .* dVy_dy_zave + Vz_zave .* dVy_dz_zave;
forcez_second = Vx_zave .* dVz_dx_zave + Vy_zave .* dVz_dy_zave + Vz_zave .* dVz_dz_zave;

forcex_zave = forcex_first_zave - forcex_second;
forcey_zave = forcey_first_zave - forcey_second;
forcez_zave = forcez_first_zave - forcez_second;

%calculate spectrum of force
k=0:min(d.nxm,d.nym)/2;
forcek=zeros(1,length(k));
    
forcexk=fft2(forcex_zave)./(d.nxm*d.nym);
forceyk=fft2(forcey_zave)./(d.nxm*d.nym);
forcezk=fft2(forcez_zave)./(d.nxm*d.nym);
    
kx=[0:d.nxm/2,-d.nxm/2+1:-1];
ky=[0:d.nym/2,-d.nym/2+1:-1];
for ikx=1:d.nxm
    for iky=1:d.nym
        mkx=kx(ikx);
        mky=ky(iky);
        %bin all wavenumbers with kH <= sqrt(kx^2+ky^2) < kH+1 together
        mk=floor(sqrt(mkx^2+mky^2));
        if mk<length(k)
            %TODO check if needed to take sqrt of different components
            forcek(1+mk)=forcek(1+mk)+abs(forcexk(ikx,iky))^2+abs(forceyk(ikx,iky))^2+abs(forcezk(ikx,iky))^2;
        end
    end
end

if gPlotting
    figure
    drawCross(d.xms,d.yms,sqrt(forcex_zave.^2+forcey_zave.^2+forcez_zave.^2)','auto','pcolor','hot',true);
    title(['$|\langle u` \cdot \nabla u` \rangle_z|$ ' gSimname])
    
    figure
    loglog(k(2:end),forcek(2:end))
    title(['K2D forcing ' gSimname])
    xlabel('Horizontal wavenumber $k_H$')
    ylabel('Spectrum $\langle u` \cdot \nabla u` \rangle_z(k_H)$')
    grid on
    box on
end

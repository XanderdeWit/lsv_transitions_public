%calculate 2D energy transferred towards the K=1 mode

kx=[0:d.nxm/2,-d.nxm/2+1:-1];
ky=[0:d.nym/2,-d.nym/2+1:-1];

K1mask=zeros(length(kx),length(ky));
K1mask(1,2)=1;
K1mask(1,end)=1;
K1mask(2,1)=1;
K1mask(2,2)=1;
K1mask(2,end)=1;
K1mask(end,1)=1;
K1mask(end,2)=1;
K1mask(end,end)=1;

%read z-averaged velocity field
q1=h5read([gDataDir gSimname '/stst3/slabzq1_00002089.h5'],'/var'); 
q2=h5read([gDataDir gSimname '/stst3/slabzq2_00002089.h5'],'/var');

%move all velocities to cell center
u=d.faceToCell(d.makePeriodic(q1,1),1);
v=d.faceToCell(d.makePeriodic(q2,2),2);

u_K1=getFilteredVelocity(u,K1mask);
v_K1=getFilteredVelocity(v,K1mask);

%calculate derivatives
du_Q1dx=getDerivativeXY(u_K1,d.dx,1,d);
du_Q1dy=getDerivativeXY(u_K1,d.dy,2,d);
dv_Q1dx=getDerivativeXY(v_K1,d.dx,1,d);
dv_Q1dy=getDerivativeXY(v_K1,d.dy,2,d);

u_K=u-u_K1;
v_K=v-v_K1;

%calculate transport to K, u_K * u * grad u
%NB: neglecting d/dz and w terms!
mtrans=u_K.*u.*du_Q1dx +...
        u_K.*v.*du_Q1dy +...
        v_K.*u.*dv_Q1dx +...
        v_K.*v.*dv_Q1dy ;
trans_K1=mean(mtrans,[1,2]);

trans_K1

function val_K=getFilteredVelocity(val,Kmask)
    %caltulate fourier transform
    val_k=fft2(val);
    
    %filter in fourier space
    val_K_k=val_k.*Kmask;

    %back to real space
    val_K=ifft2(val_K_k);
end

function deriv=getDerivativeXY(y,dx,axis,d)
    deriv=circshift(diff(d.makePeriodic(y,axis),1,axis),axis); %face value
    deriv=d.faceToCell(d.makePeriodic(deriv,axis),axis); %cell value
    deriv=deriv./dx;
end

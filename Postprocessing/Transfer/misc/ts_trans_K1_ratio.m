%calculate the ratio between the transfer to K=1 and Nu

%calculate the transfer and Nu
m_ts_trans_K1_ratios_gPlotting=gPlotting; gPlotting=false;
ts_transfer_zave_K1;
nusse;
tsNuTe=tss(4);
gPlotting=m_ts_trans_K1_ratios_gPlotting;

%interpolate and moving mean
tsNuTe=interpTs(tsNuTe,tsTransK1mm.Time);
tsNuTemm=timeseries(movmean(tsNuTe.Data,50),tsNuTe.Time,'Name','moving mean NuTe');

%transfer ratio
tsTransRatio=timeseries(1e5 .* tsTransK1mm.Data ./ tsNuTemm.Data, tsTransK1mm.Time, 'Name', 'transfer ratio');

%Do error analysis
transRatioStats=getTsStats(tsTransRatio,gPlotting,gPlotting,gTstart);

transK1mmStats=getTsStats(tsTransK1mm,gPlotting,gPlotting,gTstart);
nuTemmStats=getTsStats(tsNuTemm,gPlotting,gPlotting,gTstart);

if gPlotting
    %Plot ratio
    figure
    scatter(tsTransRatio.Time,tsTransRatio.Data,10,'s','filled')
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    title([tsTransRatio.Name ' ' gSimname])
    ylabel(tsTransRatio.Name)
    xlabel('Convective time')
    grid on
    box on
end
%Calculate 2D energy transferred towards the K=1 mode

%filename example: stst3/slabzq1_00000001.h5

%find files
files=dir([gDataDir gSimname '/stst3']);
slab_is=0;
for ifile=3:length(files)
    if files(ifile).name(5:7) == 'zq1'
        if slab_is == 0
            slab_is=str2num(files(ifile).name(9:16));
        else
            slab_is=[slab_is; str2num(files(ifile).name(9:16))];
        end
    end
end

kx=[0:d.nxm/2,-d.nxm/2+1:-1];
ky=[0:d.nym/2,-d.nym/2+1:-1];

K1mask=zeros(length(kx),length(ky));
K1mask(1,2)=1;
K1mask(1,end)=1;
K1mask(2,1)=1;
K1mask(2,2)=1;
K1mask(2,end)=1;
K1mask(end,1)=1;
K1mask(end,2)=1;
K1mask(end,end)=1;

trans_K1s=zeros(length(slab_is),1);
time=zeros(length(slab_is),1);

%tic
%for islab_i=1:length(slab_is)
parfor islab_i=1:length(slab_is)
    %read z-averaged velocity field
    mVx_zave=h5read([gDataDir gSimname '/stst3/slabzq1_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/var');
    mtime=h5read([gDataDir gSimname '/stst3/slabzq1_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/time');
    
    mVy_zave=h5read([gDataDir gSimname '/stst3/slabzq2_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/var');
    mtimeVy=h5read([gDataDir gSimname '/stst3/slabzq2_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/time');
    
    if mtime ~= mtimeVy
        disp('Error: time_vx != time_vy');
    end

    %move all velocities to cell center
    u=d.faceToCell(d.makePeriodic(mVx_zave,1),1);
    v=d.faceToCell(d.makePeriodic(mVy_zave,2),2);
    
    %filter velocity to only include K=1 mode
    u_K1=getFilteredVelocity(u,K1mask);
    v_K1=getFilteredVelocity(v,K1mask);

% FILTERED VELOCITY ON THE RIGHT
    %calculate derivatives
    du_Q1dx=getDerivativeXY(u_K1,d.dx,1,d);
    du_Q1dy=getDerivativeXY(u_K1,d.dy,2,d);
    dv_Q1dx=getDerivativeXY(v_K1,d.dx,1,d);
    dv_Q1dy=getDerivativeXY(v_K1,d.dy,2,d);
    
    %exclude K=1 from the left velocity to avoid diagonal contribution
    u_K=u-u_K1;
    v_K=v-v_K1;

    %calculate transport to K, u_K * u * grad u_Q
    %NB: neglecting d/dz and w terms!
    mtrans=u_K.*u.*du_Q1dx +...
            u_K.*v.*du_Q1dy +...
            v_K.*u.*dv_Q1dx +...
            v_K.*v.*dv_Q1dy ;
    trans_K1=mean(mtrans,[1,2]);

%FILTERED VELOCITY ON THE LEFT
%     %calculate derivatives (check if needed to exclude K=1 mode)
%     dudx=getDerivativeXY(u,d.dx,1,d);
%     dudy=getDerivativeXY(u,d.dy,2,d);
%     dvdx=getDerivativeXY(v,d.dx,1,d);
%     dvdy=getDerivativeXY(v,d.dy,2,d);
% 
%     %calculate transport to K, u_K * u * grad u_Q
%     %NB: neglecting d/dz and w terms!
%     mtrans=u_K1.*u.*dudx +...
%             u_K1.*v.*dudy +...
%             v_K1.*u.*dvdx +...
%             v_K1.*v.*dvdy ;
%     trans_K1=mean(mtrans,[1,2]);
    
    
    

    trans_K1s(islab_i)=trans_K1;
    time(islab_i)=mtime;
end
%toc

tsTransNoW_slabs=timeseries(trans_K1s,time,'Name','Horizontal transport 2D to K=1 slabs');

%interpolate to uniform grid
tsTransNoW_slabs=interpTs(tsTransNoW_slabs);

%moving mean
tsTransNoWmm_slabs=timeseries(movmean(tsTransNoW_slabs.Data,50),tsTransNoW_slabs.Time,'Name','moving mean transfer to K=1');

if gPlotting    
    %Plot time series
    figure
    scatter(tsTransNoW_slabs.Time,tsTransNoW_slabs.Data,10,'s','filled')
    hold on
    plot(tsTransNoWmm_slabs.Time,tsTransNoWmm_slabs.Data,'LineWidth',2)
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    hold off
    title(['2D energy transfer to K=1 mode ' gSimname])
    ylabel('Energy transfer')
    xlabel('Convective time')
    grid on
    box on
end

function val_K=getFilteredVelocity(val,Kmask)
    %caltulate fourier transform
    val_k=fft2(val);
    
    %filter in fourier space
    val_K_k=val_k.*Kmask;

    %back to real space
    val_K=ifft2(val_K_k);
end

function deriv=getDerivativeXY(y,dx,axis,d)
    deriv=circshift(diff(d.makePeriodic(y,axis),1,axis),axis); %face value
    deriv=d.faceToCell(d.makePeriodic(deriv,axis),axis); %cell value
    deriv=deriv./dx;
end
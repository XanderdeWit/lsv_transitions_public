%analyses one full series of cases

series='series1/'; %NB: directory of this series, appended by '/'

%find cases
dirs=dir([gDataDir series]);
dirs=dirs(3:end);

ncases=length(dirs);

tic

figure
for icase=1:ncases
    gSimname=[series dirs(icase).name];
    
    pr=h5read([gDataDir gSimname '/stafield_master.h5'],'/Pr');
    ra=h5read([gDataDir gSimname '/stafield_master.h5'],'/Ra');
    ro=h5read([gDataDir gSimname '/stafield_master.h5'],'/Ro');
    
    m_series_spectrum_k1_forcing_from_3d_gPlotting=gPlotting; gPlotting=false;
    spectrum_k2d_forcing_from_3d;
    gPlotting=m_series_spectrum_k1_forcing_from_3d_gPlotting;
    
    disp(['Ra = ' num2str(ra) ' and forceK1 = ' num2str(forcek(2))])
    
    loglog(k(2:end),forcek(2:end),'DisplayName', ['Ra=' num2str(ra)])
    hold on
end
toc

title('K2D forcing ')
xlabel('Horizontal wavenumber $k_H$')
ylabel('Spectrum $\langle u` \cdot \nabla u` \rangle_z(k_H)$')
legend
grid on
box on

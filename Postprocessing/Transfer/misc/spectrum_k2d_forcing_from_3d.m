%read velocity field
Vx=h5read([gDataDir gSimname '/continua_q1.h5'],'/Vth'); Vx=d.trim3D(Vx);
Vy=h5read([gDataDir gSimname '/continua_q2.h5'],'/Vr'); Vy=d.trim3D(Vy);
Vz=h5read([gDataDir gSimname '/continua_q3.h5'],'/Vz'); Vz=d.trim3D(Vz);

%move all velocities to cell center
Vx=d.faceToCell(d.makePeriodic(Vx,1),1);
Vy=d.faceToCell(d.makePeriodic(Vy,2),2);
Vz(:,:,end+1)=zeros(d.nxm,d.nym); Vz=d.faceToCell(Vz,3);

%calculate z-averaged velocity
Vx_zave=mean(Vx.*reshape(d.g3rm,1,1,d.nzm),3);
Vy_zave=mean(Vy.*reshape(d.g3rm,1,1,d.nzm),3);
Vz_zave=mean(Vz.*reshape(d.g3rm,1,1,d.nzm),3);

%calculate primed velocities
Vx_primed=Vx-Vx_zave;
Vy_primed=Vy-Vy_zave;
Vz_primed=Vz-Vz_zave;

%calculate gradient
[dVx_primed_dy,dVx_primed_dx,dVx_primed_dz]=gradient(Vx_primed,d.dx,d.dy,d.zms);
[dVy_primed_dy,dVy_primed_dx,dVy_primed_dz]=gradient(Vy_primed,d.dx,d.dy,d.zms);
[dVz_primed_dy,dVz_primed_dx,dVz_primed_dz]=gradient(Vz_primed,d.dx,d.dy,d.zms);

%calculate fluctuating force
forcex = Vx_primed .* dVx_primed_dx + Vy_primed .* dVx_primed_dy + Vz_primed .* dVx_primed_dz;
forcey = Vx_primed .* dVy_primed_dx + Vy_primed .* dVy_primed_dy + Vz_primed .* dVy_primed_dz;
forcez = Vx_primed .* dVz_primed_dx + Vy_primed .* dVz_primed_dy + Vz_primed .* dVz_primed_dz;

%calculate z-averaged force
forcex_zave=mean(forcex.*reshape(d.g3rm,1,1,d.nzm),3);
forcey_zave=mean(forcey.*reshape(d.g3rm,1,1,d.nzm),3);
forcez_zave=mean(forcez.*reshape(d.g3rm,1,1,d.nzm),3);

%calculate spectrum of force
k=0:min(d.nxm,d.nym)/2;
forcek=zeros(1,length(k));
    
forcexk=fft2(forcex_zave)./(d.nxm*d.nym);
forceyk=fft2(forcey_zave)./(d.nxm*d.nym);
forcezk=fft2(forcez_zave)./(d.nxm*d.nym);
    
kx=[0:d.nxm/2,-d.nxm/2+1:-1];
ky=[0:d.nym/2,-d.nym/2+1:-1];
for ikx=1:d.nxm
    for iky=1:d.nym
        mkx=kx(ikx);
        mky=ky(iky);
        %bin all wavenumbers with kH <= sqrt(kx^2+ky^2) < kH+1 together
        mk=floor(sqrt(mkx^2+mky^2));
        if mk<length(k)
            %TODO check if needed to take sqrt of different components
            forcek(1+mk)=forcek(1+mk)+abs(forcexk(ikx,iky))^2+abs(forceyk(ikx,iky))^2+abs(forcezk(ikx,iky))^2;
        end
    end
end

if gPlotting
    figure
    drawCross(d.xms,d.yms,sqrt(Vx_zave.^2+Vy_zave.^2+Vz_zave.^2)','auto','pcolor','hot',true);
    title(['$|\langle u \rangle_z|$ ' gSimname])
    
    figure
    drawCross(d.xms,d.yms,mean(sqrt(Vx_primed.^2+Vy_primed.^2+Vz_primed.^2).*reshape(d.g3rm,1,1,d.nzm),3)','auto','pcolor','hot',true);
    title(['$\langle |u`| \rangle_z$ ' gSimname])
    
    figure
    drawCross(d.xms,d.yms,sqrt(forcex_zave.^2+forcey_zave.^2+forcez_zave.^2)','auto','pcolor','hot',true);
    title(['$|\langle u` \cdot \nabla u` \rangle_z|$ ' gSimname])
    
    figure
    drawCross(d.xms,d.yms,abs(Vx_zave.*forcex_zave+Vy_zave.*forcey_zave+Vz_zave.*forcez_zave)','auto','pcolor','hot',true);
    title(['$|\langle u \rangle_z \cdot \langle u` \cdot \nabla u` \rangle_z|$ ' gSimname])
    
    ftot = mean(Vx_zave.*forcex_zave+Vy_zave.*forcey_zave+Vz_zave.*forcez_zave,'all')
    fmag = mean(sqrt(forcex_zave.^2+forcey_zave.^2+forcez_zave.^2),'all')
    fk1 = forcek(2)
    
    figure
    loglog(k(2:end),forcek(2:end))
    title(['K2D forcing ' gSimname])
    xlabel('Horizontal wavenumber $k_H$')
    ylabel('Spectrum $\langle u` \cdot \nabla u` \rangle_z(k_H)$')
    grid on
    box on
end

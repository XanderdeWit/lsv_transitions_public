%Plot kinetic energy transfer time series

%Read data
file = fopen([gDataDir gSimname '/kin_trans.out'],'r');
data = fscanf(file,'%f %f\n',[6 Inf]);

tsFtot=timeseries(data(2,:)',data(1,:)','Name','2D forcing');
tsFmag=timeseries(data(3,:)',data(1,:)','Name','2D fluctuating force');
tsFk1=timeseries(data(4,:)',data(1,:)','Name','2D fluctuating force K=1');

tsTrans=timeseries(data(5,:)',data(1,:)','Name','Transport 2D to K=1');
tsTransNoW=timeseries(data(6,:)',data(1,:)','Name','Horizontal transport 2D to K=1');

%TODO remove if statement and else block
if isfile([gDataDir gSimname '/kin_trans2.out'])
    file = fopen([gDataDir gSimname '/kin_trans2.out'],'r');
    data = fscanf(file,'%f %f\n',[3 Inf]);
 
    tsTransBC=timeseries(data(2,:)',data(1,:)','Name','Transport BC to K=1');
    tsDiss2D=timeseries(data(3,:)',data(1,:)','Name','2D dissipation');
else
    tsTransBC=timeseries(tsTrans.Data,tsTrans.Time,'Name','Transport BC to K=1');
    tsDiss2D=timeseries(-tsFtot.Data,tsFtot.Time,'Name','2D dissipation');
end

%Interpolate to uniform time grid and remove duplicate points
tsFtot=interpTs(tsFtot);
tsFmag=interpTs(tsFmag);
tsFk1=interpTs(tsFk1);
tsTrans=interpTs(tsTrans);
tsTransNoW=interpTs(tsTransNoW);
tsTransBC=interpTs(tsTransBC,tsFtot.Time);
tsDiss2D=interpTs(tsDiss2D,tsFtot.Time);

%get nusselt number
m_ts_kin_trans_gPlotting=gPlotting; gPlotting=false;
nusse;
tsNuTe=tss(4);
gPlotting=m_ts_kin_trans_gPlotting;
tsNuTe=interpTs(tsNuTe,tsFtot.Time);

% %get transport from slabs
% m_ts_kin_trans_gPlotting=gPlotting; gPlotting=false;
% ts_transfer_zave_K1;
% gPlotting=m_ts_kin_trans_gPlotting;
% tsTransNoW_slabs=interpTs(tsTransNoW_slabs,tsFtot.Time);

%calculate ratios
tsFtotRatio=timeseries(2e4 .* tsFtot.Data ./ tsNuTe.Data, tsFtot.Time, 'Name', [tsFtot.Name ' ratio']);
tsFmagRatio=timeseries(1e2 .* tsFmag.Data ./ tsNuTe.Data, tsFmag.Time, 'Name', [tsFmag.Name ' ratio']);
tsFk1Ratio=timeseries(3e5 .* tsFk1.Data ./ tsNuTe.Data, tsFk1.Time, 'Name', [tsFk1.Name ' ratio']);
tsTransRatio=timeseries(1e5 .* tsTrans.Data ./ tsNuTe.Data, tsTrans.Time, 'Name', [tsTrans.Name ' ratio']);
tsTransNoWRatio=timeseries(1e5 .* tsTransNoW.Data ./ tsNuTe.Data, tsTransNoW.Time, 'Name', [tsTransNoW.Name ' ratio']);
%tsTransNoWRatio_slabs=timeseries(1e5 .* tsTransNoW_slabs.Data ./ tsNuTe.Data, tsTransNoW_slabs.Time, 'Name', [tsTransNoW_slabs.Name ' ratio']);
tsTransBCRatio=timeseries(1e5 .* tsTransBC.Data ./ tsNuTe.Data, tsTransBC.Time, 'Name', [tsTransBC.Name ' ratio']);
tsDiss2DRatio=timeseries(-4e3 .* tsDiss2D.Data ./ tsNuTe.Data, tsDiss2D.Time, 'Name', [tsDiss2D.Name ' ratio']);

%Do error analysis
ftotRatioStats=getTsStats(tsFtotRatio,gPlotting,gPlotting,gTstart);
fmagRatioStats=getTsStats(tsFmagRatio,gPlotting,gPlotting,gTstart);
fk1RatioStats=getTsStats(tsFk1Ratio,gPlotting,gPlotting,gTstart);
transRatioStats=getTsStats(tsTransRatio,gPlotting,gPlotting,gTstart);
transNoWRatioStats=getTsStats(tsTransNoWRatio,gPlotting,gPlotting,gTstart);
transBCRatioStats=getTsStats(tsTransBCRatio,gPlotting,gPlotting,gTstart);
diss2DRatioStats=getTsStats(tsDiss2DRatio,gPlotting,gPlotting,gTstart);

ftotStats=getTsStats(tsFtot,gPlotting,gPlotting,gTstart);
fmagStats=getTsStats(tsFmag,gPlotting,gPlotting,gTstart);
fk1Stats=getTsStats(tsFk1,gPlotting,gPlotting,gTstart);
transStats=getTsStats(tsTrans,gPlotting,gPlotting,gTstart);
transNoWStats=getTsStats(tsTransNoW,gPlotting,gPlotting,gTstart);
transBCStats=getTsStats(tsTransBC,gPlotting,gPlotting,gTstart);
diss2DStats=getTsStats(tsDiss2D,gPlotting,gPlotting,gTstart);
nuTeStats=getTsStats(tsNuTe,gPlotting,gPlotting,gTstart);

if gPlotting
    %plot parameters
    figure
    %scatter(tsFtot.Time,tsFtot.Data,10,'s','filled','DisplayName',tsFtot.Name)
    hold on
    %scatter(tsFmag.Time,tsFmag.Data,10,'s','filled','DisplayName',tsFmag.Name)
    scatter(tsFk1.Time,tsFk1.Data,10,'s','filled','DisplayName',tsFk1.Name)
    scatter(tsTrans.Time,tsTrans.Data,10,'s','filled','DisplayName',tsTrans.Name)
    scatter(tsTransNoW.Time,tsTransNoW.Data,10,'s','filled','DisplayName',tsTransNoW.Name)
    %scatter(tsTransNoW_slabs.Time,tsTransNoW_slabs.Data,10,'s','filled','DisplayName',tsTransNoW_slabs.Name)
    scatter(tsTransBC.Time,tsTransBC.Data,10,'s','filled','DisplayName',tsTransBC.Name)
    legend
    title(['Kinetic transport ' gSimname])
    xlabel('Convective time')
    ylabel('Ratio')
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    grid on
    box on
    
    figure
    scatter(tsFtot.Time,tsFtot.Data,10,'s','filled','DisplayName',tsFtot.Name)
    hold on
    scatter(tsDiss2D.Time,tsDiss2D.Data,10,'s','filled','DisplayName',tsDiss2D.Name)
    legend
    title(['Kinetic transport ' gSimname])
    xlabel('Convective time')
    ylabel('Ratio')
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    grid on
    box on
    
    figure
    scatter(tsFmag.Time,tsFmag.Data,10,'s','filled','DisplayName',tsFmag.Name)
    legend
    title(['Kinetic transport ' gSimname])
    xlabel('Convective time')
    ylabel('Ratio')
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    grid on
    box on
    
    %plot ratios
    mm_time=200;
    figure
    scatter(tsFtotRatio.Time,movmean(tsFtotRatio.Data,mm_time),10,'s','filled','DisplayName',tsFtotRatio.Name)
    hold on
    scatter(tsFmagRatio.Time,movmean(tsFmagRatio.Data,mm_time),10,'s','filled','DisplayName',tsFmagRatio.Name)
    scatter(tsFk1Ratio.Time,movmean(tsFk1Ratio.Data,mm_time),10,'s','filled','DisplayName',tsFk1Ratio.Name)
    scatter(tsTransRatio.Time,movmean(tsTransRatio.Data,mm_time),10,'s','filled','DisplayName',tsTransRatio.Name)
    %scatter(tsTransNoWRatio.Time,movmean(tsTransNoWRatio.Data,mm_time),10,'s','filled','DisplayName',tsTransNoWRatio.Name)
    %scatter(tsTransNoWRatio_slabs.Time,movmean(tsTransNoWRatio_slabs.Data,mm_time),10,'s','filled','DisplayName',tsTransNoWRatio_slabs.Name)
    scatter(tsTransBCRatio.Time,movmean(tsTransBCRatio.Data,mm_time),10,'s','filled','DisplayName',tsTransBCRatio.Name)
    scatter(tsDiss2DRatio.Time,movmean(tsDiss2DRatio.Data,mm_time),10,'s','filled','DisplayName',tsDiss2DRatio.Name)
    legend
    set(gca,'ColorOrderIndex',1)
    scatter(tsFtotRatio.Time,tsFtotRatio.Data,10,'s','filled','DisplayName',tsFtotRatio.Name,'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsFmagRatio.Time,tsFmagRatio.Data,10,'s','filled','DisplayName',tsFmagRatio.Name,'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsFk1Ratio.Time,tsFk1Ratio.Data,10,'s','filled','DisplayName',tsFk1Ratio.Name,'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsTransRatio.Time,tsTransRatio.Data,10,'s','filled','DisplayName',tsTransRatio.Name,'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    %scatter(tsTransNoWRatio.Time,tsTransNoWRatio.Data,10,'s','filled','DisplayName',tsTransNoWRatio.Name,'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    %scatter(tsTransNoWRatio_slabs.Time,tsTransNoWRatio_slabs.Data,10,'s','filled','DisplayName',tsTransNoWRatio_slabs.Name,'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsTransBCRatio.Time,tsTransBCRatio.Data,10,'s','filled','DisplayName',tsTransBCRatio.Name,'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsDiss2DRatio.Time,tsDiss2DRatio.Data,10,'s','filled','DisplayName',tsDiss2DRatio.Name,'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
    title(['Kinetic transport ratios ' gSimname])
    xlabel('Convective time')
    ylabel('Ratio')
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    ylim([-0.3,1]) ; yline(0,'k');
    grid on
    box on
end
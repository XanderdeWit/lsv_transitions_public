%% REAL SPACE

% maxK, maxQ define largest K value to be included in the energy transfer analysis
maxK=min(d.nxm,d.nym)/4-1;

%read velocity field
Vx=h5read([gDataDir gSimname '/continua_q1.h5'],'/Vth'); Vx=d.trim3D(Vx);
Vy=h5read([gDataDir gSimname '/continua_q2.h5'],'/Vr'); Vy=d.trim3D(Vy);
Vz=h5read([gDataDir gSimname '/continua_q3.h5'],'/Vz'); Vz=d.trim3D(Vz);

%move all velocities to cell center
Vx=d.faceToCell(d.makePeriodic(Vx,1),1);
Vy=d.faceToCell(d.makePeriodic(Vy,2),2);
Vz(:,:,end+1)=zeros(d.nxm,d.nym); Vz=d.faceToCell(Vz,3);

%calculate z-averaged velocity
Vx_zave=mean(Vx.*reshape(d.g3rm,1,1,d.nzm),3);
Vy_zave=mean(Vy.*reshape(d.g3rm,1,1,d.nzm),3);
Vz_zave=mean(Vz.*reshape(d.g3rm,1,1,d.nzm),3);

%calculate primed velocities
Vx_primed=Vx-Vx_zave;
Vy_primed=Vy-Vy_zave;
Vz_primed=Vz-Vz_zave;

%calculate Kmask
kx=[0:d.nxm/2,-d.nxm/2+1:-1];
ky=[0:d.nym/2,-d.nym/2+1:-1];

Kmask=zeros(length(kx),length(ky),maxK+1);
for ikx=1:length(kx)
    for iky=1:length(ky)
        mkx=kx(ikx);
        mky=ky(iky);
        %bin all wavenumbers with kH <= sqrt(kx^2+ky^2) < kH+1 together
        mK=floor(sqrt(mkx^2+mky^2));
        if mK<=maxK
            Kmask(ikx,iky,mK+1)=1;
        end
    end
end

tic

%calculate forcing
forcex = zeros(d.nxm,d.nym,maxK+1);
forcey = zeros(d.nxm,d.nym,maxK+1);
forcez = zeros(d.nxm,d.nym,maxK+1);

%for iz=1:d.nzm
parfor iz=1:d.nzm
    up=Vx_primed(:,:,iz);
    vp=Vy_primed(:,:,iz);
    wp=Vz_primed(:,:,iz);
    
    u_Q=getFilteredVelocity(up,Kmask); v_Q=getFilteredVelocity(vp,Kmask); w_Q=getFilteredVelocity(wp,Kmask);
    
    if iz~=1
        u_Q_down=getFilteredVelocity(Vx_primed(:,:,iz-1),Kmask); v_Q_down=getFilteredVelocity(Vy_primed(:,:,iz-1),Kmask); w_Q_down=getFilteredVelocity(Vz_primed(:,:,iz-1),Kmask);
    end
    
    if iz~=d.nzm
        u_Q_up=getFilteredVelocity(Vx_primed(:,:,iz+1),Kmask); v_Q_up=getFilteredVelocity(Vy_primed(:,:,iz+1),Kmask); w_Q_up=getFilteredVelocity(Vz_primed(:,:,iz+1),Kmask);
    end
    
    du_Qdx=getDerivativeXY(u_Q,d.dx,1,d);
    du_Qdy=getDerivativeXY(u_Q,d.dy,2,d);
    dv_Qdx=getDerivativeXY(v_Q,d.dx,1,d);
    dv_Qdy=getDerivativeXY(v_Q,d.dy,2,d);
    dw_Qdx=getDerivativeXY(w_Q,d.dx,1,d);
    dw_Qdy=getDerivativeXY(w_Q,d.dy,2,d);
    
    %if iz==1
    %    du_Qdz=(u_Q_up-u_Q)./(d.zms(2)-d.zms(1));
    %    dv_Qdz=(v_Q_up-v_Q)./(d.zms(2)-d.zms(1));
    %    dw_Qdz=(w_Q_up-w_Q)./(d.zms(2)-d.zms(1));
    %elseif iz==d.nzm
    %    du_Qdz=(u_Q-u_Q_down)./(d.zms(d.nzm)-d.zms(d.nzm-1));
    %    dv_Qdz=(v_Q-v_Q_down)./(d.zms(d.nzm)-d.zms(d.nzm-1));
    %    dw_Qdz=(w_Q-w_Q_down)./(d.zms(d.nzm)-d.zms(d.nzm-1));
    %else
    %    du_Qdz=(u_Q_up-u_Q_down)./(d.zms(iz+1)-d.zms(iz-1));
    %    dv_Qdz=(v_Q_up-v_Q_down)./(d.zms(iz+1)-d.zms(iz-1));
    %    dw_Qdz=(w_Q_up-w_Q_down)./(d.zms(iz+1)-d.zms(iz-1));
    %end
    if iz==1 || iz==2 %stress free
       du_Qdz=0*u_Q;
       dv_Qdz=0*u_Q;
       dw_Qdz=0*u_Q;
    elseif iz==d.nzm
       du_Qdz=0*u_Q;
       dv_Qdz=0*u_Q;
       dw_Qdz=0*u_Q;
    else
       du_Qdz=(u_Q_up-u_Q_down)./(d.zms(iz+1)-d.zms(iz-1));
       dv_Qdz=(v_Q_up-v_Q_down)./(d.zms(iz+1)-d.zms(iz-1));
       dw_Qdz=(w_Q_up-w_Q_down)./(d.zms(iz+1)-d.zms(iz-1));
    end
    
    forcex = forcex + (d.g3rm(iz)/d.nzm) * (up .* du_Qdx + vp .* du_Qdy + wp .* du_Qdz);
    forcey = forcey + (d.g3rm(iz)/d.nzm) * (up .* dv_Qdx + vp .* dv_Qdy + wp .* dv_Qdz);
%    forcez = forcez + (d.g3rm(iz)/d.nzm) * (up .* dw_Qdx + vp .* dw_Qdy + wp .* dw_Qdz);
end

u_K=getFilteredVelocity(Vx_zave,Kmask);
v_K=getFilteredVelocity(Vy_zave,Kmask);
w_K=getFilteredVelocity(Vz_zave,Kmask);

%calculate transport from Q to K, u_K * u * grad u_Q
trans=zeros(maxK+1,maxK+1);
for iQ=1:size(trans,2) %loop here to keep it memory-friendly
    mtrans=u_K(:,:,:).*forcex(:,:,iQ) +...
        v_K(:,:,:).*forcey(:,:,iQ); % +...
%        w_K(:,:,:).*forcez(:,:,iQ);
    trans(:,iQ)=squeeze(mean(mtrans,[1,2]));
end

trans_sumQ=sum(trans,2);

toc

%% FOURIER SPACE (LOAD FROM CLUSTER) !!CHECK WHICH CASE THIS CONTAINS
load([gDataDir 'transfer.mat'])

%% PLOT

if gPlotting
    figure
    plot(-trans_sumQ(2:end),1:maxK,'LineWidth',1.5)
    ylabel('$K$')
    xlabel('$\bar{u}_K \cdot u^\prime \cdot \nabla u^\prime$ Real')
    box on
    grid on
    
    figure
    plot(sum(transferQK_bcbcbt_total,1),1:maxK,'LineWidth',1.5)
    ylabel('$K$')
    xlabel('$\bar{u}_K \cdot u^\prime \cdot \nabla u^\prime$ Fourier')
    box on
    grid on

    figure
    imagesc(-trans(2:end,2:end),[-3e-6,3e-6])
    set(gca,'YDir','normal')
    colormap(gMyCM)
    colorbar
    xlabel('$Q$')
    ylabel('$K$')
    title('$\bar{u}_K \cdot u^\prime \cdot \nabla u_Q^\prime$ Real')
    
    figure
    imagesc(transferQK_bcbcbt_total',[-3e-6,3e-6])
    set(gca,'YDir','normal')
    colormap(gMyCM)
    colorbar
    xlabel('$Q$')
    ylabel('$K$')
    title('$\bar{u}_K \cdot u^\prime \cdot \nabla u_Q^\prime$ Fourier')
end


function val_K=getFilteredVelocity(val,Kmask)
    %caltulate fourier transform
    val_k=fft2(val);
    
    %filter in fourier space
    val_K_k=val_k.*Kmask;

    %back to real space
    val_K=zeros(size(val,1),size(val,2),size(Kmask,3));
    for iK=1:size(val_K,3)
        val_K(:,:,iK)=ifft2(val_K_k(:,:,iK));
    end
end

function deriv=getDerivativeXY(y,dx,axis,d)
    deriv=circshift(diff(d.makePeriodic(y,axis),1,axis),axis); %face value
    deriv=d.faceToCell(d.makePeriodic(deriv,axis),axis); %cell value
    deriv=deriv./dx;
end


averagingTime=h5read([gDataDir gSimname '/stafield_master.h5'],'/averaging_time');

transQK_BT_total=h5read([gDataDir gSimname '/stafield_master.h5'],'/transqk_bt');
transQK_BT=transQK_BT_total/double(averagingTime);
transQK_BC_total=h5read([gDataDir gSimname '/stafield_master.h5'],'/transqk_bc');
transQK_BC=transQK_BC_total/double(averagingTime);

maxQ=50;
maxK=50;

kmask_correction;

if gPlotting
    
    figure
    imagesc(transQK_BT(1:maxQ,1:maxK)',[-3e-6,3e-6])
    set(gca,'YDir','normal')
    colormap(gMyCM)
    colorbar
    xlabel('$Q$')
    ylabel('$K$')
    title('$-\bar{u}_K \cdot \bar{u} \cdot \nabla \bar{u}_Q$')
    colorbar('TickLabelInterpreter','latex')
    
    figure
    imagesc(transQK_BT(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),[-3e-6,3e-6])
    set(gca,'YDir','normal')
    colormap(gMyCM)
    colorbar
    xlabel('$Q$')
    ylabel('$K$')
    title('$-\bar{u}_K \cdot \bar{u} \cdot \nabla \bar{u}_Q$ corrected')
    colorbar('TickLabelInterpreter','latex')

    figure
    plot(transQK_BT(1:maxQ,1),'LineWidth',2)
    xlabel('$Q$')
    ylabel('$-\bar{u}_{K=1} \cdot \bar{u} \cdot \nabla \bar{u}$')
    xlim([1,maxQ])
    grid on
    box on

    figure
    imagesc(transQK_BC(1:maxQ,1:maxK)',[-3e-6,3e-6])
    set(gca,'YDir','normal')
    colormap(gMyCM)
    colorbar
    xlabel('$Q$')
    ylabel('$K$')
    title('$-\bar{u}_K \cdot u^\prime \cdot \nabla u_Q^\prime$')
    colorbar('TickLabelInterpreter','latex')
    
    figure
    imagesc(transQK_BC(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),[-3e-6,3e-6])
    set(gca,'YDir','normal')
    colormap(gMyCM)
    colorbar
    xlabel('$Q$')
    ylabel('$K$')
    title('$-\bar{u}_K \cdot u^\prime \cdot \nabla u_Q^\prime$ corrected')
    colorbar('TickLabelInterpreter','latex')

    figure
    plot(transQK_BC(1:maxQ,1),'LineWidth',2)
    xlabel('$Q$')
    ylabel('$-\bar{u}_{K=1} \cdot u^\prime \cdot \nabla u^\prime$')
    xlim([1,maxQ])
    grid on
    box on
end
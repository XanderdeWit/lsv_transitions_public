%read velocity field
Vx=h5read([gDataDir gSimname '/continua_q1.h5'],'/Vth'); Vx=d.trim3D(Vx);
Vy=h5read([gDataDir gSimname '/continua_q2.h5'],'/Vr'); Vy=d.trim3D(Vy);
Vz=h5read([gDataDir gSimname '/continua_q3.h5'],'/Vz'); Vz=d.trim3D(Vz);

%move all velocities to cell center
Vx=d.faceToCell(d.makePeriodic(Vx,1),1);
Vy=d.faceToCell(d.makePeriodic(Vy,2),2);
Vz(:,:,end+1)=zeros(d.nxm,d.nym); Vz=d.faceToCell(Vz,3);

%calculate z-averaged velocity
Vx_zave=mean(Vx.*reshape(d.g3rm,1,1,d.nzm),3);
Vy_zave=mean(Vy.*reshape(d.g3rm,1,1,d.nzm),3);
%Vz_zave=mean(Vz.*reshape(d.g3rm,1,1,d.nzm),3);

%calculate primed velocities
Vx_primed=Vx-Vx_zave;
Vy_primed=Vy-Vy_zave;
Vz_primed=Vz;%-Vz_zave;

%calculate gradient
[dVx_primed_dy,dVx_primed_dx,dVx_primed_dz]=gradient(Vx_primed,d.dx,d.dy,d.zms);
[dVy_primed_dy,dVy_primed_dx,dVy_primed_dz]=gradient(Vy_primed,d.dx,d.dy,d.zms);
[dVz_primed_dy,dVz_primed_dx,dVz_primed_dz]=gradient(Vz_primed,d.dx,d.dy,d.zms);

%calculate fluctuating force
forcex = Vx_primed .* dVx_primed_dx + Vy_primed .* dVx_primed_dy + Vz_primed .* dVx_primed_dz;
forcey = Vx_primed .* dVy_primed_dx + Vy_primed .* dVy_primed_dy + Vz_primed .* dVy_primed_dz;
forcez = Vx_primed .* dVz_primed_dx + Vy_primed .* dVz_primed_dy + Vz_primed .* dVz_primed_dz;

%calculate z-averaged force
forcex_zave=mean(forcex.*reshape(d.g3rm,1,1,d.nzm),3);
forcey_zave=mean(forcey.*reshape(d.g3rm,1,1,d.nzm),3);
%forcez_zave=mean(forcez.*reshape(d.g3rm,1,1,d.nzm),3);

%calculate filtered z-averaged velocity
maxK=min(d.nxm,d.nym)/4-1;
kx=[0:d.nxm/2,-d.nxm/2+1:-1];
ky=[0:d.nym/2,-d.nym/2+1:-1];
Kmask=zeros(length(kx),length(ky),maxK+1);
for ikx=1:length(kx)
    for iky=1:length(ky)
        mkx=kx(ikx);
        mky=ky(iky);
        %bin all wavenumbers with kH <= sqrt(kx^2+ky^2) < kH+1 together
        mK=floor(sqrt(mkx^2+mky^2));
        if mK<=maxK
            Kmask(ikx,iky,mK+1)=1;
        end
    end
end
u_K=getFilteredVelocity(Vx_zave,Kmask);
v_K=getFilteredVelocity(Vy_zave,Kmask);
%w_K=getFilteredVelocity(Vz_zave,Kmask);

%calculate forcing
forcing_K_xy = u_K.*forcex_zave+v_K.*forcey_zave;%+w_K.*forcez_zave;
forcing_K = squeeze(mean(forcing_K_xy,[1,2]));

forcing_tot_xy = Vx_zave.*forcex_zave+Vy_zave.*forcey_zave;%+Vz_zave.*forcez_zave;
forcing_tot = squeeze(mean(forcing_K_xy,[1,2]));

%plot
if gPlotting
    figure
    lims=[-1,1]*max(abs(forcing_K_xy(:,:,2)),[],'all');
    drawCross(d.xms,d.yms,forcing_K_xy(:,:,2)',lims,'pcolor',gMyCM,true);
    title(['BC forcing K=1 ' gSimname])
    
    figure
    %lims=[-1,1]*max(abs(forcing_K_xy(:,:,2)),[],'all');
    drawCross(d.xms,d.yms,sum(forcing_K_xy,3)',lims,'pcolor',gMyCM,true);
    title(['BC total forcing ' gSimname])
    
    figure
    %lims=[-1,1]*max(abs(forcing_K_xy(:,:,2)),[],'all');
    drawCross(d.xms,d.yms,forcing_tot_xy',lims,'pcolor',gMyCM,true);
    title(['BC total forcing check ' gSimname])
    
    figure
    plot((0:maxK)',forcing_K(1:end))
    title(['Forcing spectrum ' gSimname])
    xlabel('Horizontal wavenumber $k_H$')
    ylabel('Spectrum')
    xlim([1,maxK])
    grid on
    box on
    
    forcing_K1=forcing_K(2)
end

function val_K=getFilteredVelocity(val,Kmask)
    %caltulate fourier transform
    val_k=fft2(val);
    
    %filter in fourier space
    val_K_k=val_k.*Kmask;

    %back to real space
    val_K=zeros(size(val,1),size(val,2),size(Kmask,3));
    for iK=1:size(val_K,3)
        val_K(:,:,iK)=ifft2(val_K_k(:,:,iK));
    end
end

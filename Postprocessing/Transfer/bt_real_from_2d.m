%visualisation of transfer to K=1 in real space;

%read z-averaged velocity field
q1=h5read([gDataDir gSimname '/stst3/slabzq1_00000001.h5'],'/var'); 
q2=h5read([gDataDir gSimname '/stst3/slabzq2_00000001.h5'],'/var');

kx=[0:d.nxm/2,-d.nxm/2+1:-1];
ky=[0:d.nym/2,-d.nym/2+1:-1];

K1mask=zeros(length(kx),length(ky));
K1mask(1,2)=1;
K1mask(1,end)=1;
K1mask(2,1)=1;
K1mask(2,2)=1;
K1mask(2,end)=1;
K1mask(end,1)=1;
K1mask(end,2)=1;
K1mask(end,end)=1;
    
%move all velocities to cell center
u=d.faceToCell(d.makePeriodic(q1,1),1);
v=d.faceToCell(d.makePeriodic(q2,2),2);

%filter velocity to only include K=1 mode
u_K1=getFilteredVelocity(u,K1mask);
v_K1=getFilteredVelocity(v,K1mask);

% FILTERED VELOCITY ON THE RIGHT
%calculate derivatives
du_Q1dx=getDerivativeXY(u_K1,d.dx,1,d);
du_Q1dy=getDerivativeXY(u_K1,d.dy,2,d);
dv_Q1dx=getDerivativeXY(v_K1,d.dx,1,d);
dv_Q1dy=getDerivativeXY(v_K1,d.dy,2,d);

%exclude K=1 from the left velocity to avoid diagonal contribution
u_K=u-u_K1;
v_K=v-v_K1;

%calculate transport to K, u_K * u * grad u_Q
%NB: neglecting d/dz and w terms!
trans_K1_xy=u_K.*u.*du_Q1dx +...
        u_K.*v.*du_Q1dy +...
        v_K.*u.*dv_Q1dx +...
        v_K.*v.*dv_Q1dy ;

trans_K1=mean(trans_K1_xy,[1,2])

if gPlotting
    figure
    drawCross(d.xms,d.yms,trans_K1_xy',[-max(abs(trans_K1_xy),[],'all'),max(abs(trans_K1_xy),[],'all')],'pcolor',gMyCM,true);
    title(['$ \bar{u} \cdot (\bar{u} \cdot \nabla \bar{u}_{K=1})$ ' gSimname])

    %figure
    %drawCross(d.xms,d.yms,getHorKin(q1,q2,d)',[0,0.2],'pcolor',gMyCM,true);
    %title(['Horizontal kinetic energy ' gSimname])
end


function val_K=getFilteredVelocity(val,Kmask)
    %caltulate fourier transform
    val_k=fft2(val);
    
    %filter in fourier space
    val_K_k=val_k.*Kmask;

    %back to real space
    val_K=zeros(size(val,1),size(val,2),size(Kmask,3));
    for iK=1:size(val_K,3)
        val_K(:,:,iK)=ifft2(val_K_k(:,:,iK));
    end
end

function deriv=getDerivativeXY(y,dx,axis,d)
    deriv=circshift(diff(d.makePeriodic(y,axis),1,axis),axis); %face value
    deriv=d.faceToCell(d.makePeriodic(deriv,axis),axis); %cell value
    deriv=deriv./dx;
end

%% Fourier space
% maxK, maxQ define largest K and Q values to be included in the energy transfer analysis
maxK=min(d.nxm,d.nym)/4-1;
maxQ=maxK;

%read z-averaged velocity field
q1=h5read([gDataDir gSimname '/stst3/slabzq1_00000001.h5'],'/var'); 
q2=h5read([gDataDir gSimname '/stst3/slabzq2_00000001.h5'],'/var'); 

%move all velocities to cell center
q1=d.faceToCell(d.makePeriodic(q1,1),1);
q2=d.faceToCell(d.makePeriodic(q2,2),2);

% the required fields
transferQK=zeros(maxQ,maxK);

% prepare wavenumbers - note that I use integer indices, later to be converted to actual wavenumbers 2*pi/L
kx=(1:1:d.nxm)'*ones(1,d.nxm)-d.nxm/2-1;
ky=ones(d.nym,1)*(1:1:d.nym)-d.nym/2-1;
kh=floor(sqrt(kx.^2+ky.^2)); %NB: was originally ceil

tic

%kLx=1;
%kLy=1;
kLx=2*pi/d.X;
kLy=2*pi/d.Y;

u=q1;
v=q2;
    
% calculate Fourier-transformed velocity fields
Fu=fftshift(fft2(u))/(d.nxm*d.nym);
Fv=fftshift(fft2(v))/(d.nxm*d.nym);

% loop over wavenumbers K and Q
for Q=1:maxQ
    for K=1:maxK
      [ixK,iyK]=find(kh==K);
      [ixQ,iyQ]=find(kh==Q);
      for kk=1:size(ixK,1)
        for qq=1:size(ixQ,1)
          k=ixK(kk);
          l=iyK(kk);
          r=ixQ(qq);
          s=iyQ(qq);
          kplusr=d.nxm/2+1+kx(k,l)+kx(r,s);
          lpluss=d.nym/2+1+ky(k,l)+ky(r,s);

          transferQK(Q,K)=transferQK(Q,K)- ...
            real(1i*(Fu(k,l)*Fu(r,s) + Fv(k,l)*Fv(r,s)) * ... 
                 (kx(r,s)*kLx*conj(Fu(kplusr,lpluss)) + ky(r,s)*kLy*conj(Fv(kplusr,lpluss))));
        end
      end
    end
end
toc
  

%% Real space

maxK=min(d.nxm,d.nym)/4-1;

kx=[0:d.nxm/2,-d.nxm/2+1:-1];
ky=[0:d.nym/2,-d.nym/2+1:-1];

Kmask=zeros(length(kx),length(ky),maxK+1);
for ikx=1:length(kx)
    for iky=1:length(ky)
        mkx=kx(ikx);
        mky=ky(iky);
        %bin all wavenumbers with kH <= sqrt(kx^2+ky^2) < kH+1 together
        mK=floor(sqrt(mkx^2+mky^2));
        if mK<=maxK
            Kmask(ikx,iky,mK+1)=1;
        end
    end
end

tic

u_K=getFilteredVelocity(u,Kmask);
v_K=getFilteredVelocity(v,Kmask);

%calculate derivatives
du_Qdx=getDerivativeXY(u_K,d.dx,1,d);
du_Qdy=getDerivativeXY(u_K,d.dy,2,d);
dv_Qdx=getDerivativeXY(v_K,d.dx,1,d);
dv_Qdy=getDerivativeXY(v_K,d.dy,2,d);

%calculate transport from Q to K, u_K * u * grad u_Q
%NB: neglecting d/dz terms!
trans=zeros(maxK+1,maxK+1);
for iQ=1:size(trans,2) %loop here to keep it memory-friendly
    mtrans=u_K(:,:,:).*u(:,:).*du_Qdx(:,:,iQ) +...
        u_K(:,:,:).*v(:,:).*du_Qdy(:,:,iQ) +...
        v_K(:,:,:).*u(:,:).*dv_Qdx(:,:,iQ) +...
        v_K(:,:,:).*v(:,:).*dv_Qdy(:,:,iQ);
    trans(:,iQ)=squeeze(mean(mtrans,[1,2]));
end

%mirror the results in diagonal, as the real space calculation is accurate for K>Q
trans_mirrored=zeros(size(trans));
for i=1:size(trans,1)
    for j=1:size(trans,2)
        if i > j
            trans_mirrored(i,j)=trans(i,j);
        elseif i < j
            trans_mirrored(i,j)=-trans(j,i);
        end
    end
end

toc

%% Plotting

if gPlotting
    figure
    imagesc(transferQK',[-3e-6,3e-6])
    set(gca,'YDir','normal')
    colormap(gMyCM)
    colorbar
    xlabel('$Q$')
    ylabel('$K$')
    title('$\bar{u}_K \cdot \bar{u} \cdot \nabla \bar{u}_Q$')
    
    figure
    imagesc(-trans(2:end,2:end),[-3e-6,3e-6])
    set(gca,'YDir','normal')
    colormap(gMyCM)
    colorbar
    xlabel('$Q$')
    ylabel('$K$')
    title('$\bar{u}_K \cdot \bar{u} \cdot \nabla \bar{u}_Q$')

    figure
    imagesc(-trans_mirrored(2:end,2:end),[-3e-6,3e-6])
    set(gca,'YDir','normal')
    colormap(gMyCM)
    colorbar
    xlabel('$Q$')
    ylabel('$K$')
    title('$\bar{u}_K \cdot \bar{u} \cdot \nabla \bar{u}_Q$')

%     figure
%     imagesc(-trans_mirrored(2:end,2:end)+transferQK,[-3e-6,3e-6])
%     set(gca,'YDir','normal')
%     colormap(gMyCM)
%     colorbar
%     xlabel('$Q$')
%     ylabel('$K$')
%     title('Difference')

    figure
    plot(1:maxK,-trans_mirrored(2,2:end),'LineWidth',1.5,'DisplayName','Real space')
    hold on
    plot(1:maxK,transferQK(:,1),'--','LineWidth',1.5,'DisplayName','Fourier')
    ylabel('$Q$')
    xlabel('$\bar{u}_{K=1} \cdot \bar{u} \cdot \nabla \bar{u}_Q$')
    legend
    box on
    grid on
end

%% compare K=1 forcing
sum(trans_mirrored(2,:))
sum(trans(2,:))
sum(trans(:,2))
sum(transferQK(1,:))
sum(transferQK(:,1))

%% UTILS
function val_K=getFilteredVelocity(val,Kmask)
    %caltulate fourier transform
    val_k=fft2(val);
    
    %filter in fourier space
    val_K_k=val_k.*Kmask;

    %back to real space
    val_K=zeros(size(val,1),size(val,2),size(Kmask,3));
    for iK=1:size(val_K,3)
        val_K(:,:,iK)=ifft2(val_K_k(:,:,iK));
    end
end

function deriv=getDerivativeXY(y,dx,axis,d)
    deriv=circshift(diff(d.makePeriodic(y,axis),1,axis),axis); %face value
    deriv=d.faceToCell(d.makePeriodic(deriv,axis),axis); %cell value
    deriv=deriv./dx;
end
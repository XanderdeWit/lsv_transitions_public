%Plot kinetic energy transfer time series

%read data
if isfile([gDataDir gSimname '/kin_trans3.out'])
    file = fopen([gDataDir gSimname '/kin_trans3.out'],'r');
    data = fscanf(file,'%f %f\n',[5 Inf]);
    
    tsBTK1=timeseries(data(2,:)',data(1,:)','Name','Transfer BT K=1');
    tsBCK1=timeseries(data(3,:)',data(1,:)','Name','Transfer BC K=1');
    tsDiss2DNoW=timeseries(data(4,:)',data(1,:)','Name','2D horizontal dissipation');
    tsBC=timeseries(data(5,:)',data(1,:)','Name','Transfer BC to BT');
else
    tsBTK1=timeseries(zeros(1,10),(1:10),'Name','Transfer BT K=1');
    tsBCK1=timeseries(zeros(1,10),(1:10),'Name','Transfer BC K=1');
    tsDiss2DNoW=timeseries(zeros(1,10),(1:10),'Name','2D horizontal dissipation');
    tsBC=timeseries(zeros(1,10),(1:10),'Name','Transfer BC to BT');
end

%Interpolate to uniform time grid and remove duplicate points
tsBTK1=interpTs(tsBTK1);
tsBCK1=interpTs(tsBCK1);
tsDiss2DNoW=interpTs(tsDiss2DNoW);
tsBC=interpTs(tsBC);

%get nusselt number and total dissipation
m_ts_kin_trans_four_gPlotting=gPlotting; gPlotting=false;
nusse;
tsNuTe=tss(4);
gPlotting=m_ts_kin_trans_four_gPlotting;
tsNuTe=interpTs(tsNuTe,tsBTK1.Time);
pr=h5read([gDataDir gSimname '/stafield_master.h5'],'/Pr');
ra=h5read([gDataDir gSimname '/stafield_master.h5'],'/Ra');
tsTotalDiss=(tsNuTe-1)/sqrt(pr*ra);

%calculate ratios
tsBTK1Ratio=timeseries(tsBTK1.Data ./ tsTotalDiss.Data, tsBTK1.Time, 'Name', [tsBTK1.Name ' ratio']);
tsBCK1Ratio=timeseries(tsBCK1.Data ./ tsTotalDiss.Data, tsBTK1.Time, 'Name', [tsBCK1.Name ' ratio']);
tsDiss2DNoWRatio=timeseries(-1 .* tsDiss2DNoW.Data ./ tsTotalDiss.Data, tsBTK1.Time, 'Name', [tsDiss2DNoW.Name ' ratio']);
tsBCRatio=timeseries(tsBC.Data ./ tsTotalDiss.Data, tsBTK1.Time, 'Name', [tsBC.Name ' ratio']);

%Do error analysis
btK1RatioStats=getTsStats(tsBTK1Ratio,gPlotting,gPlotting,gTstart);
bcK1RatioStats=getTsStats(tsBCK1Ratio,gPlotting,gPlotting,gTstart);
diss2DNoWRatioStats=getTsStats(tsDiss2DNoWRatio,gPlotting,gPlotting,gTstart);
bcRatioStats=getTsStats(tsBCRatio,gPlotting,gPlotting,gTstart);

btK1Stats=getTsStats(tsBTK1,gPlotting,gPlotting,gTstart);
bcK1Stats=getTsStats(tsBCK1,gPlotting,gPlotting,gTstart);
diss2DNoWStats=getTsStats(tsDiss2DNoW,gPlotting,gPlotting,gTstart);
bcStats=getTsStats(tsBC,gPlotting,gPlotting,gTstart);

if gPlotting
    %plot parameters
    mm_time=100;
    figure
    scatter(tsBTK1.Time,movmean(tsBTK1.Data,mm_time),10,'s','filled','DisplayName',tsBTK1.Name)
    hold on
    scatter(tsBCK1.Time,movmean(tsBCK1.Data,mm_time),10,'s','filled','DisplayName',tsBCK1.Name)
    legend
    set(gca,'ColorOrderIndex',1)
    scatter(tsBTK1.Time,tsBTK1.Data,10,'s','filled','DisplayName',tsBTK1.Name,'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsBCK1.Time,tsBCK1.Data,10,'s','filled','DisplayName',tsBCK1.Name,'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
    title(['Kinetic transport ' gSimname])
    xlabel('Convective time')
    ylabel('Forcing')
    %if gTstart ~= -1
    %    xline(gTstart,'--r');
    %end
    yline(0,'k');
    grid on
    box on
    
    mm_time=100;
    figure
    scatter(tsDiss2DNoW.Time,movmean(tsDiss2DNoW.Data,mm_time),10,'s','filled','DisplayName',tsDiss2DNoW.Name)
    hold on
    scatter(tsBC.Time,movmean(tsBC.Data,mm_time),10,'s','filled','DisplayName',tsBC.Name)
    legend
    set(gca,'ColorOrderIndex',1)
    scatter(tsDiss2DNoW.Time,tsDiss2DNoW.Data,10,'s','filled','DisplayName',tsDiss2DNoW.Name,'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsBC.Time,tsBC.Data,10,'s','filled','DisplayName',tsBC.Name,'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
    title(['Kinetic transport ' gSimname])
    xlabel('Convective time')
    ylabel('2D kinetics')
    %if gTstart ~= -1
    %    xline(gTstart,'--r');
    %end
    yline(0,'k');
    grid on
    box on
    
    %together
    mm_time=100;
    figure
    scatter(tsDiss2DNoW.Time,movmean(tsDiss2DNoW.Data,mm_time),10,'s','filled','DisplayName',tsDiss2DNoW.Name)
    hold on
    scatter(tsBC.Time,movmean(tsBC.Data,mm_time),10,'s','filled','DisplayName',tsBC.Name)
    scatter(tsBCK1.Time,movmean(tsBCK1.Data,mm_time),10,'s','filled','DisplayName',tsBCK1.Name)
    scatter(tsBTK1.Time,movmean(tsBTK1.Data,mm_time),10,'s','filled','DisplayName',tsBTK1.Name)
    legend
    set(gca,'ColorOrderIndex',1)
    scatter(tsDiss2DNoW.Time,tsDiss2DNoW.Data,10,'s','filled','DisplayName',tsDiss2DNoW.Name,'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsBC.Time,tsBC.Data,10,'s','filled','DisplayName',tsBC.Name,'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsBCK1.Time,tsBCK1.Data,10,'s','filled','DisplayName',tsBCK1.Name,'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsBTK1.Time,tsBTK1.Data,10,'s','filled','DisplayName',tsBTK1.Name,'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
    title(['Kinetic transport ' gSimname])
    xlabel('Convective time')
    ylabel('Forcing')
    %if gTstart ~= -1
    %    xline(gTstart,'--r');
    %end
    yline(0,'k');
    grid on
    box on
    
    %plot ratios
    mm_time=100;
    figure
    scatter(tsBTK1Ratio.Time,movmean(tsBTK1Ratio.Data,mm_time),10,'s','filled','DisplayName',tsBTK1Ratio.Name)
    hold on
    scatter(tsBCK1Ratio.Time,movmean(tsBCK1Ratio.Data,mm_time),10,'s','filled','DisplayName',tsBCK1Ratio.Name)
    scatter(tsDiss2DNoWRatio.Time,movmean(tsDiss2DNoWRatio.Data,mm_time),10,'s','filled','DisplayName',tsDiss2DNoWRatio.Name)
    scatter(tsBCRatio.Time,movmean(tsBCRatio.Data,mm_time),10,'s','filled','DisplayName',tsBCRatio.Name)
    legend
    set(gca,'ColorOrderIndex',1)
    scatter(tsBTK1Ratio.Time,tsBTK1Ratio.Data,10,'s','filled','DisplayName',tsBTK1Ratio.Name,'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsBCK1Ratio.Time,tsBCK1Ratio.Data,10,'s','filled','DisplayName',tsBCK1Ratio.Name,'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsDiss2DNoWRatio.Time,tsDiss2DNoWRatio.Data,10,'s','filled','DisplayName',tsDiss2DNoWRatio.Name,'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsBCRatio.Time,tsBCRatio.Data,10,'s','filled','DisplayName',tsBCRatio.Name,'MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
    title(['Kinetic transport ratios ' gSimname])
    xlabel('Convective time')
    ylabel('Ratio')
    %if gTstart ~= -1
    %    xline(gTstart,'--r');
    %end
    %ylim([-0.3,1]) ;
    yline(0,'k');
    grid on
    box on
end
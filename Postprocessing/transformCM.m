function cm_trans = transformCM(cm_orig,inner_frac_scale,inner_frac_color,logistic_param)

cm_trans=zeros(256,3);

inner=round(inner_frac_scale*size(cm_trans,1));
outer_1=round((size(cm_trans,1)-inner)/2);
outer_2=size(cm_trans,1)-inner-outer_1;

x_transCM_outer_1=linspace(0,(1-inner_frac_color)/2,outer_1+1)';
x_transCM_inner=linspace((1-inner_frac_color)/2,1-(1-inner_frac_color)/2,inner)';
x_transCM_outer_2=linspace(1-(1-inner_frac_color)/2,1,outer_2+1)';
x_transCM=[x_transCM_outer_1(1:(end-1)); x_transCM_inner; x_transCM_outer_2(2:end)];

if logistic_param > 0
    x_transCM=1./(1+exp(-logistic_param*(x_transCM-0.5)));
end

for k=1:3
  cm_trans(:,k)=interp1((0:1:(size(cm_orig,1)-1))'/(size(cm_orig,1)-1),cm_orig(:,k),x_transCM);
end


end


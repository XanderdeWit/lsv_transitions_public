%initialize grid

%plotMesh = gPlotting;
plotMesh = false;

d=Domain;

%Read files
continua_grid = fopen([gDataDir gSimname '/continua_grid.dat'],'r');
continua_grid = fscanf(continua_grid,'%f %f %f\n %f %f %f\n %f %f');
axicor = fopen([gDataDir gSimname '/axicor.out'],'r');
axicor = fscanf(axicor,'%f %f\n',[5 Inf]);

%Uniform grid in x,y direction
d.nx=continua_grid(1);
d.X=continua_grid(4);
d.xs=linspace(0,d.X,d.nx);
d.xms=0.5 .* (d.xs(1:end-1) + d.xs(2:end));
d.nxm=length(d.xms);

d.ny=continua_grid(2);
d.Y=continua_grid(5);
d.ys=linspace(0,d.Y,d.ny);
d.yms=0.5 .* (d.ys(1:end-1) + d.ys(2:end));
d.nym=length(d.yms);

%Vertical (axial) grid
d.zs = axicor(2,:);
d.zms = axicor(3,1:end-1);
d.nz = length(d.zs);
d.nzm = length(d.zms);
d.H=max(d.zs);
%d.rmsMesh=repmat(d.rms',length(d.thetams),1); %TODO

%dx,dy,dz
d.dx=d.xs(2)-d.xs(1);
d.dy=d.ys(2)-d.ys(1);
d.dz=diff(d.zs);

%g3rc, g3rm, weight of slab in vertical average of face/cell quantity resp.
d.g3rc = axicor(4,:);
d.g3rm = axicor(5,1:end-1);

%Plot vertical (x,z) cross-section of mesh
if plotMesh
    %Plot mesh
    xlin=linspace(0,d.X);
    zlin=linspace(0,d.H);

    xplot=d.xs'+0*zlin;
    zplot=d.zs'+0*xlin;

    figure
    plot(xplot,zlin,'b')
    hold on
    plot(xlin,zplot,'b')
    hold off
    title(['Domain discretization ' gSimname])
    ylabel('z')
    xlabel('x')
    grid off
    
    %xlim([d.xs(1),d.xs(end)])
    %ylim([d.zs(1),d.zs(end)])
    
    axis image
end
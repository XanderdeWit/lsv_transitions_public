%calculate energy ratios and their statistics

%calculate the different kinetic energy time series
m_ts_kin_ratios_gPlotting=gPlotting; gPlotting=false;
ts_kins;
gPlotting=m_ts_kin_ratios_gPlotting;

tsKH2Dk1=interpTs(tsKH2Dk1,tsK.Time);

%calculate different kinetic energy ratios
tsGamma=timeseries(tsK.Data ./ (3 .* tsKv.Data), tsK.Time, 'Name', 'Gamma $\Gamma$');
tsRatioGamma=timeseries(1 - 1 ./ tsGamma.Data, tsK.Time, 'Name', '$\gamma$ ratio');
tsRatio2D=timeseries(tsKH2D.Data ./ (tsKH2D.Data + tsKH3D.Data), tsK.Time, 'Name', '2D ratio');
tsRatio2Dk1=timeseries(tsKH2Dk1.Data ./ (tsKH2Dk1.Data + tsKH3D.Data), tsK.Time, 'Name', 'k=1 2D ratio');
tsRatio2Dk1_2=timeseries(tsKH2Dk1.Data ./ (tsKH2D.Data + tsKH3D.Data), tsK.Time, 'Name', 'k=1 2D ratio 2');

%Calculate LOPs
tsLRatioHor=timeseries(3*tsGamma.Data - 3, tsK.Time, 'Name', 'Horizontal ratio');
tsLRatio2D=timeseries(tsKH2D.Data ./ tsKH3D.Data,tsK.Time, 'Name', '2D ratio');
tsLRatio2Dk1=timeseries(tsKH2Dk1.Data ./ tsKH3D.Data,tsK.Time, 'Name', '2D k=1 ratio');

%calculate anisotropy parameter
%tsAni=timeseries(abs(tsVxRMS.Data.^2-tsVyRMS.Data.^2)./(tsVxRMS.Data.^2+tsVyRMS.Data.^2), tsK.Time, 'Name', 'anisotropy parameter');

%Do error analysis
gammaStats=getTsStats(tsGamma,gPlotting,gPlotting,gTstart);
ratioGammaStats=getTsStats(tsRatioGamma,gPlotting,gPlotting,gTstart);
ratio2DStats=getTsStats(tsRatio2D,gPlotting,gPlotting,gTstart);
ratio2Dk1Stats=getTsStats(tsRatio2Dk1,gPlotting,gPlotting,gTstart);
ratio2Dk1_2Stats=getTsStats(tsRatio2Dk1_2,gPlotting,gPlotting,gTstart);

lratioHorStats=getTsStats(tsLRatioHor,gPlotting,gPlotting,gTstart);
lratio2DStats=getTsStats(tsLRatio2D,gPlotting,gPlotting,gTstart);
lratio2Dk1Stats=getTsStats(tsLRatio2Dk1,gPlotting,gPlotting,gTstart);

kvStats=getTsStats(tsKv,gPlotting,gPlotting,gTstart);
kH2DStats=getTsStats(tsKH2D,gPlotting,gPlotting,gTstart);
kH3DStats=getTsStats(tsKH3D,gPlotting,gPlotting,gTstart);
kH2Dk1Stats=getTsStats(tsKH2Dk1,gPlotting,gPlotting,gTstart);

if gPlotting
    %Plot ratios
    figure
    scatter(tsGamma.Time,tsGamma.Data,10,'s','filled')
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    title([tsGamma.Name ' ' gSimname])
    ylabel(tsGamma.Name)
    xlabel('Convective time')
    grid on
    box on
    
    figure
    scatter(tsRatio2D.Time,tsRatio2D.Data,10,'s','filled','DisplayName',tsRatio2D.Name)
    hold on
    scatter(tsRatioGamma.Time,tsRatioGamma.Data,10,'s','filled','DisplayName',tsRatioGamma.Name)
    scatter(tsRatio2Dk1.Time,tsRatio2Dk1.Data,10,'s','filled','DisplayName',tsRatio2Dk1.Name)
    scatter(tsRatio2Dk1_2.Time,tsRatio2Dk1_2.Data,10,'s','filled','DisplayName',tsRatio2Dk1_2.Name)
    legend('Location','northwest')
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    title(['Kinetic energy ratios ' gSimname])
    ylabel('Ratio')
    xlabel('Convective time')
    ylim([0,1])
    grid on
    box on
    
    figure
    scatter(tsLRatioHor.Time,tsLRatioHor.Data,10,'s','filled','DisplayName',tsLRatioHor.Name)
    hold on
    scatter(tsLRatio2D.Time,tsLRatio2D.Data,10,'s','filled','DisplayName',tsLRatio2D.Name)
    scatter(tsLRatio2Dk1.Time,tsLRatio2Dk1.Data,10,'s','filled','DisplayName',tsLRatio2Dk1.Name)
    legend('Location','northwest')
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    title(['Kinetic energy large ratios ' gSimname])
    ylabel('Ratio')
    xlabel('Convective time')
    grid on
    box on
    
%     mm_time=500;
%     figure
%     scatter(tsAni.Time,movmean(tsAni.Data,mm_time),10,'s','filled','DisplayName',tsAni.Name)
%     legend('Location','northwest')
%     hold on
%     set(gca,'ColorOrderIndex',1)
%     scatter(tsAni.Time,tsAni.Data,2,'s','filled','MarkerFaceAlpha',.1,'MarkerEdgeAlpha',.1)
%     set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
%     if gTstart ~= -1
%         xline(gTstart,'--r');
%     end
%     title(['Anisotropy ' gSimname])
%     ylabel('$\alpha$')
%     xlabel('Convective time')
%     grid on
%     box on
end
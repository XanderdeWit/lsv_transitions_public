%get the anisotropy time series from the z-averaged slabs

%filename example: stst3/slabzq1_00000001.h5

%find files
files=dir([gDataDir gSimname '/stst3']);
slab_is=0;
for ifile=3:length(files)
    if files(ifile).name(5:7) == 'zq1'
        if slab_is == 0
            slab_is=str2num(files(ifile).name(9:16));
        else
            slab_is=[slab_is; str2num(files(ifile).name(9:16))];
        end
    end
end

%read full RMS velocities
%file = fopen([gDataDir gSimname '/rms_vel.out'],'r');
%data = fscanf(file,'%f %f\n',[5 Inf]);
%tsVxRMS=timeseries(data(2,:)',data(1,:)','Name','VxRMS');
%tsVyRMS=timeseries(data(3,:)',data(1,:)','Name','VyRMS');

%read slabs and calculate spectrum
ani_zave_xy=zeros(length(slab_is),1);
ani_zave_xy_2=zeros(length(slab_is),1);
ani_zave_rms=zeros(length(slab_is),1);
ani_zave_rms_2=zeros(length(slab_is),1);
time=zeros(length(slab_is),1);
%tic
parfor islab_i=1:length(slab_is)
%for islab_i=1:length(slab_is)
    mVx_zave=h5read([gDataDir gSimname '/stst3/slabzq1_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/var');
    mtime=h5read([gDataDir gSimname '/stst3/slabzq1_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/time');
    
    mVy_zave=h5read([gDataDir gSimname '/stst3/slabzq2_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/var');
    mtimeVy=h5read([gDataDir gSimname '/stst3/slabzq2_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/time');
    
    if mtime ~= mtimeVy
        disp('Error: time_vx != time_vy');
    end
    
    %mVx_ms=tsVxRMS.Data(findIn(tsVxRMS.Time,mtime))^2;
    %mVy_ms=tsVyRMS.Data(findIn(tsVyRMS.Time,mtime))^2;
    
    mVx_zave_xave2_yave=mean(mean(mVx_zave,1).^2);
    mVy_zave_yave2_xave=mean(mean(mVy_zave,2).^2);
    
    ani_zave_xy(islab_i)=abs(mVx_zave_xave2_yave-mVy_zave_yave2_xave)/(mVx_zave_xave2_yave+mVy_zave_yave2_xave);
    %ani_zave_xy_2(islab_i)=abs(mVx_zave_xave2_yave-mVy_zave_yave2_xave)/(mVx_ms+mVy_ms);
    
    mVx_zave_ms=mean(mVx_zave.^2,[1, 2]);
    mVy_zave_ms=mean(mVy_zave.^2,[1, 2]);
    
    ani_zave_rms(islab_i)=abs(mVx_zave_ms-mVy_zave_ms)/(mVx_zave_ms+mVy_zave_ms);
    %ani_zave_rms_2(islab_i)=abs(mVx_zave_ms-mVy_zave_ms)/(mVx_ms+mVy_ms);
    
    time(islab_i)=mtime;
end
%toc

%make timeseries of k=1 mode and calculate statistics
%TODO
%tsVortM2zaveK1_slabs=timeseries(vort_zave_k(:,2),time,'Name','VortM2zave k=1');

if gPlotting
    mm_time=500;
    figure
    scatter(time,movmean(ani_zave_xy,mm_time),10,'s','filled','DisplayName','Anisotropy xy')
    hold on
    %scatter(time,movmean(ani_zave_xy_2,mm_time),10,'s','filled','DisplayName','Anisotropy xy 2')
    scatter(time,movmean(ani_zave_rms,mm_time),10,'s','filled','DisplayName','Anisotropy rms')
    %scatter(time,movmean(ani_zave_rms_2,mm_time),10,'s','filled','DisplayName','Anisotropy rms 2')
    legend('Location','northwest')
    set(gca,'ColorOrderIndex',1)
    scatter(time,ani_zave_xy,2,'s','filled','MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    %scatter(time,ani_zave_xy_2,2,'s','filled','MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(time,ani_zave_rms,2,'s','filled','MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    %scatter(time,ani_zave_rms_2,2,'s','filled','MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    title(['Anisotropy z-averaged velocity ' gSimname])
    ylabel('$\alpha$')
    xlabel('Convective time')
    grid on
    box on
end
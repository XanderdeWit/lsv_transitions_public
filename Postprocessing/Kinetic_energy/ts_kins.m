%Analyze the different kinetic energy time series

%NOTE: kinetic energies are defined here as average of v^2, so as energy
%density and without factor (1/2)

file = fopen([gDataDir gSimname '/rms_vel.out'],'r');
data = fscanf(file,'%f %f\n',[5 Inf]);

tsVxRMS=timeseries(data(2,:)',data(1,:)','Name','VxRMS');
tsVyRMS=timeseries(data(3,:)',data(1,:)','Name','VyRMS');
tsVzRMS=timeseries(data(4,:)',data(1,:)','Name','VzRMS');

file = fopen([gDataDir gSimname '/zave_kin.out'],'r');
data = fscanf(file,'%f %f\n',[3 Inf]);

tsKH2D=timeseries(data(2,:)',data(1,:)','Name','KH2D');
tsKH2Dk1=timeseries(data(3,:)',data(1,:)','Name','KH2Dk1');

%Interpolate to uniform time grid and remove duplicate points
tsVxRMS=interpTs(tsVxRMS);
tsVyRMS=interpTs(tsVyRMS,tsVxRMS.Time);
tsVzRMS=interpTs(tsVzRMS,tsVxRMS.Time);
tsKH2D=interpTs(tsKH2D,tsVxRMS.Time);
tsKH2Dk1=interpTs(tsKH2Dk1,tsVxRMS.Time);

tsK=timeseries((1/2)*(tsVxRMS.Data.^2 + tsVyRMS.Data.^2 + tsVzRMS.Data.^2), tsVxRMS.Time, 'Name', 'Total kinetic energy');
tsKv=timeseries((1/2)*(tsVzRMS.Data.^2), tsVxRMS.Time, 'Name', 'Vertical kinetic energy');
tsKH=timeseries((1/2)*(tsVxRMS.Data.^2+tsVyRMS.Data.^2), tsVxRMS.Time, 'Name', 'Horizontal kinetic energy');
tsKH2D=timeseries((1/2)*(tsKH2D.Data), tsVxRMS.Time, 'Name', '2D horizontal kinetic energy');
tsKH3D=timeseries(tsK.Data - tsKH2D.Data - tsKv.Data, tsVxRMS.Time, 'Name', '3D horizontal kinetic energy');
tsKH2Dk1=timeseries((1/2)*(tsKH2Dk1.Data), tsVxRMS.Time, 'Name', '2D horizontal kinetic energy K=1');

%calculate 2D horizontal kinetic energy spectral k=1 mode from slabs
%m_ts_kins_gPlotting=gPlotting; gPlotting=false;
%ts_spectrum_KH2D;
%gPlotting=m_ts_kins_gPlotting;
%tsKH2Dk1_slabs=interpTs(tsKH2Dk1_slabs,tsVxRMS.Time);

if gPlotting
    %plot kinetic energies
    figure
    hold on
    scatter(tsKv.Time,tsKv.Data,10,'s','filled','DisplayName',tsKv.Name)
    scatter(tsKH.Time,tsKH.Data,10,'s','filled','DisplayName',tsKH.Name)
    scatter(tsKH3D.Time,tsKH3D.Data,10,'s','filled','DisplayName',tsKH3D.Name)
    scatter(tsKH2D.Time,tsKH2D.Data,10,'s','filled','DisplayName',tsKH2D.Name)
    scatter(tsKH2Dk1.Time,tsKH2Dk1.Data,10,'s','filled','DisplayName',tsKH2Dk1.Name)
    %scatter(tsKH2Dk1_slabs.Time,tsKH2Dk1_slabs.Data,10,'s','filled','DisplayName',tsKH2Dk1_slabs.Name)
    scatter(tsK.Time,tsK.Data,10,'s','filled','DisplayName',tsK.Name)
    legend('Location','northwest')
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    hold off
    title(['Kinetic energy ' gSimname])
    ylabel('Kinetic energy')
    xlabel('Convective time')
    grid on
    box on
    
    %stacked area plot
    figure
    area(tsK.Time,[tsKv.Data,tsKH3D.Data,tsKH2D.Data])
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    title(['Kinetic energy ' gSimname])
    ylabel('Kinetic energy')
    xlabel('Convective time')
    legend({tsKv.Name,tsKH3D.Name,tsKH2D.Name},'Location','northwest')
    grid on
    box on
end
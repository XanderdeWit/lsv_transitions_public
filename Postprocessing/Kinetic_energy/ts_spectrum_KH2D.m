%get the KH2D kinetic energy time series from the z-averaged slabs

%filename example: stst3/slabzq1_00000001.h5

%find files
files=dir([gDataDir gSimname '/stst3']);
slab_is=0;
for ifile=3:length(files)
    if files(ifile).name(5:7) == 'zq1'
        if slab_is == 0
            slab_is=str2num(files(ifile).name(9:16));
        else
            slab_is=[slab_is; str2num(files(ifile).name(9:16))];
        end
    end
end

%read slabs and calculate spectrum
k=1:min(d.nxm,d.nym)/2;
E2Dk=zeros(length(slab_is),length(k));
time=zeros(length(slab_is),1);
%tic
parfor islab_i=1:length(slab_is)
%for islab_i=1:length(slab_is)
    mVx_zave=h5read([gDataDir gSimname '/stst3/slabzq1_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/var');
    mtime=h5read([gDataDir gSimname '/stst3/slabzq1_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/time');
    
    mVy_zave=h5read([gDataDir gSimname '/stst3/slabzq2_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/var');
    mtimeVy=h5read([gDataDir gSimname '/stst3/slabzq2_' sprintf('%08d', slab_is(islab_i)) '.h5'],'/time');
    
    if mtime ~= mtimeVy
        disp('Error: time_vx != time_vy');
    end
    
    [mE2Dk,~]=getEkSlab(mVx_zave,mVy_zave,d,true);
    E2Dk(islab_i,:)=mE2Dk;
    
    time(islab_i)=mtime;
end
%toc

%time-averaged spectrum
mean_E2Dk=mean(E2Dk,1);
if gTstart ~= -1
    itstart=findIn(time,gTstart);
    mean_E2Dk=mean(E2Dk(itstart:end,:),1);
end

%make timeseries of k=1 mode and calculate statistics
tsKH2Dk1_slabs=timeseries(E2Dk(:,1),time,'Name','2D horizontal kinetic energy K=1 slabs');

if gPlotting
    %Plot time-averaged spectrum
    figure
    loglog(k,mean_E2Dk,'DisplayName','2D')
    title(['Horizontal kinetic energy spectrum 2D ' gSimname])
    xlabel('Horizontal wavenumber $k_H$')
    ylabel('$E(k_H)$')
    grid on
    box on
    
    %Plot time series of modes
    figure
    %plot(time,movmean(E2Dk(:,1),50),'LineWidth',2,'DisplayName','k=1 moving mean')
    %set(gca,'ColorOrderIndex',1)
    hold on
    for ik=1:10
        scatter(time,E2Dk(:,ik),10,'s','filled','DisplayName',['k=' num2str(k(ik))])
    end
    legend('Location','northwest')
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    hold off
    title(['Horizontal kinetic energy 2D spectral modes ' gSimname])
    ylabel('Kinetic energy')
    xlabel('Convective time')
    grid on
    box on
end
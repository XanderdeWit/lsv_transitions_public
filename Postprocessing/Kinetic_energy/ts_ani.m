%Analyze the anisotropy

%read
file = fopen([gDataDir gSimname '/ani.out'],'r');
data = fscanf(file,'%f %f\n',[4 Inf]);

tsAniMS=timeseries(data(2,:)',data(1,:)','Name','Total anisotropy');
tsAniZaveXY=timeseries(data(3,:)',data(1,:)','Name','2D anisotropy XY');
tsAniZaveMS=timeseries(data(4,:)',data(1,:)','Name','2D anisotropy');

%Interpolate to uniform time grid and remove duplicate points
tsAniMS=interpTs(tsAniMS);
tsAniZaveXY=interpTs(tsAniZaveXY,tsAniMS.Time);
tsAniZaveMS=interpTs(tsAniZaveMS,tsAniMS.Time);

%Do error analysis
aniMSStats=getTsStats(tsAniMS,gPlotting,gPlotting,gTstart);
aniZaveXYStats=getTsStats(tsAniZaveXY,gPlotting,gPlotting,gTstart);
aniZaveMSStats=getTsStats(tsAniZaveMS,gPlotting,gPlotting,gTstart);

%plot
if gPlotting
    mm_time=500;
    figure
    scatter(tsAniMS.Time,movmean(tsAniMS.Data,mm_time),10,'s','filled','DisplayName',tsAniMS.Name)
    hold on
    scatter(tsAniZaveXY.Time,movmean(tsAniZaveXY.Data,mm_time),10,'s','filled','DisplayName',tsAniZaveXY.Name)
    scatter(tsAniZaveMS.Time,movmean(tsAniZaveMS.Data,mm_time),10,'s','filled','DisplayName',tsAniZaveMS.Name)
    legend('Location','northwest')
    set(gca,'ColorOrderIndex',1)
    scatter(tsAniMS.Time,tsAniMS.Data,2,'s','filled','MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsAniZaveXY.Time,tsAniZaveXY.Data,2,'s','filled','MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    scatter(tsAniZaveMS.Time,tsAniZaveMS.Data,2,'s','filled','MarkerFaceAlpha',.2,'MarkerEdgeAlpha',.2)
    set(gca,'Children',fftshift(get(gca,'Children'))) %make sure transparent values are on background
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    title(['Anisotropy z-averaged velocity ' gSimname])
    ylabel('$\alpha$')
    xlabel('Convective time')
    grid on
    box on
end
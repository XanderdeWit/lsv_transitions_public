function hor_kin=getHorKin(Vx,Vy,d)
%Make Vx periodic in y-direction and vice-versa
VxCell=d.faceToCell(d.makePeriodic(Vx,1),1);
VyCell=d.faceToCell(d.makePeriodic(Vy,2),2);

hor_kin = 0.5*(VxCell.^2 + VyCell.^2);

%NOTE: hor_kin is defined at cell center

end

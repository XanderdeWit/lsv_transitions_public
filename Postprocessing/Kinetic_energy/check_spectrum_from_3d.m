%Calculate the energy spectra from 3D snapshot

%NOTE: for time-averaged spectrum of 2D horizontal kinetic energy, see ts_spectrum_KH2D.m

Vx=h5read([gDataDir gSimname '/continua_q1.h5'],'/Vth');
Vx=d.trim3D(Vx);

Vy=h5read([gDataDir gSimname '/continua_q2.h5'],'/Vr');
Vy=d.trim3D(Vy);

%Horizontal kinetic energy, z-averaged
k=0:min(d.nxm,d.nym)/2;
ETk=zeros(1,length(k));
for iz=1:d.nzm
    mVx=Vx(:,:,iz);
    mVy=Vy(:,:,iz);
    
    [mETk,~]=getEkSlab(mVx,mVy,d,false);
    
    ETk=ETk+mETk*d.g3rm(iz)/d.nzm;
end

if gPlotting
    figure
    plot(k,ETk,'DisplayName','Total')
    legend
    title(['Horizontal kinetic energy spectrum ' gSimname])
    xlabel('Horizontal wavenumber $k_H$')
    ylabel('$E(k_H)$')
    grid on
    box on
end

%Check equivalence
totalEnergy=mean((Vx.^2+Vy.^2) .*reshape(d.g3rm,1,1,d.nzm),'all')

totalEnergyInt=sum(ETk)

ETk(2)
function [Ek,k] = getEkSlab(Vx,Vy,d,discard0)
%returns the energy spectrum of the horizontal kinetic energy

%convert Vx and Vy to cell values
Vx=d.faceToCell(d.makePeriodic(Vx,1),1);
Vy=d.faceToCell(d.makePeriodic(Vy,2),2);

k=0:min(d.nxm,d.nym)/2;
Ek=zeros(1,length(k));

%FFT in x and y direction
Vx_k = my_fft_2d(Vx,d);
Vy_k = my_fft_2d(Vy,d);

kx=[0:d.nxm/2,-d.nxm/2+1:-1];
ky=[0:d.nym/2,-d.nym/2+1:-1];

for ikx=1:d.nxm
    for iky=1:d.nym
        mkx=kx(ikx);
        mky=ky(iky);
        %bin all wavenumbers with kH <= sqrt(kx^2+ky^2) < kH+1 together
        mk=floor(sqrt(mkx^2+mky^2));
        if mk<length(k)
            %kinetic energy: 1/2 * u_k*conj(u_k);
            Ek(1+mk)=Ek(1+mk)+1/2*(abs(Vx_k(ikx,iky))^2+abs(Vy_k(ikx,iky))^2);
        end
    end
end

%get rid of k=0 contribution
if discard0
    k=k(2:end);
    Ek=Ek(2:end);
end
 
end

function vk = my_fft_2d(v,d)
    vk=fft2(v)./(d.nxm*d.nym);
end
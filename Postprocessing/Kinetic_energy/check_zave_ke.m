Vx=h5read([gDataDir gSimname '/continua_q1.h5'],'/Vth');
Vx=d.trim3D(Vx);

Vy=h5read([gDataDir gSimname '/continua_q2.h5'],'/Vr');
Vy=d.trim3D(Vy);

Vz=h5read([gDataDir gSimname '/continua_q3.h5'],'/Vz');
Vz=d.trim3D(Vz);

Vx_zave=mean(Vx.*reshape(d.g3rm,1,1,d.nzm),3);
Vy_zave=mean(Vy.*reshape(d.g3rm,1,1,d.nzm),3);

kh2d = (d.nzm/(d.nzm-1)) * mean( (Vx_zave.^2 + Vy_zave.^2), 'all');
kh3d = (d.nzm/(d.nzm-1)) * mean( ((Vx-Vx_zave).^2 + (Vy-Vy_zave).^2) .* reshape(d.g3rm,1,1,d.nzm) , 'all');
kv3d = mean( Vz.^2 .* reshape(d.g3rm,1,1,d.nzm) , 'all');

khT = (d.nzm/(d.nzm-1)) * mean( (Vx.^2 + Vy.^2) .* reshape(d.g3rm,1,1,d.nzm) , 'all');

khT-kh2d

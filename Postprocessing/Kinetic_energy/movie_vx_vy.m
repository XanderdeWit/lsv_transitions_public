%Movie of z-averaged vx or vy;

stst3dir=[gDataDir gSimname '/stst3'];

%The color axis needs to be calibrated, minimum and maximum are printed after rendering
%clim='auto';
clim=[-0.2 0.2];

%slab_i_start=-1;
slab_i_start=2000;
%slab_i_end=-1;
slab_i_end=2300;

makeMovie(stst3dir,@getData,d.xms,d.yms,slab_i_start,slab_i_end,clim,'pcolor',gMyCM,0.5*[800,650],0.1,d)

function [data,time]=getData(stst3dir,slab_i,~)
    slabname='slabz'; %mid-height

    vx=h5read([stst3dir '/' slabname 'q1_' sprintf('%08d', slab_i) '.h5'],'/var');
    %vy=h5read([stst3dir '/' slabname 'q2_' sprintf('%08d', slab_i) '.h5'],'/var');

    time=h5read([stst3dir '/' slabname 'q1_' sprintf('%08d', slab_i) '.h5'],'/time');
    timeVy=h5read([stst3dir '/' slabname 'q2_' sprintf('%08d', slab_i) '.h5'],'/time');

    if time ~= timeVy
        disp('Error: time_vx != time_vy');
    end

    data=vx;
    %data=vy;
end
%Calculate the energy spectra from 3D snapshot

%NOTE: for time-averaged spectrum of 2D horizontal kinetic energy, see ts_spectrum_KH2D.m

Vx=h5read([gDataDir gSimname '/continua_q1.h5'],'/Vth');
Vx=d.trim3D(Vx);

Vy=h5read([gDataDir gSimname '/continua_q2.h5'],'/Vr');
Vy=d.trim3D(Vy);

Vz=h5read([gDataDir gSimname '/continua_q3.h5'],'/Vz');
Vz=d.trim3D(Vz);
Vz(:,:,end+1)=zeros(d.nxm,d.nym); Vz=d.faceToCell(Vz,3);

Vx_zave=mean(Vx.*reshape(d.g3rm,1,1,d.nzm),3);
Vy_zave=mean(Vy.*reshape(d.g3rm,1,1,d.nzm),3);

%Horizontal kinetic energy, z-averaged
k=1:min(d.nxm,d.nym)/2;
ETk=zeros(1,length(k));
E3Dk=zeros(1,length(k));
E2Dk=zeros(1,length(k));
Evk=zeros(1,length(k));
for iz=1:d.nzm
    mVx=Vx(:,:,iz);
    mVy=Vy(:,:,iz);
    mVz=Vz(:,:,iz);
    mVxp=mVx-Vx_zave;
    mVyp=mVy-Vy_zave;
    
    [mETk,~]=getEkSlab(mVx,mVy,d,true);
    [mE3Dk,~]=getEkSlab(mVxp,mVyp,d,true);
    [mEvk,~]=getEvkSlab(mVz,d,true);
    if iz==1
        [E2Dk,~]=getEkSlab(Vx_zave,Vy_zave,d,true);
    end
    
    ETk=ETk+mETk*d.g3rm(iz)/d.nzm;
    E3Dk=E3Dk+mE3Dk*d.g3rm(iz)/d.nzm;
    Evk=Evk+mEvk*d.g3rm(iz)/d.nzm;
end

if gPlotting
    figure
    loglog(k,ETk,'DisplayName','Total')
    hold on
    loglog(k,E3Dk,'DisplayName','3D')
    loglog(k,E2Dk,'DisplayName','2D')
    %loglog(k,E2Dk+E3Dk,'DisplayName','2D+3D')
    hold off
    legend
    title(['Horizontal kinetic energy spectrum ' gSimname])
    xlabel('Horizontal wavenumber $k_H$')
    ylabel('$E(k_H)$')
    grid on
    box on
    
    figure
    loglog(k,ETk+Evk,'DisplayName','Total')
    hold on
    loglog(k,E3Dk+Evk,'DisplayName','3D(+vert)')
    loglog(k,E2Dk,'DisplayName','2D')
    %loglog(k,E2Dk+E3Dk,'DisplayName','2D+3D')
    hold off
    legend
    title(['Kinetic energy spectrum ' gSimname])
    xlabel('Horizontal wavenumber $k_H$')
    ylabel('$E(k_H)$')
    grid on
    box on
    
    figure
    loglog(k,ETk+Evk,'DisplayName','Total')
    hold on
    loglog(k,E3Dk,'DisplayName','3D')
    loglog(k,Evk,'DisplayName','Vertical')
    loglog(k,E2Dk,'DisplayName','2D')
    %loglog(k,E2Dk+E3Dk,'DisplayName','2D+3D')
    hold off
    legend
    title(['Kinetic energy spectrum ' gSimname])
    xlabel('Horizontal wavenumber $k_H$')
    ylabel('$E(k_H)$')
    grid on
    box on
    
    figure
    loglog(k,k.^2.*ETk,'DisplayName','Total')
    hold on
    loglog(k,k.^2.*E3Dk,'DisplayName','3D')
    loglog(k,k.^2.*E2Dk,'DisplayName','2D')
    %loglog(k,E2Dk+E3Dk,'DisplayName','2D+3D')
    hold off
    legend
    title(['Horizontal kinetic energy dissipation spectrum ' gSimname])
    xlabel('Horizontal wavenumber $k_H$')
    ylabel('$k^2 E(k_H)$')
    grid on
    box on
end
%Plot horizontal kinetic energy from 3D snapshot

Vx=h5read([gDataDir gSimname '/continua_q1.h5'],'/Vth');
Vx=d.trim3D(Vx);

Vy=h5read([gDataDir gSimname '/continua_q2.h5'],'/Vr');
Vy=d.trim3D(Vy);

hor_kin = getHorKin(Vx,Vy,d);

%Average in vertical direction
hor_kin_bar = mean(hor_kin .* reshape(d.g3rm,1,1,d.nzm),3);

if gPlotting
    %Plot cross sections
    lims=1.2*[0,max(hor_kin_bar,[],'all')];
    plot3DHorCross(d.xms,d.yms,d.zms,hor_kin,'Horizontal kinetic energy',lims,'pcolor',gMyCM,true)
    plot3DVertCross(d.xms,d.yms,d.zms,hor_kin,'Horizontal kinetic energy',lims,'pcolor',gMyCM,true)

    %Plot vertical average
    figure
    %lims=[0,max(hor_kin_bar,[],'all')];
    drawCross(d.xms,d.yms,hor_kin_bar',lims,'pcolor',gMyCM,true);
    title(['Vertically averaged horizontal kinetic energy ' gSimname])
    
    %plot slices
    figure
    %lims=[0,max(hor_kin,[],'all')];
    drawSlices(d.xms,d.yms,d.zs(1:end-1),[],1.3,0.2,hor_kin,lims,gMyCM,true);
    
    figure
    %lims=[0,max(hor_kin,[],'all')];
    cutoff=round(0.8*d.nz);
    drawSlices(d.xms,d.yms,d.zs(1:cutoff),d.xms(1),d.yms(1),d.zs(cutoff),hor_kin(:,:,1:cutoff),lims,gMyCM,true);
    line(xlim,[0,0],[d.zs(cutoff),d.zs(cutoff)],'Color','k')
    line([0,0],ylim,[d.zs(cutoff),d.zs(cutoff)],'Color','k')
    line([0,0],[0,0],[0,d.zs(cutoff)],'Color','k')
end

%Different kinetic energy ratios
Vz=h5read([gDataDir gSimname '/continua_q3.h5'],'/Vz');
Vz=d.trim3D(Vz);
vert_kin_bar = 0.5*mean(Vz.^2 .* reshape(d.g3rm,1,1,d.nzm),3);

Vx_zave=mean(Vx.*reshape(d.g3rm,1,1,d.nzm),3);
Vy_zave=mean(Vy.*reshape(d.g3rm,1,1,d.nzm),3);

Vx_zave=d.faceToCell(d.makePeriodic(Vx_zave,1),1);
Vy_zave=d.faceToCell(d.makePeriodic(Vy_zave,2),2);

hor_kin_2D = 0.5*(Vx_zave.^2 + Vy_zave.^2);

ratioHorVert = squeeze((mean(2*hor_kin,2)-2*mean(Vz.^2,2)) ./ (mean(Vz.^2,2)));

ratioHorHor = (hor_kin_bar-2*vert_kin_bar) ./ (vert_kin_bar);
ratio2D = hor_kin_2D ./ (hor_kin_bar-hor_kin_2D);

if gPlotting
    figure
    %lims=[0,1];
    lims='auto';
    drawCross(d.xms,d.zms,ratioHorVert',lims,'pcolor',gMyCM,true);
    title(['$\gamma$ ratio ' gSimname])
    caxis([0,14])

    figure
    %lims=[0,1];
    lims='auto';
    drawCross(d.xms,d.yms,ratioHorHor',lims,'pcolor',gMyCM,true);
    title(['$\gamma$ ratio ' gSimname])
    caxis([0,14])

    figure
    %lims=[0,1];
    drawCross(d.xms,d.yms,ratio2D',lims,'pcolor',gMyCM,true);
    title(['2D ratio ' gSimname])
    caxis([0,14])
end
%NOTE: this script must be run before running any other scripts
%In this file, all global variables are set and the grid is initialized.

%The directory of the data
gDataDir='/Volumes/T7 Touch/master-thesis/data/';
%gDataDir='data/';

%Name directory of case
gSimname='series1/case05';
%gSimname='series2/upper/case25';
%gSimname='series_interim/case17';
%gSimname='series1_ar/small/case062';
%gSimname='series1B/case10';
%gSimname='testcasesB/case04t2';
%gSimname='case1_sandbox';
%gSimname='case20_ensemble/run2';
%gSimname='case6_ensemble_ave/case59';
%gSimname='case20_full_trans';
%gSimname='case03_1B_osc/';

gSeries='series1/';
%gSeries='series1_ar/small2/';

gHystSeries='series2/';

%Turn plotting on/of
gPlotting=true;

%starting time for ts statistics
gTstart=getTstart(gSimname);

%Set typeface
set(0,'defaultTextInterpreter','latex')
set(0,'defaultLegendInterpreter','latex')
set(0,'defaultAxesTickLabelInterpreter','latex')

set(0,'DefaultAxesFontSize',18)

set(0,'DefaultLegendAutoUpdate','off')

%Initialize the grid
make_domain;

%Set global color map
gMyCM = getRudieColorMap();

addpath('Aspect_ratio')
addpath('Ensemble_ave')
addpath('Fig_factories/Presentation')
addpath('Fig_factories/PRL')
addpath('Fig_factories/Report')
addpath('Fig_factories/Seminar')
addpath('Kinetic_energy')
addpath('Misc')
addpath('Order_params')
addpath('Transfer')
addpath('Transfer/misc')
addpath('Validation')
addpath('Vorticity')

function cm=getRudieColorMap()

    RdBu_basic=flipud([103,   0,  31;
                       178,  24,  43;
                       214,  96,  77;
                       244, 165, 130;
                       253, 219, 199;
                       247, 247, 247;
                       209, 229, 240;
                       146, 197, 222;
                        67, 147, 195;
                        33, 102, 172;
                         5,  48,  97])/255;
    x_RdBu_basic=[0; 0.14; 0.23; 0.32; 0.41; 0.5; 0.59; 0.68; 0.77; 0.86; 1];
    x_RdBu=(0:1:63)'/63;
    RdBu=zeros(64,3);
    for k=1:3
      RdBu(:,k)=interp1(x_RdBu_basic,RdBu_basic(:,k),x_RdBu);
    end
    cm=RdBu;

end

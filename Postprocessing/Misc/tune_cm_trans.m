
averagingTime=h5read([gDataDir gSimname '/stafield_master.h5'],'/averaging_time');

transQK_BT_total=h5read([gDataDir gSimname '/stafield_master.h5'],'/transqk_bt');
transQK_BT=transQK_BT_total/double(averagingTime);
transQK_BC_total=h5read([gDataDir gSimname '/stafield_master.h5'],'/transqk_bc');
transQK_BC=transQK_BC_total/double(averagingTime);

maxQ=50;
maxK=50;

kmask_correction;

field=transQK_BT;
%field=transQK_BC;


val_lims=[-max(abs(field),[],'all'),max(abs(field),[],'all')];
lims=val_lims;
%lims=[-2.5e-5,2.5e-5];

%default_vals=[8/256,0.1,11];
default_vals=[10/256,0.07,20];

%default_vals=[10/256,0.16,10];
%default_vals=[14/256,0.26,7.5];

figure
imagesc(field(1:maxQ,1:maxK)'.*correction_KQ(2:maxK+1,2:maxQ+1),lims);
set(gca,'YDir','normal')
colormap(transformCM(gMyCM,default_vals(1),default_vals(2),default_vals(3)))
colorbar
xlabel('$Q$')
ylabel('$K$')
colorbar('TickLabelInterpreter','latex')

slider1 = uicontrol('style','slider','min',0,'max',0.5,'value',default_vals(1),'position',[10 340 300 10],'SliderStep',(1/0.5)*[2/256,2/256]);
slider2 = uicontrol('style','slider','min',0,'max',1,'value',default_vals(2),'position',[10 360 300 10]);
slider3 = uicontrol('style','slider','min',0,'max',50,'value',default_vals(3),'position',[10 380 300 10]);

addlistener(slider1,'ContinuousValueChange',@(hObject, event) updateplot(get(slider1,'Value'),get(slider2,'Value'),get(slider3,'Value'),gMyCM,slider1));
addlistener(slider2,'ContinuousValueChange',@(hObject, event) updateplot(get(slider1,'Value'),get(slider2,'Value'),get(slider3,'Value'),gMyCM,slider1));
addlistener(slider3,'ContinuousValueChange',@(hObject, event) updateplot(get(slider1,'Value'),get(slider2,'Value'),get(slider3,'Value'),gMyCM,slider1));

function updateplot(val1,val2,val3,cm,slider1)
    val1 = round(val1*256/2)/(256/2);
    set(slider1, 'Value', val1);

    colormap(transformCM(cm,val1,val2,val3))

    title(['val1=' num2str(val1,3) ', val2=' num2str(val2,3) ', val3=' num2str(val3,3)])
end
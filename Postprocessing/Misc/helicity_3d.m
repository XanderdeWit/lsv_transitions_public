%read velocity field
Vx=h5read([gDataDir gSimname '/continua_q1.h5'],'/Vth'); Vx=d.trim3D(Vx);
Vy=h5read([gDataDir gSimname '/continua_q2.h5'],'/Vr'); Vy=d.trim3D(Vy);
Vz=h5read([gDataDir gSimname '/continua_q3.h5'],'/Vz'); Vz=d.trim3D(Vz);

%move all velocities to cell center
Vx=d.faceToCell(d.makePeriodic(Vx,1),1);
Vy=d.faceToCell(d.makePeriodic(Vy,2),2);
Vz(:,:,end+1)=zeros(d.nxm,d.nym); Vz=d.faceToCell(Vz,3);

%prepare fields
[xmesh,ymesh,zmesh]=meshgrid(d.xms,d.yms,d.zms);
Vx_perm=permute(Vx,[2 1 3]); Vy_perm=permute(Vy,[2 1 3]); Vz_perm=permute(Vz,[2 1 3]);

%calculate vorticity
[ome_x,ome_y,ome_z,~]=curl(xmesh,ymesh,zmesh,Vx_perm,Vy_perm,Vz_perm);
ome_x=permute(ome_x,[2 1 3]); ome_y=permute(ome_y,[2 1 3]); ome_z=permute(ome_z,[2 1 3]);

%calculate helicity
helicity=Vx.*ome_x+Vy.*ome_y+Vz.*ome_z;

%Average in vertical direction
helicity_bar = mean(helicity .* reshape(d.g3rm,1,1,d.nzm),3);

%total helicity
helicity_tot=mean(helicity_bar,'all');

if gPlotting
    %Plot cross sections
    lims=1.2*[-max(abs(helicity_bar),[],'all'),max(abs(helicity_bar),[],'all')];
    plot3DHorCross(d.xms,d.yms,d.zms,helicity,'Helicity',lims,'pcolor',gMyCM,true)
    plot3DVertCross(d.xms,d.yms,d.zms,helicity,'Helicity',lims,'pcolor',gMyCM,true)
    
    %Plot vertical average
    figure
    drawCross(d.xms,d.yms,helicity_bar',lims,'pcolor',gMyCM,true);
    title('Vertically averaged helicity')
end
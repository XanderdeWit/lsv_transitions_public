%Plot temperature from 3D snapshot

temp=h5read([gDataDir gSimname '/continua_dens.h5'],'/dens');
temp=d.trim3D(temp);

%Plot cross sections
%lims='auto';
lims=[0,1];
plot3DHorCross(d.xms,d.yms,d.zms,temp,'Temperature',lims,'pcolor',gMyCM,true)
plot3DVertCross(d.xms,d.yms,d.zms,temp,'Temperature',lims,'pcolor',gMyCM,true)

plotIsoSurf(d.xms,d.yms,d.zms,temp,[0.58, 0.42],'temperature')

%temperature fluctuation
temp_ave=mean(temp,[1,2]);
temp_fluct=temp-temp_ave;

%Plot cross sections
%lims='auto';
lims=[-0.2,0.2];
plot3DHorCross(d.xms,d.yms,d.zms,temp_fluct,'Temperature fluctuation',lims,'pcolor',gMyCM,true)
plot3DVertCross(d.xms,d.yms,d.zms,temp_fluct,'Temperature fluctuation',lims,'pcolor',gMyCM,true)

plotIsoSurf(d.xms,d.yms,d.zms,temp_fluct,0.6*[lims(2),lims(1)],'temperature fluctuation')

function plotIsoSurf(xs,ys,zs,values,isoDefault,valueTitle)
    [xmesh,ymesh,zmesh]=meshgrid(xs,ys,zs);
    valuemesh=permute(values,[2 1 3]);

    figure

    if size(isoDefault,2)==2
        p1=makeIsosurf(xmesh,ymesh,zmesh,valuemesh,isoDefault(1),'red');
        p2=makeIsosurf(xmesh,ymesh,zmesh,valuemesh,isoDefault(2),'blue');

        slider1 = uicontrol('style','slider','min',min(values(:)),'max',max(values(:)),'value',isoDefault(1),'position',[10 10 250 10]);
        slider2 = uicontrol('style','slider','min',min(values(:)),'max',max(values(:)),'value',isoDefault(2),'position',[300 10 250 10]);

        addlistener(slider1,'ContinuousValueChange',@(hObject, event) updateplot(p1,xmesh,ymesh,zmesh,valuemesh,get(slider1,'Value'),[get(slider1,'Value') get(slider2,'Value')],valueTitle));
        addlistener(slider2,'ContinuousValueChange',@(hObject, event) updateplot(p2,xmesh,ymesh,zmesh,valuemesh,get(slider2,'Value'),[get(slider1,'Value') get(slider2,'Value')],valueTitle));

        updateTitle(valueTitle,isoDefault)
    else
        p=makeIsosurf(xmesh,ymesh,zmesh,valuemesh,isoDefault,'red');

        slider = uicontrol('style','slider','min',min(values(:)),'max',max(values(:)),'value',isoDefault,'position',[10 10 300 10]);
        addlistener(slider,'ContinuousValueChange',@(hObject, event) updateplot(p,xmesh,ymesh,zmesh,valuemesh,get(slider,'Value'),get(slider,'Value'),valueTitle));

        updateTitle(valueTitle,isoDefault)
    end

    daspect([1 1 1])
    view(3); 
    %axis tight
    D=max(xs);
    H=max(zs);
    xlim([0 D])
    ylim([0 D])
    zlim([0 H])
    camlight 
    lighting gouraud
end

function p=makeIsosurf(xmesh,ymesh,zmesh,valuemesh,iso,color)
    [faces,vertices]=isosurface(xmesh,ymesh,zmesh,valuemesh,iso);
    p=patch('Faces',faces,'Vertices',vertices);
    %isonormals(xmesh,ymesh,zmesh,valuemesh,p) --> doesn't work for non-uniform
    p.FaceColor = color;
    p.EdgeColor = 'none';
end

function updateplot(p,xmesh,ymesh,zmesh,valuemesh,iso,titleIso,valueTitle)
    [faces,vertices]=isosurface(xmesh,ymesh,zmesh,valuemesh,iso);
    p.Faces=faces;
    p.Vertices=vertices;
    updateTitle(valueTitle,titleIso)
end

function updateTitle(valueTitle,titleIso)
    if size(titleIso,2)==2
        title(['Iso surface ' valueTitle ' for ' num2str(titleIso(1)) ' (red) and ' num2str(titleIso(2)) ' (blue)'])
    else
        title(['Iso surface ' valueTitle ' for ' num2str(titleIso)])
    end
end

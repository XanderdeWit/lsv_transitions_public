%read velocity field
Vx=h5read([gDataDir gSimname '/continua_q1.h5'],'/Vth'); Vx=d.trim3D(Vx);
Vy=h5read([gDataDir gSimname '/continua_q2.h5'],'/Vr'); Vy=d.trim3D(Vy);

%move all velocities to cell center
Vx=d.faceToCell(d.makePeriodic(Vx,1),1);
Vy=d.faceToCell(d.makePeriodic(Vy,2),2);

%calculate z-averaged velocity
Vx_zave=mean(Vx.*reshape(d.g3rm,1,1,d.nzm),3);
Vy_zave=mean(Vy.*reshape(d.g3rm,1,1,d.nzm),3);

%read pressure field
pr=h5read([gDataDir gSimname '/continua_pr.h5'],'/Pr'); pr=d.trim3D(pr);

%calculate z-averaged pressure
pr_zave=mean(pr.*reshape(d.g3rm,1,1,d.nzm),3);

%calculate gradient
dpr_zdx=getDerivativeXY(pr_zave,d.dx,1,d);
dpr_zdy=getDerivativeXY(pr_zave,d.dy,2,d);

%calculate u in grad p
res=Vx_zave.*dpr_zdx + Vy_zave.*dpr_zdy;

figure
drawCross(d.xms,d.yms,res',[-max(abs(res),[],'all'),max(abs(res),[],'all')],'pcolor',gMyCM,true);
title(['$ \bar{u} \cdot \nabla \bar{p}$ ' gSimname])

mean(res,'all')

function deriv=getDerivativeXY(y,dx,axis,d)
    deriv=circshift(diff(d.makePeriodic(y,axis),1,axis),axis); %face value
    deriv=d.faceToCell(d.makePeriodic(deriv,axis),axis); %cell value
    deriv=deriv./dx;
end
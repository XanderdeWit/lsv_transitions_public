%hystresis of series

all_series={'series1/','series_interim/','series2/upper/'};
all_series_case={}; tot_icase=0;

%find cases
for i=1:length(all_series)
    series=char(all_series(i));
    
    mdirs=dir([gDataDir series]);
    mdirs=mdirs(3:end);
    
    for icase=1:length(mdirs)
        tot_icase=tot_icase+1;
        all_series_case{tot_icase}=series;
    end
    
    if i==1
        dirs=mdirs;
    else
        dirs=[dirs;mdirs];
    end
end

ncases=length(dirs);

control_params=zeros(1,ncases);
control_param_name='Ra'; %NB: name of control parameter

temp_grads=zeros(1,ncases);

tic
parfor icase=1:ncases
%for icase=1:ncases
    series=char(all_series_case(icase));
    simname=[series dirs(icase).name];

    pr=h5read([gDataDir simname '/stafield_master.h5'],'/Pr');
    ra=h5read([gDataDir simname '/stafield_master.h5'],'/Ra');
    ro=h5read([gDataDir simname '/stafield_master.h5'],'/Ro');

    control_params(icase)=ra; %NB: get control parameter

    temp_grads(icase)=getTempGrad(simname);
end    
toc

%flatten arrays
control_params=control_params(:);
temp_grads=temp_grads(:);

%sort results
[control_params,sorted]=sort(control_params);
temp_grads=temp_grads(sorted);

if gPlotting
    %plot helicities
    figure
    l=plot(control_params,temp_grads,'-s');
    l.MarkerFaceColor = l.Color; %to fill the symbols
    title('Temperature gradient at mid-height')
    ylabel('Temperature gradient')
    xlabel(control_param_name)
    xlim([0.9*min(control_params),1.1*max(control_params)])
    ylim([-1 0])
    set(gca, 'XScale', 'log')
    grid on
    box on
end

%wrap this in a function so it can be parallelized
function [temp_grad]=getTempGrad(simname)

    initialize; %load global variables
    gSimname=simname;
    make_domain; %init the domain of this scase
    
    temp=h5read([gDataDir simname '/stafield_data.h5'],'/Te_m1');
    averagingTime=h5read([gDataDir simname '/stafield_master.h5'],'/averaging_time');
    temp=temp/double(averagingTime);
    
    temp_grad=(temp(round(end/2)+1)-temp(round(end/2)))/(d.zms(round(end/2)+1)-d.zms(round(end/2)));
end
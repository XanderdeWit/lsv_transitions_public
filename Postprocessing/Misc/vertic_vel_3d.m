%Plot vertical velocity from 3D snapshot

Vz=h5read([gDataDir gSimname '/continua_q3.h5'],'/Vz');
Vz=d.trim3D(Vz);

%Plot cross sections
%lims='auto';
lims=[-0.2,0.2];
plot3DHorCross(d.xms,d.yms,d.zms,Vz,'Vertical velocity',lims,'pcolor',gMyCM,true)
plot3DVertCross(d.xms,d.yms,d.zms,Vz,'Vertical velocity',lims,'pcolor',gMyCM,true)
cores=[1,2,4,8,16,24,48,72];

walltime_per_iter_pr1=[12.17,6.29,3.12,1.56,0.93,0.76,0.47,0.37];
walltime_per_iter_pr01=[49.60,25.84,13.11,6.94,4.04,3.17,2.01,1.52];


if gPlotting
    figure
    l=plot(cores,walltime_per_iter_pr1,'-d','DisplayName','$256\times256\times144$, $Pr=1$');
    l.MarkerFaceColor = l.Color; %to fill the symbols
    hold on
    l=plot(cores,walltime_per_iter_pr01,'-d','DisplayName','$512\times512\times144$, $Pr=0.1$');
    l.MarkerFaceColor = l.Color; %to fill the symbols
    legend('Location','northeast')
    title('Performance')
    ylabel('Walltime per iteration (s)')
    xlabel('Number of cores')
    set(gca, 'XScale', 'log')
    set(gca, 'YScale', 'log')
    xlim([0.7,100])
    ylim([0.2,100])
    grid on
    box on
end
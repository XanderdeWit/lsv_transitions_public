%analyses one full series of cases

series=gSeries; %NB: directory of this series, appended by '/'

%find cases
dirs=dir([gDataDir series]);
dirs=dirs(3:end);

ncases=length(dirs);

control_params=zeros(1,ncases);
control_param_name='Ra'; %NB: name of control parameter

helicities=zeros(1,ncases);

tic
parfor icase=1:ncases
%for icase=1:ncases
    simname=[series dirs(icase).name];
    
    pr=h5read([gDataDir simname '/stafield_master.h5'],'/Pr');
    ra=h5read([gDataDir simname '/stafield_master.h5'],'/Ra');
    ro=h5read([gDataDir simname '/stafield_master.h5'],'/Ro');
    
    control_params(icase)=ra; %NB: get control parameter
    
    helicities(icase)=getHelicity(simname);
end
toc

%sort results
[control_params,sorted]=sort(control_params);
helicities=helicities(sorted);

if gPlotting
    %plot helicities
    figure
    l=plot(control_params,helicities,'-s');
    l.MarkerFaceColor = l.Color; %to fill the symbols
    title(['Helicity ' series])
    ylabel('Helicity')
    xlabel(control_param_name)
    %ylim([-0.1,1.0001]) ; yline(0,'k');
    xlim([0.9*min(control_params),max(control_params)+0.1*min(control_params)])
    grid on
    box on
end

%wrap this in a function so it can be parallelized
function [helicity_tot]=getHelicity(simname)

    initialize; %load global variables
    gSimname=simname;
    gPlotting=false; make_domain; %init the domain of this scase
    gTstart=getTstart(gSimname); %set gTstart
    gPlotting=false; helicity_3d; %get order parameters
    
end
%Movie of z-averaged horizontal kinetic energy

stst3dir=[gDataDir gSimname '/stst3'];

%The color axis needs to be calibrated, minimum and maximum are printed after rendering
%clim='auto';
clim=[0.21 0.32];

%slab_i_start=-1;
slab_i_start=10354;
slab_i_end=-1;
%slab_i_end=1500;

makeMovie(stst3dir,@getData,d.xms,d.yms,slab_i_start,slab_i_end,clim,'pcolor',gMyCM,[800,650],0.2,d)

function [data,time]=getData(stst3dir,slab_i,d)
    slabname='slab9'; %mid-height

    temp=h5read([stst3dir '/' slabname 'dens_' sprintf('%08d', slab_i) '.h5'],'/var');

    time=h5read([stst3dir '/' slabname 'dens_' sprintf('%08d', slab_i) '.h5'],'/time');

    data=temp;
end
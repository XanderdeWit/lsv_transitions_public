%to check the scaling of rotating condensates postulated by Seshasayanan & Alexakis (2018)

series='series2/upper/'; %NB: directory of this series, appended by '/'

%find cases
dirs=dir([gDataDir series]);
dirs=dirs(3:end);

ncases=length(dirs);

control_params=zeros(1,ncases);
control_param_name='Ro'; %NB: name of control parameter

tic
parfor icase=1:ncases
%for icase=1:ncases
    simname=[series dirs(icase).name];
    
    pr=h5read([gDataDir simname '/stafield_master.h5'],'/Pr');
    ra=h5read([gDataDir simname '/stafield_master.h5'],'/Ra');
    ro=h5read([gDataDir simname '/stafield_master.h5'],'/Ro');
    
    control_params(icase)=ro; %NB: get control parameter
    
    [mop_stats,mop_tss,mlop_stats,mlop_tss,mp_stats,mp_tss,mani_stats,mani_tss]=getOrderParams(simname);
    
    param_stats(:,:,icase) = mp_stats;
    param_tss(:,icase) = mp_tss;
end
toc

%determine the params with error
params=squeeze(param_stats(:,1,:));
param_errors=squeeze(param_stats(:,3,:));
param_names={param_tss.Name};

%sort results
[control_params,sorted]=sort(control_params);
params=params(:,sorted);
param_errors=param_errors(:,sorted);


if gPlotting
    %plot individual parameters
    together=[4];
    iPstart=1;
    for iplot=1:length(together)
        iPstop=sum(together(1:iplot));
        figure
        for iP=iPstart:iPstop
            l=plot(control_params,params(iP,:),'-s','DisplayName',char(param_names(iP)));
            l.MarkerFaceColor = l.Color; %to fill the symbols
            hold on
        end
        legend('Location','northeast')
        set(gca,'ColorOrderIndex',1)
        for iP=iPstart:iPstop
            errorbar(control_params,params(iP,:),param_errors(iP,:), 'vertical', 'LineStyle', 'none');
        end
        title(['Parameters ' series])
        ylabel('Parameter')
        xlabel(control_param_name)
        xlim([0.9*min(control_params),max(control_params)+0.1*min(control_params)])
        grid on
        box on
        iPstart=iPstop+1;
    end
    
    %plot rotating condendsate scaling of Seshasayanan & Alexakis (2018)
    % U^2 ~ Ome^2 L^2
    % in convective units: U^2 ~ 1/Ro^2*Gamma^2
    ro_plot=linspace(min(control_params),max(control_params),100);
    constant=0.008;
    plot(ro_plot,constant./ro_plot.^2,'k--')
end

%wrap this in a function so it can be parallelized
function [op_stats,op_tss,lop_stats,lop_tss,p_stats,p_tss,ani_stats,ani_tss]=getOrderParams(simname)

    initialize; %load global variables
    gSimname=simname;
    gPlotting=false; make_domain; %init the domain of this scase
    gTstart=getTstart(gSimname); %set gTstart
    gPlotting=false; ts_order_params; %get order parameters
    
end
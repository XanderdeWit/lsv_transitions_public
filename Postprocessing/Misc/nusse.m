%Analyze the Nusselt numbers as time series

file1 = fopen([gDataDir gSimname '/nusse.out'],'r');
data1 = fscanf(file1,'%f %f\n',[2 Inf]);
tsNuVol=timeseries(data1(2,:)',data1(1,:)','Name','Volume average');

file2 = fopen([gDataDir gSimname '/nusse2.out'],'r');
data2 = fscanf(file2,'%f %f\n',[3 Inf]);
tsNuW1=timeseries(data2(2,:)',data2(1,:)','Name','Wall low');
tsNuW2=timeseries(data2(3,:)',data2(1,:)','Name','Wall high');

file3 = fopen([gDataDir gSimname '/nusse3.out'],'r');
data3 = fscanf(file3,'%f %f\n',[3 Inf]);
tsNuTe=timeseries(data3(2,:)',data3(1,:)','Name','Viscous dissipation');
tsNuTh=timeseries(data3(3,:)',data3(1,:)','Name','Thermal dissipation');

%A mistake in the code needs to be fixed in post-pro
%tsNuTe=fixNuTe(tsNuTe,gPr);

tss=[tsNuVol,tsNuW1,tsNuW2,tsNuTe,tsNuTh];

%Interpolate to uniform time grid and remove duplicate points
for i=1:length(tss)
    tss(i)=interpTs(tss(i));
end

%Error analysis
nuStats=zeros(length(tss),5);
for i=1:length(tss)
    nuStats(i,:)=getTsStats(tss(i),gPlotting,gPlotting,gTstart);
end

if gPlotting
    %Plot time series
    figure
    hold on
    for i=1:length(tss)
       scatter(tss(i).Time,tss(i).Data,10,'s','filled')
    end
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    hold off
    title(['Nusselt numbers ' gSimname])
    ylabel('Nu')
    xlabel('Convective time')
    legend({tss.Name},'Location','northeast')
    %ylim([0 150])
    grid on
    box on
    %ylim([0,2700])
    %hold on
    %breakyaxis([200 2100]);
    %hold off
end

if gPlotting
    %Show running average and print stats
    figure
    hold on
    for i=1:length(tss)
       scatter(tss(i).Time,getRunningAverage(tss(i).Data,false),10,'s','filled')
    end
    if gTstart ~= -1
        xline(gTstart,'--r');
    end
    hold off
    title(['Nusselt numbers ' gSimname ' running average'])
    ylabel('Average Nu')
    xlabel('Convective time')
    legend({tss.Name},'Location','northeast')
    %ylim([0 150])
    grid on
    box on
end


%NOT USED in cartesian code (?)
function fixedTs=fixNuTe(ts,pr)

    ts.Data=pr.*(ts.Data-1)+1;
    fixedTs=ts;

end
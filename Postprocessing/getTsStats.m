function stats = getTsStats(ts,showGraph,showPrint,tstart)

if nargin > 3
    if tstart ~= -1
        %re-assign ts as truncated
        istart = findIn(ts.Time,tstart);
        ts=timeseries(ts.Data(istart:end),ts.Time(istart:end),'Name',ts.Name);
    end
end

%set t(0)=0
ts=timeseries(ts.Data,ts.Time-ts.Time(1),'Name',ts.Name);

%calculate mean and std
meanTs=mean(ts,'Weighting','none');
stdTs=std(ts,'Weighting','none');

%calculate auto-correlation
acor=xcorr(ts.Data-meanTs);
acor=acor(length(ts.Time):end)/acor(length(ts.Time));

%find first zero crossing
zci = @(v) find(v(:).*circshift(v(:), [-1 0]) <= 0);
zcis=zci(acor);
if length(zcis) > 0
    decorTime=ts.Time(zcis(1));
else
    decorTime=ts.Time(end);
end

if decorTime==0
    decorTime=ts.Time(end);
end

%find number of decorrelation times and calculate std error
Ndecors = ts.Time(end)/decorTime;
std_error = stdTs/sqrt(Ndecors);

%save stats to array
stats=[meanTs,stdTs,std_error,decorTime,Ndecors];

%print and plot
if showPrint
    disp([ts.Name ' std = ' num2str(stdTs)])
    disp([ts.Name ' decorrelation time = ' num2str(decorTime)])
    disp([ts.Name ' Ndecors = ' num2str(Ndecors)])
    disp([ts.Name ' mean '  char(177) ' std error = ' num2str(meanTs) ' ' char(177) ' ' num2str(std_error)])
end

if showGraph
    figure
    scatter(ts.Time,acor,10,'s','filled')
    xline(decorTime,'--');
    title(['Auto-correlation ' ts.Name])
    grid on
    box on
    xlim([0,min(8*decorTime,ts.Time(end))])
end

end


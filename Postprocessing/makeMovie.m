function makeMovie(stst3dir,getData,xs,ys,slab_i_start,slab_i_end,clim,plotStyle,cm,figSize,dtime,d)
h = figure;
set(gcf, 'Position',  [100, 100, figSize(1), figSize(2)])
%axis tight manual % this ensures that getframe() returns a consistent size
filename = 'animated.gif';

%find files
files=dir(stst3dir);
slab_is=0;
for ifile=3:length(files)
    if files(ifile).name(5:7) == 'zq1'
        if slab_is == 0
            slab_is=str2num(files(ifile).name(9:16));
        else
            slab_is=[slab_is; str2num(files(ifile).name(9:16))];
        end
    end
end

if slab_i_start ~= -1
    slab_is=slab_is(findIn(slab_is,slab_i_start):end);
end
if slab_i_end ~= -1
    slab_is=slab_is(1:findIn(slab_is,slab_i_end));
end

plt=drawCross(xs,ys,getData(stst3dir,slab_is(1),d)',clim,plotStyle,cm,true);

set(gca,'xtick',[])
set(gca,'ytick',[])

minData=Inf;
maxData=-Inf;
totalMinData=0;
totalMaxData=0;

for islab_i=1:length(slab_is)
    [data,time]=getData(stst3dir,slab_is(islab_i),d);
    
    %To determine clim
    currentMin=min(min(data));
    currentMax=max(max(data));
    totalMinData=totalMinData + currentMin;
    totalMaxData=totalMaxData + currentMax;
    if currentMin < minData
        minData = currentMin;
    end
    if currentMax > maxData
        maxData = currentMax;
    end
    
    if strcmp(plotStyle,'pcolor')
        plt.CData=data';
    else
        plt.ZData=data';
    end
    
    %Put timer in title
    title(['t=' sprintf('%.1f', time)])
    
    drawnow
    % Capture the plot as an image
    frame = getframe(h);
    im = frame2im(frame);
    [imind,cm] = rgb2ind(im,256);
    % Write to the GIF File
    if islab_i == 1
        imwrite(imind,cm,filename,'gif','Loopcount',inf,'DelayTime',dtime);
    else
        imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',dtime);
    end
end

%To determine clim
disp(['absolute minData=' num2str(minData)])
disp(['absolute maxData=' num2str(maxData)])
disp(['average minData=' num2str(totalMinData / length(slab_is))])
disp(['average maxData=' num2str(totalMaxData / length(slab_is))])

end


function runningAverage=getRunningAverage(data,weights)
    runningAverage=zeros(size(data));

    runningAverage(1)=data(1);
    
    if weights
        cumsumWeights=cumsum(weights);
        for i=2:length(data)
            runningAverage(i)=(runningAverage(i-1)*cumsumWeights(i-1)+data(i)*weights(i))/cumsumWeights(i);
        end
    else
        for i=2:length(data)
            runningAverage(i)=(runningAverage(i-1)*(i-1)+data(i))/i;
        end
    end

end
function tstart = getTstart(simname)

switch simname
    case {'series1/case02','series1/case03'}
        tstart=1000;
    case 'series1/case04'
        tstart=1500;
    case 'series1/case05'
        tstart=2000;
    case 'series1/case052'
        tstart=2500;
    case 'series1/case054'
        tstart=3000;
    case 'series1/case056'
        tstart=3500;
    case 'series1/case057'
        tstart=2000;
    case 'series1/case058'
        tstart=2000;
    case 'series1/case06'
        tstart=4000;
    case {'series1/case07','series1/case08','series1/case09'}
        tstart=2000;
    case 'series1/case10'
        tstart=1500;
        
    case {'series2/lower/case20','series2/upper/case20'}
        tstart=2200;
    case 'series2/lower/case21'
        tstart=3700;
    case 'series2/lower/case22'
        tstart=1500;
    case {'series2/lower/case23','series2/lower/case24''series2/lower/case25',...
          'series2/lower/case30','series2/lower/case31','series2/lower/case32','series2/lower/case35',...
          'series2/lower/case40','series2/lower/case50'}
        tstart=1000;
    case {'series2/upper/case26','series2/upper/case27','series2/upper/case28','series2/upper/case29'}
        tstart=2000;
    case 'series2/upper/case30'
        tstart=2500;
    case 'series2/upper/case31'
        tstart=4000;
    case 'series2/upper/case313'
        tstart=8500;
    case 'series2/upper/case315'
        tstart=8000;
    case 'series2/upper/case318'
        tstart=9000;
    case 'series2/upper/case32'
        tstart=7000;
    case 'series2/upper/case325'
        tstart=3500;
    case 'series2/upper/case33'
        tstart=2900;
    case 'series2/upper/case34'
        tstart=2200;
    case 'series2/upper/case25'
        tstart=1000;
    case 'series2/upper/case35'
        tstart=2500;
    case 'series2/upper/case40'
        tstart=1200;
    case 'series2/upper/case50'
        tstart=1000;
        
    case {'series_interim/case13','series_interim/case17'}
        tstart=1600;
        
    case {'series1_ar/small/case05','series1_ar/small/case058','series1_ar/small/case06','series1_ar/small/case062',...
          'series1_ar/small/case064','series1_ar/small/case066','series1_ar/small/case07','series1_ar/small/case08'}
        tstart=3000;
        
    case {'series1_ar/small2/case05','series1_ar/small2/case056','series1_ar/small2/case064','series1_ar/small2/case07'}
        tstart=2000;
    case 'series1_ar/small2/case058'
        tstart=1000;
    case 'series1_ar/small2/case06'
        tstart=7000;
    case 'series1_ar/small2/case062'
        tstart=5000;
        
    case {'series1_ar/large2/case04','series1_ar/large/case04'}
        tstart=500;
    case 'series1_ar/large2/case05'
        tstart=2000;
    case 'series1_ar/large2/case052'
        tstart=3000;
    case 'series1_ar/large2/case06'
        tstart=6000;
    case 'series1_ar/large2/case07'
        tstart=2000;
    case 'series1_ar/large/case05'
        tstart=4000;
    case 'series1_ar/large/case06'
        tstart=5000;
    case 'series1_ar/large/case07'
        tstart=3000;
    case {'series1_ar/large/case052','series1_ar/large/case054',...
          'series1_ar/large2/case054','series1_ar/large2/case056'}
        tstart=1500;
        
    otherwise
        tstart=-1; %-1 means start at beginning
        %tstart=2000;
end

end


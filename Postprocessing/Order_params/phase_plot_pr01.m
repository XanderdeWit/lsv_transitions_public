clear mseries;

mseries(1).dir='series1B/';
mseries(1).lsv=[zeros(1,5)];

for i=1:length(mseries)
    %find cases
    dirs=dir([gDataDir mseries(i).dir]);
    dirs=dirs(3:end);

    for j=1:length(dirs)
        gSimname=[mseries(i).dir dirs(j).name];

        mseries(i).pr(j)=h5read([gDataDir gSimname '/stafield_master.h5'],'/Pr');
        mseries(i).ra(j)=h5read([gDataDir gSimname '/stafield_master.h5'],'/Ra');
        mseries(i).ro(j)=h5read([gDataDir gSimname '/stafield_master.h5'],'/Ro');

        mseries(i).ek(j)=mseries(i).ro(j)*sqrt(mseries(i).pr(j)/mseries(i).ra(j));
        mseries(i).ta(j)=1/(mseries(i).ek(j))^2;
        
    end
end

mpr=mseries(1).pr(1);

if gPlotting
    %plot
    figure
    hold on
    limEk=[10^-6.5,10^-2.5];
    limRa=[10^4,10^10];
    Ek=logspace(log10(limEk(1)),log10(limEk(2)));
    %plot critical Ra for oscillatory convection
    Rac_o=6*(pi^2/2)^(2/3)/(1+mpr)^(1/3)*(Ek./mpr).^(-4/3);
    plot(Ek,Rac_o,'k')
    area(Ek,Rac_o,'FaceColor',[0.7 0.7 0.7])
    %plot onset of stable convection
    Rac_s=((27/4)*pi^4)^(1/3)*Ek.^(-4/3);
    plot(Ek,Rac_s,'k--')
    %plot lower boundary Favier et al. 2014
    Ra_low=3.0*Rac_o;
    split=findIn(Ek,10^-4);
    plot(Ek(1:split),Ra_low(1:split),'k-')
    plot(Ek(split:end),Ra_low(split:end),'k--')
    %plot upper boundary Favier et al. 2014
    Ra_upp=0.325*Ek.^-2;
    plot(Ek(1:split),Ra_upp(1:split),'k-')
    plot(Ek(split:end),Ra_upp(split:end),'k--')
    %plot cases
    for i=1:length(mseries)
        for j=1:length(mseries(i).lsv)
            scatter(mseries(i).ek(j),mseries(i).ra(j),20,'r','+','LineWidth',1)
            if mseries(i).lsv(j)==1
                scatter(mseries(i).ek(j),mseries(i).ra(j),40,'b','o','LineWidth',1)
            elseif mseries(i).lsv(j)==2
                scatter(mseries(i).ek(j),mseries(i).ra(j),40,'g','o','LineWidth',1)
            end
        end
    end
    title('Phase space')
    ylabel('Ra')
    xlabel('Ek')
    set(gca, 'XScale', 'log')
    set(gca, 'YScale', 'log')
    %set(gca, 'XDir','reverse')
    xlim(limEk)
    ylim(limRa)
    grid on
    box on
    set(gca,'PlotBoxAspectRatio',[1 (log(limRa(2))-log(limRa(1)))/(2*(log(limEk(2))-log(limEk(1)))) 1])
end
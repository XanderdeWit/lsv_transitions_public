%get time series of the different order parameters

%calculate the different order parameter time series
m_ts_order_params_gPlotting=gPlotting; gPlotting=false;
ts_kin_ratios;
ts_vort;
%ts_kin_trans_real;
ts_kin_trans_four;
ts_ani;
gPlotting=m_ts_order_params_gPlotting;

%Select the order parameters
op_stats=[ratio2DStats; ratio2Dk1Stats; ratio2Dk1_2Stats; ratioGammaStats;
    vort2DRatioStats; vort2Dk1RatioStats; vort2Dk1_2RatioStats;
%    ftotRatioStats; diss2DRatioStats; fmagRatioStats; fk1RatioStats; transRatioStats; transBCRatioStats;
    btK1RatioStats; bcK1RatioStats; bcRatioStats; diss2DNoWRatioStats];
op_tss=[tsRatio2D; tsRatio2Dk1; tsRatio2Dk1_2; tsRatioGamma;
    tsVort2DRatio; tsVort2Dk1Ratio; tsVort2Dk1_2Ratio;
%    tsFtotRatio; tsDiss2DRatio; tsFmagRatio; tsFk1Ratio; tsTransRatio; tsTransBCRatio;
    tsBTK1Ratio; tsBCK1Ratio; tsBCRatio; tsDiss2DNoWRatio];

%Select the large order parameters
lop_stats=[lratioHorStats; lratio2DStats; lratio2Dk1Stats;
    lratioVort2DStats; lratioVort2Dk1Stats; lratioVort2Dk1_2Stats];
lop_tss=[tsLRatioHor; tsLRatio2D; tsLRatio2Dk1
    tsLRatioVort2D; tsLRatioVort2Dk1; tsLRatioVort2Dk1_2];

%Also provide stats of the indiviual components of the OPs
p_stats=[kvStats; kH2DStats; kH3DStats; kH2Dk1Stats;
    vortM2Stats; vortM2zaveStats; vortM2zaveK1Stats;
%    ftotStats; diss2DStats; fmagStats; fk1Stats; transStats; transBCStats; nuTeStats;
    btK1Stats; bcK1Stats; bcStats; diss2DNoWStats;];
p_tss=[tsKv; tsKH2D; tsKH3D; tsKH2Dk1;
    tsVortM2; tsVortM2zave; tsVortM2zaveK1;
%    tsFtot; tsDiss2D; tsFmag; tsFk1; tsTrans; tsTransBC; tsNuTe;
    tsBTK1; tsBCK1; tsBC; tsDiss2DNoW];

%Also provide anisotropy parameters
ani_stats=[aniMSStats; aniZaveXYStats; aniZaveMSStats];
ani_tss=[tsAniMS; tsAniZaveXY; tsAniZaveMS];

plot_movmean=[8,9,10,11];
mm_time=100;

if gPlotting
    together=[4,3,4];
    iOPstart=1;
    for iplot=1:length(together)
        iOPstop=sum(together(1:iplot));
        figure
        hold on
        for i=iOPstart:iOPstop
            if ismember(i,plot_movmean)
                scatter(op_tss(i).Time,movmean(op_tss(i).Data,mm_time),10,'s','filled','DisplayName',op_tss(i).Name)
            else
                scatter(op_tss(i).Time,op_tss(i).Data,10,'s','filled','DisplayName',op_tss(i).Name)
            end
        end
        legend('Location','northwest')
        if gTstart ~= -1
            xline(gTstart,'--r');
        end
        title(['Order parameters ' gSimname])
        ylabel('Order parameter')
        xlabel('Convective time')
        %ylim([0,1])
        grid on
        box on
        iOPstart=iOPstop+1;
    end
    
    together=[3,3];
    iLOPstart=1;
    for iplot=1:length(together)
        iLOPstop=sum(together(1:iplot));
        figure
        hold on
        for i=iLOPstart:iLOPstop
            scatter(lop_tss(i).Time,lop_tss(i).Data,10,'s','filled','DisplayName',lop_tss(i).Name)
        end
        legend('Location','northwest')
        if gTstart ~= -1
            xline(gTstart,'--r');
        end
        title(['Large order parameters ' gSimname])
        ylabel('Large order parameter')
        xlabel('Convective time')
        yline(0,'k');
        grid on
        box on
        iLOPstart=iLOPstop+1;
    end
end
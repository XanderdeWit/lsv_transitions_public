clear mseries;

mseries(1).dir='series1/';
mseries(1).lsv=[zeros(1,7),ones(1,7)];

%mseries(2).dir='series1_old/';
%mseries(2).lsv=[0,1,zeros(1,2),ones(1,6)];

mseries(2).dir='series_lowTa_hyst/';
mseries(2).lsv=[0];

mseries(3).dir='series2/lower/';
mseries(3).lsv=[ones(1,2),2*ones(1,6),zeros(1,4)];

mseries(4).dir='series2/upper/';
mseries(4).lsv=[1,2*ones(1,7),zeros(1,10)];

mseries(5).dir='series_interim/';
mseries(5).lsv=[1,1];

for i=1:length(mseries)
    %find cases
    dirs=dir([gDataDir mseries(i).dir]);
    dirs=dirs(3:end);

    for j=1:length(dirs)
        gSimname=[mseries(i).dir dirs(j).name];

        mseries(i).pr(j)=h5read([gDataDir gSimname '/stafield_master.h5'],'/Pr');
        mseries(i).ra(j)=h5read([gDataDir gSimname '/stafield_master.h5'],'/Ra');
        mseries(i).ro(j)=h5read([gDataDir gSimname '/stafield_master.h5'],'/Ro');

        mseries(i).ek(j)=mseries(i).ro(j)*sqrt(mseries(i).pr(j)/mseries(i).ra(j));
        mseries(i).ta(j)=1/(mseries(i).ek(j))^2;
        
    end
end

if gPlotting
    %plot
    figure
    hold on
    limEk=[10^-6.5,10^-2.5];
    limRa=[10^5,10^11];
    Ek=logspace(log10(limEk(1)),log10(limEk(2)));
    %plot critical Ra
    Rac=(27*pi^4/4)^(1/3)*Ek.^(-4/3);
    plot(Ek,Rac,'k')
    area(Ek,Rac,'FaceColor',[0.7 0.7 0.7])
    %plot lower boundary Favier et al. 2014
    Ra_low=3.0*Rac;
    split=findIn(Ek,10^-4);
    plot(Ek(1:split),Ra_low(1:split),'k-')
    plot(Ek(split:end),Ra_low(split:end),'k--')
    %plot upper boundary Favier et al. 2014
    Ra_upp=0.3115*Ek.^-2;
    plot(Ek(1:split),Ra_upp(1:split),'k-')
    plot(Ek(split:end),Ra_upp(split:end),'k--')
    %plot cases
    for i=1:length(mseries)
        for j=1:length(mseries(i).lsv)
            scatter(mseries(i).ek(j),mseries(i).ra(j),20,'r','+','LineWidth',1)
            if mseries(i).lsv(j)==1
                scatter(mseries(i).ek(j),mseries(i).ra(j),40,'b','o','LineWidth',1)
            elseif mseries(i).lsv(j)==2
                scatter(mseries(i).ek(j),mseries(i).ra(j),40,'g','o','LineWidth',1)
            end
        end
    end
    title('Phase space')
    ylabel('Ra')
    xlabel('Ek')
    set(gca, 'XScale', 'log')
    set(gca, 'YScale', 'log')
    %set(gca, 'XDir','reverse')
    xlim(limEk)
    ylim(limRa)
    grid on
    box on
    set(gca,'PlotBoxAspectRatio',[1 (log(limRa(2))-log(limRa(1)))/(2*(log(limEk(2))-log(limEk(1)))) 1])
end
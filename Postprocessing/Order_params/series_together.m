%hystresis of series

all_series={'series1/','series_interim/','series2/upper/'};

m_series_together_gPlotting=gPlotting; gPlotting=false; m_series_together_gSeries=gSeries;

all_control_params=[];
all_order_params=[];
all_order_param_errors=[];
all_large_order_params=[];
all_large_order_param_errors=[];
all_params=[];
all_param_errors=[];
all_anis=[];
all_ani_errors=[];


for i=1:length(all_series)

    gSeries=char(all_series(i));
    series_analysis;

    all_control_params=[all_control_params,control_params];
    all_order_params=[all_order_params,order_params];
    all_order_param_errors=[all_order_param_errors,order_param_errors];
    all_large_order_params=[all_large_order_params,large_order_params];
    all_large_order_param_errors=[all_large_order_param_errors,large_order_param_errors];
    all_params=[all_params,params];
    all_param_errors=[all_param_errors,param_errors];
    all_anis=[all_anis,anis];
    all_ani_errors=[all_ani_errors,ani_errors];

end

gPlotting=m_series_together_gPlotting; gSeries=m_series_together_gSeries;

if gPlotting
    control_lims=[0.9*min(all_control_params),1.1*max(all_control_params)];
    
    %plot order parameters separately
    together=[4,3,2,2];
    iOPstart=1;
    for iplot=1:length(together)
        iOPstop=sum(together(1:iplot));
        figure
        for iOP=iOPstart:iOPstop
            l=plot(all_control_params,all_order_params(iOP,:),'-s','DisplayName',char(order_param_names(iOP)));
            l.MarkerFaceColor = l.Color; %to fill the symbols
            hold on
        end
        legend('Location','northwest')
        set(gca,'ColorOrderIndex',1)
        for iOP=iOPstart:iOPstop
            errorbar(all_control_params,all_order_params(iOP,:),all_order_param_errors(iOP,:), 'vertical', 'LineStyle', 'none');
        end
        title('Order parameters')
        ylabel('Order parameter')
        xlabel(control_param_name)
        %ylim([0,1])
        xlim(control_lims)
        set(gca, 'XScale', 'log')
        grid on
        box on
        
        iOPstart=iOPstop+1;
    end
    
    %plot large order parameters separately
    together=[3,3];
    iLOPstart=1;
    for iplot=1:length(together)
        iLOPstop=sum(together(1:iplot));
        figure
        for iLOP=iLOPstart:iLOPstop
            l=plot(all_control_params,all_large_order_params(iLOP,:),'-s','DisplayName',char(large_order_param_names(iLOP)));
            l.MarkerFaceColor = l.Color; %to fill the symbols
            hold on
        end
        legend('Location','northwest')
        set(gca,'ColorOrderIndex',1)
        for iLOP=iLOPstart:iLOPstop
            errorbar(all_control_params,all_large_order_params(iLOP,:),all_large_order_param_errors(iLOP,:), 'vertical', 'LineStyle', 'none');
        end
        title('Large order parameters')
        ylabel('Large order parameter')
        xlabel(control_param_name)
        yline(0,'k');
        xlim(control_lims)
        set(gca, 'XScale', 'log')
        grid on
        box on
        
        iLOPstart=iLOPstop+1;
    end
    
    %plot individual parameters
    together=[4,3,2,2];
    iPstart=1;
    for iplot=1:length(together)
        iPstop=sum(together(1:iplot));
        figure
        for iP=iPstart:iPstop
            l=plot(all_control_params,all_params(iP,:),'-s','DisplayName',char(param_names(iP)));
            l.MarkerFaceColor = l.Color; %to fill the symbols
            hold on
        end
        legend('Location','northwest')
        set(gca,'ColorOrderIndex',1)
        for iP=iPstart:iPstop
            errorbar(all_control_params,all_params(iP,:),all_param_errors(iP,:), 'vertical', 'LineStyle', 'none');
        end
        title('Parameters')
        ylabel('Parameter')
        xlabel(control_param_name)
        xlim(control_lims)
        set(gca, 'XScale', 'log')
        grid on
        box on
        iPstart=iPstop+1;
    end
    
    %plot anisotropy
    figure
    for iAni=1:size(anis,1)
        l=plot(all_control_params,all_anis(iAni,:),'-s','DisplayName',char(ani_names(iAni)));
        l.MarkerFaceColor = l.Color; %to fill the symbols
        hold on
    end
    legend('Location','northwest')
    set(gca,'ColorOrderIndex',1)
    for iAni=1:size(anis,1)
        errorbar(all_control_params,all_anis(iAni,:),all_ani_errors(iAni,:), 'vertical', 'LineStyle', 'none');
    end
    title('Anisotropy')
    ylabel('Anisotropy')
    xlabel(control_param_name)
    ylim([0,1])
    xlim(control_lims)
    set(gca, 'XScale', 'log')
    grid on
    box on
end
%analyses one full series of cases

series=gSeries; %NB: directory of this series, appended by '/'

%find cases
dirs=dir([gDataDir series]);
dirs=dirs(3:end);

ncases=length(dirs);

control_params=zeros(1,ncases);
control_param_name='Ra'; %NB: name of control parameter

tic
parfor icase=1:ncases
%for icase=1:ncases
    simname=[series dirs(icase).name];
    
    pr=h5read([gDataDir simname '/stafield_master.h5'],'/Pr');
    ra=h5read([gDataDir simname '/stafield_master.h5'],'/Ra');
    ro=h5read([gDataDir simname '/stafield_master.h5'],'/Ro');
    
    control_params(icase)=ra; %NB: get control parameter
    
    [mop_stats,mop_tss,mlop_stats,mlop_tss,mp_stats,mp_tss,mani_stats,mani_tss]=getOrderParams(simname);
    
    order_param_stats(:,:,icase) = mop_stats;
    order_param_tss(:,icase) = mop_tss;
    
    large_order_param_stats(:,:,icase) = mlop_stats;
    large_order_param_tss(:,icase) = mlop_tss;
    
    param_stats(:,:,icase) = mp_stats;
    param_tss(:,icase) = mp_tss;
    
    ani_stats(:,:,icase)=mani_stats;
    ani_tss(:,icase)=mani_tss;
end
toc

%print Ndecors
%squeeze(order_param_stats(:,5,:))

%determine the OP with error
order_params=squeeze(order_param_stats(:,1,:));
order_param_errors=squeeze(order_param_stats(:,3,:));
order_param_names={order_param_tss.Name};

%determine the LOP with error
large_order_params=squeeze(large_order_param_stats(:,1,:));
large_order_param_errors=squeeze(large_order_param_stats(:,3,:));
large_order_param_names={large_order_param_tss.Name};

%determine the params with error
params=squeeze(param_stats(:,1,:));
param_errors=squeeze(param_stats(:,3,:));
param_names={param_tss.Name};

%determine the anisotropy with error
anis=squeeze(ani_stats(:,1,:));
ani_errors=squeeze(ani_stats(:,3,:));
ani_names={ani_tss.Name};

%sort results
[control_params,sorted]=sort(control_params);
order_params=order_params(:,sorted);
order_param_errors=order_param_errors(:,sorted);
large_order_params=large_order_params(:,sorted);
large_order_param_errors=large_order_param_errors(:,sorted);
params=params(:,sorted);
param_errors=param_errors(:,sorted);
anis=anis(:,sorted);
ani_errors=ani_errors(:,sorted);

if gPlotting
    %plot order parameters
    figure
    for iOP=1:size(order_params,1)
        l=plot(control_params,order_params(iOP,:),'-s','DisplayName',char(order_param_names(iOP)));
        l.MarkerFaceColor = l.Color; %to fill the symbols
        hold on
    end
    legend('Location','northwest')
    set(gca,'ColorOrderIndex',1)
    for iOP=1:size(order_params,1)
        errorbar(control_params,order_params(iOP,:),order_param_errors(iOP,:), 'vertical', 'LineStyle', 'none');
    end
    title(['Order parameters ' series])
    ylabel('Order parameter')
    xlabel(control_param_name)
    %ylim([0,1])
    ylim([-0.1,1.0001]) ; yline(0,'k');
    xlim([0.9*min(control_params),max(control_params)+0.1*min(control_params)])
    grid on
    box on
    
    %plot order parameters separately
    together=[4,3,2,2];
    iOPstart=1;
    for iplot=1:length(together)
        iOPstop=sum(together(1:iplot));
        figure
        for iOP=iOPstart:iOPstop
            l=plot(control_params,order_params(iOP,:),'-s','DisplayName',char(order_param_names(iOP)));
            l.MarkerFaceColor = l.Color; %to fill the symbols
            hold on
        end
        legend('Location','northwest')
        set(gca,'ColorOrderIndex',1)
        for iOP=iOPstart:iOPstop
            errorbar(control_params,order_params(iOP,:),order_param_errors(iOP,:), 'vertical', 'LineStyle', 'none');
        end
        title(['Order parameters ' series])
        ylabel('Order parameter')
        xlabel(control_param_name)
        %ylim([0,1])
        xlim([0.9*min(control_params),max(control_params)+0.1*min(control_params)])
        grid on
        box on
        
        iOPstart=iOPstop+1;
    end
    
    %plot normalized large order parameters
    figure
    for iLOP=1:size(large_order_params,1)
        l=plot(control_params,large_order_params(iLOP,:)/large_order_params(iLOP,end),'-s','DisplayName',char(large_order_param_names(iLOP)));
        l.MarkerFaceColor = l.Color; %to fill the symbols
        hold on
    end
    legend('Location','northwest')
    set(gca,'ColorOrderIndex',1)
    for iLOP=1:size(large_order_params,1)
        errorbar(control_params,large_order_params(iLOP,:)/large_order_params(iLOP,end),large_order_param_errors(iLOP,:)/large_order_params(iLOP,end), 'vertical', 'LineStyle', 'none');
    end
    title(['Normalized large order parameters ' series])
    ylabel('Normalized large order parameter')
    xlabel(control_param_name)
    ylim([-0.1,1.1]) ; yline(0,'k');
    xlim([0.9*min(control_params),max(control_params)+0.1*min(control_params)])
    grid on
    box on
    
    %plot large order parameters separately
    together=[3,3];
    iLOPstart=1;
    for iplot=1:length(together)
        iLOPstop=sum(together(1:iplot));
        figure
        for iLOP=iLOPstart:iLOPstop
            l=plot(control_params,large_order_params(iLOP,:),'-s','DisplayName',char(large_order_param_names(iLOP)));
            l.MarkerFaceColor = l.Color; %to fill the symbols
            hold on
        end
        legend('Location','northwest')
        set(gca,'ColorOrderIndex',1)
        for iLOP=iLOPstart:iLOPstop
            errorbar(control_params,large_order_params(iLOP,:),large_order_param_errors(iLOP,:), 'vertical', 'LineStyle', 'none');
        end
        title(['Large order parameters ' series])
        ylabel('Large order parameter')
        xlabel(control_param_name)
        yline(0,'k');
        xlim([0.9*min(control_params),max(control_params)+0.1*min(control_params)])
        grid on
        box on
        
        iLOPstart=iLOPstop+1;
    end
    
    %plot individual parameters
    together=[4,3,2,2];
    iPstart=1;
    for iplot=1:length(together)
        iPstop=sum(together(1:iplot));
        figure
        for iP=iPstart:iPstop
            l=plot(control_params,params(iP,:),'-s','DisplayName',char(param_names(iP)));
            l.MarkerFaceColor = l.Color; %to fill the symbols
            hold on
        end
        legend('Location','northwest')
        set(gca,'ColorOrderIndex',1)
        for iP=iPstart:iPstop
            errorbar(control_params,params(iP,:),param_errors(iP,:), 'vertical', 'LineStyle', 'none');
        end
        title(['Parameters ' series])
        ylabel('Parameter')
        xlabel(control_param_name)
        xlim([0.9*min(control_params),max(control_params)+0.1*min(control_params)])
        grid on
        box on
        iPstart=iPstop+1;
    end
    
    %plot diss/force 2D
%     figure
%     l=plot(control_params,-params(9,:)./params(8,:),'-s','DisplayName','-Diss/Force 2D Real');
%     l.MarkerFaceColor = l.Color; %to fill the symbols
%     legend('Location','northwest')
%     title(['Parameters ' series])
%     ylabel('Parameter')
%     xlabel(control_param_name)
%     xlim([0.9*min(control_params),max(control_params)+0.1*min(control_params)])
%     grid on
%     box on
    
    figure
    l=plot(control_params,-params(11,:)./params(10,:),'-s','DisplayName','-Diss/Force 2D Fourier');
    l.MarkerFaceColor = l.Color; %to fill the symbols
    legend('Location','northwest')
    title(['Parameters ' series])
    ylabel('Parameter')
    xlabel(control_param_name)
    xlim([0.9*min(control_params),max(control_params)+0.1*min(control_params)])
    grid on
    box on
    
    %plot anisotropy
    figure
    for iAni=1:size(anis,1)
        l=plot(control_params,anis(iAni,:),'-s','DisplayName',char(ani_names(iAni)));
        l.MarkerFaceColor = l.Color; %to fill the symbols
        hold on
    end
    legend('Location','northwest')
    set(gca,'ColorOrderIndex',1)
    for iAni=1:size(anis,1)
        errorbar(control_params,anis(iAni,:),ani_errors(iAni,:), 'vertical', 'LineStyle', 'none');
    end
    title(['Anisotropy ' series])
    ylabel('Anisotropy')
    xlabel(control_param_name)
    ylim([0,1])
    xlim([0.9*min(control_params),max(control_params)+0.1*min(control_params)])
    grid on
    box on
end

%wrap this in a function so it can be parallelized
function [op_stats,op_tss,lop_stats,lop_tss,p_stats,p_tss,ani_stats,ani_tss]=getOrderParams(simname)

    initialize; %load global variables
    gSimname=simname;
    gPlotting=false; make_domain; %init the domain of this scase
    gTstart=getTstart(gSimname); %set gTstart
    gPlotting=false; ts_order_params; %get order parameters
    
end
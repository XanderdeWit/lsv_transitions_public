%hystresis of series

all_series={'series1/','series_interim/','series2/upper/'};

all_series_hyst={'series2/lower/'};

m_series_together_hyst_gPlotting=gPlotting; gPlotting=false; m_series_together_hyst_gSeries=gSeries;

all_control_params=[];
all_order_params=[];
all_order_param_errors=[];
all_large_order_params=[];
all_large_order_param_errors=[];
all_params=[];
all_param_errors=[];
all_anis=[];
all_ani_errors=[];

all_control_params_hyst=[];
all_order_params_hyst=[];
all_order_param_errors_hyst=[];
all_large_order_params_hyst=[];
all_large_order_param_errors_hyst=[];
all_params_hyst=[];
all_param_errors_hyst=[];
all_anis_hyst=[];
all_ani_errors_hyst=[];


for i=1:length(all_series)

    gSeries=char(all_series(i));
    series_analysis;

    all_control_params=[all_control_params,control_params];
    all_order_params=[all_order_params,order_params];
    all_order_param_errors=[all_order_param_errors,order_param_errors];
    all_large_order_params=[all_large_order_params,large_order_params];
    all_large_order_param_errors=[all_large_order_param_errors,large_order_param_errors];
    all_params=[all_params,params];
    all_param_errors=[all_param_errors,param_errors];
    all_anis=[all_anis,anis];
    all_ani_errors=[all_ani_errors,ani_errors];

end

for i=1:length(all_series_hyst)

    gSeries=char(all_series_hyst(i));
    series_analysis;

    all_control_params_hyst=[all_control_params_hyst,control_params];
    all_order_params_hyst=[all_order_params_hyst,order_params];
    all_order_param_errors_hyst=[all_order_param_errors_hyst,order_param_errors];
    all_large_order_params_hyst=[all_large_order_params_hyst,large_order_params];
    all_large_order_param_errors_hyst=[all_large_order_param_errors_hyst,large_order_param_errors];
    all_params_hyst=[all_params_hyst,params];
    all_param_errors_hyst=[all_param_errors_hyst,param_errors];
    all_anis_hyst=[all_anis_hyst,anis];
    all_ani_errors_hyst=[all_ani_errors_hyst,ani_errors];

end

gPlotting=m_series_together_hyst_gPlotting; gSeries=m_series_together_hyst_gSeries;

if gPlotting
    control_lims=[0.9*min(min(all_control_params),min(all_control_params_hyst)),1.1*max(max(all_control_params),max(all_control_params_hyst))];
    
    %plot order parameters separately
    together=[4,3,2,2];
    iOPstart=1;
    for iplot=1:length(together)
        iOPstop=sum(together(1:iplot));
        figure
        for iOP=iOPstart:iOPstop
            l=plot(all_control_params,all_order_params(iOP,:),'-s','DisplayName',char(order_param_names(iOP)));
            l.MarkerFaceColor = l.Color; %to fill the symbols
            hold on
        end
        legend('Location','northwest')
        set(gca,'ColorOrderIndex',1)
        for iOP=iOPstart:iOPstop
            l=plot(all_control_params_hyst,all_order_params_hyst(iOP,:),'-d','DisplayName',char(order_param_names(iOP)));
        end
        set(gca,'ColorOrderIndex',1)
        for iOP=iOPstart:iOPstop
            errorbar(all_control_params,all_order_params(iOP,:),all_order_param_errors(iOP,:), 'vertical', 'LineStyle', 'none');
            set(gca,'ColorOrderIndex',iOP-iOPstart+1)
            errorbar(all_control_params_hyst,all_order_params_hyst(iOP,:),all_order_param_errors_hyst(iOP,:), 'vertical', 'LineStyle', 'none');
        end
        title('Order parameters')
        ylabel('Order parameter')
        xlabel(control_param_name)
        %ylim([0,1])
        xlim(control_lims)
        set(gca, 'XScale', 'log')
        grid on
        box on
        
        iOPstart=iOPstop+1;
    end
    
    %plot large order parameters separately
    together=[3,3];
    iLOPstart=1;
    for iplot=1:length(together)
        iLOPstop=sum(together(1:iplot));
        figure
        for iLOP=iLOPstart:iLOPstop
            l=plot(all_control_params,all_large_order_params(iLOP,:),'-s','DisplayName',char(large_order_param_names(iLOP)));
            l.MarkerFaceColor = l.Color; %to fill the symbols
            hold on
        end
        legend('Location','northwest')
        set(gca,'ColorOrderIndex',1)
        for iLOP=iLOPstart:iLOPstop
            l=plot(all_control_params_hyst,all_large_order_params_hyst(iLOP,:),'-d','DisplayName',char(large_order_param_names(iLOP)));
        end
        set(gca,'ColorOrderIndex',1)
        for iLOP=iLOPstart:iLOPstop
            errorbar(all_control_params,all_large_order_params(iLOP,:),all_large_order_param_errors(iLOP,:), 'vertical', 'LineStyle', 'none');
            set(gca,'ColorOrderIndex',iLOP-iLOPstart+1)
            errorbar(all_control_params_hyst,all_large_order_params_hyst(iLOP,:),all_large_order_param_errors_hyst(iLOP,:), 'vertical', 'LineStyle', 'none');
        end
        title('Large order parameters')
        ylabel('Large order parameter')
        xlabel(control_param_name)
        yline(0,'k');
        xlim(control_lims)
        set(gca, 'XScale', 'log')
        grid on
        box on
        
        iLOPstart=iLOPstop+1;
    end
    
    %plot individual parameters
    together=[4,3,2,2];
    iPstart=1;
    for iplot=1:length(together)
        iPstop=sum(together(1:iplot));
        figure
        for iP=iPstart:iPstop
            l=plot(all_control_params,all_params(iP,:),'-s','DisplayName',char(param_names(iP)));
            l.MarkerFaceColor = l.Color; %to fill the symbols
            hold on
        end
        legend('Location','northwest')
        set(gca,'ColorOrderIndex',1)
        for iP=iPstart:iPstop
            l=plot(all_control_params_hyst,all_params_hyst(iP,:),'-d','DisplayName',char(param_names(iP)));
        end
        set(gca,'ColorOrderIndex',1)
        for iP=iPstart:iPstop
            errorbar(all_control_params,all_params(iP,:),all_param_errors(iP,:), 'vertical', 'LineStyle', 'none');
            set(gca,'ColorOrderIndex',iP-iPstart+1)
            errorbar(all_control_params_hyst,all_params_hyst(iP,:),all_param_errors_hyst(iP,:), 'vertical', 'LineStyle', 'none');
        end
        title('Parameters')
        ylabel('Parameter')
        xlabel(control_param_name)
        xlim(control_lims)
        set(gca, 'XScale', 'log')
        grid on
        box on
        iPstart=iPstop+1;
    end
    
    %plot anisotropy
    figure
    for iAni=1:size(anis,1)
        l=plot(all_control_params,all_anis(iAni,:),'-s','DisplayName',char(ani_names(iAni)));
        l.MarkerFaceColor = l.Color; %to fill the symbols
        hold on
    end
    legend('Location','northwest')
    set(gca,'ColorOrderIndex',1)
    for iAni=1:size(anis,1)
        l=plot(all_control_params_hyst,all_anis_hyst(iAni,:),'-d','DisplayName',char(ani_names(iAni)));
    end
    set(gca,'ColorOrderIndex',1)
    for iAni=1:size(anis,1)
        errorbar(all_control_params,all_anis(iAni,:),all_ani_errors(iAni,:), 'vertical', 'LineStyle', 'none');
        set(gca,'ColorOrderIndex',iAni)
        errorbar(all_control_params_hyst,all_anis_hyst(iAni,:),all_ani_errors_hyst(iAni,:), 'vertical', 'LineStyle', 'none');
    end
    title('Anisotropy')
    ylabel('Anisotropy')
    xlabel(control_param_name)
    ylim([0,1])
    xlim(control_lims)
    set(gca, 'XScale', 'log')
    grid on
    box on
end
%hystresis of series

hystseries=gHystSeries; %NB: directory of this hystseries, appended by '/'

m_hyst_series_gPlotting=gPlotting; gPlotting=false; m_hyst_series_gSeries=gSeries;

gSeries=[hystseries 'lower/'];
series_analysis;

control_params_lower=control_params;
order_params_lower=order_params;
order_param_errors_lower=order_param_errors;
large_order_params_lower=large_order_params;
large_order_param_errors_lower=large_order_param_errors;
params_lower=params;
param_errors_lower=param_errors;
anis_lower=anis;
ani_errors_lower=ani_errors;

gSeries=[hystseries 'upper/'];
series_analysis;

control_params_upper=control_params;
order_params_upper=order_params;
order_param_errors_upper=order_param_errors;
large_order_params_upper=large_order_params;
large_order_param_errors_upper=large_order_param_errors;
params_upper=params;
param_errors_upper=param_errors;
anis_upper=anis;
ani_errors_upper=ani_errors;

gPlotting=m_hyst_series_gPlotting; gSeries=m_hyst_series_gSeries;

if gPlotting
    control_lims=[0.9*min(min(control_params_lower),min(control_params_upper)),max(max(control_params_lower),max(control_params_upper))+0.1*min(min(control_params_lower),min(control_params_upper))];
    
    %plot order parameters separately
    together=[4,3,2,2];
    iOPstart=1;
    for iplot=1:length(together)
        iOPstop=sum(together(1:iplot));
        figure
        for iOP=iOPstart:iOPstop
            l=plot(control_params_upper,order_params_upper(iOP,:),'-s','DisplayName',char(order_param_names(iOP)));
            l.MarkerFaceColor = l.Color; %to fill the symbols
            hold on
        end
        legend('Location','northwest')
        set(gca,'ColorOrderIndex',1)
        for iOP=iOPstart:iOPstop
            l=plot(control_params_lower,order_params_lower(iOP,:),'-d','DisplayName',char(order_param_names(iOP)));
        end
        set(gca,'ColorOrderIndex',1)
        for iOP=iOPstart:iOPstop
            errorbar(control_params_upper,order_params_upper(iOP,:),order_param_errors_upper(iOP,:), 'vertical', 'LineStyle', 'none');
            set(gca,'ColorOrderIndex',iOP-iOPstart+1)
            errorbar(control_params_lower,order_params_lower(iOP,:),order_param_errors_lower(iOP,:), 'vertical', 'LineStyle', 'none');
        end
        title(['Order parameters ' hystseries])
        ylabel('Order parameter')
        xlabel(control_param_name)
        %ylim([0,1])
        xlim(control_lims)
        grid on
        box on
        
        iOPstart=iOPstop+1;
    end
    
    %plot large order parameters separately
    together=[3,3];
    iLOPstart=1;
    for iplot=1:length(together)
        iLOPstop=sum(together(1:iplot));
        figure
        for iLOP=iLOPstart:iLOPstop
            l=plot(control_params_upper,large_order_params_upper(iLOP,:),'-s','DisplayName',char(large_order_param_names(iLOP)));
            l.MarkerFaceColor = l.Color; %to fill the symbols
            hold on
        end
        legend('Location','northwest')
        set(gca,'ColorOrderIndex',1)
        for iLOP=iLOPstart:iLOPstop
            l=plot(control_params_lower,large_order_params_lower(iLOP,:),'-d','DisplayName',char(large_order_param_names(iLOP)));
        end
        set(gca,'ColorOrderIndex',1)
        for iLOP=iLOPstart:iLOPstop
            errorbar(control_params_upper,large_order_params_upper(iLOP,:),large_order_param_errors_upper(iLOP,:), 'vertical', 'LineStyle', 'none');
            set(gca,'ColorOrderIndex',iLOP-iLOPstart+1)
            errorbar(control_params_lower,large_order_params_lower(iLOP,:),large_order_param_errors_lower(iLOP,:), 'vertical', 'LineStyle', 'none');
        end
        title(['Large order parameters ' hystseries])
        ylabel('Large order parameter')
        xlabel(control_param_name)
        yline(0,'k');
        xlim(control_lims)
        grid on
        box on
        
        iLOPstart=iLOPstop+1;
    end
    
    %plot individual parameters
    together=[4,3,2,2];
    iPstart=1;
    for iplot=1:length(together)
        iPstop=sum(together(1:iplot));
        figure
        for iP=iPstart:iPstop
            l=plot(control_params_upper,params_upper(iP,:),'-s','DisplayName',char(param_names(iP)));
            l.MarkerFaceColor = l.Color; %to fill the symbols
            hold on
        end
        legend('Location','northwest')
        set(gca,'ColorOrderIndex',1)
        for iP=iPstart:iPstop
            l=plot(control_params_lower,params_lower(iP,:),'-d','DisplayName',char(param_names(iP)));
        end
        set(gca,'ColorOrderIndex',1)
        for iP=iPstart:iPstop
            errorbar(control_params_upper,params_upper(iP,:),param_errors_upper(iP,:), 'vertical', 'LineStyle', 'none');
            set(gca,'ColorOrderIndex',iP-iPstart+1)
            errorbar(control_params_lower,params_lower(iP,:),param_errors_lower(iP,:), 'vertical', 'LineStyle', 'none');
        end
        title(['Parameters ' hystseries])
        ylabel('Parameter')
        xlabel(control_param_name)
        xlim(control_lims)
        grid on
        box on
        iPstart=iPstop+1;
    end
    
    %plot anisotropy
    figure
    for iAni=1:size(anis,1)
        l=plot(control_params_upper,anis_upper(iAni,:),'-s','DisplayName',char(ani_names(iAni)));
        l.MarkerFaceColor = l.Color; %to fill the symbols
        hold on
    end
    legend('Location','northwest')
    set(gca,'ColorOrderIndex',1)
    for iAni=1:size(anis,1)
        l=plot(control_params_lower,anis_lower(iAni,:),'-d','DisplayName',char(ani_names(iAni)));
    end
    set(gca,'ColorOrderIndex',1)
    for iAni=1:size(anis,1)
        errorbar(control_params_upper,anis_upper(iAni,:),ani_errors_upper(iAni,:), 'vertical', 'LineStyle', 'none');
        set(gca,'ColorOrderIndex',iAni)
        errorbar(control_params_lower,anis_lower(iAni,:),ani_errors_lower(iAni,:), 'vertical', 'LineStyle', 'none');
    end
    title(['Anisotropy ' hystseries])
    ylabel('Anisotropy')
    xlabel(control_param_name)
    ylim([0,1])
    xlim(control_lims)
    grid on
    box on
end

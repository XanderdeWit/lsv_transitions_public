function tauW = getTauW(q,U,B)

%find first minimum and maximum;
[~,imin]=min(U(1:round(end/2)));
[~,imax]=max(U(imin:round(end/2)));
imax=imax+imin;

a=(U(imax)-U(imin))/(q(imax)-q(imin));
iave=round((imin+imax)/2);
b=B(iave)/q(iave);

disp(['Qmin = ' num2str(q(imin))])
disp(['Qmax = ' num2str(q(imax))])
disp(['a = ' num2str(a)])
disp(['b = ' num2str(b)])

tauW=(2/(a+b))*(q(imin)-q(imax)+(b/a)*q(imax)*(-1+(q(imax)/q(imin))^(a/b)));

end


%sample
n=100;
lambda=5.4e3;
samples=exprnd(lambda,1,n);

[h,p]=lillietest(samples,'Distribution','exponential');
[hnorm,pnorm]=swtest(samples);

p
pnorm

%histogram
figure
histogram(samples)

%cumulative distribution
[m_ecdf,m_ecdf_x]=ecdf(samples,'Bounds','on');
[m_fit,m_gof] = fit(m_ecdf_x,m_ecdf,fittype('1-exp(-x/lam)'),'StartPoint',lambda);
lambda_fit=m_fit.lam
lambda_MLE=mean(samples)

figure
ecdf(samples,'Bounds','on');
hold on
plot(m_ecdf_x,1-exp(-m_ecdf_x/lambda_MLE),'r--','LineWidth',2)
grid on
box on
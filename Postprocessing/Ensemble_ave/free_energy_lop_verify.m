%analyses one full series of cases

series='case6_ensemble_ave/'; %NB: directory of the ensemble average, appended by '/'

%find cases
dirs=dir([gDataDir series]);
dirs=dirs(4:end-1);

ncases=length(dirs);

lsv_times=zeros(1,ncases);

%bin energies
%nlevels=400;
nlevels=40;
lops=linspace(0,8,nlevels);
%choose dt
dt=2;

%choose E'
e_prime=1.62;
%e_prime=3;
%e_prime=5.5;
i_e_prime=findIn(lops,e_prime);

%define fields
delta_es=[];

tic
parfor icase=1:ncases
%for icase=1:ncases
    simname=[series dirs(icase).name];
    mdelta_es=getDeltaEs(gDataDir,simname,lops,dt,i_e_prime);
    
    delta_es=[delta_es; mdelta_es];
end
toc

if gPlotting
    [epdf,edges] = histcounts(delta_es,100,'Normalization', 'pdf');
    xi=0.5*(edges(2:end)+edges(1:end-1));
    figure
    plot(xi,epdf,'LineWidth',2);
    set(gca, 'YScale', 'log')
    grid on
    hold on
    mu=mean(delta_es)
    sigma=std(delta_es)
    pd = makedist('Normal','mu',mu,'sigma',sigma);
    plot(xi,pdf(pd,xi),'--','LineWidth',2)
    ylim([10^floor(log10(min(epdf))),10^ceil(log10(max(epdf)))])
    xlim([-1.1*max(abs(delta_es)),1.1*max(abs(delta_es))])
    xlabel('$Q-Q^\prime$')
    ylabel('$p(Q,t+\Delta t | Q^\prime, t)$')
    
    figure
    qqplot(delta_es)
    grid on
    box on
    ylabel('Quantiles of $p(Q,t+\Delta t | Q^\prime, t)$')
    title('QQ-Plot of $p(Q,t+\Delta t | Q^\prime, t)$ vs. Normal dist')
    
    [h_normal,p_normal,~]=swtest(delta_es);
    p_normal
end

%wrap this in a function so it can be parallelized
function delta_es=getDeltaEs(datadir,simname,lops,dt,i_e_prime)

    file = fopen([datadir simname '/rms_vel.out'],'r');
    data = fscanf(file,'%f %f\n',[5 Inf]);

    tsVxRMS=timeseries(data(2,:)',data(1,:)','Name','VxRMS');
    tsVyRMS=timeseries(data(3,:)',data(1,:)','Name','VyRMS');

    file = fopen([datadir simname '/zave_kin.out'],'r');
    data = fscanf(file,'%f %f\n',[3 Inf]);

    tsKH2D=timeseries(data(2,:)',data(1,:)','Name','KH2D');
    tsKH2Dk1=timeseries(data(3,:)',data(1,:)','Name','KH2Dk1');

    %Interpolate to uniform time grid and remove duplicate points
    tsVxRMS=interpTs(tsVxRMS);
    tsVyRMS=interpTs(tsVyRMS,tsVxRMS.Time);
    tsKH2D=interpTs(tsKH2D,tsVxRMS.Time);
    tsKH2Dk1=interpTs(tsKH2Dk1,tsVxRMS.Time);

    tsKH=timeseries((1/2)*(tsVxRMS.Data.^2+tsVyRMS.Data.^2), tsVxRMS.Time, 'Name', 'Horizontal kinetic energy');
    tsKH2D=timeseries((1/2)*(tsKH2D.Data), tsVxRMS.Time, 'Name', '2D horizontal kinetic energy');
    tsKH3D=timeseries(tsKH.Data - tsKH2D.Data, tsVxRMS.Time, 'Name', '3D horizontal kinetic energy');
    tsKH2Dk1=timeseries((1/2)*(tsKH2Dk1.Data), tsVxRMS.Time, 'Name', '2D horizontal kinetic energy K=1');
    
    tsLRatio2Dk1=timeseries(tsKH2Dk1.Data ./ tsKH3D.Data,tsVxRMS.Time, 'Name', '2D k=1 ratio');
    
    ndt=round(dt/(tsLRatio2Dk1.Time(end)-tsLRatio2Dk1.Time(end-1)));
    
    delta_es=[];
    
    for it=1:(length(tsLRatio2Dk1.Time)-ndt)
        i_energy=findIn(lops,tsLRatio2Dk1.Data(it));
        
        if i_energy == i_e_prime
            delta_es=[delta_es; tsLRatio2Dk1.Data(it+ndt)-tsLRatio2Dk1.Data(it)];
        end
        
    end
    
end
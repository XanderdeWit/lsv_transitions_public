%analyses one full series of cases

series='case6_ensemble_ave/'; %NB: directory of the ensemble average, appended by '/'

%find cases
dirs=dir([gDataDir series]);
dirs=dirs(4:end-1);

ncases=length(dirs);

%bin energies
nlevels=500;
energies=linspace(0,0.015,nlevels);
%choose dt
dt=3;

%define fields
tot_left_A=zeros(1,length(energies));
tot_left_B=zeros(1,length(energies));
n_left=zeros(1,length(energies));

tic
parfor icase=1:ncases
%for icase=1:ncases
    simname=[series dirs(icase).name];
    [mtot_left_A,mtot_left_B,mn_left]=getLeftAandB(gDataDir,simname,energies,dt);
    
    tot_left_A=tot_left_A+mtot_left_A;
    tot_left_B=tot_left_B+mtot_left_B;
    n_left=n_left+mn_left;
end
toc

tot_left_A=tot_left_A(n_left~=0);
tot_left_B=tot_left_B(n_left~=0);
n_left=n_left(n_left~=0);
energies=energies(n_left~=0);

left_A=tot_left_A./n_left;
left_B=tot_left_B./n_left;

A=left_A/dt;
B=left_B/(2*dt);
%B=(left_B-left_A.^2)/(2*dt);

%correct A
A=A-0.012/((5.3+3.7)*10^3);

%integrate A to find free energy
U=cumtrapz(energies,-A);

tauW=getTauW(energies,U,B)

if gPlotting
    figure
    plot(energies,U,'LineWidth',2)
    box on
    grid on
    %xlim([0,0.013])
    ylabel('``Free energy"')
    xlabel('$\hat{E}_\textrm{2D}(k=1)$')
    
    c=[0 0.4470 0.7410];
    figure
    scatter(energies,B,10,c*0.4+0.6,'s','filled')
    hold on
    plot(energies,movmean(B,nlevels/12),'LineWidth',2,'Color',c)
    box on
    grid on
    %xlim([0,0.013])
    ylabel('$B$')
    xlabel('$\hat{E}_\textrm{2D}(k=1)$')
end

%wrap this in a function so it can be parallelized
function [tot_left_A,tot_left_B,n_left]=getLeftAandB(datadir,simname,energies,dt)

    file = fopen([datadir simname '/zave_kin.out'],'r');
    data = fscanf(file,'%f %f\n',[3 Inf]);

    tsKH2Dk1=timeseries((1/2)*data(3,:)',data(1,:)','Name','KH2Dk1');
    tsKH2Dk1=interpTs(tsKH2Dk1);
    
    %tsKH2Dk1;
    ndt=round(dt/(tsKH2Dk1.Time(end)-tsKH2Dk1.Time(end-1)));
    
    tot_left_A=zeros(1,length(energies));
    tot_left_B=zeros(1,length(energies));
    n_left=zeros(1,length(energies));
    
    for it=1:(length(tsKH2Dk1.Time)-ndt)
        i_energy=findIn(energies,tsKH2Dk1.Data(it));
        
        tot_left_A(i_energy)=tot_left_A(i_energy)+(tsKH2Dk1.Data(it+ndt)-tsKH2Dk1.Data(it));
        tot_left_B(i_energy)=tot_left_B(i_energy)+(tsKH2Dk1.Data(it+ndt)-tsKH2Dk1.Data(it))^2;
        
        n_left(i_energy)=n_left(i_energy)+1;
    end
    
end
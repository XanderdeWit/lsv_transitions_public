%first gather free energy landscape and diffusion coefficient
free_energy_kin;
qs=energies; threshold=0.0085;
A=movmean(A,30);
B=movmean(B,30); B(440:end)=B(440);
%input functions/params: qs, A, B, dt

%perform Monte Carlo sims
nruns=102; %same as our ensemble
threshold_sustain=2000; %same as our ensemble

rng(1998) % For reproducibility

tLSVs=zeros(1,nruns);
qts=cell(1,nruns);

%parfor irun=1:nruns %introduces (extra) randomness
for irun=1:nruns
    qt=0;
    it=0;
    threshold_crossed=-1;
    
    while true
        w=normrnd(0,dt);
        iqt=findIn(qs,qt(it+1));
        
        qt(it+2)=qt(it+1)+A(iqt)*dt+sqrt(2*B(iqt))*w;
        it=it+1;
        if qt(it+1)<0
            qt(it+1)=0;
        end
        
        if qt(it+1)>threshold
            if threshold_crossed==-1
                threshold_crossed=it*dt;
            else
                if (it*dt-threshold_crossed)>=threshold_sustain
                    tLSVs(irun)=it*dt;
                    break
                end
            end
        else
            threshold_crossed=-1;
        end
    end
    
    qts{irun}=qt;
end

%determine t0
[m_ecdf_lsv,m_ecdf_x_lsv]=ecdf(tLSVs);
[m_fit_lsv,m_gof_lsv] = fit(m_ecdf_x_lsv,m_ecdf_lsv,fittype('1-exp(-(x-t0)/(mu-t0))','problem','mu'),'problem',mean(tLSVs),'StartPoint',min(tLSVs));
t0=m_fit_lsv.t0

%substract initialization time
waiting_times=tLSVs-m_fit_lsv.t0;

%test
[h,p,kstat,critval]=lillietest(waiting_times,'Distribution','exponential');
p_exp=p
[h_normal,p_normal,~]=swtest(waiting_times);
[h_normal_lillie,p_normal_lillie,kstat_normal_lillie,critval_normal_lillie]=lillietest(waiting_times,'Distribution','normal');

%cumulative distribution
[m_ecdf,m_ecdf_x,m_ecdf_lo,m_ecdf_up]=ecdf(waiting_times);
[m_fit,m_gof] = fit(m_ecdf_x,m_ecdf,fittype('1-exp(-x/lam)'),'StartPoint',mean(waiting_times));
lambda_fit=m_fit.lam
lambda_MLE=mean(waiting_times)

disp(['Monte-Carlo of Langevin model gives t_0=' num2str(t0,2) ' (real: 3.7e3) and tau_W=' num2str(lambda_MLE,2) ' (real: 5.3e3)'])

if gPlotting
    %plot trajectories
    figure
    cm=linspace(0.5,1.3,256)'*[0, 0.4470, 0.7410];
    trajs=[13,60,67,100,54,21];
    hold on
    for itraj=1:length(trajs)
        plot((0:length(qts{trajs(itraj)})-1)*dt,qts{trajs(itraj)},'LineWidth',2,'Color',cm(1+round(255*(length(trajs)-itraj+1)/length(trajs)),:))
    end
    yline(threshold,'--k','LineWidth',1.5);
    ylabel('$\mathcal{Q}$')
    xlabel('Time $t / (U^{-1}H)$')
    grid on
    box on
    
    %plot cumulative distribution
    figure
    c = [0 0.4470 0.7410];
    stairs(m_ecdf_x_lsv,m_ecdf_lsv,'-','Color',c,'LineWidth',2.5)
    hold on
    stairs(m_ecdf_x_lsv,[m_ecdf_lo, m_ecdf_up],':','Color',0.7*c+0.3,'LineWidth',2)
    plot(m_ecdf_x_lsv,1-exp(-(m_ecdf_x_lsv-m_fit_lsv.t0)/(m_fit_lsv.mu-m_fit_lsv.t0)),'--','Color',[0.8500 0.3250 0.0980],'LineWidth',2.8)
    xlabel('LSV time $t_\textrm{LSV} / (U^{-1}H)$')
    ylabel('Empirical cumulative distribution')
    ylim([0,1])
    xlim([0,m_ecdf_x_lsv(end)])
    area([0,m_fit_lsv.t0],[9,9],'FaceColor',[0,0,0],'FaceAlpha',0.2,'EdgeAlpha',0)
    grid on
    box on
end


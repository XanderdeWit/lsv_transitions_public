gPlottingCase20=gPlotting; gPlotting=false;

clear mtss;

gSimname='series2/upper/case20'; gTstart=getTstart(gSimname); make_domain;
ts_kin_ratios;
mtss(1)=tsLRatio2Dk1;

gSimname='case20_ensemble/run1'; gTstart=getTstart(gSimname); make_domain;
ts_kin_ratios;
mtss(end+1)=tsLRatio2Dk1;

gSimname='case20_ensemble/run2'; gTstart=getTstart(gSimname); %make_domain;
ts_kin_ratios;
mtss(end+1)=tsLRatio2Dk1;

gSimname='case20_ensemble/run3'; gTstart=getTstart(gSimname); make_domain;
ts_kin_ratios;
mtss(end+1)=tsLRatio2Dk1;

gSimname='case20_ensemble/run4'; gTstart=getTstart(gSimname); make_domain;
ts_kin_ratios;
mtss(end+1)=tsLRatio2Dk1;

gSimname='case20_ensemble/run5'; gTstart=getTstart(gSimname); make_domain;
ts_kin_ratios;
mtss(end+1)=tsLRatio2Dk1;

figure
for i=1:length(mtss)
    scatter(mtss(i).Time,mtss(i).Data,10,'s','filled')
    hold on
end
ylabel('Characteristic quantity $Q_\textrm{2D}(k=1)$')
xlabel('time $t/(U^{-1}H)$')
ylim([0,14])
grid on
box on

gPlotting=gPlottingCase20;
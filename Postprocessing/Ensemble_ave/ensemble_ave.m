%analyses one full series of cases

series='case6_ensemble_ave/'; %NB: directory of the ensemble average, appended by '/'

%find cases
dirs=dir([gDataDir series]);
dirs=dirs(4:end-1);

ncases=length(dirs);

lsv_times=zeros(1,ncases);

tic
%parfor icase=1:ncases
for icase=1:ncases
    simname=[series dirs(icase).name];
    lsv_times(icase)=getWaitingTime(gDataDir,simname);
end
toc

%sort
[lsv_times,sorted]=sort(lsv_times);
dirs=dirs(sorted);

%determine t0
[m_ecdf_lsv,m_ecdf_x_lsv]=ecdf(lsv_times);
[m_fit_lsv,m_gof_lsv] = fit(m_ecdf_x_lsv,m_ecdf_lsv,fittype('1-exp(-(x-t0)/(mu-t0))','problem','mu'),'problem',mean(lsv_times),'StartPoint',lsv_times(1));
lsv_time_1=lsv_times(1)
t0=m_fit_lsv.t0

%substract initialization time
waiting_times=lsv_times-m_fit_lsv.t0;

%test
[h,p,kstat,critval]=lillietest(waiting_times,'Distribution','exponential');
[h_normal,p_normal,~]=swtest(waiting_times);
[h_normal_lillie,p_normal_lillie,kstat_normal_lillie,critval_normal_lillie]=lillietest(waiting_times,'Distribution','normal');

%cumulative distribution
[m_ecdf,m_ecdf_x,m_ecdf_lo,m_ecdf_up]=ecdf(waiting_times);
[m_fit,m_gof] = fit(m_ecdf_x,m_ecdf,fittype('1-exp(-x/lam)'),'StartPoint',mean(waiting_times));
lambda_fit=m_fit.lam
lambda_MLE=mean(waiting_times)

if gPlotting
    %plot histogram
    figure
    bins=10; edges=waiting_times(1)+(0:bins)*(waiting_times(end)-waiting_times(1))/bins;
    histogram(waiting_times,edges)
    xlabel('Waiting time')
    grid on
    box on

    %plot cumulative distribution
    figure
    c = [0 0.4470 0.7410];
    stairs(m_ecdf_x,m_ecdf,'-','Color',c,'LineWidth',2)
    hold on
    stairs(m_ecdf_x,[m_ecdf_lo, m_ecdf_up],':','Color',c,'LineWidth',1.5)
    plot(m_ecdf_x,1-exp(-m_ecdf_x/lambda_MLE),'--','Color',[0.8500 0.3250 0.0980],'LineWidth',2.5)
    xlabel('Waiting time')
    ylabel('Emperical cumulative distribution')
    ylim([0,1])
    xlim([waiting_times(1),waiting_times(end)])
    grid on
    box on
end

%wrap this in a function so it can be parallelized
function waiting_time=getWaitingTime(datadir,simname)

    file = fopen([datadir simname '/continua_grid.dat'],'r');
    data = fscanf(file,'%f %f\n',[3 2]);
    
    waiting_time=data(3,2);
    
end
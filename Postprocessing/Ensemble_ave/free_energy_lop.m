%analyses one full series of cases

series='case6_ensemble_ave/'; %NB: directory of the ensemble average, appended by '/'

%find cases
dirs=dir([gDataDir series]);
dirs=dirs(4:end-1);

ncases=length(dirs);

lsv_times=zeros(1,ncases);

%bin energies
nlevels=400;
lops=linspace(0,8,nlevels);
%choose dt
dt=2;

%define fields
tot_left_A=zeros(1,length(lops));
tot_left_B=zeros(1,length(lops));
n_left=zeros(1,length(lops));

tic
parfor icase=1:ncases
%for icase=1:ncases
    simname=[series dirs(icase).name];
    [mtot_left_A,mtot_left_B,mn_left]=getLeftAandB(gDataDir,simname,lops,dt);
    
    tot_left_A=tot_left_A+mtot_left_A;
    tot_left_B=tot_left_B+mtot_left_B;
    n_left=n_left+mn_left;
end
toc

tot_left_A=tot_left_A(n_left~=0);
tot_left_B=tot_left_B(n_left~=0);
n_left=n_left(n_left~=0);
lops=lops(n_left~=0);

left_A=tot_left_A./n_left;
left_B=tot_left_B./n_left;

A=left_A/dt;
B=left_B/(2*dt);

%correct A
A=A-5.6/((5.3+3.7)*10^3);

%integrate A to find free energy
U=cumtrapz(lops,-A);

tauW=getTauW(lops,U,B)

if gPlotting
    figure
    plot(lops,U,'LineWidth',2)
    box on
    grid on
    %xlim([0,6])
    %ylim([-0.23,0.13])
    ylabel('``Free energy"')
    xlabel('$Q_\textrm{2D}(k=1)$')
    
    c=[0 0.4470 0.7410];
    figure
    scatter(lops,B,10,c*0.4+0.6,'s','filled')
    hold on
    plot(lops,movmean(B,nlevels/12),'LineWidth',2,'Color',c)
    box on
    grid on
    %xlim([0,6])
    ylabel('$B$')
    xlabel('$Q_\textrm{2D}(k=1)$')
end

%wrap this in a function so it can be parallelized
function [tot_left_A,tot_left_B,n_left]=getLeftAandB(datadir,simname,lops,dt)

    file = fopen([datadir simname '/rms_vel.out'],'r');
    data = fscanf(file,'%f %f\n',[5 Inf]);

    tsVxRMS=timeseries(data(2,:)',data(1,:)','Name','VxRMS');
    tsVyRMS=timeseries(data(3,:)',data(1,:)','Name','VyRMS');

    file = fopen([datadir simname '/zave_kin.out'],'r');
    data = fscanf(file,'%f %f\n',[3 Inf]);

    tsKH2D=timeseries(data(2,:)',data(1,:)','Name','KH2D');
    tsKH2Dk1=timeseries(data(3,:)',data(1,:)','Name','KH2Dk1');

    %Interpolate to uniform time grid and remove duplicate points
    tsVxRMS=interpTs(tsVxRMS);
    tsVyRMS=interpTs(tsVyRMS,tsVxRMS.Time);
    tsKH2D=interpTs(tsKH2D,tsVxRMS.Time);
    tsKH2Dk1=interpTs(tsKH2Dk1,tsVxRMS.Time);

    tsKH=timeseries((1/2)*(tsVxRMS.Data.^2+tsVyRMS.Data.^2), tsVxRMS.Time, 'Name', 'Horizontal kinetic energy');
    tsKH2D=timeseries((1/2)*(tsKH2D.Data), tsVxRMS.Time, 'Name', '2D horizontal kinetic energy');
    tsKH3D=timeseries(tsKH.Data - tsKH2D.Data, tsVxRMS.Time, 'Name', '3D horizontal kinetic energy');
    tsKH2Dk1=timeseries((1/2)*(tsKH2Dk1.Data), tsVxRMS.Time, 'Name', '2D horizontal kinetic energy K=1');
    
    tsLRatio2Dk1=timeseries(tsKH2Dk1.Data ./ tsKH3D.Data,tsVxRMS.Time, 'Name', '2D k=1 ratio');
    
    ndt=round(dt/(tsLRatio2Dk1.Time(end)-tsLRatio2Dk1.Time(end-1)));
    
    tot_left_A=zeros(1,length(lops));
    tot_left_B=zeros(1,length(lops));
    n_left=zeros(1,length(lops));
    
    for it=1:(length(tsLRatio2Dk1.Time)-ndt)
        i_energy=findIn(lops,tsLRatio2Dk1.Data(it));
        
        tot_left_A(i_energy)=tot_left_A(i_energy)+(tsLRatio2Dk1.Data(it+ndt)-tsLRatio2Dk1.Data(it));
        tot_left_B(i_energy)=tot_left_B(i_energy)+(tsLRatio2Dk1.Data(it+ndt)-tsLRatio2Dk1.Data(it))^2;
        
        n_left(i_energy)=n_left(i_energy)+1;
    end
    
end
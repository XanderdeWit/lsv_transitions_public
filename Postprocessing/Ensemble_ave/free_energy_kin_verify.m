%analyses one full series of cases

series='case6_ensemble_ave/'; %NB: directory of the ensemble average, appended by '/'

%find cases
dirs=dir([gDataDir series]);
dirs=dirs(4:end-1);

ncases=length(dirs);

lsv_times=zeros(1,ncases);

%bin energies
%nlevels=400;
nlevels=40;
energies=linspace(0,0.015,nlevels);
%choose dt
dt=3;

%choose E'
e_prime=0.004;
%e_prime=0.008;
%e_prime=0.012;
i_e_prime=findIn(energies,e_prime);

%define fields
delta_es=[];

tic
parfor icase=1:ncases
%for icase=1:ncases
    simname=[series dirs(icase).name];
    mdelta_es=getDeltaEs(gDataDir,simname,energies,dt,i_e_prime);
    
    delta_es=[delta_es; mdelta_es];
end
toc

if gPlotting
    [epdf,edges] = histcounts(delta_es,100,'Normalization', 'pdf');
    xi=0.5*(edges(2:end)+edges(1:end-1));
    figure
    plot(xi,epdf,'LineWidth',2);
    set(gca, 'YScale', 'log')
    grid on
    hold on
    mu=mean(delta_es)
    sigma=std(delta_es)
    pd = makedist('Normal','mu',mu,'sigma',sigma);
    plot(xi,pdf(pd,xi),'--','LineWidth',2)
    ylim([10^floor(log10(min(epdf))),10^ceil(log10(max(epdf)))])
    xlim([-1.1*max(abs(delta_es)),1.1*max(abs(delta_es))])
    xlabel('$E-E^\prime$')
    ylabel('$p(E,t+\Delta t | E^\prime, t)$')
    
    figure
    qqplot(delta_es)
    grid on
    box on
    ylabel('Quantiles of $p(E,t+\Delta t | E^\prime, t)$')
    title('QQ-Plot of $p(E,t+\Delta t | E^\prime, t)$ vs. Normal dist')
    
    [h_normal,p_normal,~]=swtest(delta_es);
    p_normal
end

%wrap this in a function so it can be parallelized
function delta_es=getDeltaEs(datadir,simname,energies,dt,i_e_prime)

    file = fopen([datadir simname '/zave_kin.out'],'r');
    data = fscanf(file,'%f %f\n',[3 Inf]);

    tsKH2Dk1=timeseries((1/2)*data(3,:)',data(1,:)','Name','KH2Dk1');
    tsKH2Dk1=interpTs(tsKH2Dk1);
    
    ndt=round(dt/(tsKH2Dk1.Time(end)-tsKH2Dk1.Time(end-1)));
    
    delta_es=[];
    
    for it=1:(length(tsKH2Dk1.Time)-ndt)
        i_energy=findIn(energies,tsKH2Dk1.Data(it));
        
        if i_energy == i_e_prime
            delta_es=[delta_es; tsKH2Dk1.Data(it+ndt)-tsKH2Dk1.Data(it)];
        end
        
    end
    
end
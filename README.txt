This repo contains the codes underlying the master thesis 'The transition towards a Large-Scale Vortex in fluid turbulence' (de Wit, 2021).

POSTPROCESSING

The postprocessing code is written in Matlab R2019b.

To start using any of the postprocessing scripts, first the script initialize.m should be run. This sets up the path to the directory containing the simulation data in the variable `gDataDir`. Here, one can also specify the simulation to be analyzed `gSimname` or series of simulations `gSeries` or `gHystSeries` as a relative path to the data directory. Furthermore this script sets up domain variables corresponding to the current simulation as well as the default fonts for the plots. Any global variables should be set in this script.

Now, one can run any of the postprocessing scripts, which are categorized into
- Aspect_ratio: analyzes the aspect ratio dependence of the LSV behavior.
- Ensemble_ave: analyzes an ensemble of runs, focusing on the growth statistics of the LSV and also contains the scripts on the Langevin modelling.
- Kinetic_energy: analyzes everything related to the kinetic energy of the flow.
- Misc: miscellaneous post-processing scripts that do not belong to any of the other categories.
- Order_params: analyzes of timeseries as well as control parameter dependence of the different order paramters of the LSV.
- Transfer: analyzes the transfer maps of kinetic energy
- Validation: the different validation methods for the DNS.
- Vorticity: analyzes everything related to the axial vorticity of the flow.

To obtain a 1-to-1 reproduction of the figures in the report, presentation or PRL, one can use the scripts provided in the corresponding Fig_factories. They contain a script ..._all_figs.m that generates all the respective figures that have been used.


PRODUCTION CODE

The production codes that have been used are modified Cartesian versions of the code of Verzicco. (see Verzicco and Orlandi (1996)).

We modified a few different versions:

- 3DRBcart113_20201113: the default version
- 3DRBcart113_20201113_ensemble_ave: the modified version for perfoming an ensemble average. Uses randomly perturbed initial conditions and contains a custom stop condition once a certain threshold in the horizontal kinetic energy is crossed and sustained. The latter is set in the section termed 'c   exit when in LSV state for >2000 time units' within gcurv.F.
- 3DRBcart113_20201113_rand: modified version that includes randomly perturbed initial conditions (but not the custom stop condition).
- 3DRBcart113_20201113_fourier: modified version that implements the fourier approach for calculating scale-to-scale energy transfer maps. This is implemented in stst.F in the section termed 'c  CALCULATE TRANSFER MAPS USING FOURIER METHOD (for square domain)'. Here, one can also set the maxq and maxk maximum wavenumbers to be used. These transfermaps are saved into 'stafield_master.h5'. Note that this is a computationally expensive calculation. It is possible to use the default version of the code first to arrive at an equilibrium state and then use this version of the code from that point on, in order to save resources spent on the transfer maps calculation in the transient phases.
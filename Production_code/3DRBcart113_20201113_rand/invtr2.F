
************************************************************************
c                       SUBROUTINE INVTR2
c   This subroutine performs the computation of Q~~ for the q2 momentum 
c   equation (radial direction) by a factored implicit scheme.
c   For details see the introduction of INVTR1
c   
c
      subroutine invtr2
      use param
      use local_arrays, only: q2,pr,rhs,dph,ru2
      use mpi_param, only: kstart,kend
      implicit none
      integer :: jc,kc,km,kp,jp,jm,ic,im,ip
      real    :: udx2,amm,app,acc
      real    :: dcq2,dpx22
      real    :: d22q2,d33q2,d11q2
      real    :: alre,udx1q,udx2q

      
cm
      alre=al/ren
      udx2=dx2*al
      udx1q=dx1q
      udx2q=dx2q
c
c  compute the rhs of the factored equation
c  everything at i+1/2,j,k+1/2
c
c    points inside the flowfield
c
        do kc=kstart,kend
          km=kmv(kc)
          kp=kpv(kc)
          amm=am3sk(kc)
          acc=ac3sk(kc)
          app=ap3sk(kc)
!$OMP  PARALLEL DO
!$OMP$ DEFAULT(SHARED)
!$OMP$ PRIVATE(jc,jm,jp,ic,ip,im)
!$OMP$ PRIVATE(d11q2,d22q2,d33q2,dcq2,dpx22)
          do jc=1,n2m
           jm=jmv(jc)
           jp=jpv(jc)
            do ic=1,n1m
            im=imv(ic)
            ip=ipv(ic)
c
c   11 second derivative of q2
c
            d11q2=(q2(ip,jc,kc)
     %            -2.0*q2(ic,jc,kc)
     %            +q2(im,jc,kc))*udx1q

c
c   22 second derivative of q2
c
            d22q2=(q2(ic,jp,kc)
     %            -2.0*q2(ic,jc,kc)
     %            +q2(ic,jm,kc))*udx2q
c
c   33 second derivative of q2
c
            d33q2=q2(ic,jc,kp)*app
     %           +q2(ic,jc,kc)*acc
     %           +q2(ic,jc,km)*amm

c
c    viscid terms
c
            dcq2=d22q2+d33q2+d11q2
c
c
c   component of grad(pr) along 2 direction
c
            dpx22=(pr(ic,jc,kc)-pr(ic,jm,kc))*udx2

c
            rhs(ic,jc,kc)=(ga*dph(ic,jc,kc)+ro*ru2(ic,jc,kc)
     %                    +alre*dcq2-dpx22)*dt

cm===========================================================
c
            ru2(ic,jc,kc)=dph(ic,jc,kc)
         enddo
       enddo
!$OMP  END PARALLEL DO
      enddo

      call solxi(beta*al*dx1q)

      call solxj(beta*al*dx2q)
      
      call solq12k(q2(1:n1,1:n2,kstart:kend))

      q2(:,n2,:) = q2(:,1,:)
     
      return
      end
c

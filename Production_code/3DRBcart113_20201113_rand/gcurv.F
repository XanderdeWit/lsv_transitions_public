************************************************************************
      subroutine gcurv
      use mpih
      use mpi_param,only: kstart,kend
      use param
      use local_arrays, only: q2,q3,dens,pr,q1
      use hdf5
#ifdef STATS
      use stat_arrays, only: timeint_cdsp
#endif
      implicit none
      integer :: ntstf, l, hdf_error
      real    :: cflm,dmax
      real    :: ti(2), tin(3)
      character(len=80) :: str

c
c     Code for the computation of three-dimensional incompressible flows    
c     in cylindrical polar coordinates.                                             
c                                                                       
c     This code solves flow fields bounded in the x3 (axial) and 
c     x2 (radial) directions. All boundaries can be no-slip or free-slip
c     by setting the appropriate indices in the input file.
c     The geometry  (a cylindrical can) is given in the subroutine cordi.
c     The discretization is uniform in the axial (3) and azimuthal (1)
c     directions, while can be non-uniform in the radial direction.
c
c     The equations for the following variables
c                                                                       
c      q1=v(theta)    q2=v(r)*r     q3=v(zeta)                          
c                                                                       
c     are discretized by finite-difference schemes.                    
c     The introduction of the variable q2 is necessary to avoid the
c     problem of the singularity of the equations at the axis of symmetry
c     (r=0).
c
c     All spatial derivatives are discretized by central second-order
c     accurate finite-difference schemes including the non linear terms.                                   
c
c     The nonlinear terms are treated explicitly, while the viscous terms
c     are computed implicitly. This would lead to the inversion of a
c     large banded matrix, that however is avoided by introducing
c     a factored scheme bringing to the solution of three tridiagonal
c     matrices for each velocity component (subroutine INVTR*).
c                              
c     In time a fractional-step procedure is used in the version of 
c     Nagi Mansour introducing the pressure in the first step.                         
c
c     The non-linear terms and the cross derivatives of the viscous terms
c     are discretized by explicit  Adams-Bashfort or 3rd order Runge-Kutta
c     method (A. Wray, personal communication).                      
c
c     The scalar quantity Phi, which projects the provisional velocity field
c     onto  a divergence free field, is solved by a direct method. 
c     For the axial and azimuthal directions modified wave numbers coupled 
c     with trigonometric expansions (FFTs) are used. The equation is then
c     solved by simply inverting a tridiagonal matrix for the radial direction.
c     No explicit boundary conditions are necessary for this Poisson equation.      
c                                                                       
c     Other details of the scheme are given in the introduction of the  
c     subroutine TSCHEM
c
c     timings
c                                                                       
c
c
      tin(1) = MPI_WTIME()
      
c
      call initia
      call meshes
      call indic                                                        
      call cordin
      call h5open_f(hdf_error)
#ifdef STATS
      call initstst
#endif
#ifdef STATS2
      call initstst2
#endif
c #ifdef STATS3
      if(tslab.gt.0.d0) then
        call initstst3
      endif
c #endif
C**************************
cm       imovie = 0
cm       tframe = 0.05
C**************************
c                                                                       
c     grid information                                                 
c                                                                       
cm===================================                                                      
#ifdef MOVIE
      call inimov
#endif
cm===================================
cm===================================
      if(myid.eq.0) then
      write(6,754)n1,n2,n3                                              
  754 format(5x,'grid resolution: ',' n1 = ',i5,' n2 = ',i5,
     % ' n3 = ',i5)                       
      write(6,755) 1.d0/dx1,1.d0/dx2,1.d0/dx3,dt,ntst                  
  755 format(5x,'dx1=',e10.3,' dx2=',e10.3,' dx3=',e10.3,' dt='
     & ,e10.3,' ntst=',i10,/)
      endif

cm===================================
cm===================================     
      
      time=0.d0
c                                                                       
c   read or create initial fields                                       
c                                                                       
#ifdef DEBUG
      write(*,*) 'starting phini'
#endif
      call phini
c
        do 22 l=1,ndv                                                   
           vmax(l)=0.d0
   22   continue                                                        
c
#ifdef DEBUG
      write(*,*) 'starting densbo'
#endif
      call densbo

cm================================ 
c      create the initial conditions
c
      if(nread.eq.0) then

        if(myid.eq.0) then
          write(6,'(5x,a,/)')'nread=0 ---> new initialization' 
        endif

        ntime=0                                                         
        time=0.d0
        cflm=0.d0
        
#ifdef DEBUG                                                               
        call MPI_BARRIER(MPI_COMM_WORLD,ierr)
        write(*,*) 'starting inqpr'
#endif
        call inqpr
        
cm================================ 
cm
      else
cm
cm================================
      if(myid.eq.0) then
        write(6,'(5x,a,/)')'nread=1 ---> reading files' 
      endif

#ifdef DEBUG                                                               
        call MPI_BARRIER(MPI_COMM_WORLD,ierr)
        write(*,*) 'starting inirea' 
#endif
        call inirea
        tmax = tmax + time

      endif                                                             

cm================================        
      call update_both_ghosts(n1,n2,dens,kstart,kend)
      call update_both_ghosts(n1,n2,pr,kstart,kend)
      call update_both_ghosts(n1,n2,q1,kstart,kend)
      call update_both_ghosts(n1,n2,q2,kstart,kend)
      call update_both_ghosts(n1,n2,q3,kstart,kend)

cm================================        
#ifdef DEBUG                                                               
        write(*,*) 'starting divgck' 
#endif
       call divgck(dmax)
        if(myid.eq.0) then
        write(6,'(5x,a,es10.3,/)')'Initial max div:',dmax
      endif

cm==============================               
      ntstf=ntst                                                   
cm================================
        if(myid.eq.0) then
      write(6,711) tprint,ntstf,tpin
711   format(5x,'check in cond: tprint =',f10.1, 
     &       '  ntstf =',i10,2x,'tpin =',f10.1/)
      endif
cm================================ 
#ifdef DEBUG                                                               
        write(*,*) 'starting densmc' 
#endif
       call densmc
            if(idtv.eq.1) then
            
cm================================
        if(myid.eq.0) then
      write(6,*)ntime,time,vmax(1),vmax(2),vmax(3),
     & dt,dmax,densm,denmax,denmin
      endif
cm================================  
            else
cm================================
        if(myid.eq.0) then
      write(6,*)ntime,time,vmax(1),vmax(2),vmax(3),
     % cflm,dmax, densm,denmax,denmin
      endif
cm================================     
     
        cflm=cflm*dt
            endif
c
#ifdef DEBUG                                                               
        write(*,*) 'starting coetar' 
#endif
         call coetar
c      
c
c  ********* starts the time dependent calculation ***

      if(myid.eq.0) then
        tin(2) = MPI_WTIME()
        write(6,'(/,5x,a,f10.3,a,/)') 'Initialization Time = ',
     %  tin(2)-tin(1), ' sec.'
      endif
c                                                                       
      do 350 ntime=0,ntstf                                           
c
c     the calculation stops if the velocities are diverging for numerical
c     stability conditions (courant number restrictions)                
c
        ti(1) = MPI_WTIME()
c
#ifdef DEBUG                                                               
        if(myid.eq.0) then
          write(*,*) 'starting cfl' 
        endif
#endif
        call cfl(cflm)
#ifdef DEBUG                                                               
        write(*,*) 'finished cfl' ,myid,cflm
#endif

        if(idtv.eq.1) then
          if(ntime.ne.1) then
            dt=cflmax/cflm
            if(dt.gt.dtmax) dt=dtmax
            if(dt.lt.1.d-8) go to 166
          endif
        else
          cflm=cflm*dt
          if(cflm.gt.cfllim) go to 165
        endif

        beta=dt/ren*0.5d0

#ifdef DEBUG                                                               
        write(*,*) 'starting tschem'
#endif
        call tschem
        time=time+dt

        ti(2) = MPI_WTIME()
        if(time.eq.1 .or. mod(time,tpin).lt.dt) then
          call vmaxv
          if(vmax(1).gt.1000.d0.and.vmax(2).gt.1000.d0) go to 266
          call cfl(cflm)
          call divgck(dmax)
          call densmc
          if(time.gt.tsta) then
#ifdef STATS
            call stst
#endif
#ifdef BALANCE
            call balance
#endif
          endif

          if(myid.eq.0) then
            write(6,*) '----------------------------------------'
            str = '(a,f9.4,a,i6,a,es8.2)'
            write(6,str) ' T = ',time,' NTIME = ',ntime,' DT = ',dt
            str = '(a,es8.2)'
            write(6,str) ' Divg dmax = ',dmax
            str = '(a,f6.2,a)'
            write(6,str) ' Iteration Time = ', ti(2)-ti(1), ' sec.'
          endif

          if(idtv.eq.0) then
              cflm=cflm*dt
          endif

          if(dmax.gt.resid) go to 169
        endif

#ifdef MOVIE
        if(mod(time,tframe).lt.dt) then
          call mkmov
        endif
#endif

#ifdef STATS2
        if(mod(time,tslab).lt.dt .and. time.gt.tsta) then
          call stst2
        endif
#endif

c  #ifdef STATS3
        if(tslab.gt.0.d0) then
          if(mod(time,tslab).lt.dt .and. time.gt.tsta) then
            call stst3
            if(myid.eq.0) then
              write(6,*) '======================='
              write(6,'(a,f9.4)') ' Slab saved at T = ',time
              write(6,*) '======================='
            endif
          endif
        endif
c  #endif

cm===============================================
c   exit when reach tmax
        if(time.ge.tmax) go to 333

cm===============================================
c   exit when reach  walltimemax (max 5 days on Cartesius)
        if( (ti(2)-tin(1)) .gt. walltimemax) go to 333

c================================================           
c   save data every 24h   DOESN'T WORK!
c        if( mod(ti(2)-tin(1), 85000.d0) .lt. (ti(2)-ti(1)) )then
c        if( mod(ti(2)-tin(1), 25.d0) .lt. (ti(2)-ti(1)) )then
c          if(myid.eq.0)then
c            write(6,*)' '
c            write(6,*) '======================='
c            write(6,*) ' Restart files updated every 24 hours'
c            write(6,*)' '
c          endif
c
c          call mpi_write_continua
c          call MPI_BARRIER(MPI_COMM_WORLD,ierr)
c          if(myid.eq.0)write(*,*)' continua files written'
c
c          call ststwr
c          call MPI_BARRIER(MPI_COMM_WORLD,ierr)
c          if(myid.eq.0)then
c            write(*,*)' statistics files written'
c            write(6,*) '======================='
c          endif
c
c        endif

c=====================================================

  350 continue   ! END OF TIME LOOP

c=====================================================
 
333   continue
  
      tin(3) = MPI_WTIME()
      if(myid.eq.0) then
        write(6,'(/,a,f9.2,a)')
     &  '  Total Iteration Time = ',(tin(3)-tin(2))/3600.,'h'
      endif

      call mpi_write_continua
      call MPI_BARRIER(MPI_COMM_WORLD,ierr)
      if(myid.eq.0)write(*,*)' continua files written'

#ifdef STATS
      call ststwr
#endif
      call MPI_BARRIER(MPI_COMM_WORLD,ierr)
      if(myid.eq.0)write(*,*)' statistics files written'


      go to 167                                                         
  165 continue 

cm======================================
      if(myid.eq.0) then
      write(6,164)                                                      
  164 format(10x,'cfl too large  ')
      endif
cm======================================                                  
      go to 167                                                         
  166 continue
cm======================================
      if(myid.eq.0) then
      write(6,168) dt 
  168 format(10x,'dt too small, DT= ',e14.7)
      endif
cm======================================   
      go to 167                                                         
  266 continue
cm======================================
      if(myid.eq.0) then
      write(6,268)                                                      
  268 format(10x,'velocities diverged')
      endif
cm======================================                                 
      go to 167                                                         
  169 continue
  
cm======================================
      if(myid.eq.0) then
      write(6,178) dmax                                 
  178 format(10x,'too large local residue for mass conservation : '     
     1       ,e12.5,' at ')
     
      endif
      call divgloc
cm======================================

  167 continue                                                          
c
#ifdef _OPENMP
      call dfftw_cleanup_threads()
#endif

      call dfftw_destroy_plan(fwd_plan)
      call dfftw_destroy_plan(bck_plan)

c      call h5close_f(hdf_error)
      
      return                                                            
      end                                                               
c

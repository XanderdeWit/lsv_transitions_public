c
c     this subroutine calculates divg(q).
c     q are the "fluxes"
c      q2=v(r)     q3=v(zeta)
c
      subroutine divg
      use param
      use local_arrays, only: q1,q2,q3,dph
      use mpi_param, only: kstart,kend
      implicit none
      integer :: jc,jp,kc,kp,ic,ip
      real    :: usdtal,dqcap   

      usdtal = 1.d0/(dt*al)

      do kc=kstart,kend
        kp=kc+1
!$OMP  PARALLEL DO
!$OMP$ DEFAULT(SHARED)
!$OMP$ PRIVATE(jc,ic,jp,ip,dqcap)
        do jc=1,n2m
          jp=jpv(jc)
            do ic=1,n1m
              ip=ipv(ic)
              dqcap= (q1(ip,jc,kc)-q1(ic,jc,kc))*dx1
     &              +(q2(ic,jp,kc)-q2(ic,jc,kc))*dx2
     %              +(q3(ic,jc,kp)-q3(ic,jc,kc))*udx3m(kc)
              dph(ic,jc,kc)=dqcap*usdtal
            enddo
         enddo
!$OMP  END PARALLEL DO
      enddo

      return
      end
c
c
c

c===============================================
      subroutine block(n, p, irank, istart, iend, blcsz)
      implicit none
      integer,intent(in) :: n,p,irank
      integer,intent(out) :: istart,iend
      integer :: i
      integer,dimension(0:p-1),intent(out) :: blcsz
      
      do i=0,p-1
      blcsz(i) = floor(real((n+p-i-1)/p))
      enddo
      istart = sum(blcsz(0:irank))-blcsz(irank)+1
      iend = istart+blcsz(irank)-1

      end subroutine block
c=================================================           
      subroutine mpi_workdistribution
      use param
      use mpih 
      use mpi_param
      implicit none
      integer :: i
      
      if(.not. allocated(countj)) allocate(countj(0:numtasks-1))
      if(.not. allocated(countjp)) allocate(countjp(0:numtasks-1))
      if(.not. allocated(countk)) allocate(countk(0:numtasks-1))

!EP   For PERIODIC pressure solver
      call block(n2+1, numtasks, myid, jstartp, jendp, countjp)
      djp=jendp-jstartp+1

      call block(n2m, numtasks, myid, jstart, jend, countj)
      dj=jend-jstart+1
      
      call block(n3m, numtasks, myid, kstart, kend, countk)
      dk=kend-kstart+1

#ifdef DEBUG
      write(*,*) "jstart: ",jstart
      write(*,*) "jend: ",jend
      write(*,*) "jstartp: ",jstart
      write(*,*) "jendp: ",jend
      write(*,*) "kstart: ",kstart
      write(*,*) "kend: ",kend
#endif

      if( dj .lt. 1 ) then            
       write(6,*)'process ',myid,' has work load <1 cell in j direction'
       write(6,*)"Check grid dimensions and number of processes"
       
       call MPI_Abort(MPI_COMM_WORLD, 1, ierr )
      endif

      if( dk .lt. 1 ) then            
       write(6,*)'process ',myid,' has work load <1 cell in k direction'
       write(6,*)"Check grid dimensions and number of processes"
       
       call MPI_Abort(MPI_COMM_WORLD, 1, ierr )
      endif
  
      if(.not. allocated(offsetjp)) allocate(offsetjp(0:numtasks-1))
      if(.not. allocated(offsetj)) allocate(offsetj(0:numtasks-1))
      if(.not. allocated(offsetk)) allocate(offsetk(0:numtasks-1))
      
      offsetjp(:)=0
      offsetj(:)=0
      offsetk(:)=0
      do i=1,numtasks-1
        offsetjp(i)= offsetjp(i-1) + countjp(i-1)
        offsetj(i)= offsetj(i-1) + countj(i-1)
        offsetk(i)= offsetk(i-1) + countk(i-1)
      end do
      
      !-------For MPI-IO--------------------------------
      mydata= n2*dk*n1
      mydatam = n2m*dk*n1m

      if(myid .eq. numtasks-1) mydata = n2*(dk+1)*n1
      
      if(.not. allocated(countf)) allocate(countf(0:numtasks-1))
      if(.not. allocated(offsetf)) allocate(offsetf(0:numtasks-1))
       
      call MPI_ALLGATHER(mydata, 1, MPI_INTEGER, countf, 1, MPI_INTEGER,
     & MPI_COMM_WORLD,ierr)
    
      offsetf(:)=0
      do i=1,numtasks-1
        offsetf(i)= offsetf(i-1) + countf(i-1)
      end do
      
      !------------------------------------------------
      
      end subroutine mpi_workdistribution 
c===============================================
      subroutine update_both_ghosts(n1,n2,q1,ks,ke)
      use mpih
      implicit none
      integer, intent(in) :: ks,ke
      real,intent(inout) :: q1(n1,n2,ks-1:ke+1)
      integer,intent(in) :: n1,n2
      integer :: mydata
      integer :: my_down, my_up,tag
      
      mydata= n1*n2
      
      my_down=myid-1
      
      my_up=myid+1

      if(myid .eq. 0) my_down=MPI_PROC_NULL
      if(myid .eq. numtasks-1) my_up=MPI_PROC_NULL

      tag=1
      call MPI_ISEND(q1(1,1,ke), mydata, MDP,
     & my_up,tag,MPI_COMM_WORLD,req(1),ierr)
      
      call MPI_ISEND(q1(1,1,ks), mydata,  MDP,
     & my_down,tag,MPI_COMM_WORLD,req(2), ierr)
     
      call MPI_IRECV(q1(1,1,ks-1), mydata,  MDP, 
     & my_down,tag,MPI_COMM_WORLD,req(3),ierr)
     
      call MPI_IRECV(q1(1,1,ke+1), mydata,  MDP,
     & my_up, tag,MPI_COMM_WORLD,req(4),ierr)
     
      call MPI_Waitall(4,req,status,ierr)

      end subroutine update_both_ghosts
c=========================================
      subroutine update_upper_ghost(n1,n2,q1)
      use mpih
      use mpi_param, only: kstart,kend,dk
      implicit none
      real,intent(inout) :: q1(n1,n2,kstart-1:kend+1)
      integer,intent(in) :: n1,n2
      integer :: mydata
      integer :: my_down, my_up,tag
       
      mydata= n1*n2
      
      my_down= myid-1
      
      my_up= myid+1

      if(myid .eq. 0) my_down= MPI_PROC_NULL
      if(myid .eq. numtasks-1) my_up= MPI_PROC_NULL
     
      tag=1
      
      call MPI_ISEND(q1(1,1,kstart), mydata, MDP,
     & my_down, tag, MPI_COMM_WORLD, req(1), ierr)
      
      call MPI_IRECV(q1(1,1,kend+1), mydata, MDP,
     & my_up,tag, MPI_COMM_WORLD, req(2), ierr)
       
      call MPI_Waitall(2,req,status,ierr)
     
      end subroutine update_upper_ghost
c=========================================
      subroutine update_lower_ghost(n1,n2,q1)
      use mpih
      use mpi_param, only: kstart,kend,dk
      implicit none
      real,intent(inout) :: q1(n1,n2,kstart-1:kend+1)
      integer,intent(in) :: n1,n2
      integer :: mydata
      integer :: my_down, my_up,tag
       
      mydata= n1*n2
      
      my_down= myid-1
      
      my_up= myid+1

      if(myid .eq. 0) my_down= MPI_PROC_NULL
      if(myid .eq. numtasks-1) my_up= MPI_PROC_NULL
      
      
      tag=1
      
      call MPI_ISEND(q1(1,1,kend), mydata,  MDP,
     & my_up, tag, MPI_COMM_WORLD, req(1), ierr)
      
      call MPI_IRECV(q1(1,1,kstart-1), mydata,  MDP,
     & my_down,tag, MPI_COMM_WORLD, req(2), ierr)
       
      call MPI_Waitall(2,req,status,ierr)
     
      end subroutine update_lower_ghost
c==============================================
      subroutine mem_alloc
      use mpih
      use param, only: n2,n1
      use mpi_param
      use local_arrays
#ifdef STATS
      use stat_arrays
#endif
      implicit none
      integer :: merr
      
      !-------------------------------------------------
      ! Arrays with ghost cells
      !-------------------------------------------------
      allocate(q1(1:n1,1:n2,kstart-1:kend+1), stat=merr)
      
      if(merr .ne. 0) then
        write(6,*)"process  ",myid," failed to allocate memory for q1"
        call MPI_Abort(MPI_COMM_WORLD, 1, ierr )
      endif 
      !-------------------------------------------------
      allocate(q2(1:n1,1:n2,kstart-1:kend+1), stat=merr)
      
      if(merr .ne. 0) then
        write(6,*)"process  ",myid," failed to allocate memory for q2"
        call MPI_Abort(MPI_COMM_WORLD, 1, ierr )
      endif 
      !-------------------------------------------------
      allocate(q3(1:n1,1:n2,kstart-1:kend+1), stat=merr)
      
      if(merr .ne. 0) then
        write(6,*)"process  ",myid," failed to allocate memory for q3"
        call MPI_Abort(MPI_COMM_WORLD, 1, ierr )
      endif 
      !------------------------------------------------
      allocate(pr(1:n1,1:n2,kstart-1:kend+1), stat=merr)
      
      if(merr .ne. 0) then
        write(6,*)"process  ",myid," failed to allocate memory for pr"
        call MPI_Abort(MPI_COMM_WORLD, 1, ierr )
      endif 
      !---------------------------------------------------
      allocate(dens(1:n1,1:n2,kstart-1:kend+1), stat=merr)
      
      if(merr .ne. 0) then
      write(6,*)"process  ",myid," failed to allocate memory for dens"
        call MPI_Abort(MPI_COMM_WORLD, 1, ierr )
      endif  
      !----------------------------------------------------
      allocate(dph(1:n1,1:n2+1,kstart-1:kend+1), stat=merr)
      
      if(merr .ne. 0) then
      write(6,*)"process  ",myid," failed to allocate memory for dph"
        call MPI_Abort(MPI_COMM_WORLD, 1, ierr )
      endif
      !-----------------------------------------------
      ! Arrays without ghost cells
      !-----------------------------------------------
      allocate(rhs(1:n1,1:n2,kstart:kend), stat=merr)
      
      if(merr .ne. 0) then
      write(6,*)"process  ",myid," failed to allocate memory for rhs"
        call MPI_Abort(MPI_COMM_WORLD, 1, ierr )
      endif
      !----------------------------------------------------
      allocate(dq(1:n1,1:n2,kstart:kend), stat=merr)
      
      if(merr .ne. 0) then
      write(6,*)"process  ",myid," failed to allocate memory for dq"
        call MPI_Abort(MPI_COMM_WORLD, 1, ierr )
      endif
      !-----------------------------------------------
      allocate(ru1(1:n1,1:n2,kstart:kend), stat=merr)
      
      if(merr .ne. 0) then
      write(6,*)"process  ",myid," failed to allocate memory for ru1"
        call MPI_Abort(MPI_COMM_WORLD, 1, ierr )
      endif
      !-----------------------------------------------
      allocate(ru2(1:n1,1:n2,kstart:kend), stat=merr)
      
      if(merr .ne. 0) then
      write(6,*)"process  ",myid," failed to allocate memory for ru2"
        call MPI_Abort(MPI_COMM_WORLD, 1, ierr )
      endif
      !----------------------------------------------
      allocate(ru3(1:n1,1:n2,kstart:kend), stat=merr)
      
      if(merr .ne. 0) then
      write(6,*)"process  ",myid," failed to allocate memory for ru3"
        call MPI_Abort(MPI_COMM_WORLD, 1, ierr )
      endif
      !-----------------------------------------------
      allocate(qcap(1:n1,1:n2,kstart:kend), stat=merr)
      
      if(merr .ne. 0) then
      write(6,*)"process  ",myid," failed to allocate memory for qcap"
        call MPI_Abort(MPI_COMM_WORLD, 1, ierr )
      endif
      !----------------------------------------------
      allocate(hro(1:n1,1:n2,kstart:kend), stat=merr)
      
      if(merr .ne. 0) then
      write(6,*)"process  ",myid," failed to allocate memory for rho"
        call MPI_Abort(MPI_COMM_WORLD, 1, ierr )
      endif
      !------------------------------------------------
      allocate(ruro(1:n1,1:n2,kstart:kend), stat=merr)
      
      if(merr .ne. 0) then
      write(6,*)"process  ",myid," failed to allocate memory for ruro"
        call MPI_Abort(MPI_COMM_WORLD, 1, ierr )
      endif
      
#ifdef STATS
      allocate(q1_m1(kstart:kend))
      allocate(q2_m1(kstart:kend))
      allocate(q3_m1(kstart:kend))
      allocate(te_m1(kstart:kend))
      allocate(q1_m2(kstart:kend))
      allocate(q2_m2(kstart:kend))
      allocate(q3_m2(kstart:kend))
      allocate(te_m2(kstart:kend))
      allocate(q1_m3(kstart:kend))
      allocate(q2_m3(kstart:kend))
      allocate(q3_m3(kstart:kend))
      allocate(te_m3(kstart:kend))
      allocate(q1_m4(kstart:kend))
      allocate(q2_m4(kstart:kend))
      allocate(q3_m4(kstart:kend))
      allocate(te_m4(kstart:kend))
      allocate(q1xq2(kstart:kend))
      allocate(q3xq1_m2(kstart:kend))
      allocate(q3xq2_m2(kstart:kend))
      allocate(q3xte(kstart:kend))
      allocate(q3xte_m2(kstart:kend))
      allocate(q3xpr(kstart:kend))
      allocate(q1xS13(kstart:kend))
      allocate(q2xS23(kstart:kend))
      allocate(q3xS33(kstart:kend))
      allocate(prxdudx(kstart:kend))
      allocate(prxdvdy(kstart:kend))
      allocate(prxdwdz(kstart:kend))
      allocate(q1_zave(1:n1,1:n2))
      allocate(q2_zave(1:n1,1:n2))
      allocate(q3_zave(1:n1,1:n2))
      allocate(te_zave(1:n1,1:n2))
#ifdef BALANCE
      allocate(disste(kstart:kend))
      allocate(dissth(kstart:kend))
      allocate(disstx(kstart:kend))
      allocate(dissty(kstart:kend))
      allocate(disstz(kstart:kend))
#endif
#ifdef CORIO
      allocate(vort3_m1(kstart:kend))
      allocate(vort3_m2(kstart:kend))
      allocate(vort3_m3(kstart:kend))
      allocate(vort3_m4(kstart:kend))
      allocate(vort3xq3(kstart:kend))
#endif
#endif
          
      end subroutine mem_alloc

c==================================================      
      
      subroutine mem_dealloc
      use local_arrays
      use mpi_param
      use stat_arrays
      implicit none
      
      if(allocated(q1)) deallocate(q1)
      if(allocated(q2)) deallocate(q2)
      if(allocated(q3)) deallocate(q3)
      
      if(allocated(qcap)) deallocate(qcap)
      
      if(allocated(dens)) deallocate(dens)
      if(allocated(pr)) deallocate(pr)
      if(allocated(hro)) deallocate(hro)
      
      if(allocated(rhs)) deallocate(rhs)
      
      if(allocated(dph)) deallocate(dph)
      
      if(allocated(ru1)) deallocate(ru1)
      if(allocated(ru2)) deallocate(ru2)
      if(allocated(ru3)) deallocate(ru3)
      
      if(allocated(ruro)) deallocate(ruro)
      
      !---------------------------------------
      if(allocated(countj)) deallocate(countj)
      if(allocated(countk)) deallocate(countk)

      if(allocated(offsetj)) deallocate(offsetj)
      if(allocated(offsetk)) deallocate(offsetk)
      
      if(allocated(countf)) deallocate(countf)
      
      if(allocated(offsetf)) deallocate(offsetf)
      
#ifdef STATS
      if(allocated(q1_m1)) deallocate(q1_m1)
      if(allocated(q2_m1)) deallocate(q2_m1)
      if(allocated(q3_m1)) deallocate(q3_m1)
      if(allocated(te_m1)) deallocate(te_m1)
      if(allocated(q1_m2)) deallocate(q1_m2)
      if(allocated(q2_m2)) deallocate(q2_m2)
      if(allocated(q3_m2)) deallocate(q3_m2)
      if(allocated(te_m2)) deallocate(te_m2)
      if(allocated(q1_m3)) deallocate(q1_m3)
      if(allocated(q2_m3)) deallocate(q2_m3)
      if(allocated(q3_m3)) deallocate(q3_m3)
      if(allocated(te_m3)) deallocate(te_m3)
      if(allocated(q1_m4)) deallocate(q1_m4)
      if(allocated(q2_m4)) deallocate(q2_m4)
      if(allocated(q3_m4)) deallocate(q3_m4)
      if(allocated(te_m4)) deallocate(te_m4)
      if(allocated(q1xq2)) deallocate(q1xq2)
      if(allocated(q3xq1_m2)) deallocate(q3xq1_m2)
      if(allocated(q3xq2_m2)) deallocate(q3xq2_m2)
      if(allocated(q3xte)) deallocate(q3xte)
      if(allocated(q3xte_m2)) deallocate(q3xte_m2)
      if(allocated(q3xpr)) deallocate(q3xpr)
      if(allocated(q1xS13)) deallocate(q1xS13)
      if(allocated(q2xS23)) deallocate(q2xS23)
      if(allocated(q3xS33)) deallocate(q3xS33)
      if(allocated(prxdudx)) deallocate(prxdudx)
      if(allocated(prxdvdy)) deallocate(prxdvdy)
      if(allocated(prxdwdz)) deallocate(prxdwdz)
      if(allocated(q1_zave)) deallocate(q1_zave)
      if(allocated(q2_zave)) deallocate(q2_zave)
      if(allocated(q3_zave)) deallocate(q3_zave)
      if(allocated(te_zave)) deallocate(te_zave)
#ifdef BALANCE
      if(allocated(disste)) deallocate(disste)
      if(allocated(dissth)) deallocate(dissth)
      if(allocated(disstx)) deallocate(disstx)
      if(allocated(dissty)) deallocate(dissty)
      if(allocated(disstz)) deallocate(disstz)
#endif
#ifdef CORIO
      if(allocated(vort3_m1)) deallocate(vort3_m1)
      if(allocated(vort3_m2)) deallocate(vort3_m2)
      if(allocated(vort3_m3)) deallocate(vort3_m3)
      if(allocated(vort3_m4)) deallocate(vort3_m4)
      if(allocated(vort3xq3)) deallocate(vort3xq3)
#endif
#endif
    
      end subroutine mem_dealloc
c================================================
      subroutine mpi_write_continua
      use param
      use mpih, only: myid
      use local_arrays, only: dens,q2,q3,q1,pr
      implicit none
      character*30 filnam1,varnam1


      filnam1 = trim('continua_dens.h5')
      varnam1 = trim('dens')
      call HdfWriteRealHalo3D(filnam1,dens,varnam1)
      filnam1 = trim('continua_q1.h5')
      varnam1 = trim('Vth')
      call HdfWriteRealHalo3D(filnam1,q1,varnam1)
      filnam1 = trim('continua_q2.h5')
      varnam1 = trim('Vr')
      call HdfWriteRealHalo3D(filnam1,q2,varnam1)
      filnam1 = trim('continua_q3.h5')
      varnam1 = trim('Vz')
      call HdfWriteRealHalo3D(filnam1,q3,varnam1)

!AJAG print continua pressure field
      filnam1 = trim('continua_pr.h5')
      varnam1 = trim('Pr')
      call HdfWriteRealHalo3D(filnam1,pr,varnam1)

      if (myid .eq. 0) then
       open(13,file='continua_grid.dat',status='unknown')
       rewind(13)                                                      
       write(13,*) n1,n2,n3
       write(13,*) rext,rext2,time
       write(13,*) istr3,str3
       close(13)
      endif
      
      end subroutine mpi_write_continua
c================================================
      subroutine HdfWriteRealHalo3D(filnam1,qua,varname)
      use param
      use mpih
      use hdf5
      use mpi_param, only: kstart,kend

      implicit none

      integer hdf_error

      integer(HID_T) :: file_id
      integer(HID_T) :: filespace
      integer(HID_T) :: slabspace
      integer(HID_T) :: memspace

      integer(HID_T) :: dset

      integer(HSIZE_T) :: dims(3)

      integer(HID_T) :: plist_id
      integer(HSIZE_T), dimension(3) :: data_count  
      integer(HSSIZE_T), dimension(3) :: data_offset 

      integer :: comm, info
      integer :: ndims

      real, intent(in), dimension(1:n1,1:n2,kstart:kend) ::qua

      character*30,intent(in) :: filnam1,varname

!RO   Sort out MPI definitions

      comm = MPI_COMM_WORLD
      info = MPI_INFO_NULL

!RO   Set offsets and element counts
   
      ndims = 3

      dims(1)=n1
      dims(2)=n2
      dims(3)=n3m

      call h5screate_simple_f(ndims, dims, filespace, hdf_error)

      data_count(1) = n1
      data_count(2) = n2
      data_count(3) = kend-kstart+1

      data_offset(1) = 0
      data_offset(2) = 0
      data_offset(3) = kstart-1

      call h5pcreate_f(H5P_FILE_ACCESS_F, plist_id, hdf_error)
      call h5pset_fapl_mpio_f(plist_id, comm, info, hdf_error)
      call h5fcreate_f(filnam1, H5F_ACC_TRUNC_F, file_id,
     & hdf_error, access_prp=plist_id)
      call h5pclose_f(plist_id, hdf_error)
      call h5screate_simple_f(ndims, data_count, memspace, hdf_error)  ! added
      call h5dcreate_f(file_id, varname, H5T_NATIVE_DOUBLE,
     &                filespace, dset, hdf_error)
      call h5sclose_f(filespace, hdf_error)                            ! added
      call h5screate_simple_f(ndims, data_count, memspace, hdf_error)
      call h5dget_space_f(dset, slabspace, hdf_error)
      call h5sselect_hyperslab_f (slabspace, H5S_SELECT_SET_F,
     &                      data_offset, data_count, hdf_error)
      call h5pcreate_f(H5P_DATASET_XFER_F, plist_id, hdf_error) 
      call h5pset_dxpl_mpio_f(plist_id,H5FD_MPIO_COLLECTIVE_F,hdf_error)
      call h5dwrite_f(dset, H5T_NATIVE_DOUBLE,
     &   qua(1:n1,1:n2,kstart:kend), dims, 
     &   hdf_error, file_space_id = slabspace, mem_space_id = memspace,
     &   xfer_prp = plist_id)
      call h5pclose_f(plist_id, hdf_error)
      call h5dclose_f(dset, hdf_error)
      call h5sclose_f(slabspace, hdf_error)                            ! added
      call h5sclose_f(memspace, hdf_error)
      call h5fclose_f(file_id, hdf_error)

      end subroutine HdfWriteRealHalo3D
c================================================
      subroutine mpi_read_continua(n1o,n2o,n3o,ks,ke,intvar,qua)
      use mpih
      use param
      use hdf5
      implicit none
      integer, intent(in) :: ks,ke,n2o,n1o,n3o
      real, dimension(1:n1o,1:n2o,ks-1:ke+1)::qua

      integer hdf_error

      integer(HID_T) :: file_id
      integer(HID_T) :: slabspace
      integer(HID_T) :: memspace

      integer(HID_T) :: dset_qua

      integer(HSIZE_T) :: dims(3)

      integer(HID_T) :: plist_id
      integer(HSIZE_T), dimension(3) :: data_count  
      integer(HSSIZE_T), dimension(3) :: data_offset 

      integer :: comm, info
      integer :: ndims

      integer, intent(in) :: intvar
      character*70 :: filnam1
      character*10 :: dsetname

      comm = MPI_COMM_WORLD
      info = MPI_INFO_NULL

!EP   Select file and dataset based on intvar

      select case (intvar)
        case (1)
          dsetname = trim('Vth')
          filnam1 = trim('continua_q1.h5')
        case (2)
          dsetname = trim('Vr')
          filnam1 = trim('continua_q2.h5')
        case (3)
          dsetname = trim('Vz')
          filnam1 = trim('continua_q3.h5')
        case (4)
          dsetname = trim('dens')
          filnam1 = trim('continua_dens.h5')
      end select

!RO   Set offsets and element counts
   
      ndims = 3

      dims(1)=n1o
      dims(2)=n2o
      dims(3)=n3o-1


      data_count(1) = n1o
      data_count(2) = n2o
      data_count(3) = ke-ks+1

      data_offset(1) = 0
      data_offset(2) = 0
      data_offset(3) = ks-1


      call h5pcreate_f(H5P_FILE_ACCESS_F, plist_id,
     %    hdf_error)


      call h5pset_fapl_mpio_f(plist_id, comm, info,
     &  hdf_error)

      call h5fopen_f(filnam1, H5F_ACC_RDONLY_F, file_id,
     & hdf_error, access_prp=plist_id)

      call h5dopen_f(file_id, dsetname,
     &                dset_qua, hdf_error)

      call h5screate_simple_f(ndims, data_count, memspace, hdf_error) 

      call h5dget_space_f(dset_qua, slabspace, hdf_error)
      call h5sselect_hyperslab_f (slabspace, H5S_SELECT_SET_F,
     &                      data_offset, data_count, hdf_error)
      call h5pcreate_f(H5P_DATASET_XFER_F, plist_id, hdf_error) 
      call h5pset_dxpl_mpio_f(plist_id, H5FD_MPIO_COLLECTIVE_F,
     &                        hdf_error)
       call h5dread_f(dset_qua, H5T_NATIVE_DOUBLE,
     &   qua(1:n1o,1:n2o,ks:ke), dims, 
     &   hdf_error, file_space_id = slabspace, mem_space_id = memspace, 
     &   xfer_prp = plist_id)
      call h5pclose_f(plist_id, hdf_error)

      call h5dclose_f(dset_qua, hdf_error)

      call h5sclose_f(memspace, hdf_error)
      call h5fclose_f(file_id, hdf_error)

      end subroutine mpi_read_continua


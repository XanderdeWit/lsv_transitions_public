
************************************************************************
c                       SUBROUTINE INVTRRO
c   This subroutine performs the computation of he scalar field.
c   For details see the introduction of INVTR1
c
      subroutine invtrro
      use param
      use local_arrays, only: dens,hro,ruro,rhs
      use mpi_param, only: kstart,kend
      implicit none
      integer :: jc,kc,km,kp,jp,jm,ic,ip,im
      real    :: dq32,dq33,dcq3,dq31
      real    :: app,acc,amm
      real    :: alpec,udx1q,udx2q
      real    :: del1,del2, fcder
c
      alpec=al/pec
      udx1q=dx1q
      udx2q=dx2q
c
c  compute the rhs of the factored equation
c  everything at i+1/2,j+1/2,k+1/2
c
      do kc=kstart,kend
      if( (kc.ge.2) .and. (kc.le.n3m-1) ) then
        km=kmv(kc)
        kp=kpv(kc)
        app=ap3ssk(kc)
        acc=ac3ssk(kc)
        amm=am3ssk(kc)
!$OMP  PARALLEL DO
!$OMP$ DEFAULT(SHARED)
!$OMP$ PRIVATE(jc,jm,jp,ic,ip,im)
!$OMP$ PRIVATE(dq31,dq32,dq33,dcq3)
        do jc=1,n2m
          jm=jmv(jc)
          jp=jpv(jc)
          do ic=1,n1m
            im=imv(ic)
            ip=ipv(ic)
c
c   11 second derivatives of dens
c
            dq31=(dens(ip,jc,kc)
     &           -2.0*dens(ic,jc,kc)
     &           +dens(im,jc,kc))*udx1q
      
c
c   22 second derivatives of dens
c
            dq32=(dens(ic,jp,kc)
     &           -2.0*dens(ic,jc,kc)
     &           +dens(ic,jm,kc))*udx2q
c
c   33 second derivatives of dens
c
               dq33= dens(ic,jc,kp)*app
     %              +dens(ic,jc,kc)*acc
     %              +dens(ic,jc,km)*amm
c
            dcq3=dq32+dq33+dq31
c
c    right hand side of the density equation
c
cm===========================================================
            rhs(ic,jc,kc)=(ga*hro(ic,jc,kc)+ro*ruro(ic,jc,kc)
     %              +alpec*dcq3)*dt
cm===========================================================
c
c    updating of the non-linear terms
c
            ruro(ic,jc,kc)=hro(ic,jc,kc)
        enddo
        enddo
!$OMP  END PARALLEL DO
      endif
      if(kc.eq.1) then
      del1 = zm(1)-zz(1)
      del2 = zm(2)-zm(1)
      fcder = 2.d0/(del1*del2*(del1+del2))
        kp = kc + 1
!$OMP  PARALLEL DO
!$OMP$ DEFAULT(SHARED)
!$OMP$ PRIVATE(jc,jm,jp,ic,ip,im)
!$OMP$ PRIVATE(dq31,dq32,dq33,dcq3)
        do jc=1,n2m
              jm=jmv(jc)
              jp=jpv(jc)
          do ic=1,n1m
            im=imv(ic)
            ip=ipv(ic)
            dq31=(dens(ip,jc,kc)-2.0*dens(ic,jc,kc)
     %          +dens(im,jc,kc))*udx1q
            dq32=(dens(ic,jp,kc)-2.0*dens(ic,jc,kc)
     %          +dens(ic,jm,kc))*udx2q
            dq33=(dens(ic,jc,kp)*del1-dens(ic,jc,kc)*(del1+del2)
     %            +denbs(ic,jc)*del2)
     %              *fcder
c
            dcq3=dq32+dq33+dq31
cm=======================================================
            rhs(ic,jc,kc)=(ga*hro(ic,jc,kc)+ro*ruro(ic,jc,kc)
     %              +alpec*dcq3)*dt
cm=======================================================
            ruro(ic,jc,kc)=hro(ic,jc,kc)
            enddo
        enddo
!$OMP  END PARALLEL DO
      endif
c
c       UPPER COLD WALL
c     
      if(kc.eq.n3m) then
      del1 = zz(n3)-zm(n3m)
      del2 = zm(n3m)-zm(n3m-1)
      fcder = 2.d0/(del1*del2*(del1+del2))
        km = kc - 1 
!$OMP  PARALLEL DO
!$OMP$ DEFAULT(SHARED)
!$OMP$ PRIVATE(jc,jm,jp,ic,ip,im)
!$OMP$ PRIVATE(dq31,dq32,dq33,dcq3)
            do jc=1,n2m
              jm=jmv(jc)
              jp=jpv(jc)
          do ic=1,n1m
            im=imv(ic)
            ip=ipv(ic)
            dq31=(dens(ip,jc,kc)-2.0*dens(ic,jc,kc)
     %          +dens(im,jc,kc))*udx1q
            dq32=(dens(ic,jp,kc)-2.0*dens(ic,jc,kc)
     %          +dens(ic,jm,kc))*udx2q
            dq33=(dens(ic,jc,km)*del1-dens(ic,jc,kc)*(del1+del2)
     %            +denbn(ic,jc)*del2)
     %              *fcder
c
            dcq3=dq32+dq33+dq31
cm========================================================
            rhs(ic,jc,kc)=(ga*hro(ic,jc,kc)+ro*ruro(ic,jc,kc)
     %              +alpec*dcq3)*dt
cm========================================================
            ruro(ic,jc,kc)=hro(ic,jc,kc)
            enddo
        enddo
!$OMP  END PARALLEL DO
      endif
      enddo

      call solxi(al*dt*0.5*dx1q/pec)

      call solxj(al*dt*0.5*dx2q/pec)

      call solrok
c

      return
      end

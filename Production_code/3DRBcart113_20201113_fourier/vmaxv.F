      subroutine vmaxv
!EP   This routine calculates the maximum velocities and volume averaged nusselt number
      use param
      use local_arrays, only: q2,q3,q1,dens
      use mpi_param, only: kstart,kend
      use mpih
      implicit none
      real    :: my_vmax2,my_vmax3,my_vmax1
      integer :: jc,kc,kp,ic
      real :: anusin,my_anusin,vol,q3cen,fac2

        my_vmax1=-100.d0
        my_vmax2=-100.d0
        my_vmax3=-100.d0
      my_anusin=0.d0 
      anusin=0.d0    
      vol = 1.d0/(alx3*dx3*real(n1m)*real(n2m))
        do kc=kstart,kend
        kp = kc + 1
        fac2 = g3rm(kc)
!$OMP  PARALLEL DO
!$OMP$ DEFAULT(SHARED)
!$OMP$ PRIVATE(jc,ic)
!$OMP$ REDUCTION(max: my_vmax1,my_vmax2,my_vmax3)
!$OMP$ REDUCTION(+: my_anusin)
          do jc=1,n2m
            do ic=1,n1m
              my_vmax1 = max(my_vmax1,abs(q1(ic,jc,kc)))
              my_vmax2 = max(my_vmax2,abs(q2(ic,jc,kc)))
              my_vmax3 = max(my_vmax3,abs(q3(ic,jc,kc)))
              q3cen = (q3(ic,jc,kc)+q3(ic,jc,kp))*0.5d0
              my_anusin=my_anusin+dens(ic,jc,kc)*q3cen*fac2
       enddo
       enddo
!$OMP  END PARALLEL DO
       enddo

      call MPI_REDUCE(my_anusin,anusin,1,MDP,MPI_SUM,0,
     & MPI_COMM_WORLD,ierr)
      call MPI_ALLREDUCE(my_vmax1,vmax(1),1,MDP,MPI_MAX,
     & MPI_COMM_WORLD,ierr)
      call MPI_ALLREDUCE(my_vmax2,vmax(2),1,MDP,MPI_MAX,
     & MPI_COMM_WORLD,ierr)
      call MPI_ALLREDUCE(my_vmax3,vmax(3),1,MDP,MPI_MAX,
     & MPI_COMM_WORLD,ierr)

      if(myid.eq.0) then
      anusin=1.d0 + dsqrt(pra*ray)*anusin*vol
      write(95,*) time, anusin
      endif

      return   
      end     


************************************************************************
c                       SUBROUTINE INVTR1
c   This subroutine performs the computation of Q~~ for the q1 momentum 
c   equation (radial direction) by a factored implicit scheme.
c   For details see the introduction of INVTR1
c   
c
      subroutine invtr1
      use param
      use local_arrays, only: pr,rhs,ru1,q1,dq
      use mpi_param, only: kstart,kend

      implicit none
      integer :: jc,kc,km,kp,jp,jm,ic,im,ip
      real    :: udx1,amm,acc,app
      real    :: dcq1,dpx11
      real    :: d22q1,d33q1,d11q1
      real    :: alre,udx1q,udx2q

      
cm
      alre=al/ren

      udx1=dx1*al
      udx1q=dx1q
      udx2q=dx2q
c
c  compute the rhs of the factored equation
c  everything at i,j+1/2,k+1/2
c
c    points inside the flowfield
c
        do kc=kstart,kend
          km=kmv(kc)
          kp=kpv(kc)
          amm=am3sk(kc)
          acc=ac3sk(kc)
          app=ap3sk(kc)
!$OMP  PARALLEL DO
!$OMP$ DEFAULT(SHARED)
!$OMP$ PRIVATE(jc,jm,jp,ic,ip,im)
!$OMP$ PRIVATE(d11q1,d22q1,d33q1,dcq1,dpx11)
          do jc=1,n2m
           jm=jmv(jc)
           jp=jpv(jc)
            do ic=1,n1m
            im=imv(ic)
            ip=ipv(ic)

c
c   11 second derivative of q1
c
            d11q1=(q1(ip,jc,kc)
     %            -2.0*q1(ic,jc,kc)
     %            +q1(im,jc,kc))*udx1q
c
c   22 second derivative of q1
c
            d22q1=(q1(ic,jp,kc)
     %            -2.0*q1(ic,jc,kc)
     %            +q1(ic,jm,kc))*udx2q
c
c   33 second derivative of q1
c
            d33q1=q1(ic,jc,kp)*app
     %           +q1(ic,jc,kc)*acc
     %           +q1(ic,jc,km)*amm

c
c    viscid terms
c
            dcq1=d11q1+d22q1+d33q1
c
c
c   component of grad(pr) along 2 direction
c
            dpx11=(pr(ic,jc,kc)-pr(im,jc,kc))*udx1

c
            rhs(ic,jc,kc)=(ga*dq(ic,jc,kc)+ro*ru1(ic,jc,kc)
     %                    +alre*dcq1-dpx11)*dt

cm===========================================================
c
            ru1(ic,jc,kc)=dq(ic,jc,kc)
         enddo
       enddo
!$OMP  END PARALLEL DO
      enddo

      call solxi(beta*al*dx1q)

      call solxj(beta*al*dx2q)
      
      call solq12k(q1(1:n1,1:n2,kstart:kend))
      
      q1(n1,:,:) = q1(1,:,:)
     
      return
      end
c

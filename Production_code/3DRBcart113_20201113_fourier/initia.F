
c***********************************************************************
      subroutine initia
      use param
      use local_arrays
      use mpi_param, only: kstart,kend
      implicit none
      integer :: j,k,kc,i
      
cm=================================
      do k=kstart,kend
!$OMP  PARALLEL DO
!$OMP$ DEFAULT(SHARED)
!$OMP$ PRIVATE(j,i)
      do j=1,n2
      do i=1,n1
      pr(i,j,k)=0.d0
      dph(i,j,k)=0.d0
      dq(i,j,k)=0.d0
      rhs(i,j,k)=0.d0
      ru1(i,j,k)=0.d0
      ru2(i,j,k)=0.d0
      ru3(i,j,k)=0.d0
      ruro(i,j,k)=0.d0
      qcap(i,j,k)=0.d0
      hro(i,j,k)=0.d0
      enddo
      enddo
!$OMP  END PARALLEL DO
      enddo
      
      do kc=kstart-1,kend+1
!$OMP  PARALLEL DO
!$OMP$ DEFAULT(SHARED)
!$OMP$ PRIVATE(j,i)
      do j=1,n2
      do i=1,n1
      k = kc
      if(k.lt.1) k=1
      if(k.gt.n3) k=n3
      q2(i,j,k)=0.d0
      q3(i,j,k)=0.d0
      dens(i,j,k)=1.d0
      enddo
      enddo
!$OMP  END PARALLEL DO
      enddo
cm==================================         

      do i=1,n1
        tc(i) = 0.d0
        tm(i) = 0.d0
      end do
      do j=1,n2
        rc(j) = 0.d0
        rm(j) = 0.d0
      end do
      do k=1,n3
        am3ssk(k) = 0.d0
        ac3ssk(k) = 0.d0
        ap3ssk(k) = 0.d0
        zz(k) = 0.d0
        zm(k) = 0.d0
        g3rc(k) = 0.d0
        g3rm(k) = 0.d0
      end do
      return 
      end   

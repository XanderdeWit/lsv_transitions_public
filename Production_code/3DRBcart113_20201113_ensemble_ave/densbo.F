
c***********************************************************************
c                                                                      *
c                       CONDIZIONI AL CONTORNO                         *
c                                                                      *
c***********************************************************************
      subroutine densbo
      use param
      implicit none
      integer :: j,i
!$OMP  PARALLEL DO
!$OMP$ DEFAULT(SHARED)
!$OMP$ PRIVATE(i,j)
      do j=1,n2
      do i=1,n1
              denbn(i,j)=0.d0
              denbs(i,j)=1.d0
      enddo
      enddo
!$OMP  END PARALLEL DO

      return
      end
C

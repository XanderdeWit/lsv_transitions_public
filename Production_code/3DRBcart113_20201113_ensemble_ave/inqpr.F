c***********************************************************************
c                                                                      *
c                       INITIAL CONDITION                              *
c                                                                      *
c***********************************************************************
      subroutine inqpr
      use param
      use local_arrays, only: q2,q3,dens,q1
      use mpi_param, only: kstart,kend
      use mpih
      implicit none
      integer :: j,k,i
      real :: xxx,yyy,eps
      integer :: kc
      real :: r

      eps = 0.01d0

      do k=kstart-1,kend+1
        do j=1,n2
           do i=1,n1
             q1(i,j,k) = 0.0d0
             q2(i,j,k) = 0.0d0
             q3(i,j,k) = 0.0d0
         enddo
        enddo
      enddo

      call random_seed()

c     unstable linear temperature profile with random noise
      do k=kstart,kend
        do j=1,n2m    
          do i=1,n1m
            call random_number(r)
            dens(i,j,k) = denbs(i,j) - (denbs(i,j)-denbn(i,j))*zm(k)
     &                    + eps * ( 2.d0*r - 1.d0 )
          enddo
        end do 
      end do
      if(myid.eq.0)write(6,'(5x,a,/)')'initial temperature linear'

      return                                                            
      end                                                               
